.class Lcom/localytics/android/InAppManager;
.super Lcom/localytics/android/BaseMarketingManager;
.source "InAppManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/localytics/android/InAppManager$13;,
        Lcom/localytics/android/InAppManager$ManifestHolder;,
        Lcom/localytics/android/InAppManager$FrequencyCappingFilter;
    }
.end annotation


# static fields
.field private static final AUGMENTED_BLACKOUT_TIMES_RULE:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final AUGMENTED_BLACKOUT_WEEKDAYS_RULE:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final DATE_SDF:Ljava/text/SimpleDateFormat;

.field private static final DEFAULT_FREQUENCY_CAPPING_RULE:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final GLOBAL_FREQUENCY_CAPPING_RULE_CAMPAIGN_ID:I = -0x1

.field private static final GLOBAL_FREQUENCY_CAPPING_RULE_REQUIRED_KEYS:[Ljava/lang/String;

.field private static final INDIVIDUAL_FREQUENCY_CAPPING_RULE_REQUIRED_KEYS:[Ljava/lang/String;

.field private static final PROJECTION_IN_APP_RULE_RECORD:[Ljava/lang/String;

.field private static final TIME_SDF:Ljava/text/SimpleDateFormat;


# instance fields
.field protected mCreativeManager:Lcom/localytics/android/CreativeManager;

.field private mFragmentManager:Landroid/app/FragmentManager;

.field private mIsMidDisplayingInApp:Z

.field private mMarketingHandler:Lcom/localytics/android/MarketingHandler;

.field private mTestCampaignsListAdapter:Lcom/localytics/android/InAppMessagesAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 49
    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "display_frequencies"

    aput-object v2, v1, v4

    sput-object v1, Lcom/localytics/android/InAppManager;->GLOBAL_FREQUENCY_CAPPING_RULE_REQUIRED_KEYS:[Ljava/lang/String;

    .line 50
    new-array v1, v3, [Ljava/lang/String;

    const-string v2, "max_display_count"

    aput-object v2, v1, v4

    const-string v2, "ignore_global"

    aput-object v2, v1, v5

    sput-object v1, Lcom/localytics/android/InAppManager;->INDIVIDUAL_FREQUENCY_CAPPING_RULE_REQUIRED_KEYS:[Ljava/lang/String;

    .line 52
    new-array v1, v3, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v1, v4

    const-string v2, "version"

    aput-object v2, v1, v5

    sput-object v1, Lcom/localytics/android/InAppManager;->PROJECTION_IN_APP_RULE_RECORD:[Ljava/lang/String;

    .line 57
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "HH:mm"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v1, Lcom/localytics/android/InAppManager;->TIME_SDF:Ljava/text/SimpleDateFormat;

    .line 58
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v1, Lcom/localytics/android/InAppManager;->DATE_SDF:Ljava/text/SimpleDateFormat;

    .line 62
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/localytics/android/InAppManager;->AUGMENTED_BLACKOUT_TIMES_RULE:Ljava/util/List;

    .line 63
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 64
    .local v0, "rule":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "start"

    const-string v2, "00:00"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    const-string v1, "end"

    const-string v2, "23:59"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v1, Lcom/localytics/android/InAppManager;->AUGMENTED_BLACKOUT_TIMES_RULE:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Integer;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lcom/localytics/android/InAppManager;->AUGMENTED_BLACKOUT_WEEKDAYS_RULE:Ljava/util/List;

    .line 70
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/localytics/android/InAppManager;->DEFAULT_FREQUENCY_CAPPING_RULE:Ljava/util/Map;

    .line 71
    sget-object v1, Lcom/localytics/android/InAppManager;->DEFAULT_FREQUENCY_CAPPING_RULE:Ljava/util/Map;

    const-string v2, "max_display_count"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object v1, Lcom/localytics/android/InAppManager;->DEFAULT_FREQUENCY_CAPPING_RULE:Ljava/util/Map;

    const-string v2, "ignore_global"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v1, Lcom/localytics/android/InAppManager;->TIME_SDF:Ljava/text/SimpleDateFormat;

    invoke-virtual {v1, v4}, Ljava/text/SimpleDateFormat;->setLenient(Z)V

    .line 75
    sget-object v1, Lcom/localytics/android/InAppManager;->DATE_SDF:Ljava/text/SimpleDateFormat;

    invoke-virtual {v1, v4}, Ljava/text/SimpleDateFormat;->setLenient(Z)V

    .line 76
    return-void
.end method

.method constructor <init>(Lcom/localytics/android/LocalyticsDao;Lcom/localytics/android/MarketingHandler;)V
    .locals 1
    .param p1, "localyticsDao"    # Lcom/localytics/android/LocalyticsDao;
    .param p2, "marketingHandler"    # Lcom/localytics/android/MarketingHandler;

    .prologue
    .line 93
    new-instance v0, Lcom/localytics/android/CreativeManager;

    invoke-direct {v0, p1, p2}, Lcom/localytics/android/CreativeManager;-><init>(Lcom/localytics/android/LocalyticsDao;Landroid/os/Handler;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/localytics/android/InAppManager;-><init>(Lcom/localytics/android/LocalyticsDao;Lcom/localytics/android/MarketingHandler;Lcom/localytics/android/CreativeManager;)V

    .line 94
    return-void
.end method

.method constructor <init>(Lcom/localytics/android/LocalyticsDao;Lcom/localytics/android/MarketingHandler;Lcom/localytics/android/CreativeManager;)V
    .locals 1
    .param p1, "localyticsDao"    # Lcom/localytics/android/LocalyticsDao;
    .param p2, "marketingHandler"    # Lcom/localytics/android/MarketingHandler;
    .param p3, "creativeManager"    # Lcom/localytics/android/CreativeManager;

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/localytics/android/BaseMarketingManager;-><init>(Lcom/localytics/android/LocalyticsDao;)V

    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/localytics/android/InAppManager;->mIsMidDisplayingInApp:Z

    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/localytics/android/InAppManager;->mTestCampaignsListAdapter:Lcom/localytics/android/InAppMessagesAdapter;

    .line 100
    iput-object p2, p0, Lcom/localytics/android/InAppManager;->mMarketingHandler:Lcom/localytics/android/MarketingHandler;

    .line 101
    iput-object p3, p0, Lcom/localytics/android/InAppManager;->mCreativeManager:Lcom/localytics/android/CreativeManager;

    .line 102
    return-void
.end method

.method private _bindRuleToEvents(JLjava/util/List;)V
    .locals 11
    .param p1, "ruleId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "eventNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 487
    iget-object v3, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v4, "marketing_ruleevent"

    const-string v5, "%s = ?"

    new-array v6, v9, [Ljava/lang/Object;

    const-string v7, "rule_id_ref"

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-array v6, v9, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v3, v4, v5, v6}, Lcom/localytics/android/BaseProvider;->remove(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 490
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 492
    .local v0, "eventName":Ljava/lang/String;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 493
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "event_name"

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    const-string v3, "rule_id_ref"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 495
    iget-object v3, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v4, "marketing_ruleevent"

    invoke-virtual {v3, v4, v2}, Lcom/localytics/android/BaseProvider;->insert(Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_0

    .line 497
    .end local v0    # "eventName":Ljava/lang/String;
    .end local v2    # "values":Landroid/content/ContentValues;
    :cond_0
    return-void
.end method

.method private _controlGroupMessages(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/localytics/android/MarketingMessage;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/localytics/android/MarketingMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1068
    .local p1, "messages":Ljava/util/List;, "Ljava/util/List<Lcom/localytics/android/MarketingMessage;>;"
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 1069
    .local v1, "controlGroupMessages":Ljava/util/List;, "Ljava/util/List<Lcom/localytics/android/MarketingMessage;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/localytics/android/MarketingMessage;

    .line 1071
    .local v3, "msg":Lcom/localytics/android/MarketingMessage;
    const-string v4, "control_group"

    invoke-static {v3, v4}, Lcom/localytics/android/JsonHelper;->getSafeIntegerFromMap(Ljava/util/Map;Ljava/lang/String;)I

    move-result v0

    .line 1072
    .local v0, "controlGroup":I
    if-lez v0, :cond_0

    .line 1074
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1078
    .end local v0    # "controlGroup":I
    .end local v3    # "msg":Lcom/localytics/android/MarketingMessage;
    :cond_1
    return-object v1
.end method

.method private _deleteInAppCreative(I)V
    .locals 6
    .param p1, "ruleId"    # I

    .prologue
    .line 360
    int-to-long v4, p1

    iget-object v3, p0, Lcom/localytics/android/InAppManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-static {v4, v5, v3}, Lcom/localytics/android/CreativeManager;->getInAppUnzipFileDirPath(JLcom/localytics/android/LocalyticsDao;)Ljava/lang/String;

    move-result-object v0

    .line 361
    .local v0, "basePath":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 363
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 364
    .local v1, "dir":Ljava/io/File;
    invoke-static {v1}, Lcom/localytics/android/Utils;->deleteFile(Ljava/io/File;)V

    .line 366
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".zip"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 367
    .local v2, "zip":Ljava/io/File;
    invoke-static {v2}, Lcom/localytics/android/Utils;->deleteFile(Ljava/io/File;)V

    .line 369
    .end local v1    # "dir":Ljava/io/File;
    .end local v2    # "zip":Ljava/io/File;
    :cond_0
    return-void
.end method

.method private _deleteInAppRuleEventsAndConditions(I)V
    .locals 13
    .param p1, "ruleId"    # I

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 347
    int-to-long v6, p1

    invoke-direct {p0, v6, v7}, Lcom/localytics/android/InAppManager;->_getConditionIdFromRuleId(J)[J

    move-result-object v1

    .line 348
    .local v1, "conditionIds":[J
    move-object v0, v1

    .local v0, "arr$":[J
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-wide v2, v0, v4

    .line 350
    .local v2, "conditionId":J
    iget-object v6, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v7, "marketing_condition_values"

    const-string v8, "%s = ?"

    new-array v9, v12, [Ljava/lang/Object;

    const-string v10, "condition_id_ref"

    aput-object v10, v9, v11

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    new-array v9, v12, [Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-virtual {v6, v7, v8, v9}, Lcom/localytics/android/BaseProvider;->remove(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 348
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 352
    .end local v2    # "conditionId":J
    :cond_0
    iget-object v6, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v7, "marketing_conditions"

    const-string v8, "%s = ?"

    new-array v9, v12, [Ljava/lang/Object;

    const-string v10, "rule_id_ref"

    aput-object v10, v9, v11

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    new-array v9, v12, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-virtual {v6, v7, v8, v9}, Lcom/localytics/android/BaseProvider;->remove(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 355
    iget-object v6, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v7, "marketing_ruleevent"

    const-string v8, "%s = ?"

    new-array v9, v12, [Ljava/lang/Object;

    const-string v10, "rule_id_ref"

    aput-object v10, v9, v11

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    new-array v9, v12, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-virtual {v6, v7, v8, v9}, Lcom/localytics/android/BaseProvider;->remove(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 356
    return-void
.end method

.method private _destroyLocalInAppMessage(Lcom/localytics/android/MarketingMessage;)V
    .locals 9
    .param p1, "inAppMessage"    # Lcom/localytics/android/MarketingMessage;

    .prologue
    .line 326
    :try_start_0
    const-string v3, "campaign_id"

    invoke-virtual {p1, v3}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 327
    .local v0, "campaignId":I
    invoke-direct {p0, v0}, Lcom/localytics/android/InAppManager;->_getRuleIdFromCampaignId(I)I

    move-result v2

    .line 330
    .local v2, "ruleId":I
    invoke-direct {p0, v2}, Lcom/localytics/android/InAppManager;->_deleteInAppRuleEventsAndConditions(I)V

    .line 333
    iget-object v3, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v4, "marketing_rules"

    const-string v5, "%s = ?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "_id"

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v3, v4, v5, v6}, Lcom/localytics/android/BaseProvider;->remove(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 336
    invoke-direct {p0, v2}, Lcom/localytics/android/InAppManager;->_deleteInAppCreative(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 342
    .end local v0    # "campaignId":I
    .end local v2    # "ruleId":I
    :goto_0
    return-void

    .line 338
    :catch_0
    move-exception v1

    .line 340
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "Localytics library threw an uncaught exception"

    invoke-static {v3, v1}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private _getCampaignIdsFromFrequencyCappingQuery(Ljava/lang/String;)Ljava/util/Set;
    .locals 1
    .param p1, "query"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1549
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/localytics/android/InAppManager;->_getCampaignIdsFromFrequencyCappingQuery(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private _getCampaignIdsFromFrequencyCappingQuery(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/Set;
    .locals 3
    .param p1, "query"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "inputs"    # [Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1555
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 1556
    .local v0, "campaignIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1558
    const/4 v1, 0x0

    .line 1561
    .local v1, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v2, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    iget-object v2, v2, Lcom/localytics/android/BaseProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2, p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1563
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1565
    const-string v2, "campaign_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1570
    :catchall_0
    move-exception v2

    if-eqz v1, :cond_0

    .line 1572
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v2

    .line 1570
    :cond_1
    if-eqz v1, :cond_2

    .line 1572
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1577
    .end local v1    # "cursor":Landroid/database/Cursor;
    :cond_2
    return-object v0
.end method

.method private _getConditionIdFromRuleId(J)[J
    .locals 11
    .param p1, "ruleId"    # J

    .prologue
    .line 1922
    const/4 v6, 0x0

    .line 1924
    .local v6, "conditionIds":[J
    const/4 v7, 0x0

    .line 1927
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "marketing_conditions"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "%s = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v10, "rule_id_ref"

    aput-object v10, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/localytics/android/BaseProvider;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1928
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-array v6, v0, [J

    .line 1930
    const/4 v8, 0x0

    .local v8, "i":I
    move v9, v8

    .line 1931
    .end local v8    # "i":I
    .local v9, "i":I
    :goto_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1933
    add-int/lit8 v8, v9, 0x1

    .end local v9    # "i":I
    .restart local v8    # "i":I
    const-string v0, "_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    aput-wide v0, v6, v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v9, v8

    .end local v8    # "i":I
    .restart local v9    # "i":I
    goto :goto_0

    .line 1938
    :cond_0
    if-eqz v7, :cond_1

    .line 1940
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1943
    :cond_1
    return-object v6

    .line 1938
    .end local v9    # "i":I
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_2

    .line 1940
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private _getInAppConditionValues(I)Ljava/util/Vector;
    .locals 11
    .param p1, "conditionId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1954
    const/4 v8, 0x0

    .line 1956
    .local v8, "values":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    const/4 v6, 0x0

    .line 1959
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "marketing_condition_values"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "value"

    aput-object v4, v2, v3

    const-string v3, "%s = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v10, "condition_id_ref"

    aput-object v10, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/localytics/android/BaseProvider;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    move-object v9, v8

    .line 1960
    .end local v8    # "values":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    .local v9, "values":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    :goto_0
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1962
    const-string v0, "value"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1964
    .local v7, "value":Ljava/lang/String;
    if-nez v9, :cond_3

    .line 1966
    new-instance v8, Ljava/util/Vector;

    invoke-direct {v8}, Ljava/util/Vector;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1969
    .end local v9    # "values":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    .restart local v8    # "values":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    :goto_1
    :try_start_2
    invoke-virtual {v8, v7}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v9, v8

    .line 1970
    .end local v8    # "values":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    .restart local v9    # "values":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    goto :goto_0

    .line 1974
    .end local v7    # "value":Ljava/lang/String;
    :cond_0
    if-eqz v6, :cond_1

    .line 1976
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1980
    :cond_1
    return-object v9

    .line 1974
    .end local v9    # "values":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    .restart local v8    # "values":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v6, :cond_2

    .line 1976
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 1974
    .end local v8    # "values":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    .restart local v9    # "values":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    :catchall_1
    move-exception v0

    move-object v8, v9

    .end local v9    # "values":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    .restart local v8    # "values":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    goto :goto_2

    .end local v8    # "values":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    .restart local v7    # "value":Ljava/lang/String;
    .restart local v9    # "values":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    :cond_3
    move-object v8, v9

    .end local v9    # "values":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    .restart local v8    # "values":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    goto :goto_1
.end method

.method private _getInAppConditions(I)Ljava/util/Vector;
    .locals 15
    .param p1, "ruleId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Vector",
            "<",
            "Lcom/localytics/android/MarketingCondition;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1851
    const/4 v9, 0x0

    .line 1854
    .local v9, "inAppConditions":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/localytics/android/MarketingCondition;>;"
    const/4 v8, 0x0

    .line 1857
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "marketing_conditions"

    const/4 v2, 0x0

    const-string v3, "%s = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v14, "rule_id_ref"

    aput-object v14, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/localytics/android/BaseProvider;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    move-object v10, v9

    .line 1858
    .end local v9    # "inAppConditions":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/localytics/android/MarketingCondition;>;"
    .local v10, "inAppConditions":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/localytics/android/MarketingCondition;>;"
    :goto_0
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1860
    const-string v0, "_id"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 1861
    .local v7, "conditionId":I
    const-string v0, "attribute_name"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 1862
    .local v11, "name":Ljava/lang/String;
    const-string v0, "operator"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1863
    .local v12, "operator":Ljava/lang/String;
    invoke-direct {p0, v7}, Lcom/localytics/android/InAppManager;->_getInAppConditionValues(I)Ljava/util/Vector;

    move-result-object v13

    .line 1865
    .local v13, "values":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    if-nez v10, :cond_3

    .line 1867
    new-instance v9, Ljava/util/Vector;

    invoke-direct {v9}, Ljava/util/Vector;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1870
    .end local v10    # "inAppConditions":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/localytics/android/MarketingCondition;>;"
    .restart local v9    # "inAppConditions":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/localytics/android/MarketingCondition;>;"
    :goto_1
    :try_start_2
    new-instance v6, Lcom/localytics/android/MarketingCondition;

    invoke-direct {v6, v11, v12, v13}, Lcom/localytics/android/MarketingCondition;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Vector;)V

    .line 1871
    .local v6, "condition":Lcom/localytics/android/MarketingCondition;
    iget-object v0, p0, Lcom/localytics/android/InAppManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v0}, Lcom/localytics/android/LocalyticsDao;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/localytics/android/MarketingCondition;->setPackageName(Ljava/lang/String;)V

    .line 1872
    invoke-virtual {v9, v6}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v10, v9

    .line 1873
    .end local v9    # "inAppConditions":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/localytics/android/MarketingCondition;>;"
    .restart local v10    # "inAppConditions":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/localytics/android/MarketingCondition;>;"
    goto :goto_0

    .line 1877
    .end local v6    # "condition":Lcom/localytics/android/MarketingCondition;
    .end local v7    # "conditionId":I
    .end local v11    # "name":Ljava/lang/String;
    .end local v12    # "operator":Ljava/lang/String;
    .end local v13    # "values":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    :cond_0
    if-eqz v8, :cond_1

    .line 1879
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1883
    :cond_1
    return-object v10

    .line 1877
    .end local v10    # "inAppConditions":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/localytics/android/MarketingCondition;>;"
    .restart local v9    # "inAppConditions":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/localytics/android/MarketingCondition;>;"
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v8, :cond_2

    .line 1879
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 1877
    .end local v9    # "inAppConditions":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/localytics/android/MarketingCondition;>;"
    .restart local v10    # "inAppConditions":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/localytics/android/MarketingCondition;>;"
    :catchall_1
    move-exception v0

    move-object v9, v10

    .end local v10    # "inAppConditions":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/localytics/android/MarketingCondition;>;"
    .restart local v9    # "inAppConditions":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/localytics/android/MarketingCondition;>;"
    goto :goto_2

    .end local v9    # "inAppConditions":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/localytics/android/MarketingCondition;>;"
    .restart local v7    # "conditionId":I
    .restart local v10    # "inAppConditions":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/localytics/android/MarketingCondition;>;"
    .restart local v11    # "name":Ljava/lang/String;
    .restart local v12    # "operator":Ljava/lang/String;
    .restart local v13    # "values":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    :cond_3
    move-object v9, v10

    .end local v10    # "inAppConditions":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/localytics/android/MarketingCondition;>;"
    .restart local v9    # "inAppConditions":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/localytics/android/MarketingCondition;>;"
    goto :goto_1
.end method

.method private _getInAppMessageMaps(Ljava/lang/String;)Ljava/util/List;
    .locals 10
    .param p1, "eventName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/localytics/android/MarketingMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1166
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 1168
    .local v3, "inAppMessageMaps":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/localytics/android/MarketingMessage;>;"
    const/4 v0, 0x0

    .line 1171
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v6, p0, Lcom/localytics/android/InAppManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v6}, Lcom/localytics/android/LocalyticsDao;->getCurrentTimeMillis()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    .line 1172
    .local v4, "now":Ljava/lang/String;
    const-string v6, "SELECT * FROM %s WHERE %s IN (SELECT %s FROM %s WHERE %s = ?) AND %s > ?;"

    const/4 v7, 0x6

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, "marketing_rules"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string v9, "_id"

    aput-object v9, v7, v8

    const/4 v8, 0x2

    const-string v9, "rule_id_ref"

    aput-object v9, v7, v8

    const/4 v8, 0x3

    const-string v9, "marketing_ruleevent"

    aput-object v9, v7, v8

    const/4 v8, 0x4

    const-string v9, "event_name"

    aput-object v9, v7, v8

    const/4 v8, 0x5

    const-string v9, "expiration"

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1180
    .local v5, "sql":Ljava/lang/String;
    iget-object v6, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    iget-object v6, v6, Lcom/localytics/android/BaseProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    const/4 v8, 0x1

    aput-object v4, v7, v8

    invoke-virtual {v6, v5, v7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1182
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-ge v1, v6, :cond_0

    .line 1184
    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1186
    new-instance v2, Lcom/localytics/android/MarketingMessage;

    invoke-direct {v2}, Lcom/localytics/android/MarketingMessage;-><init>()V

    .line 1188
    .local v2, "inAppMessageMap":Lcom/localytics/android/MarketingMessage;
    const-string v6, "_id"

    const-string v7, "_id"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1189
    const-string v6, "campaign_id"

    const-string v7, "campaign_id"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1190
    const-string v6, "expiration"

    const-string v7, "expiration"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1191
    const-string v6, "display_seconds"

    const-string v7, "display_seconds"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1192
    const-string v6, "display_session"

    const-string v7, "display_session"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1193
    const-string v6, "version"

    const-string v7, "version"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1194
    const-string v6, "phone_location"

    const-string v7, "phone_location"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1195
    const-string v6, "phone_size_width"

    const-string v7, "phone_size_width"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1196
    const-string v6, "phone_size_height"

    const-string v7, "phone_size_height"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1197
    const-string v6, "tablet_location"

    const-string v7, "tablet_location"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1198
    const-string v6, "tablet_size_width"

    const-string v7, "tablet_size_width"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1199
    const-string v6, "tablet_size_height"

    const-string v7, "tablet_size_height"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1200
    const-string v6, "time_to_display"

    const-string v7, "time_to_display"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1201
    const-string v6, "internet_required"

    const-string v7, "internet_required"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1202
    const-string v6, "ab_test"

    const-string v7, "ab_test"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1203
    const-string v6, "rule_name_non_unique"

    const-string v7, "rule_name_non_unique"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1204
    const-string v6, "location"

    const-string v7, "location"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1205
    const-string v6, "devices"

    const-string v7, "devices"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1206
    const-string v6, "control_group"

    const-string v7, "control_group"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1207
    const-string v6, "schema_version"

    const-string v7, "schema_version"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1209
    iget-object v6, p0, Lcom/localytics/android/InAppManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    const/4 v7, 0x1

    invoke-static {v2, v6, v7}, Lcom/localytics/android/InAppManager;->updateMessageWithSpecialKeys(Lcom/localytics/android/MarketingMessage;Lcom/localytics/android/LocalyticsDao;Z)V

    .line 1211
    invoke-virtual {v3, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1182
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 1216
    .end local v2    # "inAppMessageMap":Lcom/localytics/android/MarketingMessage;
    :cond_0
    if-eqz v0, :cond_1

    .line 1218
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1222
    :cond_1
    return-object v3

    .line 1216
    .end local v1    # "i":I
    .end local v4    # "now":Ljava/lang/String;
    .end local v5    # "sql":Ljava/lang/String;
    :catchall_0
    move-exception v6

    if-eqz v0, :cond_2

    .line 1218
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v6
.end method

.method private _getRuleIdFromCampaignId(I)I
    .locals 9
    .param p1, "campaignId"    # I

    .prologue
    .line 1894
    const/4 v7, 0x0

    .line 1895
    .local v7, "ruleId":I
    const/4 v6, 0x0

    .line 1898
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "marketing_rules"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "%s = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v8, "campaign_id"

    aput-object v8, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/localytics/android/BaseProvider;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1899
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1901
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 1906
    :cond_0
    if-eqz v6, :cond_1

    .line 1908
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1911
    :cond_1
    return v7

    .line 1906
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 1908
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private _isConnectingToInternet()Z
    .locals 8

    .prologue
    .line 1791
    iget-object v6, p0, Lcom/localytics/android/InAppManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v6}, Lcom/localytics/android/LocalyticsDao;->getAppContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "connectivity"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    .line 1792
    .local v2, "connectivity":Landroid/net/ConnectivityManager;
    if-eqz v2, :cond_1

    .line 1794
    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getAllNetworkInfo()[Landroid/net/NetworkInfo;

    move-result-object v4

    .line 1795
    .local v4, "info":[Landroid/net/NetworkInfo;
    if-eqz v4, :cond_1

    .line 1797
    move-object v1, v4

    .local v1, "arr$":[Landroid/net/NetworkInfo;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v5, :cond_1

    aget-object v0, v1, v3

    .line 1799
    .local v0, "anInfo":Landroid/net/NetworkInfo;
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v6

    sget-object v7, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v6, v7, :cond_0

    .line 1801
    const/4 v6, 0x1

    .line 1806
    .end local v0    # "anInfo":Landroid/net/NetworkInfo;
    .end local v1    # "arr$":[Landroid/net/NetworkInfo;
    .end local v3    # "i$":I
    .end local v4    # "info":[Landroid/net/NetworkInfo;
    .end local v5    # "len$":I
    :goto_1
    return v6

    .line 1797
    .restart local v0    # "anInfo":Landroid/net/NetworkInfo;
    .restart local v1    # "arr$":[Landroid/net/NetworkInfo;
    .restart local v3    # "i$":I
    .restart local v4    # "info":[Landroid/net/NetworkInfo;
    .restart local v5    # "len$":I
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1806
    .end local v0    # "anInfo":Landroid/net/NetworkInfo;
    .end local v1    # "arr$":[Landroid/net/NetworkInfo;
    .end local v3    # "i$":I
    .end local v4    # "info":[Landroid/net/NetworkInfo;
    .end local v5    # "len$":I
    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method private _isInAppMessageSatisfiedConditions(Lcom/localytics/android/MarketingMessage;Ljava/util/Map;)Z
    .locals 7
    .param p1, "inAppMessage"    # Lcom/localytics/android/MarketingMessage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/localytics/android/MarketingMessage;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1818
    .local p2, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v5, 0x1

    .line 1821
    .local v5, "satisfied":Z
    const-string v6, "campaign_id"

    invoke-virtual {p1, v6}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1822
    .local v0, "campaignId":I
    invoke-direct {p0, v0}, Lcom/localytics/android/InAppManager;->_getRuleIdFromCampaignId(I)I

    move-result v4

    .line 1825
    .local v4, "ruleId":I
    invoke-direct {p0, v4}, Lcom/localytics/android/InAppManager;->_getInAppConditions(I)Ljava/util/Vector;

    move-result-object v3

    .line 1828
    .local v3, "inAppConditions":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/localytics/android/MarketingCondition;>;"
    if-eqz v3, :cond_1

    .line 1830
    invoke-virtual {v3}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/localytics/android/MarketingCondition;

    .line 1832
    .local v1, "condition":Lcom/localytics/android/MarketingCondition;
    invoke-virtual {v1, p2}, Lcom/localytics/android/MarketingCondition;->isSatisfiedByAttributes(Ljava/util/Map;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1834
    const/4 v5, 0x0

    .line 1840
    .end local v1    # "condition":Lcom/localytics/android/MarketingCondition;
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    return v5
.end method

.method private _parseInAppMessage(ILcom/localytics/android/MarketingMessage;Ljava/util/Map;)Landroid/content/ContentValues;
    .locals 8
    .param p1, "ruleId"    # I
    .param p2, "inAppMessage"    # Lcom/localytics/android/MarketingMessage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/localytics/android/MarketingMessage;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Landroid/content/ContentValues;"
        }
    .end annotation

    .prologue
    .local p3, "config":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v6, 0x1

    .line 399
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 401
    .local v4, "values":Landroid/content/ContentValues;
    const-string v7, "_id"

    if-lez p1, :cond_1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    :goto_0
    invoke-virtual {v4, v7, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 402
    const-string v5, "campaign_id"

    const-string v7, "campaign_id"

    invoke-static {p2, v7}, Lcom/localytics/android/JsonHelper;->getSafeIntegerFromMap(Ljava/util/Map;Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 403
    const-string v5, "expiration"

    const-string v7, "expiration"

    invoke-static {p2, v7}, Lcom/localytics/android/JsonHelper;->getSafeIntegerFromMap(Ljava/util/Map;Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 404
    const-string v5, "display_seconds"

    const-string v7, "display_seconds"

    invoke-static {p2, v7}, Lcom/localytics/android/JsonHelper;->getSafeIntegerFromMap(Ljava/util/Map;Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 405
    const-string v5, "display_session"

    const-string v7, "display_session"

    invoke-static {p2, v7}, Lcom/localytics/android/JsonHelper;->getSafeIntegerFromMap(Ljava/util/Map;Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 406
    const-string v5, "version"

    const-string v7, "version"

    invoke-static {p2, v7}, Lcom/localytics/android/JsonHelper;->getSafeIntegerFromMap(Ljava/util/Map;Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 407
    const-string v5, "phone_location"

    const-string v7, "phone_location"

    invoke-static {p2, v7}, Lcom/localytics/android/JsonHelper;->getSafeStringFromMap(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    const-string v5, "phone_size"

    invoke-static {p2, v5}, Lcom/localytics/android/JsonHelper;->getSafeMapFromMap(Ljava/util/Map;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 409
    .local v0, "phoneSize":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v5, "phone_size_width"

    const-string v7, "width"

    invoke-static {v0, v7}, Lcom/localytics/android/JsonHelper;->getSafeIntegerFromMap(Ljava/util/Map;Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 410
    const-string v5, "phone_size_height"

    const-string v7, "height"

    invoke-static {v0, v7}, Lcom/localytics/android/JsonHelper;->getSafeIntegerFromMap(Ljava/util/Map;Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 411
    const-string v5, "tablet_size"

    invoke-static {p2, v5}, Lcom/localytics/android/JsonHelper;->getSafeMapFromMap(Ljava/util/Map;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    .line 412
    .local v3, "tabletSize":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v5, "tablet_location"

    const-string v7, "tablet_location"

    invoke-static {p2, v7}, Lcom/localytics/android/JsonHelper;->getSafeStringFromMap(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    const-string v5, "tablet_size_width"

    const-string v7, "width"

    invoke-static {v3, v7}, Lcom/localytics/android/JsonHelper;->getSafeIntegerFromMap(Ljava/util/Map;Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 414
    const-string v5, "tablet_size_height"

    const-string v7, "height"

    invoke-static {v3, v7}, Lcom/localytics/android/JsonHelper;->getSafeIntegerFromMap(Ljava/util/Map;Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 415
    const-string v5, "time_to_display"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 416
    const-string v7, "internet_required"

    const-string v5, "internet_required"

    invoke-static {p2, v5}, Lcom/localytics/android/JsonHelper;->getSafeBooleanFromMap(Ljava/util/Map;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v5, v6

    :goto_1
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v7, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 417
    const-string v5, "ab_test"

    const-string v6, "ab"

    invoke-static {p2, v6}, Lcom/localytics/android/JsonHelper;->getSafeStringFromMap(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    const-string v5, "rule_name"

    invoke-static {p2, v5}, Lcom/localytics/android/JsonHelper;->getSafeStringFromMap(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 419
    .local v1, "ruleName":Ljava/lang/String;
    const-string v5, "rule_name_non_unique"

    invoke-virtual {v4, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    const-string v5, "rule_name"

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    const-string v5, "location"

    const-string v6, "location"

    invoke-static {p2, v6}, Lcom/localytics/android/JsonHelper;->getSafeStringFromMap(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    const-string v5, "devices"

    const-string v6, "devices"

    invoke-static {p2, v6}, Lcom/localytics/android/JsonHelper;->getSafeStringFromMap(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    const-string v5, "control_group"

    const-string v6, "control_group"

    invoke-static {p2, v6}, Lcom/localytics/android/JsonHelper;->getSafeIntegerFromMap(Ljava/util/Map;Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 424
    if-eqz p3, :cond_0

    .line 426
    const-string v5, "schema_version"

    invoke-static {p3, v5}, Lcom/localytics/android/JsonHelper;->getSafeIntegerFromMap(Ljava/util/Map;Ljava/lang/String;)I

    move-result v2

    .line 427
    .local v2, "schemaVersion":I
    if-lez v2, :cond_0

    .line 429
    const-string v5, "schema_version"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 433
    .end local v2    # "schemaVersion":I
    :cond_0
    return-object v4

    .line 401
    .end local v0    # "phoneSize":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v1    # "ruleName":Ljava/lang/String;
    .end local v3    # "tabletSize":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_1
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 416
    .restart local v0    # "phoneSize":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .restart local v3    # "tabletSize":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_2
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private _saveInAppConditions(JLjava/util/List;)V
    .locals 19
    .param p1, "ruleId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 438
    .local p3, "conditions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    if-nez p3, :cond_1

    .line 474
    :cond_0
    return-void

    .line 444
    :cond_1
    invoke-direct/range {p0 .. p2}, Lcom/localytics/android/InAppManager;->_getConditionIdFromRuleId(J)[J

    move-result-object v6

    .line 445
    .local v6, "conditionIds":[J
    move-object v2, v6

    .local v2, "arr$":[J
    array-length v9, v2

    .local v9, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_0
    if-ge v8, v9, :cond_2

    aget-wide v4, v2, v8

    .line 447
    .local v4, "conditionId":J
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v13, "marketing_condition_values"

    const-string v14, "%s = ?"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    const-string v17, "condition_id_ref"

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v12, v13, v14, v15}, Lcom/localytics/android/BaseProvider;->remove(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 445
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 449
    .end local v4    # "conditionId":J
    :cond_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v13, "marketing_conditions"

    const-string v14, "%s = ?"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    const-string v17, "rule_id_ref"

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v12, v13, v14, v15}, Lcom/localytics/android/BaseProvider;->remove(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 451
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    .local v10, "obj":Ljava/lang/Object;
    move-object v3, v10

    .line 454
    check-cast v3, Ljava/util/List;

    .line 458
    .local v3, "condition":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 459
    .local v11, "values":Landroid/content/ContentValues;
    const-string v13, "attribute_name"

    const/4 v12, 0x0

    invoke-interface {v3, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-virtual {v11, v13, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    const-string v13, "operator"

    const/4 v12, 0x1

    invoke-interface {v3, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-virtual {v11, v13, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    const-string v12, "rule_id_ref"

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 462
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v13, "marketing_conditions"

    invoke-virtual {v12, v13, v11}, Lcom/localytics/android/BaseProvider;->insert(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 466
    .restart local v4    # "conditionId":J
    const/4 v7, 0x2

    .local v7, "i":I
    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v12

    if-ge v7, v12, :cond_3

    .line 468
    new-instance v11, Landroid/content/ContentValues;

    .end local v11    # "values":Landroid/content/ContentValues;
    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 469
    .restart local v11    # "values":Landroid/content/ContentValues;
    const-string v12, "value"

    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    invoke-static {v13}, Lcom/localytics/android/JsonHelper;->getSafeStringFromValue(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    const-string v12, "condition_id_ref"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 471
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v13, "marketing_condition_values"

    invoke-virtual {v12, v13, v11}, Lcom/localytics/android/BaseProvider;->insert(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 466
    add-int/lit8 v7, v7, 0x1

    goto :goto_1
.end method

.method private _tagAmpActionEventForControlGroup(Lcom/localytics/android/MarketingMessage;)V
    .locals 7
    .param p1, "marketingMessage"    # Lcom/localytics/android/MarketingMessage;

    .prologue
    .line 1098
    new-instance v2, Ljava/util/TreeMap;

    invoke-direct {v2}, Ljava/util/TreeMap;-><init>()V

    .line 1100
    .local v2, "impressionAttributes":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v5, "campaign_id"

    invoke-virtual {p1, v5}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1101
    .local v1, "campaignId":Ljava/lang/String;
    const-string v5, "rule_name_non_unique"

    invoke-virtual {p1, v5}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1102
    .local v3, "ruleName":Ljava/lang/String;
    const-string v5, "schema_version"

    invoke-virtual {p1, v5}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1103
    .local v4, "schemaVersion":Ljava/lang/String;
    const-string v5, "ab_test"

    invoke-virtual {p1, v5}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1104
    .local v0, "ab":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1106
    const-string v5, "ampAB"

    invoke-virtual {v2, v5, v0}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1109
    :cond_0
    const-string v5, "ampAction"

    const-string v6, "control"

    invoke-virtual {v2, v5, v6}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1110
    const-string v5, "type"

    const-string v6, "Control"

    invoke-virtual {v2, v5, v6}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1111
    const-string v5, "ampCampaignId"

    invoke-virtual {v2, v5, v1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112
    const-string v5, "ampCampaign"

    invoke-virtual {v2, v5, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113
    const-string v5, "Schema Version - Client"

    const/4 v6, 0x5

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1114
    const-string v5, "Schema Version - Server"

    invoke-virtual {v2, v5, v4}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1115
    iget-object v5, p0, Lcom/localytics/android/InAppManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    const-string v6, "ampView"

    invoke-interface {v5, v6, v2}, Lcom/localytics/android/LocalyticsDao;->tagEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 1116
    return-void
.end method

.method private _timeStringToSeconds(Ljava/lang/String;)I
    .locals 4
    .param p1, "time"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 889
    :try_start_0
    const-string v2, ":"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 890
    .local v0, "components":[Ljava/lang/String;
    const/4 v2, 0x0

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    mul-int/lit16 v2, v2, 0xe10

    const/4 v3, 0x1

    aget-object v3, v0, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    mul-int/lit8 v3, v3, 0x3c

    add-int/2addr v2, v3

    .line 894
    .end local v0    # "components":[Ljava/lang/String;
    :goto_0
    return v2

    .line 892
    :catch_0
    move-exception v1

    .line 894
    .local v1, "e":Ljava/lang/Exception;
    const/4 v2, -0x1

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/localytics/android/InAppManager;)Lcom/localytics/android/MarketingHandler;
    .locals 1
    .param p0, "x0"    # Lcom/localytics/android/InAppManager;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/localytics/android/InAppManager;->mMarketingHandler:Lcom/localytics/android/MarketingHandler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/localytics/android/InAppManager;)Landroid/app/FragmentManager;
    .locals 1
    .param p0, "x0"    # Lcom/localytics/android/InAppManager;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/localytics/android/InAppManager;->mFragmentManager:Landroid/app/FragmentManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/localytics/android/InAppManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/localytics/android/InAppManager;

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/localytics/android/InAppManager;->mIsMidDisplayingInApp:Z

    return v0
.end method

.method static synthetic access$202(Lcom/localytics/android/InAppManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/localytics/android/InAppManager;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/localytics/android/InAppManager;->mIsMidDisplayingInApp:Z

    return p1
.end method

.method static synthetic access$300(Lcom/localytics/android/InAppManager;Lcom/localytics/android/MarketingMessage;Ljava/lang/String;Ljava/util/Map;)Lcom/localytics/android/InAppCampaign;
    .locals 1
    .param p0, "x0"    # Lcom/localytics/android/InAppManager;
    .param p1, "x1"    # Lcom/localytics/android/MarketingMessage;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/util/Map;

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Lcom/localytics/android/InAppManager;->getInAppCampaignFromMarketingMessage(Lcom/localytics/android/MarketingMessage;Ljava/lang/String;Ljava/util/Map;)Lcom/localytics/android/InAppCampaign;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$402(Lcom/localytics/android/InAppManager;Lcom/localytics/android/InAppMessagesAdapter;)Lcom/localytics/android/InAppMessagesAdapter;
    .locals 0
    .param p0, "x0"    # Lcom/localytics/android/InAppManager;
    .param p1, "x1"    # Lcom/localytics/android/InAppMessagesAdapter;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/localytics/android/InAppManager;->mTestCampaignsListAdapter:Lcom/localytics/android/InAppMessagesAdapter;

    return-object p1
.end method

.method static synthetic access$500(Lcom/localytics/android/InAppManager;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/localytics/android/InAppManager;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Lcom/localytics/android/InAppManager;->copyToClipboard(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/localytics/android/MarketingMessage;Lcom/localytics/android/LocalyticsDao;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/localytics/android/MarketingMessage;
    .param p1, "x1"    # Lcom/localytics/android/LocalyticsDao;
    .param p2, "x2"    # Z

    .prologue
    .line 46
    invoke-static {p0, p1, p2}, Lcom/localytics/android/InAppManager;->updateMessageWithSpecialKeys(Lcom/localytics/android/MarketingMessage;Lcom/localytics/android/LocalyticsDao;Z)V

    return-void
.end method

.method private copyToClipboard(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "appContext"    # Landroid/content/Context;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "valueDescription"    # Ljava/lang/String;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 2325
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2327
    invoke-static {}, Lcom/localytics/android/DatapointHelper;->getApiLevel()I

    move-result v1

    const/16 v2, 0xb

    if-ge v1, v2, :cond_0

    .line 2329
    const-string v1, "clipboard"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/ClipboardManager;

    .line 2330
    .local v0, "clipboard":Landroid/text/ClipboardManager;
    invoke-virtual {v0, p2}, Landroid/text/ClipboardManager;->setText(Ljava/lang/CharSequence;)V

    .line 2337
    .end local v0    # "clipboard":Landroid/text/ClipboardManager;
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has been copied to the clipboard."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 2343
    :goto_1
    return-void

    .line 2334
    :cond_0
    const-string v1, "clipboard"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 2335
    .local v0, "clipboard":Landroid/content/ClipboardManager;
    invoke-virtual {v0, p2}, Landroid/content/ClipboardManager;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2341
    .end local v0    # "clipboard":Landroid/content/ClipboardManager;
    :cond_1
    const-string v1, "No %s found. Please check your integration."

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p3, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method private getInAppCampaignFromMarketingMessage(Lcom/localytics/android/MarketingMessage;Ljava/lang/String;Ljava/util/Map;)Lcom/localytics/android/InAppCampaign;
    .locals 6
    .param p1, "message"    # Lcom/localytics/android/MarketingMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "event"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/localytics/android/MarketingMessage;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/localytics/android/InAppCampaign;"
        }
    .end annotation

    .prologue
    .line 2349
    .local p3, "eventAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2350
    .local v0, "webViewAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "html_url"

    const-string v1, "html_url"

    invoke-virtual {p1, v1}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2351
    const-string v2, "base_path"

    const-string v1, "base_path"

    invoke-virtual {p1, v1}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2352
    const-string v1, "display_width"

    const-string v2, "display_width"

    invoke-virtual {p1, v2}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2353
    const-string v1, "display_height"

    const-string v2, "display_height"

    invoke-virtual {p1, v2}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2355
    new-instance v2, Lcom/localytics/android/InAppCampaign$Builder;

    invoke-direct {v2}, Lcom/localytics/android/InAppCampaign$Builder;-><init>()V

    const-string v1, "campaign_id"

    invoke-virtual {p1, v1}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v4, v1

    invoke-virtual {v2, v4, v5}, Lcom/localytics/android/InAppCampaign$Builder;->setCampaignId(J)Lcom/localytics/android/InAppCampaign$Builder;

    move-result-object v2

    const-string v1, "rule_name_non_unique"

    invoke-virtual {p1, v1}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/localytics/android/InAppCampaign$Builder;->setRuleName(Ljava/lang/String;)Lcom/localytics/android/InAppCampaign$Builder;

    move-result-object v2

    const-string v1, "version"

    invoke-virtual {p1, v1}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/localytics/android/InAppCampaign$Builder;->setVersion(J)Lcom/localytics/android/InAppCampaign$Builder;

    move-result-object v2

    const-string v1, "schema_version"

    invoke-virtual {p1, v1}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v4, v1

    invoke-virtual {v2, v4, v5}, Lcom/localytics/android/InAppCampaign$Builder;->setSchemaVersion(J)Lcom/localytics/android/InAppCampaign$Builder;

    move-result-object v2

    const-string v1, "location"

    invoke-virtual {p1, v1}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/localytics/android/InAppCampaign$Builder;->setDisplayLocation(Ljava/lang/String;)Lcom/localytics/android/InAppCampaign$Builder;

    move-result-object v2

    const-string v1, "ab_test"

    invoke-virtual {p1, v1}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/localytics/android/InAppCampaign$Builder;->setAbTest(Ljava/lang/String;)Lcom/localytics/android/InAppCampaign$Builder;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/localytics/android/InAppCampaign$Builder;->setEventAttributes(Ljava/util/Map;)Lcom/localytics/android/InAppCampaign$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/localytics/android/InAppCampaign$Builder;->setWebViewAttributes(Ljava/util/Map;)Lcom/localytics/android/InAppCampaign$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/localytics/android/InAppCampaign$Builder;->build()Lcom/localytics/android/InAppCampaign;

    move-result-object v1

    return-object v1
.end method

.method private static getInAppRemoteFileURL(Lcom/localytics/android/MarketingMessage;)Ljava/lang/String;
    .locals 2
    .param p0, "marketingMessage"    # Lcom/localytics/android/MarketingMessage;

    .prologue
    .line 1227
    const-string v1, "devices"

    invoke-static {p0, v1}, Lcom/localytics/android/JsonHelper;->getSafeStringFromMap(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1228
    .local v0, "devices":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "tablet"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1230
    const-string v1, "tablet_location"

    invoke-static {p0, v1}, Lcom/localytics/android/JsonHelper;->getSafeStringFromMap(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1232
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "phone_location"

    invoke-static {p0, v1}, Lcom/localytics/android/JsonHelper;->getSafeStringFromMap(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static updateMessageWithSpecialKeys(Lcom/localytics/android/MarketingMessage;Lcom/localytics/android/LocalyticsDao;Z)V
    .locals 12
    .param p0, "message"    # Lcom/localytics/android/MarketingMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "localyticsDao"    # Lcom/localytics/android/LocalyticsDao;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "includeDimensions"    # Z

    .prologue
    .line 1240
    const-string v7, "_id"

    invoke-virtual {p0, v7}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 1242
    .local v6, "ruleId":I
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "file://"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    int-to-long v8, v6

    invoke-static {v8, v9, p1}, Lcom/localytics/android/CreativeManager;->getInAppLocalHtmlLocation(JLcom/localytics/android/LocalyticsDao;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1243
    .local v4, "localHtmlURL":Ljava/lang/String;
    invoke-static {p0}, Lcom/localytics/android/InAppManager;->getInAppRemoteFileURL(Lcom/localytics/android/MarketingMessage;)Ljava/lang/String;

    move-result-object v5

    .line 1244
    .local v5, "remoteFileLocation":Ljava/lang/String;
    int-to-long v8, v6

    const-string v7, ".zip"

    invoke-virtual {v5, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    invoke-static {v8, v9, v7, p1}, Lcom/localytics/android/CreativeManager;->getInAppLocalFileURL(JZLcom/localytics/android/LocalyticsDao;)Ljava/lang/String;

    move-result-object v3

    .line 1245
    .local v3, "localFileLocation":Ljava/lang/String;
    const-string v7, "creative_url"

    invoke-virtual {p0, v7, v5}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1246
    const-string v7, "html_url"

    invoke-virtual {p0, v7, v4}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1247
    const-string v7, "base_path"

    int-to-long v8, v6

    invoke-static {v8, v9, p1}, Lcom/localytics/android/CreativeManager;->getInAppUnzipFileDirPath(JLcom/localytics/android/LocalyticsDao;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v7, v8}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1248
    const-string v7, "zip_name"

    const-string v8, "amp_rule_%d.zip"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v7, v8}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1249
    const-string v7, "local_file_location"

    invoke-virtual {p0, v7, v3}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1250
    const-string v7, "download_url"

    invoke-virtual {p0, v7, v5}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1252
    if-eqz p2, :cond_0

    .line 1256
    const-string v7, "devices"

    invoke-virtual {p0, v7}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1258
    .local v0, "devices":Ljava/lang/String;
    const-string v7, "tablet"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1260
    const-string v7, "tablet_size_width"

    invoke-virtual {p0, v7}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1261
    .local v2, "displayWidth":I
    const-string v7, "tablet_size_height"

    invoke-virtual {p0, v7}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1274
    .local v1, "displayHeight":I
    :goto_0
    const-string v7, "display_width"

    int-to-float v8, v2

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    invoke-virtual {p0, v7, v8}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1275
    const-string v7, "display_height"

    int-to-float v8, v1

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    invoke-virtual {p0, v7, v8}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1277
    .end local v0    # "devices":Ljava/lang/String;
    .end local v1    # "displayHeight":I
    .end local v2    # "displayWidth":I
    :cond_0
    return-void

    .line 1263
    .restart local v0    # "devices":Ljava/lang/String;
    :cond_1
    const-string v7, "both"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1265
    const-string v7, "phone_size_width"

    invoke-virtual {p0, v7}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1266
    .restart local v2    # "displayWidth":I
    const-string v7, "phone_size_height"

    invoke-virtual {p0, v7}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .restart local v1    # "displayHeight":I
    goto :goto_0

    .line 1270
    .end local v1    # "displayHeight":I
    .end local v2    # "displayWidth":I
    :cond_2
    const-string v7, "phone_size_width"

    invoke-virtual {p0, v7}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1271
    .restart local v2    # "displayWidth":I
    const-string v7, "phone_size_height"

    invoke-virtual {p0, v7}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .restart local v1    # "displayHeight":I
    goto :goto_0
.end method


# virtual methods
.method _additionalFCDatesAndTimesRulesValidation(Ljava/util/List;Ljava/text/SimpleDateFormat;)Z
    .locals 6
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "formatter"    # Ljava/text/SimpleDateFormat;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;",
            "Ljava/text/SimpleDateFormat;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 623
    .local p1, "rules":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    if-eqz p1, :cond_1

    .line 625
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    .line 629
    .local v3, "rule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :try_start_0
    const-string v5, "start"

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {p2, v5}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    .line 630
    .local v4, "startTime":Ljava/util/Date;
    const-string v5, "end"

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {p2, v5}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 631
    .local v1, "endTime":Ljava/util/Date;
    invoke-virtual {v4, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 633
    new-instance v5, Ljava/lang/Exception;

    invoke-direct {v5}, Ljava/lang/Exception;-><init>()V

    throw v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 636
    .end local v1    # "endTime":Ljava/util/Date;
    .end local v4    # "startTime":Ljava/util/Date;
    :catch_0
    move-exception v0

    .line 638
    .local v0, "e":Ljava/lang/Exception;
    const/4 v5, 0x0

    .line 643
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "rule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :goto_0
    return v5

    :cond_1
    const/4 v5, 0x1

    goto :goto_0
.end method

.method _additionalFCDisplayFrequencyRulesValidation(Ljava/util/List;)Z
    .locals 4
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>;)Z"
        }
    .end annotation

    .prologue
    .local p1, "displayFrequencyRules":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    const/4 v3, 0x1

    .line 607
    if-eqz p1, :cond_2

    .line 609
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 611
    .local v0, "displayFrequencyRule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    const-string v2, "days"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lt v2, v3, :cond_1

    const-string v2, "count"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ge v2, v3, :cond_0

    .line 613
    :cond_1
    const/4 v2, 0x0

    .line 618
    .end local v0    # "displayFrequencyRule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v1    # "i$":Ljava/util/Iterator;
    :goto_0
    return v2

    :cond_2
    move v2, v3

    goto :goto_0
.end method

.method _additionalFCWeekdaysRulesValidation(Ljava/util/List;)Z
    .locals 5
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "weekdaysRules":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v2, 0x0

    .line 648
    if-eqz p1, :cond_3

    .line 650
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x7

    if-le v3, v4, :cond_1

    .line 666
    :cond_0
    :goto_0
    return v2

    .line 656
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 658
    .local v1, "weekday":Ljava/lang/Integer;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ltz v3, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x6

    if-le v3, v4, :cond_2

    goto :goto_0

    .line 666
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "weekday":Ljava/lang/Integer;
    :cond_3
    const/4 v2, 0x1

    goto :goto_0
.end method

.method _augmentBlackoutRule(Ljava/util/Map;)Ljava/util/Map;
    .locals 7
    .param p1    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 792
    .local p1, "blackoutRule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 793
    .local v0, "augmentedRule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v4, "dates"

    invoke-interface {p1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    .line 794
    .local v1, "hasDates":Z
    const-string v4, "weekdays"

    invoke-interface {p1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    .line 795
    .local v3, "hasWeekdays":Z
    const-string v4, "times"

    invoke-interface {p1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    .line 797
    .local v2, "hasTimes":Z
    if-nez v1, :cond_0

    if-eqz v3, :cond_2

    :cond_0
    if-nez v2, :cond_2

    .line 799
    const-string v4, "times"

    new-instance v5, Ljava/util/ArrayList;

    sget-object v6, Lcom/localytics/android/InAppManager;->AUGMENTED_BLACKOUT_TIMES_RULE:Ljava/util/List;

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 806
    :cond_1
    :goto_0
    return-object v0

    .line 801
    :cond_2
    if-eqz v2, :cond_1

    if-nez v1, :cond_1

    if-nez v3, :cond_1

    .line 803
    const-string v4, "weekdays"

    new-instance v5, Ljava/util/ArrayList;

    sget-object v6, Lcom/localytics/android/InAppManager;->AUGMENTED_BLACKOUT_WEEKDAYS_RULE:Ljava/util/List;

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method _campaignsSatisfyingConditions(Ljava/util/List;Ljava/util/Map;)Ljava/util/List;
    .locals 6
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/localytics/android/MarketingMessage;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/localytics/android/MarketingMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "inAppMessages":Ljava/util/List;, "Ljava/util/List<Lcom/localytics/android/MarketingMessage;>;"
    .local p2, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v5, 0x1

    .line 1762
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 1763
    .local v0, "candidates":Ljava/util/List;, "Ljava/util/List<Lcom/localytics/android/MarketingMessage;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/localytics/android/MarketingMessage;

    .line 1766
    .local v3, "message":Lcom/localytics/android/MarketingMessage;
    const-string v4, "internet_required"

    invoke-virtual {v3, v4}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v4, v5, :cond_2

    move v2, v5

    .line 1767
    .local v2, "internetRequired":Z
    :goto_1
    if-eqz v2, :cond_1

    invoke-direct {p0}, Lcom/localytics/android/InAppManager;->_isConnectingToInternet()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1773
    :cond_1
    invoke-direct {p0, v3, p2}, Lcom/localytics/android/InAppManager;->_isInAppMessageSatisfiedConditions(Lcom/localytics/android/MarketingMessage;Ljava/util/Map;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1778
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1766
    .end local v2    # "internetRequired":Z
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 1781
    .end local v3    # "message":Lcom/localytics/android/MarketingMessage;
    :cond_3
    return-object v0
.end method

.method _checkFrequencyCappingRuleArray(Ljava/util/List;[Ljava/lang/String;)Z
    .locals 10
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "subKeys"    # [Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<*>;[",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .local p1, "rulesList":Ljava/util/List;, "Ljava/util/List<*>;"
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 577
    if-eqz p1, :cond_3

    .line 579
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v9

    if-ge v9, v8, :cond_0

    .line 602
    :goto_0
    return v7

    .line 585
    :cond_0
    array-length v9, p2

    if-lez v9, :cond_3

    .line 587
    move-object v4, p1

    .line 588
    .local v4, "listOfMaps":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map;>;"
    move-object v0, p2

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    move v2, v1

    .end local v1    # "i$":I
    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_3

    aget-object v6, v0, v2

    .line 590
    .local v6, "subKey":Ljava/lang/String;
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .end local v2    # "i$":I
    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map;

    .line 592
    .local v5, "m":Ljava/util/Map;
    invoke-interface {v5, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    goto :goto_0

    .line 588
    .end local v5    # "m":Ljava/util/Map;
    :cond_2
    add-int/lit8 v1, v2, 0x1

    .local v1, "i$":I
    move v2, v1

    .end local v1    # "i$":I
    .restart local v2    # "i$":I
    goto :goto_1

    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "listOfMaps":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map;>;"
    .end local v6    # "subKey":Ljava/lang/String;
    :cond_3
    move v7, v8

    .line 602
    goto :goto_0
.end method

.method _deleteFrequencyCappingRule(Ljava/lang/Integer;)Z
    .locals 9
    .param p1, "campaignId"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 709
    :try_start_0
    iget-object v3, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v4, "frequency_capping_rules"

    const-string v5, "%s = ?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "campaign_id"

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v3, v4, v5, v6}, Lcom/localytics/android/BaseProvider;->remove(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 714
    :goto_0
    return v1

    .line 712
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    move v1, v2

    .line 714
    goto :goto_0
.end method

.method _filterInAppMessagesDisqualifiedByFrequencyCappingRules(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/localytics/android/MarketingMessage;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/localytics/android/MarketingMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1282
    .local p1, "candidates":Ljava/util/List;, "Ljava/util/List<Lcom/localytics/android/MarketingMessage;>;"
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 1283
    .local v1, "eligibleCampaignIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/localytics/android/MarketingMessage;

    .line 1285
    .local v3, "message":Lcom/localytics/android/MarketingMessage;
    const-string v5, "campaign_id"

    invoke-virtual {v3, v5}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-interface {v1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1288
    .end local v3    # "message":Lcom/localytics/android/MarketingMessage;
    :cond_0
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v5

    if-lez v5, :cond_1

    .line 1290
    sget-object v5, Lcom/localytics/android/InAppManager$FrequencyCappingFilter;->MAX_COUNT:Lcom/localytics/android/InAppManager$FrequencyCappingFilter;

    invoke-virtual {p0, v5, v1}, Lcom/localytics/android/InAppManager;->_getDisqualifiedCampaigns(Lcom/localytics/android/InAppManager$FrequencyCappingFilter;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 1292
    :cond_1
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v5

    if-lez v5, :cond_2

    .line 1294
    sget-object v5, Lcom/localytics/android/InAppManager$FrequencyCappingFilter;->FREQUENCY:Lcom/localytics/android/InAppManager$FrequencyCappingFilter;

    invoke-virtual {p0, v5, v1}, Lcom/localytics/android/InAppManager;->_getDisqualifiedCampaigns(Lcom/localytics/android/InAppManager$FrequencyCappingFilter;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 1296
    :cond_2
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v5

    if-lez v5, :cond_3

    .line 1298
    sget-object v5, Lcom/localytics/android/InAppManager$FrequencyCappingFilter;->BLACKOUT:Lcom/localytics/android/InAppManager$FrequencyCappingFilter;

    invoke-virtual {p0, v5, v1}, Lcom/localytics/android/InAppManager;->_getDisqualifiedCampaigns(Lcom/localytics/android/InAppManager$FrequencyCappingFilter;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 1300
    :cond_3
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v5

    if-lez v5, :cond_4

    .line 1302
    sget-object v5, Lcom/localytics/android/InAppManager$FrequencyCappingFilter;->FREQUENCY:Lcom/localytics/android/InAppManager$FrequencyCappingFilter;

    invoke-virtual {p0, v5, v1}, Lcom/localytics/android/InAppManager;->_getGloballyDisqualifiedCampaigns(Lcom/localytics/android/InAppManager$FrequencyCappingFilter;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 1304
    :cond_4
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v5

    if-lez v5, :cond_5

    .line 1306
    sget-object v5, Lcom/localytics/android/InAppManager$FrequencyCappingFilter;->BLACKOUT:Lcom/localytics/android/InAppManager$FrequencyCappingFilter;

    invoke-virtual {p0, v5, v1}, Lcom/localytics/android/InAppManager;->_getGloballyDisqualifiedCampaigns(Lcom/localytics/android/InAppManager$FrequencyCappingFilter;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 1309
    :cond_5
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 1311
    .local v4, "stillEligibleCandidates":Ljava/util/List;, "Ljava/util/List<Lcom/localytics/android/MarketingMessage;>;"
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v5

    if-lez v5, :cond_7

    .line 1313
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/localytics/android/MarketingMessage;

    .line 1315
    .restart local v3    # "message":Lcom/localytics/android/MarketingMessage;
    const-string v5, "campaign_id"

    invoke-virtual {v3, v5}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1316
    .local v0, "campaignId":I
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1318
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1323
    .end local v0    # "campaignId":I
    .end local v3    # "message":Lcom/localytics/android/MarketingMessage;
    :cond_7
    return-object v4
.end method

.method _getDisqualifiedCampaigns(Lcom/localytics/android/InAppManager$FrequencyCappingFilter;Ljava/util/Set;)Ljava/util/Set;
    .locals 13
    .param p1, "filter"    # Lcom/localytics/android/InAppManager$FrequencyCappingFilter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/Set;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/localytics/android/InAppManager$FrequencyCappingFilter;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .local p2, "campaignIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    const/4 v12, 0x3

    const/4 v11, 0x7

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1329
    sget-object v4, Lcom/localytics/android/InAppManager$13;->$SwitchMap$com$localytics$android$InAppManager$FrequencyCappingFilter:[I

    invoke-virtual {p1}, Lcom/localytics/android/InAppManager$FrequencyCappingFilter;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 1417
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    :goto_0
    return-object v1

    .line 1332
    :pswitch_0
    const-string v4, "SELECT fc.%s FROM %s AS fc JOIN %s AS cd ON fc.%s=cd.%s WHERE cd.%s in (%s) GROUP BY fc.%s HAVING count(*) >= fc.%s;"

    const/16 v5, 0x9

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "campaign_id"

    aput-object v6, v5, v8

    const-string v6, "frequency_capping_rules"

    aput-object v6, v5, v9

    const-string v6, "campaigns_displayed"

    aput-object v6, v5, v10

    const-string v6, "campaign_id"

    aput-object v6, v5, v12

    const/4 v6, 0x4

    const-string v7, "campaign_id"

    aput-object v7, v5, v6

    const/4 v6, 0x5

    const-string v7, "campaign_id"

    aput-object v7, v5, v6

    const/4 v6, 0x6

    const-string v7, ","

    invoke-static {v7, p2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const-string v6, "campaign_id"

    aput-object v6, v5, v11

    const/16 v6, 0x8

    const-string v7, "max_display_count"

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/localytics/android/InAppManager;->_getCampaignIdsFromFrequencyCappingQuery(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v1

    goto :goto_0

    .line 1349
    :pswitch_1
    const-string v4, "SELECT DISTINCT fc.%s FROM %s AS fc JOIN %s AS df ON fc.%s=df.%s JOIN %s AS cd ON fc.%s=cd.%s WHERE (cd.%s BETWEEN datetime(\'%s\',\'-\'||df.%s||\' days\') AND datetime(\'%s\')) AND fc.%s in (%s) GROUP BY df.%s HAVING count(cd.%s) >= df.%s;"

    const/16 v5, 0x11

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "campaign_id"

    aput-object v6, v5, v8

    const-string v6, "frequency_capping_rules"

    aput-object v6, v5, v9

    const-string v6, "frequency_capping_display_frequencies"

    aput-object v6, v5, v10

    const-string v6, "_id"

    aput-object v6, v5, v12

    const/4 v6, 0x4

    const-string v7, "frequency_id"

    aput-object v7, v5, v6

    const/4 v6, 0x5

    const-string v7, "campaigns_displayed"

    aput-object v7, v5, v6

    const/4 v6, 0x6

    const-string v7, "campaign_id"

    aput-object v7, v5, v6

    const-string v6, "campaign_id"

    aput-object v6, v5, v11

    const/16 v6, 0x8

    const-string v7, "date"

    aput-object v7, v5, v6

    const/16 v6, 0x9

    iget-object v7, p0, Lcom/localytics/android/InAppManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v7}, Lcom/localytics/android/LocalyticsDao;->getTimeStringForSQLite()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0xa

    const-string v7, "days"

    aput-object v7, v5, v6

    const/16 v6, 0xb

    iget-object v7, p0, Lcom/localytics/android/InAppManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v7}, Lcom/localytics/android/LocalyticsDao;->getTimeStringForSQLite()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0xc

    const-string v7, "campaign_id"

    aput-object v7, v5, v6

    const/16 v6, 0xd

    const-string v7, ","

    invoke-static {v7, p2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0xe

    const-string v7, "_id"

    aput-object v7, v5, v6

    const/16 v6, 0xf

    const-string v7, "date"

    aput-object v7, v5, v6

    const/16 v6, 0x10

    const-string v7, "count"

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/localytics/android/InAppManager;->_getCampaignIdsFromFrequencyCappingQuery(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v1

    goto/16 :goto_0

    .line 1372
    :pswitch_2
    iget-object v4, p0, Lcom/localytics/android/InAppManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v4}, Lcom/localytics/android/LocalyticsDao;->getCalendar()Ljava/util/Calendar;

    move-result-object v0

    .line 1373
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0, v11}, Ljava/util/Calendar;->get(I)I

    move-result v4

    add-int/lit8 v3, v4, -0x1

    .line 1374
    .local v3, "currentWeekday":I
    const/16 v4, 0xb

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    mul-int/lit8 v4, v4, 0x3c

    const/16 v5, 0xc

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    add-int/2addr v4, v5

    mul-int/lit8 v4, v4, 0x3c

    const/16 v5, 0xd

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    add-int v2, v4, v5

    .line 1376
    .local v2, "currentTime":I
    const-string v4, "SELECT fc.%s FROM %s AS fc, %s AS d, %s AS t WHERE ((fc.%s=t.%s) AND (fc.%s=d.%s)) AND (d.%s=t.%s) AND (datetime(\'%s\',\'localtime\') BETWEEN d.%s and d.%s) AND (? BETWEEN t.%s AND t.%s) AND fc.%s IN (%s);"

    const/16 v5, 0x11

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "campaign_id"

    aput-object v6, v5, v8

    const-string v6, "frequency_capping_rules"

    aput-object v6, v5, v9

    const-string v6, "frequency_capping_blackout_dates"

    aput-object v6, v5, v10

    const-string v6, "frequency_capping_blackout_times"

    aput-object v6, v5, v12

    const/4 v6, 0x4

    const-string v7, "_id"

    aput-object v7, v5, v6

    const/4 v6, 0x5

    const-string v7, "frequency_id"

    aput-object v7, v5, v6

    const/4 v6, 0x6

    const-string v7, "_id"

    aput-object v7, v5, v6

    const-string v6, "frequency_id"

    aput-object v6, v5, v11

    const/16 v6, 0x8

    const-string v7, "rule_group_id"

    aput-object v7, v5, v6

    const/16 v6, 0x9

    const-string v7, "rule_group_id"

    aput-object v7, v5, v6

    const/16 v6, 0xa

    iget-object v7, p0, Lcom/localytics/android/InAppManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v7}, Lcom/localytics/android/LocalyticsDao;->getTimeStringForSQLite()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0xb

    const-string v7, "start"

    aput-object v7, v5, v6

    const/16 v6, 0xc

    const-string v7, "end"

    aput-object v7, v5, v6

    const/16 v6, 0xd

    const-string v7, "start"

    aput-object v7, v5, v6

    const/16 v6, 0xe

    const-string v7, "end"

    aput-object v7, v5, v6

    const/16 v6, 0xf

    const-string v7, "campaign_id"

    aput-object v7, v5, v6

    const/16 v6, 0x10

    const-string v7, ","

    invoke-static {v7, p2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-array v5, v9, [Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-direct {p0, v4, v5}, Lcom/localytics/android/InAppManager;->_getCampaignIdsFromFrequencyCappingQuery(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/Set;

    move-result-object v1

    .line 1395
    .local v1, "campaigns":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    const-string v4, "SELECT fc.%s FROM %s AS fc, %s AS w, %s AS t WHERE ((fc.%s=t.%s) AND (fc.%s=w.%s)) AND (w.%s=t.%s) AND (w.%s=?)  AND (? BETWEEN t.%s AND t.%s) AND fc.%s IN (%s);"

    const/16 v5, 0xf

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "campaign_id"

    aput-object v6, v5, v8

    const-string v6, "frequency_capping_rules"

    aput-object v6, v5, v9

    const-string v6, "frequency_capping_blackout_weekdays"

    aput-object v6, v5, v10

    const-string v6, "frequency_capping_blackout_times"

    aput-object v6, v5, v12

    const/4 v6, 0x4

    const-string v7, "_id"

    aput-object v7, v5, v6

    const/4 v6, 0x5

    const-string v7, "frequency_id"

    aput-object v7, v5, v6

    const/4 v6, 0x6

    const-string v7, "_id"

    aput-object v7, v5, v6

    const-string v6, "frequency_id"

    aput-object v6, v5, v11

    const/16 v6, 0x8

    const-string v7, "rule_group_id"

    aput-object v7, v5, v6

    const/16 v6, 0x9

    const-string v7, "rule_group_id"

    aput-object v7, v5, v6

    const/16 v6, 0xa

    const-string v7, "day"

    aput-object v7, v5, v6

    const/16 v6, 0xb

    const-string v7, "start"

    aput-object v7, v5, v6

    const/16 v6, 0xc

    const-string v7, "end"

    aput-object v7, v5, v6

    const/16 v6, 0xd

    const-string v7, "campaign_id"

    aput-object v7, v5, v6

    const/16 v6, 0xe

    const-string v7, ","

    invoke-static {v7, p2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-array v5, v10, [Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-direct {p0, v4, v5}, Lcom/localytics/android/InAppManager;->_getCampaignIdsFromFrequencyCappingQuery(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/Set;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0

    .line 1329
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method _getGloballyDisqualifiedCampaigns(Lcom/localytics/android/InAppManager$FrequencyCappingFilter;Ljava/util/Set;)Ljava/util/Set;
    .locals 13
    .param p1, "filter"    # Lcom/localytics/android/InAppManager$FrequencyCappingFilter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/Set;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/localytics/android/InAppManager$FrequencyCappingFilter;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1423
    .local p2, "campaignIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    const/4 v11, -0x1

    .line 1424
    .local v11, "frequencyId":I
    const/4 v10, 0x0

    .line 1427
    .local v10, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "frequency_capping_rules"

    const/4 v2, 0x0

    const-string v3, "campaign_id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v12, -0x1

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/localytics/android/BaseProvider;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1428
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1430
    const-string v0, "_id"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v11

    .line 1435
    :cond_0
    if-eqz v10, :cond_1

    .line 1437
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 1441
    :cond_1
    const/4 v0, -0x1

    if-eq v11, v0, :cond_3

    .line 1445
    :try_start_1
    sget-object v0, Lcom/localytics/android/InAppManager$13;->$SwitchMap$com$localytics$android$InAppManager$FrequencyCappingFilter:[I

    invoke-virtual {p1}, Lcom/localytics/android/InAppManager$FrequencyCappingFilter;->ordinal()I

    move-result v1

    aget v0, v0, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    packed-switch v0, :pswitch_data_0

    .line 1523
    :cond_2
    if-eqz v10, :cond_3

    .line 1525
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 1530
    :cond_3
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    :cond_4
    :goto_0
    return-object v0

    .line 1435
    :catchall_0
    move-exception v0

    if-eqz v10, :cond_5

    .line 1437
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 1448
    :pswitch_0
    :try_start_2
    iget-object v0, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    iget-object v0, v0, Lcom/localytics/android/BaseProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "SELECT df.%s FROM %s AS fc JOIN %s AS df ON fc.%s = df.%s JOIN %s AS cd WHERE cd.%s = 0 AND fc.%s = ? AND (cd.%s BETWEEN datetime(\'%s\',\'-\'||df.%s||\' days\') AND datetime(\'%s\')) GROUP BY df.%s HAVING count(cd.%s) >= df.%s;"

    const/16 v2, 0xf

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "frequency_capping_rules"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "frequency_capping_display_frequencies"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "frequency_id"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "campaigns_displayed"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "ignore_global"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "campaign_id"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "date"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    iget-object v4, p0, Lcom/localytics/android/InAppManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v4}, Lcom/localytics/android/LocalyticsDao;->getTimeStringForSQLite()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "days"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    iget-object v4, p0, Lcom/localytics/android/InAppManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v4}, Lcom/localytics/android/LocalyticsDao;->getTimeStringForSQLite()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "date"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, "count"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1469
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 1471
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p2}, Lcom/localytics/android/InAppManager;->_getIgnoresGlobalCampaigns(ZLjava/util/Set;)Ljava/util/Set;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    .line 1523
    if-eqz v10, :cond_4

    .line 1525
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 1476
    :pswitch_1
    :try_start_3
    iget-object v0, p0, Lcom/localytics/android/InAppManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v0}, Lcom/localytics/android/LocalyticsDao;->getCalendar()Ljava/util/Calendar;

    move-result-object v6

    .line 1477
    .local v6, "calendar":Ljava/util/Calendar;
    const/4 v0, 0x7

    invoke-virtual {v6, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/lit8 v9, v0, -0x1

    .line 1478
    .local v9, "currentWeekday":I
    const/16 v0, 0xb

    invoke-virtual {v6, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x3c

    const/16 v1, 0xc

    invoke-virtual {v6, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x3c

    const/16 v1, 0xd

    invoke-virtual {v6, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    add-int v8, v0, v1

    .line 1480
    .local v8, "currentTime":I
    iget-object v0, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    iget-object v0, v0, Lcom/localytics/android/BaseProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "SELECT * FROM %s AS d, %s AS t WHERE ((d.%s=?) AND (t.%s=?)) AND (d.%s=t.%s) AND (datetime(\'%s\',\'localtime\') BETWEEN d.%s and d.%s) AND (? BETWEEN t.%s AND t.%s);"

    const/16 v2, 0xb

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "frequency_capping_blackout_dates"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "frequency_capping_blackout_times"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "frequency_id"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "frequency_id"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "rule_group_id"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "rule_group_id"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    iget-object v4, p0, Lcom/localytics/android/InAppManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v4}, Lcom/localytics/android/LocalyticsDao;->getTimeStringForSQLite()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "start"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "end"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "start"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "end"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1494
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v7

    .line 1495
    .local v7, "count":I
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 1497
    iget-object v0, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    iget-object v0, v0, Lcom/localytics/android/BaseProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "SELECT * FROM %s AS w, %s AS t WHERE ((w.%s=?) AND (t.%s=?)) AND (w.%s=t.%s) AND (w.%s=?) AND (? BETWEEN t.%s AND t.%s);"

    const/16 v2, 0x9

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "frequency_capping_blackout_weekdays"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "frequency_capping_blackout_times"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "frequency_id"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "frequency_id"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "rule_group_id"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "rule_group_id"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "day"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "start"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "end"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1511
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v0

    add-int/2addr v7, v0

    .line 1513
    if-lez v7, :cond_2

    .line 1515
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p2}, Lcom/localytics/android/InAppManager;->_getIgnoresGlobalCampaigns(ZLjava/util/Set;)Ljava/util/Set;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v0

    .line 1523
    if-eqz v10, :cond_4

    .line 1525
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 1523
    .end local v6    # "calendar":Ljava/util/Calendar;
    .end local v7    # "count":I
    .end local v8    # "currentTime":I
    .end local v9    # "currentWeekday":I
    :catchall_1
    move-exception v0

    if-eqz v10, :cond_6

    .line 1525
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    .line 1445
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method _getIgnoresGlobalCampaigns(ZLjava/util/Set;)Ljava/util/Set;
    .locals 6
    .param p1, "ignoreGlobal"    # Z
    .param p2    # Ljava/util/Set;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .local p2, "campaignIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1536
    const-string v2, "SELECT %s FROM %s WHERE %s = %s AND %s in (%s);"

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "campaign_id"

    aput-object v4, v3, v1

    const-string v4, "frequency_capping_rules"

    aput-object v4, v3, v0

    const/4 v4, 0x2

    const-string v5, "ignore_global"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    if-eqz p1, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x4

    const-string v1, "campaign_id"

    aput-object v1, v3, v0

    const/4 v0, 0x5

    const-string v1, ","

    invoke-static {v1, p2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/localytics/android/InAppManager;->_getCampaignIdsFromFrequencyCappingQuery(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method _getInAppCreativeUrls()Landroid/util/SparseArray;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 978
    new-instance v8, Landroid/util/SparseArray;

    invoke-direct {v8}, Landroid/util/SparseArray;-><init>()V

    .line 979
    .local v8, "creativeUrlMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    const/4 v9, 0x0

    .line 982
    .local v9, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "marketing_rules"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "campaign_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "phone_location"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "tablet_location"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "devices"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/localytics/android/BaseProvider;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 989
    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 991
    new-instance v10, Lcom/localytics/android/MarketingMessage;

    invoke-direct {v10}, Lcom/localytics/android/MarketingMessage;-><init>()V

    .line 992
    .local v10, "marketingMessage":Lcom/localytics/android/MarketingMessage;
    const-string v0, "campaign_id"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 993
    .local v6, "campaignId":I
    const-string v0, "phone_location"

    const-string v1, "phone_location"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 994
    const-string v0, "tablet_location"

    const-string v1, "tablet_location"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 995
    const-string v0, "devices"

    const-string v1, "devices"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 996
    invoke-static {v10}, Lcom/localytics/android/InAppManager;->getInAppRemoteFileURL(Lcom/localytics/android/MarketingMessage;)Ljava/lang/String;

    move-result-object v7

    .line 997
    .local v7, "creativeUrl":Ljava/lang/String;
    invoke-virtual {v8, v6, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1002
    .end local v6    # "campaignId":I
    .end local v7    # "creativeUrl":Ljava/lang/String;
    .end local v10    # "marketingMessage":Lcom/localytics/android/MarketingMessage;
    :catchall_0
    move-exception v0

    if-eqz v9, :cond_0

    .line 1004
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 1002
    :cond_1
    if-eqz v9, :cond_2

    .line 1004
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 1008
    :cond_2
    return-object v8
.end method

.method _getQualifiedInAppMessageForTrigger(Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p1, "event"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/localytics/android/MarketingMessage;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1121
    invoke-static {}, Lcom/localytics/android/Constants;->isTestModeEnabled()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1155
    :cond_0
    :goto_0
    return-object v4

    .line 1127
    :cond_1
    const-string v5, "open"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1129
    invoke-static {}, Lcom/localytics/android/Constants;->isTestModeEnabled()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1131
    const-string v1, "AMP Loaded"

    .line 1144
    .local v1, "eventName":Ljava/lang/String;
    :goto_1
    invoke-direct {p0, v1}, Lcom/localytics/android/InAppManager;->_getInAppMessageMaps(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 1145
    .local v3, "inAppMessages":Ljava/util/List;, "Ljava/util/List<Lcom/localytics/android/MarketingMessage;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_2

    .line 1147
    iget-object v4, p0, Lcom/localytics/android/InAppManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v4}, Lcom/localytics/android/LocalyticsDao;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 1148
    .local v0, "appContext":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1150
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 1151
    .local v2, "eventString":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/localytics/android/InAppManager;->_getInAppMessageMaps(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 1155
    .end local v0    # "appContext":Landroid/content/Context;
    .end local v2    # "eventString":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0, v3}, Lcom/localytics/android/InAppManager;->_filterInAppMessagesDisqualifiedByFrequencyCappingRules(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    goto :goto_0

    .line 1140
    .end local v1    # "eventName":Ljava/lang/String;
    .end local v3    # "inAppMessages":Ljava/util/List;, "Ljava/util/List<Lcom/localytics/android/MarketingMessage;>;"
    :cond_3
    move-object v1, p1

    .restart local v1    # "eventName":Ljava/lang/String;
    goto :goto_1
.end method

.method _handleTestCampaigns()Z
    .locals 3

    .prologue
    .line 203
    iget-object v1, p0, Lcom/localytics/android/InAppManager;->mTestCampaignsListAdapter:Lcom/localytics/android/InAppMessagesAdapter;

    if-eqz v1, :cond_0

    .line 205
    iget-object v0, p0, Lcom/localytics/android/InAppManager;->mTestCampaignsListAdapter:Lcom/localytics/android/InAppMessagesAdapter;

    .line 206
    .local v0, "adapter":Lcom/localytics/android/InAppMessagesAdapter;
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Lcom/localytics/android/InAppManager$2;

    invoke-direct {v2, p0, v0}, Lcom/localytics/android/InAppManager$2;-><init>(Lcom/localytics/android/InAppManager;Lcom/localytics/android/InAppMessagesAdapter;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 221
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/localytics/android/InAppManager;->mTestCampaignsListAdapter:Lcom/localytics/android/InAppMessagesAdapter;

    .line 223
    const/4 v1, 0x1

    .line 226
    .end local v0    # "adapter":Lcom/localytics/android/InAppMessagesAdapter;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method _inAppMessageTrigger(Ljava/lang/String;Ljava/util/Map;)V
    .locals 5
    .param p1, "event"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1022
    .local p2, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p0, p1}, Lcom/localytics/android/InAppManager;->_getQualifiedInAppMessageForTrigger(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 1023
    .local v0, "messages":Ljava/util/List;, "Ljava/util/List<Lcom/localytics/android/MarketingMessage;>;"
    if-eqz v0, :cond_0

    .line 1025
    invoke-virtual {p0, v0, p2}, Lcom/localytics/android/InAppManager;->_campaignsSatisfyingConditions(Ljava/util/List;Ljava/util/Map;)Ljava/util/List;

    move-result-object v0

    .line 1027
    invoke-virtual {p0, v0}, Lcom/localytics/android/InAppManager;->_messagesReadyToDisplay(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 1028
    .local v1, "readyToDisplay":Ljava/util/List;, "Ljava/util/List<Lcom/localytics/android/MarketingMessage;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 1030
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/localytics/android/MarketingMessage;

    invoke-virtual {p0, v3, p2}, Lcom/localytics/android/InAppManager;->_triggerMessage(Lcom/localytics/android/MarketingMessage;Ljava/util/Map;)V

    .line 1049
    .end local v1    # "readyToDisplay":Ljava/util/List;, "Ljava/util/List<Lcom/localytics/android/MarketingMessage;>;"
    :cond_0
    :goto_0
    return-void

    .line 1032
    .restart local v1    # "readyToDisplay":Ljava/util/List;, "Ljava/util/List<Lcom/localytics/android/MarketingMessage;>;"
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 1034
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2, v0}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 1035
    .local v2, "withoutCreatives":Ljava/util/List;, "Ljava/util/List<Lcom/localytics/android/MarketingMessage;>;"
    invoke-interface {v2, v1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 1036
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 1038
    iget-object v3, p0, Lcom/localytics/android/InAppManager;->mCreativeManager:Lcom/localytics/android/CreativeManager;

    new-instance v4, Lcom/localytics/android/InAppManager$3;

    invoke-direct {v4, p0, p1, p2}, Lcom/localytics/android/InAppManager$3;-><init>(Lcom/localytics/android/InAppManager;Ljava/lang/String;Ljava/util/Map;)V

    invoke-virtual {v3, v2, v4}, Lcom/localytics/android/CreativeManager;->priorityDownloadCreatives(Ljava/util/List;Lcom/localytics/android/CreativeManager$FirstDownloadedCallback;)V

    goto :goto_0
.end method

.method _manifestProcessingAllowed()Z
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 134
    iget-object v0, p0, Lcom/localytics/android/InAppManager;->mFragmentManager:Landroid/app/FragmentManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/localytics/android/InAppManager;->mFragmentManager:Landroid/app/FragmentManager;

    const-string v1, "marketing_dialog"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/localytics/android/InAppManager;->mCreativeManager:Lcom/localytics/android/CreativeManager;

    invoke-virtual {v0}, Lcom/localytics/android/CreativeManager;->hasPendingDownloads()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected _messagesReadyToDisplay(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/localytics/android/MarketingMessage;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/localytics/android/MarketingMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1053
    .local p1, "messages":Ljava/util/List;, "Ljava/util/List<Lcom/localytics/android/MarketingMessage;>;"
    invoke-direct {p0, p1}, Lcom/localytics/android/InAppManager;->_controlGroupMessages(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 1055
    .local v0, "controlGroupMessages":Ljava/util/List;, "Ljava/util/List<Lcom/localytics/android/MarketingMessage;>;"
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1, p1}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 1056
    .local v1, "nonControlGroupMessages":Ljava/util/List;, "Ljava/util/List<Lcom/localytics/android/MarketingMessage;>;"
    invoke-interface {v1, v0}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 1058
    iget-object v4, p0, Lcom/localytics/android/InAppManager;->mCreativeManager:Lcom/localytics/android/CreativeManager;

    invoke-virtual {v4, v1}, Lcom/localytics/android/CreativeManager;->inAppCampaignsWithDownloadedCreatives(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    .line 1060
    .local v3, "withCreatives":Ljava/util/List;, "Ljava/util/List<Lcom/localytics/android/MarketingMessage;>;"
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2, v3}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 1061
    .local v2, "readyToDisplay":Ljava/util/List;, "Ljava/util/List<Lcom/localytics/android/MarketingMessage;>;"
    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1063
    return-object v2
.end method

.method _processMarketingObject(Ljava/util/Map;Ljava/util/Map;)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 140
    .local p1, "marketingMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .local p2, "config":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 142
    .local v10, "inAppMessages":Ljava/util/List;, "Ljava/util/List<Lcom/localytics/android/MarketingMessage;>;"
    const-string v17, "amp"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    .line 143
    .local v11, "inapps":Ljava/lang/Object;
    if-eqz v11, :cond_0

    .line 146
    invoke-static {v11}, Lcom/localytics/android/JsonHelper;->toJSON(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/json/JSONArray;

    invoke-static/range {v17 .. v17}, Lcom/localytics/android/JsonHelper;->toList(Lorg/json/JSONArray;)Ljava/util/List;

    move-result-object v9

    .line 147
    .local v9, "inAppList":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/Map;

    .line 149
    .local v14, "obj":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    new-instance v17, Lcom/localytics/android/MarketingMessage;

    move-object/from16 v0, v17

    invoke-direct {v0, v14}, Lcom/localytics/android/MarketingMessage;-><init>(Ljava/util/Map;)V

    move-object/from16 v0, v17

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 154
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v9    # "inAppList":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    .end local v14    # "obj":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/localytics/android/InAppManager;->_removeDeactivatedInAppMessages(Ljava/util/List;)V

    .line 156
    const/16 v17, -0x1

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/localytics/android/InAppManager;->_validateAndStoreFrequencyCappingRule(Ljava/util/Map;Ljava/lang/Integer;)Z

    .line 158
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 160
    .local v6, "creativesToDownload":Ljava/util/List;, "Ljava/util/List<Lcom/localytics/android/MarketingMessage;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/localytics/android/InAppManager;->_getInAppCreativeUrls()Landroid/util/SparseArray;

    move-result-object v5

    .line 163
    .local v5, "creativeUrls":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .restart local v7    # "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/localytics/android/MarketingMessage;

    .line 165
    .local v8, "inApp":Lcom/localytics/android/MarketingMessage;
    const-string v17, "campaign_id"

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 166
    .local v15, "oldRemoteFileUrl":Ljava/lang/String;
    invoke-static {v8}, Lcom/localytics/android/InAppManager;->getInAppRemoteFileURL(Lcom/localytics/android/MarketingMessage;)Ljava/lang/String;

    move-result-object v13

    .line 167
    .local v13, "newRemoteFileUrl":Ljava/lang/String;
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_2

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_2

    const/4 v12, 0x1

    .line 169
    .local v12, "isNewUrl":Z
    :goto_2
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v8, v1, v12}, Lcom/localytics/android/InAppManager;->_saveInAppMessage(Lcom/localytics/android/MarketingMessage;Ljava/util/Map;Z)I

    move-result v16

    .line 170
    .local v16, "ruleId":I
    if-lez v16, :cond_1

    const-string v17, "campaign_id"

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v8, v1}, Lcom/localytics/android/InAppManager;->_validateAndStoreFrequencyCappingRule(Ljava/util/Map;Ljava/lang/Integer;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 172
    if-eqz v12, :cond_1

    .line 174
    const-string v17, "control_group"

    move-object/from16 v0, v17

    invoke-static {v8, v0}, Lcom/localytics/android/JsonHelper;->getSafeIntegerFromMap(Ljava/util/Map;Ljava/lang/String;)I

    move-result v3

    .line 175
    .local v3, "controlGroup":I
    if-nez v3, :cond_1

    .line 177
    new-instance v4, Lcom/localytics/android/MarketingMessage;

    invoke-direct {v4, v8}, Lcom/localytics/android/MarketingMessage;-><init>(Ljava/util/Map;)V

    .line 178
    .local v4, "copy":Lcom/localytics/android/MarketingMessage;
    const-string v17, "_id"

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v4, v0, v1}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/InAppManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v4, v0, v1}, Lcom/localytics/android/InAppManager;->updateMessageWithSpecialKeys(Lcom/localytics/android/MarketingMessage;Lcom/localytics/android/LocalyticsDao;Z)V

    .line 180
    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 167
    .end local v3    # "controlGroup":I
    .end local v4    # "copy":Lcom/localytics/android/MarketingMessage;
    .end local v12    # "isNewUrl":Z
    .end local v16    # "ruleId":I
    :cond_2
    const/4 v12, 0x0

    goto :goto_2

    .line 186
    .end local v8    # "inApp":Lcom/localytics/android/MarketingMessage;
    .end local v13    # "newRemoteFileUrl":Ljava/lang/String;
    .end local v15    # "oldRemoteFileUrl":Ljava/lang/String;
    :cond_3
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v17

    if-lez v17, :cond_4

    invoke-static {}, Lcom/localytics/android/Constants;->isTestModeEnabled()Z

    move-result v17

    if-nez v17, :cond_4

    .line 188
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/InAppManager;->mCreativeManager:Lcom/localytics/android/CreativeManager;

    move-object/from16 v17, v0

    new-instance v18, Lcom/localytics/android/InAppManager$1;

    invoke-direct/range {v18 .. v19}, Lcom/localytics/android/InAppManager$1;-><init>(Lcom/localytics/android/InAppManager;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v6, v1}, Lcom/localytics/android/CreativeManager;->downloadCreatives(Ljava/util/List;Lcom/localytics/android/CreativeManager$LastDownloadedCallback;)V

    .line 198
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/localytics/android/BaseProvider;->vacuumIfNecessary()V

    .line 199
    return-void
.end method

.method _removeDeactivatedInAppMessages(Ljava/util/List;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/localytics/android/MarketingMessage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 907
    .local p1, "inAppMessages":Ljava/util/List;, "Ljava/util/List<Lcom/localytics/android/MarketingMessage;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 910
    .local v7, "campaignIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/localytics/android/MarketingMessage;

    .line 912
    .local v10, "inApp":Lcom/localytics/android/MarketingMessage;
    const-string v0, "campaign_id"

    invoke-static {v10, v0}, Lcom/localytics/android/JsonHelper;->getSafeIntegerFromMap(Ljava/util/Map;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 915
    .end local v10    # "inApp":Lcom/localytics/android/MarketingMessage;
    :cond_0
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/localytics/android/InAppManager;->_deleteFrequencyCappingRule(Ljava/lang/Integer;)Z

    .line 918
    new-instance v12, Ljava/util/Vector;

    invoke-direct {v12}, Ljava/util/Vector;-><init>()V

    .line 919
    .local v12, "inAppMessageMaps":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/localytics/android/MarketingMessage;>;"
    const/4 v13, 0x0

    .line 922
    .local v13, "rulesCursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "marketing_rules"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/localytics/android/BaseProvider;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 924
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ge v8, v0, :cond_3

    .line 926
    invoke-interface {v13, v8}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 930
    const-string v0, "campaign_id"

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 931
    .local v6, "campaignId":I
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 924
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 936
    :cond_1
    new-instance v11, Lcom/localytics/android/MarketingMessage;

    invoke-direct {v11}, Lcom/localytics/android/MarketingMessage;-><init>()V

    .line 938
    .local v11, "inAppMessageMap":Lcom/localytics/android/MarketingMessage;
    const-string v0, "_id"

    const-string v1, "_id"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 939
    const-string v0, "campaign_id"

    const-string v1, "campaign_id"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 940
    const-string v0, "expiration"

    const-string v1, "expiration"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 941
    const-string v0, "display_seconds"

    const-string v1, "display_seconds"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 942
    const-string v0, "display_session"

    const-string v1, "display_session"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 943
    const-string v0, "version"

    const-string v1, "version"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 944
    const-string v0, "phone_location"

    const-string v1, "phone_location"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 945
    const-string v0, "phone_size_width"

    const-string v1, "phone_size_width"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 946
    const-string v0, "phone_size_height"

    const-string v1, "phone_size_height"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 947
    const-string v0, "tablet_location"

    const-string v1, "tablet_location"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 948
    const-string v0, "tablet_size_width"

    const-string v1, "tablet_size_width"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 949
    const-string v0, "tablet_size_height"

    const-string v1, "tablet_size_height"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 950
    const-string v0, "time_to_display"

    const-string v1, "time_to_display"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 951
    const-string v0, "internet_required"

    const-string v1, "internet_required"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 952
    const-string v0, "ab_test"

    const-string v1, "ab_test"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 953
    const-string v0, "rule_name_non_unique"

    const-string v1, "rule_name_non_unique"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 954
    const-string v0, "location"

    const-string v1, "location"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 955
    const-string v0, "devices"

    const-string v1, "devices"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Lcom/localytics/android/MarketingMessage;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 957
    invoke-virtual {v12, v11}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_2

    .line 962
    .end local v6    # "campaignId":I
    .end local v8    # "i":I
    .end local v11    # "inAppMessageMap":Lcom/localytics/android/MarketingMessage;
    :catchall_0
    move-exception v0

    if-eqz v13, :cond_2

    .line 964
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 962
    .restart local v8    # "i":I
    :cond_3
    if-eqz v13, :cond_4

    .line 964
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 969
    :cond_4
    invoke-virtual {v12}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/localytics/android/MarketingMessage;

    .line 971
    .restart local v10    # "inApp":Lcom/localytics/android/MarketingMessage;
    invoke-direct {p0, v10}, Lcom/localytics/android/InAppManager;->_destroyLocalInAppMessage(Lcom/localytics/android/MarketingMessage;)V

    .line 972
    const-string v0, "campaign_id"

    invoke-virtual {v10, v0}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {p0, v0}, Lcom/localytics/android/InAppManager;->_deleteFrequencyCappingRule(Ljava/lang/Integer;)Z

    goto :goto_3

    .line 974
    .end local v10    # "inApp":Lcom/localytics/android/MarketingMessage;
    :cond_5
    return-void
.end method

.method _saveFrequencyCappingRule(Ljava/util/Map;Ljava/lang/Integer;)Z
    .locals 8
    .param p1    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "campaignId"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Integer;",
            ")Z"
        }
    .end annotation

    .prologue
    .local p1, "rule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 686
    iget-object v6, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    iget-object v6, v6, Lcom/localytics/android/BaseProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 688
    invoke-virtual {p0, p1, p2}, Lcom/localytics/android/InAppManager;->_saveFrequencyCappingRuleBase(Ljava/util/Map;Ljava/lang/Integer;)J

    move-result-wide v6

    long-to-int v1, v6

    .line 690
    .local v1, "frequencyId":I
    const-string v6, "display_frequencies"

    invoke-interface {p1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 691
    .local v2, "frequencyRules":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    if-lez v1, :cond_1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p0, v2, v6}, Lcom/localytics/android/InAppManager;->_saveFrequencyCappingRuleDisplayFrequency(Ljava/util/List;Ljava/lang/Integer;)Z

    move-result v6

    if-eqz v6, :cond_1

    move v3, v4

    .line 693
    .local v3, "success":Z
    :goto_0
    const-string v6, "blackout_rules"

    invoke-interface {p1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 694
    .local v0, "blackoutRules":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    if-eqz v3, :cond_2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p0, v0, v6}, Lcom/localytics/android/InAppManager;->_saveFrequencyCappingRuleBlackout(Ljava/util/List;Ljava/lang/Integer;)Z

    move-result v6

    if-eqz v6, :cond_2

    move v3, v4

    .line 696
    :goto_1
    if-eqz v3, :cond_0

    .line 698
    iget-object v4, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    iget-object v4, v4, Lcom/localytics/android/BaseProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 700
    :cond_0
    iget-object v4, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    iget-object v4, v4, Lcom/localytics/android/BaseProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 702
    return v3

    .end local v0    # "blackoutRules":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    .end local v3    # "success":Z
    :cond_1
    move v3, v5

    .line 691
    goto :goto_0

    .restart local v0    # "blackoutRules":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    .restart local v3    # "success":Z
    :cond_2
    move v3, v5

    .line 694
    goto :goto_1
.end method

.method _saveFrequencyCappingRuleBase(Ljava/util/Map;Ljava/lang/Integer;)J
    .locals 4
    .param p1    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "campaignId"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;",
            "Ljava/lang/Integer;",
            ")J"
        }
    .end annotation

    .prologue
    .line 720
    .local p1, "rule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 721
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "campaign_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 722
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 724
    const-string v2, "max_display_count"

    const-string v1, "max_display_count"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 725
    const-string v2, "ignore_global"

    const-string v1, "ignore_global"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 733
    :goto_0
    iget-object v1, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    iget-object v1, v1, Lcom/localytics/android/BaseProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "frequency_capping_rules"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    return-wide v2

    .line 729
    :cond_0
    const-string v1, "max_display_count"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 730
    const-string v1, "ignore_global"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_0
.end method

.method _saveFrequencyCappingRuleBlackout(Ljava/util/List;Ljava/lang/Integer;)Z
    .locals 9
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "frequencyId"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;",
            "Ljava/lang/Integer;",
            ")Z"
        }
    .end annotation

    .prologue
    .local p1, "rules":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    const/4 v7, 0x0

    .line 758
    if-eqz p1, :cond_2

    .line 760
    const/4 v5, 0x0

    .line 761
    .local v5, "groupId":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 763
    .local v2, "blackoutRule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-virtual {p0, v2}, Lcom/localytics/android/InAppManager;->_augmentBlackoutRule(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 764
    .local v0, "augmentedBlackoutRule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v8, "dates"

    invoke-interface {v0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 765
    .local v1, "blackoutDatesRules":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {p0, v1, p2, v8}, Lcom/localytics/android/InAppManager;->_saveFrequencyCappingRuleBlackoutDates(Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Integer;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 786
    .end local v0    # "augmentedBlackoutRule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v1    # "blackoutDatesRules":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v2    # "blackoutRule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v5    # "groupId":I
    .end local v6    # "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    return v7

    .line 770
    .restart local v0    # "augmentedBlackoutRule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .restart local v1    # "blackoutDatesRules":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    .restart local v2    # "blackoutRule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .restart local v5    # "groupId":I
    .restart local v6    # "i$":Ljava/util/Iterator;
    :cond_1
    const-string v8, "weekdays"

    invoke-interface {v0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 771
    .local v4, "blackoutWeekdaysRules":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {p0, v4, p2, v8}, Lcom/localytics/android/InAppManager;->_saveFrequencyCappingRuleBlackoutWeekdays(Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Integer;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 776
    const-string v8, "times"

    invoke-interface {v0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 777
    .local v3, "blackoutTimesRules":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {p0, v3, p2, v8}, Lcom/localytics/android/InAppManager;->_saveFrequencyCappingRuleBlackoutTimes(Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Integer;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 782
    add-int/lit8 v5, v5, 0x1

    .line 783
    goto :goto_0

    .line 786
    .end local v0    # "augmentedBlackoutRule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v1    # "blackoutDatesRules":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v2    # "blackoutRule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v3    # "blackoutTimesRules":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v4    # "blackoutWeekdaysRules":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v5    # "groupId":I
    .end local v6    # "i$":Ljava/util/Iterator;
    :cond_2
    const/4 v7, 0x1

    goto :goto_1
.end method

.method _saveFrequencyCappingRuleBlackoutDates(Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Integer;)Z
    .locals 10
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "frequencyId"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "groupId"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ")Z"
        }
    .end annotation

    .prologue
    .local p1, "rules":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 811
    if-eqz p1, :cond_0

    .line 813
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 817
    .local v0, "blackoutRule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :try_start_0
    iget-object v5, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    iget-object v5, v5, Lcom/localytics/android/BaseProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v6, "INSERT INTO %s VALUES (?, ?, datetime(?,\'start of day\'), datetime(?,\'start of day\',\'1 day\',\'-1 second\'));"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, "frequency_capping_blackout_dates"

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p2, v7, v8

    const/4 v8, 0x1

    aput-object p3, v7, v8

    const/4 v8, 0x2

    const-string v9, "start"

    invoke-interface {v0, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x3

    const-string v9, "end"

    invoke-interface {v0, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 825
    :catch_0
    move-exception v1

    .line 832
    .end local v0    # "blackoutRule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    :goto_1
    return v3

    :cond_0
    move v3, v4

    goto :goto_1
.end method

.method _saveFrequencyCappingRuleBlackoutTimes(Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Integer;)Z
    .locals 10
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "frequencyId"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "groupId"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 857
    .local p1, "rules":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    if-eqz p1, :cond_3

    .line 859
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 861
    .local v0, "blackoutRule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v5, "start"

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/localytics/android/InAppManager;->_timeStringToSeconds(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 862
    .local v3, "start":Ljava/lang/Integer;
    const-string v5, "end"

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/localytics/android/InAppManager;->_timeStringToSeconds(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 864
    .local v1, "end":Ljava/lang/Integer;
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v6, -0x1

    if-ne v5, v6, :cond_2

    .line 866
    :cond_1
    const/4 v5, 0x0

    .line 881
    .end local v0    # "blackoutRule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1    # "end":Ljava/lang/Integer;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "start":Ljava/lang/Integer;
    :goto_0
    return v5

    .line 869
    .restart local v0    # "blackoutRule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v1    # "end":Ljava/lang/Integer;
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v3    # "start":Ljava/lang/Integer;
    :cond_2
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 870
    .local v4, "values":Landroid/content/ContentValues;
    const-string v5, "frequency_id"

    invoke-virtual {v4, v5, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 871
    const-string v5, "rule_group_id"

    invoke-virtual {v4, v5, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 872
    const-string v5, "start"

    invoke-virtual {v4, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 873
    const-string v5, "end"

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    add-int/lit8 v6, v6, 0x3b

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 874
    iget-object v5, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v6, "frequency_capping_blackout_times"

    invoke-virtual {v5, v6, v4}, Lcom/localytics/android/BaseProvider;->insert(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-gtz v5, :cond_0

    .line 876
    const/4 v5, 0x0

    goto :goto_0

    .line 881
    .end local v0    # "blackoutRule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1    # "end":Ljava/lang/Integer;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "start":Ljava/lang/Integer;
    .end local v4    # "values":Landroid/content/ContentValues;
    :cond_3
    const/4 v5, 0x1

    goto :goto_0
.end method

.method _saveFrequencyCappingRuleBlackoutWeekdays(Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Integer;)Z
    .locals 8
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "frequencyId"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "groupId"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 837
    .local p1, "rules":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    if-eqz p1, :cond_1

    .line 839
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 841
    .local v2, "weekday":Ljava/lang/Integer;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 842
    .local v1, "values":Landroid/content/ContentValues;
    const-string v3, "frequency_id"

    invoke-virtual {v1, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 843
    const-string v3, "rule_group_id"

    invoke-virtual {v1, v3, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 844
    const-string v3, "day"

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 845
    iget-object v3, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v4, "frequency_capping_blackout_weekdays"

    invoke-virtual {v3, v4, v1}, Lcom/localytics/android/BaseProvider;->insert(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-gtz v3, :cond_0

    .line 847
    const/4 v3, 0x0

    .line 852
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "values":Landroid/content/ContentValues;
    .end local v2    # "weekday":Ljava/lang/Integer;
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method _saveFrequencyCappingRuleDisplayFrequency(Ljava/util/List;Ljava/lang/Integer;)Z
    .locals 8
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "frequencyId"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>;",
            "Ljava/lang/Integer;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 738
    .local p1, "rules":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    if-eqz p1, :cond_1

    .line 740
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 742
    .local v0, "displayFrequencyRule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 743
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "frequency_id"

    invoke-virtual {v2, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 744
    const-string v4, "count"

    const-string v3, "count"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 745
    const-string v4, "days"

    const-string v3, "days"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 746
    iget-object v3, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v4, "frequency_capping_display_frequencies"

    invoke-virtual {v3, v4, v2}, Lcom/localytics/android/BaseProvider;->insert(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-gtz v3, :cond_0

    .line 748
    const/4 v3, 0x0

    .line 753
    .end local v0    # "displayFrequencyRule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "values":Landroid/content/ContentValues;
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method _saveInAppMessage(Lcom/localytics/android/MarketingMessage;Ljava/util/Map;Z)I
    .locals 18
    .param p1, "inAppMessage"    # Lcom/localytics/android/MarketingMessage;
    .param p3, "shouldDeleteCreative"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/localytics/android/MarketingMessage;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;Z)I"
        }
    .end annotation

    .prologue
    .line 239
    .local p2, "config":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-virtual/range {p0 .. p1}, Lcom/localytics/android/InAppManager;->_validateInAppMessage(Lcom/localytics/android/MarketingMessage;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 241
    const/4 v15, 0x0

    .line 318
    :cond_0
    :goto_0
    return v15

    .line 245
    :cond_1
    const-string v4, "campaign_id"

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/localytics/android/JsonHelper;->getSafeIntegerFromMap(Ljava/util/Map;Ljava/lang/String;)I

    move-result v10

    .line 248
    .local v10, "campaignId":I
    const/4 v15, 0x0

    .local v15, "ruleId":I
    const/4 v13, 0x0

    .line 251
    .local v13, "localVersion":I
    const/4 v11, 0x0

    .line 254
    .local v11, "cursorRule":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v5, "marketing_rules"

    sget-object v6, Lcom/localytics/android/InAppManager;->PROJECTION_IN_APP_RULE_RECORD:[Ljava/lang/String;

    const-string v7, "%s = ?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v17, "campaign_id"

    aput-object v17, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v8, v9

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Lcom/localytics/android/BaseProvider;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 255
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 257
    const-string v4, "_id"

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 258
    const-string v4, "version"

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v13

    .line 263
    :cond_2
    if-eqz v11, :cond_3

    .line 265
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 270
    :cond_3
    if-lez v15, :cond_7

    .line 272
    const-string v4, "Existing in-app rule already exists for this campaign\n\t campaignID = %d\n\t ruleID = %d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    .line 274
    const-string v4, "version"

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/localytics/android/JsonHelper;->getSafeIntegerFromMap(Ljava/util/Map;Ljava/lang/String;)I

    move-result v14

    .line 275
    .local v14, "remoteVersion":I
    if-lt v13, v14, :cond_5

    .line 277
    const-string v4, "No update needed. Campaign version has not been updated\n\t version: %d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    .line 278
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 263
    .end local v14    # "remoteVersion":I
    :catchall_0
    move-exception v4

    if-eqz v11, :cond_4

    .line 265
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v4

    .line 282
    .restart local v14    # "remoteVersion":I
    :cond_5
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/localytics/android/InAppManager;->_deleteInAppRuleEventsAndConditions(I)V

    .line 284
    if-eqz p3, :cond_6

    .line 286
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/localytics/android/InAppManager;->_deleteInAppCreative(I)V

    .line 295
    .end local v14    # "remoteVersion":I
    :cond_6
    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v15, v1, v2}, Lcom/localytics/android/InAppManager;->_parseInAppMessage(ILcom/localytics/android/MarketingMessage;Ljava/util/Map;)Landroid/content/ContentValues;

    move-result-object v16

    .line 296
    .local v16, "values":Landroid/content/ContentValues;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v5, "marketing_rules"

    move-object/from16 v0, v16

    invoke-virtual {v4, v5, v0}, Lcom/localytics/android/BaseProvider;->replace(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    long-to-int v15, v4

    .line 298
    if-lez v15, :cond_0

    .line 301
    int-to-long v4, v15

    const-string v6, "conditions"

    move-object/from16 v0, p1

    invoke-static {v0, v6}, Lcom/localytics/android/JsonHelper;->getSafeListFromMap(Ljava/util/Map;Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6}, Lcom/localytics/android/InAppManager;->_saveInAppConditions(JLjava/util/List;)V

    .line 304
    const/4 v12, 0x0

    .line 307
    .local v12, "eventNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_1
    const-string v4, "display_events"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/localytics/android/MarketingMessage;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Lcom/localytics/android/JsonHelper;->toJSON(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/json/JSONArray;

    invoke-static {v4}, Lcom/localytics/android/JsonHelper;->toList(Lorg/json/JSONArray;)Ljava/util/List;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v12

    .line 312
    :goto_2
    if-eqz v12, :cond_0

    .line 314
    int-to-long v4, v15

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v12}, Lcom/localytics/android/InAppManager;->_bindRuleToEvents(JLjava/util/List;)V

    goto/16 :goto_0

    .line 292
    .end local v12    # "eventNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v16    # "values":Landroid/content/ContentValues;
    :cond_7
    const-string v4, "In-app campaign not found. Creating a new one."

    invoke-static {v4}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    goto :goto_1

    .line 309
    .restart local v12    # "eventNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v16    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v4

    goto :goto_2
.end method

.method _setInAppMessageAsDisplayed(I)Z
    .locals 11
    .param p1, "campaignId"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1699
    const/4 v6, 0x0

    .line 1702
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "frequency_capping_rules"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "ignore_global"

    aput-object v4, v2, v3

    const-string v3, "%s = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v10, "campaign_id"

    aput-object v10, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/localytics/android/BaseProvider;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1703
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1705
    const-string v0, "ignore_global"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 1707
    .local v7, "ignoresGlobal":I
    iget-object v0, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    iget-object v0, v0, Lcom/localytics/android/BaseProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "INSERT INTO %s VALUES (?, datetime(\'%s\'), ?);"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "campaigns_displayed"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/localytics/android/InAppManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v4}, Lcom/localytics/android/LocalyticsDao;->getTimeStringForSQLite()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Integer;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1719
    if-eqz v6, :cond_0

    .line 1721
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    move v0, v8

    .line 1725
    .end local v7    # "ignoresGlobal":I
    :goto_0
    return v0

    .line 1719
    :cond_1
    if-eqz v6, :cond_2

    .line 1721
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move v0, v9

    .line 1725
    goto :goto_0

    .line 1719
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 1721
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method _setInAppMessageAsNotDisplayed(I)Z
    .locals 14
    .param p1, "campaignId"    # I

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 1730
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    .line 1731
    .local v6, "campaignIdString":Ljava/lang/String;
    const/4 v7, 0x0

    .line 1734
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "campaigns_displayed"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "date"

    aput-object v4, v2, v3

    const-string v3, "%s = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v11, "campaign_id"

    aput-object v11, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v6, v4, v5

    const-string v5, "%s DESC"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const-string v13, "date"

    aput-object v13, v11, v12

    invoke-static {v5, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/localytics/android/BaseProvider;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1739
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1741
    const-string v0, "date"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1742
    .local v8, "date":Ljava/lang/String;
    iget-object v0, p0, Lcom/localytics/android/InAppManager;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "campaigns_displayed"

    const-string v2, "%s = ? AND %s = ?"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "campaign_id"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "date"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v6, v3, v4

    const/4 v4, 0x1

    aput-object v8, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/localytics/android/BaseProvider;->remove(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_1

    move v0, v9

    .line 1749
    :goto_0
    if-eqz v7, :cond_0

    .line 1751
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1755
    .end local v8    # "date":Ljava/lang/String;
    :cond_0
    :goto_1
    return v0

    .restart local v8    # "date":Ljava/lang/String;
    :cond_1
    move v0, v10

    .line 1742
    goto :goto_0

    .line 1749
    .end local v8    # "date":Ljava/lang/String;
    :cond_2
    if-eqz v7, :cond_3

    .line 1751
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_3
    move v0, v10

    .line 1755
    goto :goto_1

    .line 1749
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_4

    .line 1751
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method protected _triggerMessage(Lcom/localytics/android/MarketingMessage;Ljava/util/Map;)V
    .locals 3
    .param p1, "message"    # Lcom/localytics/android/MarketingMessage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/localytics/android/MarketingMessage;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1083
    .local p2, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "campaign_id"

    invoke-static {p1, v2}, Lcom/localytics/android/JsonHelper;->getSafeIntegerFromMap(Ljava/util/Map;Ljava/lang/String;)I

    move-result v0

    .line 1084
    .local v0, "campaignId":I
    const-string v2, "control_group"

    invoke-static {p1, v2}, Lcom/localytics/android/JsonHelper;->getSafeIntegerFromMap(Ljava/util/Map;Ljava/lang/String;)I

    move-result v1

    .line 1085
    .local v1, "controlGroup":I
    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 1087
    invoke-virtual {p0, v0}, Lcom/localytics/android/InAppManager;->_setInAppMessageAsDisplayed(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1089
    invoke-direct {p0, p1}, Lcom/localytics/android/InAppManager;->_tagAmpActionEventForControlGroup(Lcom/localytics/android/MarketingMessage;)V

    .line 1094
    :cond_0
    :goto_0
    return-void

    .line 1093
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/localytics/android/InAppManager;->_tryDisplayingInAppCampaign(Lcom/localytics/android/MarketingMessage;Ljava/util/Map;)V

    goto :goto_0
.end method

.method _tryDisplayingInAppCampaign(Lcom/localytics/android/MarketingMessage;Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .param p1, "inAppMessage"    # Lcom/localytics/android/MarketingMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "event"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/localytics/android/MarketingMessage;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1591
    .local p3, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/localytics/android/InAppManager$4;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/localytics/android/InAppManager$4;-><init>(Lcom/localytics/android/InAppManager;Lcom/localytics/android/MarketingMessage;Ljava/lang/String;Ljava/util/Map;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1695
    return-void
.end method

.method _tryDisplayingInAppCampaign(Lcom/localytics/android/MarketingMessage;Ljava/util/Map;)V
    .locals 1
    .param p1, "inAppMessage"    # Lcom/localytics/android/MarketingMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/localytics/android/MarketingMessage;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1583
    .local p2, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/localytics/android/InAppManager;->_tryDisplayingInAppCampaign(Lcom/localytics/android/MarketingMessage;Ljava/lang/String;Ljava/util/Map;)V

    .line 1584
    return-void
.end method

.method _validateAndStoreFrequencyCappingRule(Ljava/util/Map;Ljava/lang/Integer;)Z
    .locals 4
    .param p1    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "campaignId"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Integer;",
            ")Z"
        }
    .end annotation

    .prologue
    .local p1, "objectWithRule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v3, -0x1

    .line 671
    const/4 v1, 0x0

    .line 672
    .local v1, "success":Z
    const-string v2, "frequency_capping"

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 673
    .local v0, "rule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v0, :cond_2

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v3, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {p0, v0, v2}, Lcom/localytics/android/InAppManager;->_validateFrequencyCappingRule(Ljava/util/Map;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 675
    invoke-virtual {p0, v0, p2}, Lcom/localytics/android/InAppManager;->_saveFrequencyCappingRule(Ljava/util/Map;Ljava/lang/Integer;)Z

    move-result v1

    .line 681
    :cond_0
    :goto_1
    return v1

    .line 673
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 677
    :cond_2
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, v3, :cond_0

    .line 679
    new-instance v2, Ljava/util/HashMap;

    sget-object v3, Lcom/localytics/android/InAppManager;->DEFAULT_FREQUENCY_CAPPING_RULE:Ljava/util/Map;

    invoke-direct {v2, v3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    invoke-virtual {p0, v2, p2}, Lcom/localytics/android/InAppManager;->_saveFrequencyCappingRule(Ljava/util/Map;Ljava/lang/Integer;)Z

    move-result v1

    goto :goto_1
.end method

.method _validateFrequencyCappingRule(Ljava/util/Map;Z)Z
    .locals 13
    .param p1    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "isGlobal"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    .line 501
    .local p1, "rule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz p2, :cond_0

    sget-object v0, Lcom/localytics/android/InAppManager;->GLOBAL_FREQUENCY_CAPPING_RULE_REQUIRED_KEYS:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    :goto_0
    array-length v8, v0

    .local v8, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_1
    if-ge v6, v8, :cond_2

    aget-object v7, v0, v6

    .line 503
    .local v7, "key":Ljava/lang/String;
    invoke-interface {p1, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 505
    const/4 v10, 0x0

    .line 572
    .end local v6    # "i$":I
    .end local v7    # "key":Ljava/lang/String;
    :goto_2
    return v10

    .line 501
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v8    # "len$":I
    :cond_0
    sget-object v0, Lcom/localytics/android/InAppManager;->INDIVIDUAL_FREQUENCY_CAPPING_RULE_REQUIRED_KEYS:[Ljava/lang/String;

    goto :goto_0

    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v6    # "i$":I
    .restart local v7    # "key":Ljava/lang/String;
    .restart local v8    # "len$":I
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 509
    .end local v7    # "key":Ljava/lang/String;
    :cond_2
    const-string v10, "display_frequencies"

    invoke-interface {p1, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 510
    .local v5, "frequencyRules":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    const-string v12, "days"

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-string v12, "count"

    aput-object v12, v10, v11

    invoke-virtual {p0, v5, v10}, Lcom/localytics/android/InAppManager;->_checkFrequencyCappingRuleArray(Ljava/util/List;[Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 512
    const/4 v10, 0x0

    goto :goto_2

    .line 514
    :cond_3
    invoke-virtual {p0, v5}, Lcom/localytics/android/InAppManager;->_additionalFCDisplayFrequencyRulesValidation(Ljava/util/List;)Z

    move-result v10

    if-nez v10, :cond_4

    .line 516
    const/4 v10, 0x0

    goto :goto_2

    .line 519
    :cond_4
    const-string v10, "blackout_rules"

    invoke-interface {p1, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 520
    .local v2, "blackoutRulesList":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map;>;"
    if-eqz v2, :cond_e

    .line 522
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v10

    const/4 v11, 0x1

    if-ge v10, v11, :cond_5

    .line 524
    const/4 v10, 0x0

    goto :goto_2

    .line 528
    :cond_5
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_e

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Map;

    .line 530
    .local v9, "m":Ljava/util/Map;
    const-string v10, "times"

    invoke-interface {v9, v10}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_7

    const-string v10, "dates"

    invoke-interface {v9, v10}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_7

    const-string v10, "weekdays"

    invoke-interface {v9, v10}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_7

    .line 534
    const/4 v10, 0x0

    goto :goto_2

    .line 538
    :cond_7
    const-string v10, "times"

    invoke-interface {v9, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 539
    .local v3, "blackoutTimesRules":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    const-string v12, "start"

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-string v12, "end"

    aput-object v12, v10, v11

    invoke-virtual {p0, v3, v10}, Lcom/localytics/android/InAppManager;->_checkFrequencyCappingRuleArray(Ljava/util/List;[Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_8

    .line 541
    const/4 v10, 0x0

    goto/16 :goto_2

    .line 543
    :cond_8
    sget-object v10, Lcom/localytics/android/InAppManager;->TIME_SDF:Ljava/text/SimpleDateFormat;

    invoke-virtual {p0, v3, v10}, Lcom/localytics/android/InAppManager;->_additionalFCDatesAndTimesRulesValidation(Ljava/util/List;Ljava/text/SimpleDateFormat;)Z

    move-result v10

    if-nez v10, :cond_9

    .line 545
    const/4 v10, 0x0

    goto/16 :goto_2

    .line 548
    :cond_9
    const-string v10, "dates"

    invoke-interface {v9, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 549
    .local v1, "blackoutDatesRules":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    const-string v12, "start"

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-string v12, "end"

    aput-object v12, v10, v11

    invoke-virtual {p0, v1, v10}, Lcom/localytics/android/InAppManager;->_checkFrequencyCappingRuleArray(Ljava/util/List;[Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_a

    .line 551
    const/4 v10, 0x0

    goto/16 :goto_2

    .line 553
    :cond_a
    sget-object v10, Lcom/localytics/android/InAppManager;->DATE_SDF:Ljava/text/SimpleDateFormat;

    invoke-virtual {p0, v1, v10}, Lcom/localytics/android/InAppManager;->_additionalFCDatesAndTimesRulesValidation(Ljava/util/List;Ljava/text/SimpleDateFormat;)Z

    move-result v10

    if-nez v10, :cond_b

    .line 555
    const/4 v10, 0x0

    goto/16 :goto_2

    .line 558
    :cond_b
    const-string v10, "weekdays"

    invoke-interface {v9, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 559
    .local v4, "blackoutWeekdaysRules":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    if-eqz v4, :cond_d

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v10

    const/4 v11, 0x1

    if-lt v10, v11, :cond_c

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v10

    const/4 v11, 0x7

    if-le v10, v11, :cond_d

    .line 561
    :cond_c
    const/4 v10, 0x0

    goto/16 :goto_2

    .line 563
    :cond_d
    invoke-virtual {p0, v4}, Lcom/localytics/android/InAppManager;->_additionalFCWeekdaysRulesValidation(Ljava/util/List;)Z

    move-result v10

    if-nez v10, :cond_6

    .line 565
    const/4 v10, 0x0

    goto/16 :goto_2

    .line 572
    .end local v1    # "blackoutDatesRules":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v3    # "blackoutTimesRules":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v4    # "blackoutWeekdaysRules":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v9    # "m":Ljava/util/Map;
    :cond_e
    const/4 v10, 0x1

    goto/16 :goto_2
.end method

.method protected _validateInAppMessage(Lcom/localytics/android/MarketingMessage;)Z
    .locals 12
    .param p1, "inAppMessage"    # Lcom/localytics/android/MarketingMessage;

    .prologue
    .line 379
    const-string v7, "campaign_id"

    invoke-static {p1, v7}, Lcom/localytics/android/JsonHelper;->getSafeIntegerFromMap(Ljava/util/Map;Ljava/lang/String;)I

    move-result v0

    .line 380
    .local v0, "campaignId":I
    const-string v7, "rule_name"

    invoke-static {p1, v7}, Lcom/localytics/android/JsonHelper;->getSafeStringFromMap(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 381
    .local v6, "ruleName":Ljava/lang/String;
    const-string v7, "display_events"

    invoke-static {p1, v7}, Lcom/localytics/android/JsonHelper;->getSafeListFromMap(Ljava/util/Map;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 382
    .local v1, "eventNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    const-string v7, "expiration"

    invoke-static {p1, v7}, Lcom/localytics/android/JsonHelper;->getSafeIntegerFromMap(Ljava/util/Map;Ljava/lang/String;)I

    move-result v2

    .line 383
    .local v2, "expiration":I
    const-string v7, "location"

    invoke-static {p1, v7}, Lcom/localytics/android/JsonHelper;->getSafeStringFromMap(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 385
    .local v3, "location":Ljava/lang/String;
    iget-object v7, p0, Lcom/localytics/android/InAppManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v7}, Lcom/localytics/android/LocalyticsDao;->getCurrentTimeMillis()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long v4, v8, v10

    .line 388
    .local v4, "now":J
    if-eqz v0, :cond_1

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    if-eqz v1, :cond_1

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    int-to-long v8, v2

    cmp-long v7, v8, v4

    if-gtz v7, :cond_0

    invoke-static {}, Lcom/localytics/android/Constants;->isTestModeEnabled()Z

    move-result v7

    if-eqz v7, :cond_1

    :cond_0
    const/4 v7, 0x1

    :goto_0
    return v7

    :cond_1
    const/4 v7, 0x0

    goto :goto_0
.end method

.method dismissCurrentInAppMessage()V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 112
    iget-object v4, p0, Lcom/localytics/android/InAppManager;->mFragmentManager:Landroid/app/FragmentManager;

    if-nez v4, :cond_1

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/localytics/android/InAppManager;->mFragmentManager:Landroid/app/FragmentManager;

    const-string v5, "marketing_dialog"

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 119
    .local v1, "currentFragment":Landroid/app/Fragment;
    instance-of v4, v1, Lcom/localytics/android/InAppDialogFragment;

    if-eqz v4, :cond_0

    .line 121
    move-object v0, v1

    check-cast v0, Lcom/localytics/android/InAppDialogFragment;

    move-object v2, v0

    .line 122
    .local v2, "currentInAppFragment":Lcom/localytics/android/InAppDialogFragment;
    invoke-virtual {v2}, Lcom/localytics/android/InAppDialogFragment;->dismissCampaign()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 125
    .end local v1    # "currentFragment":Landroid/app/Fragment;
    .end local v2    # "currentInAppFragment":Lcom/localytics/android/InAppDialogFragment;
    :catch_0
    move-exception v3

    .line 127
    .local v3, "e":Ljava/lang/Exception;
    const-string v4, "Localytics library threw an uncaught exception"

    invoke-static {v4, v3}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method getInAppMessages()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/localytics/android/MarketingMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2369
    iget-object v0, p0, Lcom/localytics/android/InAppManager;->mMarketingHandler:Lcom/localytics/android/MarketingHandler;

    new-instance v1, Lcom/localytics/android/InAppManager$12;

    invoke-direct {v1, p0}, Lcom/localytics/android/InAppManager$12;-><init>(Lcom/localytics/android/InAppManager;)V

    invoke-virtual {v0, v1}, Lcom/localytics/android/MarketingHandler;->getList(Ljava/util/concurrent/Callable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method setFragmentManager(Landroid/app/FragmentManager;)V
    .locals 0
    .param p1, "fragmentManager"    # Landroid/app/FragmentManager;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/localytics/android/InAppManager;->mFragmentManager:Landroid/app/FragmentManager;

    .line 107
    return-void
.end method

.method showInAppTest()V
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 1992
    :try_start_0
    iget-object v7, p0, Lcom/localytics/android/InAppManager;->mFragmentManager:Landroid/app/FragmentManager;

    if-nez v7, :cond_1

    .line 2320
    :cond_0
    :goto_0
    return-void

    .line 1997
    :cond_1
    iget-object v7, p0, Lcom/localytics/android/InAppManager;->mFragmentManager:Landroid/app/FragmentManager;

    const-string v8, "marketing_test_mode_button"

    invoke-virtual {v7, v8}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/localytics/android/InAppManager;->mFragmentManager:Landroid/app/FragmentManager;

    const-string v8, "marketing_test_mode_list"

    invoke-virtual {v7, v8}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v7

    if-nez v7, :cond_0

    .line 2002
    invoke-static {}, Lcom/localytics/android/TestModeButton;->newInstance()Lcom/localytics/android/TestModeButton;

    move-result-object v2

    .line 2003
    .local v2, "button":Lcom/localytics/android/TestModeButton;
    invoke-static {}, Lcom/localytics/android/TestModeListView;->newInstance()Lcom/localytics/android/TestModeListView;

    move-result-object v6

    .line 2004
    .local v6, "listView":Lcom/localytics/android/TestModeListView;
    iget-object v7, p0, Lcom/localytics/android/InAppManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v7}, Lcom/localytics/android/LocalyticsDao;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 2005
    .local v1, "appContext":Landroid/content/Context;
    new-instance v0, Lcom/localytics/android/InAppMessagesAdapter;

    invoke-direct {v0, v1, p0}, Lcom/localytics/android/InAppMessagesAdapter;-><init>(Landroid/content/Context;Lcom/localytics/android/InAppManager;)V

    .line 2007
    .local v0, "adapter":Lcom/localytics/android/InAppMessagesAdapter;
    invoke-virtual {v6, v0}, Lcom/localytics/android/TestModeListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2012
    new-instance v3, Ljava/util/TreeMap;

    invoke-direct {v3}, Ljava/util/TreeMap;-><init>()V

    .line 2014
    .local v3, "callbacksForButton":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/localytics/android/MarketingCallable;>;"
    const/16 v7, 0x8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    new-instance v8, Lcom/localytics/android/InAppManager$5;

    invoke-direct {v8, p0, v0, v6}, Lcom/localytics/android/InAppManager$5;-><init>(Lcom/localytics/android/InAppManager;Lcom/localytics/android/InAppMessagesAdapter;Lcom/localytics/android/TestModeListView;)V

    invoke-interface {v3, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2033
    invoke-virtual {v2, v3}, Lcom/localytics/android/TestModeButton;->setCallbacks(Ljava/util/Map;)Lcom/localytics/android/TestModeButton;

    .line 2034
    iget-object v7, p0, Lcom/localytics/android/InAppManager;->mFragmentManager:Landroid/app/FragmentManager;

    const-string v8, "marketing_test_mode_button"

    invoke-virtual {v2, v7, v8}, Lcom/localytics/android/TestModeButton;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 2040
    iget-object v7, p0, Lcom/localytics/android/InAppManager;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v7}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    .line 2045
    new-instance v4, Ljava/util/TreeMap;

    invoke-direct {v4}, Ljava/util/TreeMap;-><init>()V

    .line 2048
    .local v4, "callbacksForList":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/localytics/android/MarketingCallable;>;"
    const/16 v7, 0x9

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    new-instance v8, Lcom/localytics/android/InAppManager$6;

    invoke-direct {v8, p0, v2}, Lcom/localytics/android/InAppManager$6;-><init>(Lcom/localytics/android/InAppManager;Lcom/localytics/android/TestModeButton;)V

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2067
    const/16 v7, 0xb

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    new-instance v8, Lcom/localytics/android/InAppManager$7;

    invoke-direct {v8, p0, v0}, Lcom/localytics/android/InAppManager$7;-><init>(Lcom/localytics/android/InAppManager;Lcom/localytics/android/InAppMessagesAdapter;)V

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2087
    const/16 v7, 0xc

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    new-instance v8, Lcom/localytics/android/InAppManager$8;

    invoke-direct {v8, p0, v1}, Lcom/localytics/android/InAppManager$8;-><init>(Lcom/localytics/android/InAppManager;Landroid/content/Context;)V

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2129
    const/16 v7, 0xd

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    new-instance v8, Lcom/localytics/android/InAppManager$9;

    invoke-direct {v8, p0, v1}, Lcom/localytics/android/InAppManager$9;-><init>(Lcom/localytics/android/InAppManager;Landroid/content/Context;)V

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2169
    const/16 v7, 0xe

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    new-instance v8, Lcom/localytics/android/InAppManager$10;

    invoke-direct {v8, p0, v1}, Lcom/localytics/android/InAppManager$10;-><init>(Lcom/localytics/android/InAppManager;Landroid/content/Context;)V

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2209
    const/16 v7, 0xa

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    new-instance v8, Lcom/localytics/android/InAppManager$11;

    invoke-direct {v8, p0, v1}, Lcom/localytics/android/InAppManager$11;-><init>(Lcom/localytics/android/InAppManager;Landroid/content/Context;)V

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2314
    invoke-virtual {v6, v4}, Lcom/localytics/android/TestModeListView;->setCallbacks(Ljava/util/Map;)Lcom/localytics/android/TestModeListView;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 2316
    .end local v0    # "adapter":Lcom/localytics/android/InAppMessagesAdapter;
    .end local v1    # "appContext":Landroid/content/Context;
    .end local v2    # "button":Lcom/localytics/android/TestModeButton;
    .end local v3    # "callbacksForButton":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/localytics/android/MarketingCallable;>;"
    .end local v4    # "callbacksForList":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/localytics/android/MarketingCallable;>;"
    .end local v6    # "listView":Lcom/localytics/android/TestModeListView;
    :catch_0
    move-exception v5

    .line 2318
    .local v5, "e":Ljava/lang/Exception;
    const-string v7, "Exception while attempting to show in-app test"

    invoke-static {v7, v5}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0
.end method
