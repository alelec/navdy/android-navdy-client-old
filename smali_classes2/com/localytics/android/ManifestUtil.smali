.class Lcom/localytics/android/ManifestUtil;
.super Ljava/lang/Object;
.source "ManifestUtil.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getPackageInfo(Landroid/content/Context;I)Landroid/content/pm/PackageInfo;
    .locals 3
    .param p0, "appContext"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "flags"    # I
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 128
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 135
    :goto_0
    return-object v1

    .line 130
    :catch_0
    move-exception v0

    .line 132
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-static {v0}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/Throwable;)I

    .line 135
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static isActivityInManifest(Landroid/content/Context;Ljava/lang/Class;)Z
    .locals 4
    .param p0, "appContext"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Class;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "activityClass":Ljava/lang/Class;, "Ljava/lang/Class<+Landroid/app/Activity;>;"
    const/4 v1, 0x1

    .line 34
    invoke-static {p0, v1}, Lcom/localytics/android/ManifestUtil;->getPackageInfo(Landroid/content/Context;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 35
    .local v0, "packageInfo":Landroid/content/pm/PackageInfo;
    if-eqz v0, :cond_0

    iget-object v2, v0, Landroid/content/pm/PackageInfo;->activities:[Landroid/content/pm/ActivityInfo;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/localytics/android/ManifestUtil;->isClassInManifest([Landroid/content/pm/ActivityInfo;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static isClassInManifest([Landroid/content/pm/ActivityInfo;Ljava/lang/String;)Z
    .locals 5
    .param p0, "infoArray"    # [Landroid/content/pm/ActivityInfo;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "className"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 93
    if-eqz p0, :cond_1

    .line 95
    move-object v1, p0

    .local v1, "arr$":[Landroid/content/pm/ActivityInfo;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v0, v1, v2

    .line 97
    .local v0, "ai":Landroid/content/pm/ActivityInfo;
    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 99
    const/4 v4, 0x1

    .line 104
    .end local v0    # "ai":Landroid/content/pm/ActivityInfo;
    .end local v1    # "arr$":[Landroid/content/pm/ActivityInfo;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :goto_1
    return v4

    .line 95
    .restart local v0    # "ai":Landroid/content/pm/ActivityInfo;
    .restart local v1    # "arr$":[Landroid/content/pm/ActivityInfo;
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 104
    .end local v0    # "ai":Landroid/content/pm/ActivityInfo;
    .end local v1    # "arr$":[Landroid/content/pm/ActivityInfo;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private static isClassInManifest([Landroid/content/pm/ServiceInfo;Ljava/lang/String;)Z
    .locals 5
    .param p0, "infoArray"    # [Landroid/content/pm/ServiceInfo;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "className"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 109
    if-eqz p0, :cond_1

    .line 111
    move-object v0, p0

    .local v0, "arr$":[Landroid/content/pm/ServiceInfo;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 113
    .local v3, "si":Landroid/content/pm/ServiceInfo;
    iget-object v4, v3, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 115
    const/4 v4, 0x1

    .line 120
    .end local v0    # "arr$":[Landroid/content/pm/ServiceInfo;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "si":Landroid/content/pm/ServiceInfo;
    :goto_1
    return v4

    .line 111
    .restart local v0    # "arr$":[Landroid/content/pm/ServiceInfo;
    .restart local v1    # "i$":I
    .restart local v2    # "len$":I
    .restart local v3    # "si":Landroid/content/pm/ServiceInfo;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 120
    .end local v0    # "arr$":[Landroid/content/pm/ServiceInfo;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "si":Landroid/content/pm/ServiceInfo;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method static isPermissionInManifest(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 7
    .param p0, "appContext"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "permission"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 50
    const/16 v6, 0x1000

    invoke-static {p0, v6}, Lcom/localytics/android/ManifestUtil;->getPackageInfo(Landroid/content/Context;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 51
    .local v3, "packageInfo":Landroid/content/pm/PackageInfo;
    if-eqz v3, :cond_1

    .line 53
    iget-object v4, v3, Landroid/content/pm/PackageInfo;->permissions:[Landroid/content/pm/PermissionInfo;

    .line 54
    .local v4, "permissions":[Landroid/content/pm/PermissionInfo;
    if-eqz v4, :cond_1

    .line 56
    move-object v0, v4

    .local v0, "arr$":[Landroid/content/pm/PermissionInfo;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v5, v0, v1

    .line 58
    .local v5, "pi":Landroid/content/pm/PermissionInfo;
    iget-object v6, v5, Landroid/content/pm/PermissionInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 60
    const/4 v6, 0x1

    .line 66
    .end local v0    # "arr$":[Landroid/content/pm/PermissionInfo;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v4    # "permissions":[Landroid/content/pm/PermissionInfo;
    .end local v5    # "pi":Landroid/content/pm/PermissionInfo;
    :goto_1
    return v6

    .line 56
    .restart local v0    # "arr$":[Landroid/content/pm/PermissionInfo;
    .restart local v1    # "i$":I
    .restart local v2    # "len$":I
    .restart local v4    # "permissions":[Landroid/content/pm/PermissionInfo;
    .restart local v5    # "pi":Landroid/content/pm/PermissionInfo;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 66
    .end local v0    # "arr$":[Landroid/content/pm/PermissionInfo;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v4    # "permissions":[Landroid/content/pm/PermissionInfo;
    .end local v5    # "pi":Landroid/content/pm/PermissionInfo;
    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method static isReceiverInManifest(Landroid/content/Context;Ljava/lang/Class;)Z
    .locals 1
    .param p0, "appContext"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Class;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<+",
            "Landroid/content/BroadcastReceiver;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 20
    .local p1, "receiverClass":Ljava/lang/Class;, "Ljava/lang/Class<+Landroid/content/BroadcastReceiver;>;"
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/localytics/android/ManifestUtil;->isReceiverInManifest(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static isReceiverInManifest(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2
    .param p0, "appContext"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "receiverClassName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 26
    const/4 v1, 0x2

    invoke-static {p0, v1}, Lcom/localytics/android/ManifestUtil;->getPackageInfo(Landroid/content/Context;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 27
    .local v0, "packageInfo":Landroid/content/pm/PackageInfo;
    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/content/pm/PackageInfo;->receivers:[Landroid/content/pm/ActivityInfo;

    invoke-static {v1, p1}, Lcom/localytics/android/ManifestUtil;->isClassInManifest([Landroid/content/pm/ActivityInfo;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static isRequestedPermissionInManifest(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 7
    .param p0, "appContext"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "requestedPermission"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 72
    const/16 v6, 0x1000

    invoke-static {p0, v6}, Lcom/localytics/android/ManifestUtil;->getPackageInfo(Landroid/content/Context;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 73
    .local v3, "packageInfo":Landroid/content/pm/PackageInfo;
    if-eqz v3, :cond_1

    .line 75
    iget-object v4, v3, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    .line 76
    .local v4, "requestedPermissions":[Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 78
    move-object v0, v4

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v5, v0, v1

    .line 80
    .local v5, "rp":Ljava/lang/String;
    invoke-virtual {p1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 82
    const/4 v6, 0x1

    .line 88
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v4    # "requestedPermissions":[Ljava/lang/String;
    .end local v5    # "rp":Ljava/lang/String;
    :goto_1
    return v6

    .line 78
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v1    # "i$":I
    .restart local v2    # "len$":I
    .restart local v4    # "requestedPermissions":[Ljava/lang/String;
    .restart local v5    # "rp":Ljava/lang/String;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 88
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v4    # "requestedPermissions":[Ljava/lang/String;
    .end local v5    # "rp":Ljava/lang/String;
    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method static isServiceInManifest(Landroid/content/Context;Ljava/lang/Class;)Z
    .locals 3
    .param p0, "appContext"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Class;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Service;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 42
    .local p1, "serviceClass":Ljava/lang/Class;, "Ljava/lang/Class<+Landroid/app/Service;>;"
    const/4 v1, 0x4

    invoke-static {p0, v1}, Lcom/localytics/android/ManifestUtil;->getPackageInfo(Landroid/content/Context;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 43
    .local v0, "packageInfo":Landroid/content/pm/PackageInfo;
    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/content/pm/PackageInfo;->services:[Landroid/content/pm/ServiceInfo;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/localytics/android/ManifestUtil;->isClassInManifest([Landroid/content/pm/ServiceInfo;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
