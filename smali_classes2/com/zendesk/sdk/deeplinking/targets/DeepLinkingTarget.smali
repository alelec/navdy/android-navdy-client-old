.class public abstract Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;
.super Ljava/lang/Object;
.source "DeepLinkingTarget.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final EXTRA_APP_ID:Ljava/lang/String; = "extra_appid"

.field private static final EXTRA_AUTH_BUNDLE:Ljava/lang/String; = "extra_auth_bundle"

.field private static final EXTRA_BACK_STACK_ACTIVITIES:Ljava/lang/String; = "extra_follow_up_activities"

.field private static final EXTRA_CLIENT_ID:Ljava/lang/String; = "extra_client_id"

.field private static final EXTRA_DEEP_LINK_TYPE:Ljava/lang/String; = "extra_deeplink"

.field private static final EXTRA_EXTRA_BUNDLE:Ljava/lang/String; = "extra_extra_bundle"

.field private static final EXTRA_FALLBACK_ACTIVITY:Ljava/lang/String; = "extra_fallback_activity"

.field private static final EXTRA_LOGGABLE:Ljava/lang/String; = "extra_loggable"

.field private static final EXTRA_URL:Ljava/lang/String; = "extra_url"

.field private static final EXTRA_USER_TOKEN:Ljava/lang/String; = "extra_user_token"

.field private static final LOG_TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    .local p0, "this":Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;, "Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget<TE;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private applyAuthBundle(Landroid/content/Context;Landroid/os/Bundle;)Z
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bundle"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .local p0, "this":Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;, "Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget<TE;>;"
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 258
    if-eqz p2, :cond_0

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "extra_client_id"

    aput-object v4, v3, v1

    const-string v4, "extra_appid"

    aput-object v4, v3, v2

    const/4 v4, 0x2

    const-string v5, "extra_url"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "extra_user_token"

    aput-object v5, v3, v4

    invoke-direct {p0, p2, v3}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;->hasBundleValidStrings(Landroid/os/Bundle;[Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 259
    :cond_0
    sget-object v2, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Auth bundle content is invalid"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 278
    :goto_0
    return v1

    .line 263
    :cond_1
    sget-object v3, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    const-string v4, "extra_url"

    .line 265
    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "extra_appid"

    .line 266
    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "extra_client_id"

    .line 267
    invoke-virtual {p2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 263
    invoke-virtual {v3, p1, v4, v5, v6}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->init(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    const-string v3, "extra_user_token"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 273
    .local v0, "userToken":Ljava/lang/String;
    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;->getUserToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 274
    sget-object v2, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;->LOG_TAG:Ljava/lang/String;

    const-string v3, "User configuration has changed"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    move v1, v2

    .line 278
    goto :goto_0
.end method

.method private checkIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 313
    .local p0, "this":Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;, "Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget<TE;>;"
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_0

    .line 314
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Illegal Intent provided. Make sure that the provided Intent has a ComponentName. To achieve this you should for example this constructor: ``Intent(Context packageContext, Class<?> cls)``"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 318
    :cond_0
    return-void
.end method

.method private fireFallBackActivity(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 305
    .local p0, "this":Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;, "Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget<TE;>;"
    if-eqz p2, :cond_0

    .line 306
    invoke-direct {p0, p2}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;->checkIntent(Landroid/content/Intent;)V

    .line 307
    const/high16 v0, 0x10000000

    invoke-virtual {p2, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 308
    invoke-virtual {p1, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 310
    :cond_0
    return-void
.end method

.method private getAuthBundle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "appId"    # Ljava/lang/String;
    .param p3, "oAuth"    # Ljava/lang/String;
    .param p4, "userToken"    # Ljava/lang/String;

    .prologue
    .line 245
    .local p0, "this":Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;, "Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget<TE;>;"
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 246
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "extra_url"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    const-string v1, "extra_appid"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    const-string v1, "extra_client_id"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    const-string v1, "extra_user_token"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    return-object v0
.end method

.method public static getDeepLinkType(Landroid/content/Intent;)Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;
    .locals 1
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 329
    const-string v0, "extra_deeplink"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "extra_deeplink"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    instance-of v0, v0, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    if-nez v0, :cond_1

    .line 330
    :cond_0
    sget-object v0, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;->Unknown:Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    .line 332
    :goto_0
    return-object v0

    :cond_1
    const-string v0, "extra_deeplink"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    goto :goto_0
.end method

.method protected static getTaskStackBuilder(Landroid/content/Context;Ljava/util/List;)Landroid/support/v4/app/TaskStackBuilder;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;)",
            "Landroid/support/v4/app/TaskStackBuilder;"
        }
    .end annotation

    .prologue
    .line 234
    .local p1, "backStackActivities":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    invoke-static {p0}, Landroid/support/v4/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/support/v4/app/TaskStackBuilder;

    move-result-object v1

    .line 236
    .local v1, "taskStackBuilder":Landroid/support/v4/app/TaskStackBuilder;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 237
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {v1, v0}, Landroid/support/v4/app/TaskStackBuilder;->addNextIntentWithParentStack(Landroid/content/Intent;)Landroid/support/v4/app/TaskStackBuilder;

    goto :goto_0

    .line 240
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    return-object v1
.end method

.method private getUserToken()Ljava/lang/String;
    .locals 2

    .prologue
    .line 292
    .local p0, "this":Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;, "Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget<TE;>;"
    sget-object v1, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->storage()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v1

    invoke-interface {v1}, Lcom/zendesk/sdk/storage/StorageStore;->identityStorage()Lcom/zendesk/sdk/storage/IdentityStorage;

    move-result-object v1

    invoke-interface {v1}, Lcom/zendesk/sdk/storage/IdentityStorage;->getIdentity()Lcom/zendesk/sdk/model/access/Identity;

    move-result-object v0

    .line 294
    .local v0, "identity":Lcom/zendesk/sdk/model/access/Identity;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;

    if-eqz v1, :cond_0

    .line 295
    check-cast v0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;

    .end local v0    # "identity":Lcom/zendesk/sdk/model/access/Identity;
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->getSdkGuid()Ljava/lang/String;

    move-result-object v1

    .line 301
    :goto_0
    return-object v1

    .line 297
    .restart local v0    # "identity":Lcom/zendesk/sdk/model/access/Identity;
    :cond_0
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/zendesk/sdk/model/access/JwtIdentity;

    if-eqz v1, :cond_1

    .line 298
    check-cast v0, Lcom/zendesk/sdk/model/access/JwtIdentity;

    .end local v0    # "identity":Lcom/zendesk/sdk/model/access/Identity;
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/access/JwtIdentity;->getJwtUserIdentifier()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 301
    .restart local v0    # "identity":Lcom/zendesk/sdk/model/access/Identity;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private varargs hasBundleValidStrings(Landroid/os/Bundle;[Ljava/lang/String;)Z
    .locals 5
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "keys"    # [Ljava/lang/String;

    .prologue
    .line 282
    .local p0, "this":Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;, "Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget<TE;>;"
    const/4 v0, 0x1

    .line 283
    .local v0, "isValid":Z
    array-length v3, p2

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v1, p2, v2

    .line 284
    .local v1, "key":Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 285
    :cond_0
    const/4 v0, 0x0

    .line 283
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 288
    .end local v1    # "key":Ljava/lang/String;
    :cond_2
    return v0
.end method


# virtual methods
.method public execute(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .local p0, "this":Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;, "Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget<TE;>;"
    const/4 v6, 0x0

    .line 179
    const-string v7, "extra_loggable"

    invoke-virtual {p2, v7, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    invoke-static {v7}, Lcom/zendesk/logger/Logger;->setLoggable(Z)V

    .line 181
    const/4 v4, 0x0

    .line 182
    .local v4, "fallbackActivity":Landroid/content/Intent;
    const-string v7, "extra_fallback_activity"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    const-string v7, "extra_fallback_activity"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    instance-of v7, v7, Landroid/content/Intent;

    if-eqz v7, :cond_0

    .line 183
    const-string v7, "extra_fallback_activity"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    .end local v4    # "fallbackActivity":Landroid/content/Intent;
    check-cast v4, Landroid/content/Intent;

    .line 186
    .restart local v4    # "fallbackActivity":Landroid/content/Intent;
    :cond_0
    const-string v7, "extra_auth_bundle"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v7

    invoke-direct {p0, p1, v7}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;->applyAuthBundle(Landroid/content/Context;Landroid/os/Bundle;)Z

    move-result v0

    .line 187
    .local v0, "applyBundle":Z
    if-nez v0, :cond_1

    .line 188
    sget-object v7, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;->LOG_TAG:Ljava/lang/String;

    const-string v8, "No valid auth bundle found"

    new-array v9, v6, [Ljava/lang/Object;

    invoke-static {v7, v8, v9}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 189
    invoke-direct {p0, p1, v4}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;->fireFallBackActivity(Landroid/content/Context;Landroid/content/Intent;)V

    .line 221
    :goto_0
    return v6

    .line 193
    :cond_1
    const-string v7, "extra_deeplink"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    const-string v7, "extra_deeplink"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v7

    instance-of v7, v7, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    if-nez v7, :cond_3

    .line 194
    :cond_2
    sget-object v7, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;->LOG_TAG:Ljava/lang/String;

    const-string v8, "No valid deep link type found"

    new-array v9, v6, [Ljava/lang/Object;

    invoke-static {v7, v8, v9}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 195
    invoke-direct {p0, p1, v4}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;->fireFallBackActivity(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 198
    :cond_3
    const-string v7, "extra_deeplink"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    .line 200
    .local v3, "deepLinkTypeType":Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 201
    .local v1, "backStack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    const-string v7, "extra_follow_up_activities"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 202
    const-string v7, "extra_follow_up_activities"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 203
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Intent;

    .line 204
    .local v5, "i":Landroid/content/Intent;
    invoke-direct {p0, v5}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;->checkIntent(Landroid/content/Intent;)V

    goto :goto_1

    .line 208
    .end local v5    # "i":Landroid/content/Intent;
    :cond_4
    const-string v7, "extra_extra_bundle"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    const-string v7, "extra_extra_bundle"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v7

    if-nez v7, :cond_6

    .line 209
    :cond_5
    sget-object v7, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;->LOG_TAG:Ljava/lang/String;

    const-string v8, "No activity configuration found"

    new-array v9, v6, [Ljava/lang/Object;

    invoke-static {v7, v8, v9}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 210
    invoke-direct {p0, p1, v4}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;->fireFallBackActivity(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 214
    :cond_6
    const-string v7, "extra_extra_bundle"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v7

    new-instance v8, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;

    invoke-direct {v8, v3, v1, v4}, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;-><init>(Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;Ljava/util/List;Landroid/content/Intent;)V

    invoke-virtual {p0, v7, v8}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;->extractConfiguration(Landroid/os/Bundle;Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;)Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;

    move-result-object v2

    .line 216
    .local v2, "config":Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;, "TE;"
    if-nez v2, :cond_7

    .line 217
    invoke-direct {p0, p1, v4}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;->fireFallBackActivity(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 220
    :cond_7
    invoke-virtual {p0, p1, v2}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;->openTargetActivity(Landroid/content/Context;Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;)V

    .line 221
    const/4 v6, 0x1

    goto/16 :goto_0
.end method

.method abstract extractConfiguration(Landroid/os/Bundle;Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;)Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;",
            ")TE;"
        }
    .end annotation
.end method

.method abstract getExtra(Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;)Landroid/os/Bundle;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "Landroid/os/Bundle;"
        }
    .end annotation
.end method

.method public getIntent(Landroid/content/Context;Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;)Landroid/content/Intent;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "TE;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 111
    .local p0, "this":Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;, "Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget<TE;>;"
    .local p2, "configuration":Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;, "TE;"
    sget-object v0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 112
    const/4 v0, 0x0

    .line 114
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->getZendeskUrl()Ljava/lang/String;

    move-result-object v3

    sget-object v0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->getApplicationId()Ljava/lang/String;

    move-result-object v4

    sget-object v0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->getOauthClientId()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;->getIntent(Landroid/content/Context;Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public getIntent(Landroid/content/Context;Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "zendeskUrl"    # Ljava/lang/String;
    .param p4, "applicationId"    # Ljava/lang/String;
    .param p5, "oauthClientId"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "TE;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 136
    .local p0, "this":Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;, "Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget<TE;>;"
    .local p2, "configuration":Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;, "TE;"
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingBroadcastReceiver;

    invoke-direct {v1, p1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 138
    .local v1, "intent":Landroid/content/Intent;
    invoke-direct {p0}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;->getUserToken()Ljava/lang/String;

    move-result-object v2

    .line 139
    .local v2, "userToken":Ljava/lang/String;
    invoke-static {v2}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 140
    sget-object v3, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;->LOG_TAG:Ljava/lang/String;

    const-string v4, "No user configuration found"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 141
    const/4 v1, 0x0

    .line 162
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    return-object v1

    .line 144
    .restart local v1    # "intent":Landroid/content/Intent;
    :cond_0
    const-string v3, "extra_loggable"

    invoke-static {}, Lcom/zendesk/logger/Logger;->isLoggable()Z

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 145
    const-string v3, "extra_auth_bundle"

    invoke-direct {p0, p3, p4, p5, v2}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;->getAuthBundle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 146
    const-string v3, "extra_deeplink"

    invoke-virtual {p2}, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;->getDeepLinkType()Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 148
    invoke-virtual {p2}, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;->getFallbackActivity()Landroid/content/Intent;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 149
    invoke-virtual {p2}, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;->getFallbackActivity()Landroid/content/Intent;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;->checkIntent(Landroid/content/Intent;)V

    .line 150
    const-string v3, "extra_fallback_activity"

    invoke-virtual {p2}, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;->getFallbackActivity()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 153
    :cond_1
    invoke-virtual {p2}, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;->getBackStackActivities()Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 154
    invoke-virtual {p2}, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;->getBackStackActivities()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 155
    .local v0, "i":Landroid/content/Intent;
    invoke-direct {p0, v0}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;->checkIntent(Landroid/content/Intent;)V

    goto :goto_1

    .line 157
    .end local v0    # "i":Landroid/content/Intent;
    :cond_2
    const-string v3, "extra_follow_up_activities"

    invoke-virtual {p2}, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;->getBackStackActivities()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 160
    :cond_3
    const-string v3, "extra_extra_bundle"

    invoke-virtual {p0, p2}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;->getExtra(Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;)Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    goto :goto_0
.end method

.method abstract openTargetActivity(Landroid/content/Context;Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;)V
    .param p2    # Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "TE;)V"
        }
    .end annotation
.end method
