.class public Lcom/zendesk/sdk/rating/impl/MetricsRateMyAppRule;
.super Ljava/lang/Object;
.source "MetricsRateMyAppRule.java"

# interfaces
.implements Lcom/zendesk/sdk/rating/RateMyAppRule;


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "MetricsRateMyAppRule"


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/zendesk/sdk/rating/impl/MetricsRateMyAppRule;->mContext:Landroid/content/Context;

    .line 44
    return-void

    .line 43
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public permitsShowOfDialog()Z
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 49
    iget-object v6, p0, Lcom/zendesk/sdk/rating/impl/MetricsRateMyAppRule;->mContext:Landroid/content/Context;

    if-nez v6, :cond_1

    .line 65
    :cond_0
    :goto_0
    return v5

    .line 53
    :cond_1
    new-instance v3, Lcom/zendesk/sdk/storage/RateMyAppStorage;

    iget-object v6, p0, Lcom/zendesk/sdk/rating/impl/MetricsRateMyAppRule;->mContext:Landroid/content/Context;

    invoke-direct {v3, v6}, Lcom/zendesk/sdk/storage/RateMyAppStorage;-><init>(Landroid/content/Context;)V

    .line 55
    .local v3, "rateMyAppStorage":Lcom/zendesk/sdk/storage/RateMyAppStorage;
    invoke-virtual {v3}, Lcom/zendesk/sdk/storage/RateMyAppStorage;->isNumberOfLaunchesMet()Z

    move-result v2

    .line 56
    .local v2, "numberOfLaunchesMet":Z
    invoke-virtual {v3}, Lcom/zendesk/sdk/storage/RateMyAppStorage;->isLaunchTimeMet()Z

    move-result v1

    .line 57
    .local v1, "launchTimeMet":Z
    invoke-virtual {v3}, Lcom/zendesk/sdk/storage/RateMyAppStorage;->isRatedForCurrentVersion()Z

    move-result v4

    .line 58
    .local v4, "ratedForCurrentVersion":Z
    invoke-virtual {v3}, Lcom/zendesk/sdk/storage/RateMyAppStorage;->isDontShowAgain()Z

    move-result v0

    .line 60
    .local v0, "dontShowAgain":Z
    const-string v6, "MetricsRateMyAppRule"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Number of launches met: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-array v8, v5, [Ljava/lang/Object;

    invoke-static {v6, v7, v8}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 61
    const-string v6, "MetricsRateMyAppRule"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Launch time met: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-array v8, v5, [Ljava/lang/Object;

    invoke-static {v6, v7, v8}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    const-string v6, "MetricsRateMyAppRule"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Rated for current version: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-array v8, v5, [Ljava/lang/Object;

    invoke-static {v6, v7, v8}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 63
    const-string v6, "MetricsRateMyAppRule"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Don\'t show again: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-array v8, v5, [Ljava/lang/Object;

    invoke-static {v6, v7, v8}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 65
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    if-nez v4, :cond_0

    if-nez v0, :cond_0

    const/4 v5, 0x1

    goto/16 :goto_0
.end method
