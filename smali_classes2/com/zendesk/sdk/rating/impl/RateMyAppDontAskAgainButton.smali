.class public Lcom/zendesk/sdk/rating/impl/RateMyAppDontAskAgainButton;
.super Lcom/zendesk/sdk/rating/impl/BaseRateMyAppButton;
.source "RateMyAppDontAskAgainButton.java"


# instance fields
.field private final mLabel:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/zendesk/sdk/rating/impl/BaseRateMyAppButton;-><init>()V

    .line 19
    sget v0, Lcom/zendesk/sdk/R$string;->rate_my_app_dialog_dismiss_action_label:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/rating/impl/RateMyAppDontAskAgainButton;->mLabel:Ljava/lang/String;

    .line 20
    return-void
.end method


# virtual methods
.method public getId()I
    .locals 1

    .prologue
    .line 39
    sget v0, Lcom/zendesk/sdk/R$id;->rma_dont_ask_again:I

    return v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/zendesk/sdk/rating/impl/RateMyAppDontAskAgainButton;->mLabel:Ljava/lang/String;

    return-object v0
.end method

.method public getOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/zendesk/sdk/rating/impl/RateMyAppDontAskAgainButton$1;

    invoke-direct {v0, p0}, Lcom/zendesk/sdk/rating/impl/RateMyAppDontAskAgainButton$1;-><init>(Lcom/zendesk/sdk/rating/impl/RateMyAppDontAskAgainButton;)V

    return-object v0
.end method
