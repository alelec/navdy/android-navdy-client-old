.class Lcom/zendesk/sdk/storage/StubHelpCenterSessionCache;
.super Ljava/lang/Object;
.source "StubHelpCenterSessionCache.java"

# interfaces
.implements Lcom/zendesk/sdk/storage/HelpCenterSessionCache;


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "StubZendeskHelpCenterSessionCache"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getLastSearch()Lcom/zendesk/sdk/model/helpcenter/LastSearch;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 19
    const-string v0, "StubZendeskHelpCenterSessionCache"

    const-string v1, "Zendesk not initialised"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 20
    new-instance v0, Lcom/zendesk/sdk/model/helpcenter/LastSearch;

    const-string v1, ""

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/model/helpcenter/LastSearch;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method public isUniqueSearchResultClick()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 35
    const-string v0, "StubZendeskHelpCenterSessionCache"

    const-string v1, "Zendesk not initialised"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 36
    return v3
.end method

.method public setLastSearch(Ljava/lang/String;I)V
    .locals 3
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "resultCount"    # I

    .prologue
    .line 25
    const-string v0, "StubZendeskHelpCenterSessionCache"

    const-string v1, "Zendesk not initialised"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 26
    return-void
.end method

.method public unsetUniqueSearchResultClick()V
    .locals 3

    .prologue
    .line 30
    const-string v0, "StubZendeskHelpCenterSessionCache"

    const-string v1, "Zendesk not initialised"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 31
    return-void
.end method
