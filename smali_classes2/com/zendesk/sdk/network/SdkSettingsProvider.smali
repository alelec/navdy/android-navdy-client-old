.class public interface abstract Lcom/zendesk/sdk/network/SdkSettingsProvider;
.super Ljava/lang/Object;
.source "SdkSettingsProvider.java"


# virtual methods
.method public abstract getSettings(Lcom/zendesk/service/ZendeskCallback;)V
    .param p1    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/settings/SafeMobileSettings;",
            ">;)V"
        }
    .end annotation
.end method
