.class Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$5;
.super Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;
.source "ZendeskUserProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;->getUser(Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/SdkConfiguration;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;

.field final synthetic val$callback:Lcom/zendesk/service/ZendeskCallback;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;
    .param p2, "callback"    # Lcom/zendesk/service/ZendeskCallback;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$5;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;

    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$5;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-direct {p0, p2}, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V
    .locals 4
    .param p1, "config"    # Lcom/zendesk/sdk/model/SdkConfiguration;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$5;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;

    invoke-static {v0}, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;->access$100(Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;)Lcom/zendesk/sdk/network/impl/ZendeskUserService;

    move-result-object v0

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getBearerAuthorizationHeader()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$5$1;

    iget-object v3, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$5;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-direct {v2, p0, v3}, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$5$1;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$5;Lcom/zendesk/service/ZendeskCallback;)V

    invoke-virtual {v0, v1, v2}, Lcom/zendesk/sdk/network/impl/ZendeskUserService;->getUser(Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V

    .line 125
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 113
    check-cast p1, Lcom/zendesk/sdk/model/SdkConfiguration;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$5;->onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V

    return-void
.end method
