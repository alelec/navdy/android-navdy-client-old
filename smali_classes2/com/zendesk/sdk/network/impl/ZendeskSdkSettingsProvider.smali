.class Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider;
.super Ljava/lang/Object;
.source "ZendeskSdkSettingsProvider.java"

# interfaces
.implements Lcom/zendesk/sdk/network/SdkSettingsProvider;


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "ZendeskSdkSettingsProvider"


# instance fields
.field private final applicationId:Ljava/lang/String;

.field private final deviceLocale:Ljava/util/Locale;

.field private final localeConverter:Lcom/zendesk/sdk/network/impl/HelpCenterLocaleConverter;

.field private final sdkSettingsStorage:Lcom/zendesk/sdk/storage/SdkSettingsStorage;

.field private final settingsService:Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsService;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsService;Ljava/util/Locale;Lcom/zendesk/sdk/storage/SdkSettingsStorage;Ljava/lang/String;Lcom/zendesk/sdk/network/impl/HelpCenterLocaleConverter;)V
    .locals 0
    .param p1, "settingsService"    # Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsService;
    .param p2, "deviceLocale"    # Ljava/util/Locale;
    .param p3, "sdkSettingsStorage"    # Lcom/zendesk/sdk/storage/SdkSettingsStorage;
    .param p4, "applicationId"    # Ljava/lang/String;
    .param p5, "localeConverter"    # Lcom/zendesk/sdk/network/impl/HelpCenterLocaleConverter;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider;->settingsService:Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsService;

    .line 47
    iput-object p2, p0, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider;->deviceLocale:Ljava/util/Locale;

    .line 48
    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider;->sdkSettingsStorage:Lcom/zendesk/sdk/storage/SdkSettingsStorage;

    .line 49
    iput-object p4, p0, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider;->applicationId:Ljava/lang/String;

    .line 50
    iput-object p5, p0, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider;->localeConverter:Lcom/zendesk/sdk/network/impl/HelpCenterLocaleConverter;

    .line 51
    return-void
.end method

.method static synthetic access$000(Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider;)Lcom/zendesk/sdk/storage/SdkSettingsStorage;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider;->sdkSettingsStorage:Lcom/zendesk/sdk/storage/SdkSettingsStorage;

    return-object v0
.end method


# virtual methods
.method public getSettings(Lcom/zendesk/service/ZendeskCallback;)V
    .locals 4
    .param p1    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/settings/SafeMobileSettings;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 56
    .local p1, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/settings/SafeMobileSettings;>;"
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider;->localeConverter:Lcom/zendesk/sdk/network/impl/HelpCenterLocaleConverter;

    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider;->deviceLocale:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Lcom/zendesk/sdk/network/impl/HelpCenterLocaleConverter;->toHelpCenterLocaleString(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 58
    .local v0, "locale":Ljava/lang/String;
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider;->settingsService:Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsService;

    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider;->applicationId:Ljava/lang/String;

    new-instance v3, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider$1;

    invoke-direct {v3, p0, p1, v0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider$1;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider;Lcom/zendesk/service/ZendeskCallback;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V

    invoke-virtual {v1, v0, v2, v3}, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsService;->getSettings(Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V

    .line 82
    return-void
.end method
