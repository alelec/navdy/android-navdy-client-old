.class final Lcom/zendesk/sdk/network/impl/ZendeskAccessService$1;
.super Ljava/lang/Object;
.source "ZendeskAccessService.java"

# interfaces
.implements Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/network/impl/ZendeskAccessService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor",
        "<",
        "Lcom/zendesk/sdk/model/access/AuthenticationResponse;",
        "Lcom/zendesk/sdk/model/access/AccessToken;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public extract(Lcom/zendesk/sdk/model/access/AuthenticationResponse;)Lcom/zendesk/sdk/model/access/AccessToken;
    .locals 1
    .param p1, "data"    # Lcom/zendesk/sdk/model/access/AuthenticationResponse;

    .prologue
    .line 28
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/access/AuthenticationResponse;->getAuthentication()Lcom/zendesk/sdk/model/access/AccessToken;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic extract(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    check-cast p1, Lcom/zendesk/sdk/model/access/AuthenticationResponse;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskAccessService$1;->extract(Lcom/zendesk/sdk/model/access/AuthenticationResponse;)Lcom/zendesk/sdk/model/access/AccessToken;

    move-result-object v0

    return-object v0
.end method
