.class Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$2;
.super Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;
.source "ZendeskUserProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;->deleteTags(Ljava/util/List;Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/SdkConfiguration;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;

.field final synthetic val$callback:Lcom/zendesk/service/ZendeskCallback;

.field final synthetic val$tags:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;Lcom/zendesk/service/ZendeskCallback;Ljava/util/List;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;
    .param p2, "callback"    # Lcom/zendesk/service/ZendeskCallback;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$2;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;

    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$2;->val$tags:Ljava/util/List;

    iput-object p4, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$2;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-direct {p0, p2}, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V
    .locals 5
    .param p1, "config"    # Lcom/zendesk/sdk/model/SdkConfiguration;

    .prologue
    .line 60
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$2;->val$tags:Ljava/util/List;

    invoke-static {v1}, Lcom/zendesk/util/CollectionUtils;->ensureEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/zendesk/util/StringUtils;->toCsvString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 61
    .local v0, "tagsCsv":Ljava/lang/String;
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$2;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;

    invoke-static {v1}, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;->access$100(Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;)Lcom/zendesk/sdk/network/impl/ZendeskUserService;

    move-result-object v1

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getBearerAuthorizationHeader()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$2$1;

    iget-object v4, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$2;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-direct {v3, p0, v4}, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$2$1;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$2;Lcom/zendesk/service/ZendeskCallback;)V

    invoke-virtual {v1, v2, v0, v3}, Lcom/zendesk/sdk/network/impl/ZendeskUserService;->deleteTags(Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V

    .line 70
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 57
    check-cast p1, Lcom/zendesk/sdk/model/SdkConfiguration;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$2;->onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V

    return-void
.end method
