.class Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$5;
.super Ljava/lang/Object;
.source "ContactZendeskFragment.java"

# interfaces
.implements Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->setUpCallbacks()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    .prologue
    .line 498
    iput-object p1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$5;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public attachmentRemoved(Ljava/io/File;)V
    .locals 1
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 501
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$5;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$100(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->removeImage(Ljava/io/File;)V

    .line 502
    return-void
.end method
