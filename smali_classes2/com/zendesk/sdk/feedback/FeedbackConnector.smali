.class public interface abstract Lcom/zendesk/sdk/feedback/FeedbackConnector;
.super Ljava/lang/Object;
.source "FeedbackConnector.java"

# interfaces
.implements Ljava/io/Serializable;


# virtual methods
.method public abstract isValid()Z
.end method

.method public abstract sendFeedback(Ljava/lang/String;Ljava/util/List;Lcom/zendesk/service/ZendeskCallback;)V
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/CreateRequest;",
            ">;)V"
        }
    .end annotation
.end method
