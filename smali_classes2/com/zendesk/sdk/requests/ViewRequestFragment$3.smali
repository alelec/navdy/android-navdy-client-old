.class Lcom/zendesk/sdk/requests/ViewRequestFragment$3;
.super Ljava/lang/Object;
.source "ViewRequestFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/requests/ViewRequestFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/requests/ViewRequestFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/requests/ViewRequestFragment;

    .prologue
    .line 196
    iput-object p1, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$3;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 199
    sget-object v1, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;->INSTANCE:Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;

    iget-object v2, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$3;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-virtual {v2}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;->getBelvedere(Landroid/content/Context;)Lcom/zendesk/belvedere/Belvedere;

    move-result-object v0

    .line 200
    .local v0, "belvedere":Lcom/zendesk/belvedere/Belvedere;
    iget-object v1, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$3;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-virtual {v1}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/zendesk/belvedere/Belvedere;->showDialog(Landroid/support/v4/app/FragmentManager;)V

    .line 201
    return-void
.end method
