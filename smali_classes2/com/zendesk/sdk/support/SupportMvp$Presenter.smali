.class public interface abstract Lcom/zendesk/sdk/support/SupportMvp$Presenter;
.super Ljava/lang/Object;
.source "SupportMvp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/support/SupportMvp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Presenter"
.end annotation


# virtual methods
.method public abstract determineFirstScreen(Landroid/os/Bundle;)V
.end method

.method public abstract initWithBundle(Landroid/os/Bundle;)V
.end method

.method public abstract onErrorWithRetry(Lcom/zendesk/sdk/support/SupportMvp$ErrorType;Lcom/zendesk/sdk/network/RetryAction;)V
.end method

.method public abstract onLoad()V
.end method

.method public abstract onPause()V
.end method

.method public abstract onResume(Lcom/zendesk/sdk/support/SupportMvp$View;)V
.end method

.method public abstract onSearchSubmit(Ljava/lang/String;)V
.end method

.method public abstract shouldShowConversationsMenuItem()Z
.end method

.method public abstract shouldShowSearchMenuItem()Z
.end method
