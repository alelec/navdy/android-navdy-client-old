.class Lcom/zendesk/sdk/support/ViewArticleActivity$ArticleAttachmentAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ViewArticleActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/support/ViewArticleActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ArticleAttachmentAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/zendesk/sdk/model/helpcenter/Attachment;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/Attachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 554
    .local p2, "attachments":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/Attachment;>;"
    sget v0, Lcom/zendesk/sdk/R$layout;->row_article_attachment:I

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 555
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 562
    instance-of v1, p2, Lcom/zendesk/sdk/support/ViewArticleActivity$ArticleAttachmentRow;

    if-eqz v1, :cond_0

    move-object v0, p2

    .line 563
    check-cast v0, Lcom/zendesk/sdk/support/ViewArticleActivity$ArticleAttachmentRow;

    .line 568
    .local v0, "sectionRow":Lcom/zendesk/sdk/support/ViewArticleActivity$ArticleAttachmentRow;
    :goto_0
    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/support/ViewArticleActivity$ArticleAttachmentAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/zendesk/sdk/model/helpcenter/Attachment;

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/support/ViewArticleActivity$ArticleAttachmentRow;->bind(Lcom/zendesk/sdk/model/helpcenter/Attachment;)V

    .line 570
    invoke-virtual {v0}, Lcom/zendesk/sdk/support/ViewArticleActivity$ArticleAttachmentRow;->getView()Landroid/view/View;

    move-result-object v1

    return-object v1

    .line 565
    .end local v0    # "sectionRow":Lcom/zendesk/sdk/support/ViewArticleActivity$ArticleAttachmentRow;
    :cond_0
    new-instance v0, Lcom/zendesk/sdk/support/ViewArticleActivity$ArticleAttachmentRow;

    invoke-virtual {p0}, Lcom/zendesk/sdk/support/ViewArticleActivity$ArticleAttachmentAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/zendesk/sdk/support/ViewArticleActivity$ArticleAttachmentRow;-><init>(Landroid/content/Context;)V

    .restart local v0    # "sectionRow":Lcom/zendesk/sdk/support/ViewArticleActivity$ArticleAttachmentRow;
    goto :goto_0
.end method
