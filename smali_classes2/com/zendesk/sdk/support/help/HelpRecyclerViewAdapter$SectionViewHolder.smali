.class Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SectionViewHolder;
.super Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$HelpViewHolder;
.source "HelpRecyclerViewAdapter.java"


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SectionViewHolder"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;Landroid/view/View;)V
    .locals 1
    .param p1, "this$0"    # Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 253
    iput-object p1, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SectionViewHolder;->this$0:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;

    .line 254
    invoke-direct {p0, p2}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$HelpViewHolder;-><init>(Landroid/view/View;)V

    .line 255
    sget v0, Lcom/zendesk/sdk/R$id;->section_title:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SectionViewHolder;->textView:Landroid/widget/TextView;

    .line 256
    return-void
.end method


# virtual methods
.method public bindTo(Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;I)V
    .locals 3
    .param p1, "item"    # Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;
    .param p2, "position"    # I

    .prologue
    .line 261
    if-nez p1, :cond_0

    .line 262
    const-string v0, "HelpRecyclerViewAdapter"

    const-string v1, "Section item was null, cannot bind"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 267
    :goto_0
    return-void

    .line 266
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SectionViewHolder;->textView:Landroid/widget/TextView;

    invoke-interface {p1}, Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
