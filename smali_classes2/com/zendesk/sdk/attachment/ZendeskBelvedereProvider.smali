.class public final enum Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;
.super Ljava/lang/Enum;
.source "ZendeskBelvedereProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider$BelvedereZendeskLogger;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;

.field public static final enum INSTANCE:Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;


# instance fields
.field private mBelvedere:Lcom/zendesk/belvedere/Belvedere;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 11
    new-instance v0, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;->INSTANCE:Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;

    .line 10
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;

    sget-object v1, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;->INSTANCE:Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;

    aput-object v1, v0, v2

    sput-object v0, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;->$VALUES:[Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 10
    const-class v0, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;

    return-object v0
.end method

.method public static values()[Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;->$VALUES:[Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;

    invoke-virtual {v0}, [Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;

    return-object v0
.end method


# virtual methods
.method public getBelvedere(Landroid/content/Context;)Lcom/zendesk/belvedere/Belvedere;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;->mBelvedere:Lcom/zendesk/belvedere/Belvedere;

    if-nez v0, :cond_0

    .line 18
    const-class v1, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;

    monitor-enter v1

    .line 19
    :try_start_0
    invoke-static {p1}, Lcom/zendesk/belvedere/Belvedere;->from(Landroid/content/Context;)Lcom/zendesk/belvedere/BelvedereConfig$Builder;

    move-result-object v0

    const/4 v2, 0x1

    .line 20
    invoke-virtual {v0, v2}, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->withAllowMultiple(Z)Lcom/zendesk/belvedere/BelvedereConfig$Builder;

    move-result-object v0

    const-string v2, "image/*"

    .line 21
    invoke-virtual {v0, v2}, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->withContentType(Ljava/lang/String;)Lcom/zendesk/belvedere/BelvedereConfig$Builder;

    move-result-object v0

    new-instance v2, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider$BelvedereZendeskLogger;

    invoke-direct {v2}, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider$BelvedereZendeskLogger;-><init>()V

    .line 22
    invoke-virtual {v0, v2}, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->withCustomLogger(Lcom/zendesk/belvedere/BelvedereLogger;)Lcom/zendesk/belvedere/BelvedereConfig$Builder;

    move-result-object v0

    .line 23
    invoke-static {}, Lcom/zendesk/logger/Logger;->isLoggable()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->withDebug(Z)Lcom/zendesk/belvedere/BelvedereConfig$Builder;

    move-result-object v0

    .line 24
    invoke-virtual {v0}, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->build()Lcom/zendesk/belvedere/Belvedere;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;->mBelvedere:Lcom/zendesk/belvedere/Belvedere;

    .line 25
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 28
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;->mBelvedere:Lcom/zendesk/belvedere/Belvedere;

    return-object v0

    .line 25
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
