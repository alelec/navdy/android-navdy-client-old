.class public Lcom/zendesk/sdk/model/SdkConfiguration;
.super Ljava/lang/Object;
.source "SdkConfiguration.java"


# static fields
.field private static final BEARER_FORMAT:Ljava/lang/String; = "Bearer %s"


# instance fields
.field private final mAccessToken:Lcom/zendesk/sdk/model/access/AccessToken;

.field private final mMobileSettings:Lcom/zendesk/sdk/model/settings/SafeMobileSettings;


# direct methods
.method public constructor <init>(Lcom/zendesk/sdk/model/access/AccessToken;Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)V
    .locals 0
    .param p1, "accessToken"    # Lcom/zendesk/sdk/model/access/AccessToken;
    .param p2, "mobileSettings"    # Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/zendesk/sdk/model/SdkConfiguration;->mAccessToken:Lcom/zendesk/sdk/model/access/AccessToken;

    .line 26
    iput-object p2, p0, Lcom/zendesk/sdk/model/SdkConfiguration;->mMobileSettings:Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    .line 27
    return-void
.end method


# virtual methods
.method public getAccessToken()Lcom/zendesk/sdk/model/access/AccessToken;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/zendesk/sdk/model/SdkConfiguration;->mAccessToken:Lcom/zendesk/sdk/model/access/AccessToken;

    return-object v0
.end method

.method public getBearerAuthorizationHeader()Ljava/lang/String;
    .locals 5

    .prologue
    .line 54
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Bearer %s"

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/zendesk/sdk/model/SdkConfiguration;->mAccessToken:Lcom/zendesk/sdk/model/access/AccessToken;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/model/SdkConfiguration;->mAccessToken:Lcom/zendesk/sdk/model/access/AccessToken;

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/access/AccessToken;->getAccessToken()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getMobileSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/zendesk/sdk/model/SdkConfiguration;->mMobileSettings:Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    return-object v0
.end method
