.class public Lcom/zendesk/sdk/model/request/UserFieldOption;
.super Ljava/lang/Object;
.source "UserFieldOption.java"


# instance fields
.field private id:Ljava/lang/Long;

.field private name:Ljava/lang/String;

.field private rawName:Ljava/lang/String;

.field private value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/UserFieldOption;->id:Ljava/lang/Long;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/UserFieldOption;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getRawName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/UserFieldOption;->rawName:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/UserFieldOption;->value:Ljava/lang/String;

    return-object v0
.end method
