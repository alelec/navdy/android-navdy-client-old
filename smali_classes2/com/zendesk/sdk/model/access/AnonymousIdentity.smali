.class public Lcom/zendesk/sdk/model/access/AnonymousIdentity;
.super Ljava/lang/Object;
.source "AnonymousIdentity.java"

# interfaces
.implements Lcom/zendesk/sdk/model/access/Identity;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;
    }
.end annotation


# instance fields
.field private email:Ljava/lang/String;

.field private externalId:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private sdkGuid:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    return-void
.end method

.method synthetic constructor <init>(Lcom/zendesk/sdk/model/access/AnonymousIdentity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/zendesk/sdk/model/access/AnonymousIdentity$1;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/zendesk/sdk/model/access/AnonymousIdentity;-><init>()V

    return-void
.end method

.method static synthetic access$102(Lcom/zendesk/sdk/model/access/AnonymousIdentity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/model/access/AnonymousIdentity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->sdkGuid:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$202(Lcom/zendesk/sdk/model/access/AnonymousIdentity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/model/access/AnonymousIdentity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->externalId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/zendesk/sdk/model/access/AnonymousIdentity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/model/access/AnonymousIdentity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->email:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$402(Lcom/zendesk/sdk/model/access/AnonymousIdentity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/model/access/AnonymousIdentity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->name:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 44
    if-ne p0, p1, :cond_1

    .line 70
    :cond_0
    :goto_0
    return v1

    .line 48
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    .line 49
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 52
    check-cast v0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;

    .line 54
    .local v0, "that":Lcom/zendesk/sdk/model/access/AnonymousIdentity;
    iget-object v3, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->email:Ljava/lang/String;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->email:Ljava/lang/String;

    iget-object v4, v0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->email:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    :cond_4
    move v1, v2

    .line 55
    goto :goto_0

    .line 54
    :cond_5
    iget-object v3, v0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->email:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 58
    :cond_6
    iget-object v3, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->externalId:Ljava/lang/String;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->externalId:Ljava/lang/String;

    iget-object v4, v0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->externalId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    :cond_7
    move v1, v2

    .line 59
    goto :goto_0

    .line 58
    :cond_8
    iget-object v3, v0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->externalId:Ljava/lang/String;

    if-nez v3, :cond_7

    .line 62
    :cond_9
    iget-object v3, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->name:Ljava/lang/String;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->name:Ljava/lang/String;

    iget-object v4, v0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    :cond_a
    move v1, v2

    .line 63
    goto :goto_0

    .line 62
    :cond_b
    iget-object v3, v0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->name:Ljava/lang/String;

    if-nez v3, :cond_a

    .line 66
    :cond_c
    iget-object v3, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->sdkGuid:Ljava/lang/String;

    if-eqz v3, :cond_d

    iget-object v3, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->sdkGuid:Ljava/lang/String;

    iget-object v4, v0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->sdkGuid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    move v1, v2

    .line 67
    goto :goto_0

    .line 66
    :cond_d
    iget-object v3, v0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->sdkGuid:Ljava/lang/String;

    if-eqz v3, :cond_0

    goto :goto_1
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 89
    sget-object v0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->isCoppaEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->email:Ljava/lang/String;

    goto :goto_0
.end method

.method public getExternalId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->externalId:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 99
    sget-object v0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->isCoppaEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSdkGuid()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 118
    iget-object v0, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->sdkGuid:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 75
    iget-object v2, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->sdkGuid:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->sdkGuid:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 76
    .local v0, "result":I
    :goto_0
    mul-int/lit8 v3, v0, 0x1f

    iget-object v2, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->externalId:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->externalId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 77
    mul-int/lit8 v3, v0, 0x1f

    iget-object v2, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->email:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->email:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 78
    mul-int/lit8 v2, v0, 0x1f

    iget-object v3, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->name:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 79
    return v0

    .end local v0    # "result":I
    :cond_1
    move v0, v1

    .line 75
    goto :goto_0

    .restart local v0    # "result":I
    :cond_2
    move v2, v1

    .line 76
    goto :goto_1

    :cond_3
    move v2, v1

    .line 77
    goto :goto_2
.end method

.method public reloadGuid()V
    .locals 1

    .prologue
    .line 130
    sget-object v0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->storage()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/storage/StorageStore;->identityStorage()Lcom/zendesk/sdk/storage/IdentityStorage;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/storage/IdentityStorage;->getUUID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->sdkGuid:Ljava/lang/String;

    .line 131
    return-void
.end method
