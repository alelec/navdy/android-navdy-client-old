.class public Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;
.super Ljava/lang/Object;
.source "AnonymousIdentity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/model/access/AnonymousIdentity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private email:Ljava/lang/String;

.field private externalId:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private sdkGuid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 138
    const-class v0, Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    sget-object v0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->storage()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/storage/StorageStore;->identityStorage()Lcom/zendesk/sdk/storage/IdentityStorage;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/storage/IdentityStorage;->getUUID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;->sdkGuid:Ljava/lang/String;

    .line 150
    return-void
.end method


# virtual methods
.method public build()Lcom/zendesk/sdk/model/access/Identity;
    .locals 4

    .prologue
    .line 203
    new-instance v0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/zendesk/sdk/model/access/AnonymousIdentity;-><init>(Lcom/zendesk/sdk/model/access/AnonymousIdentity$1;)V

    .line 205
    .local v0, "user":Lcom/zendesk/sdk/model/access/AnonymousIdentity;
    iget-object v1, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;->sdkGuid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->access$102(Lcom/zendesk/sdk/model/access/AnonymousIdentity;Ljava/lang/String;)Ljava/lang/String;

    .line 206
    iget-object v1, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;->externalId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->access$202(Lcom/zendesk/sdk/model/access/AnonymousIdentity;Ljava/lang/String;)Ljava/lang/String;

    .line 208
    sget-object v1, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->isCoppaEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 209
    sget-object v1, Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Ignoring name and / or email because we are in COPPA mode"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 215
    :goto_0
    return-object v0

    .line 211
    :cond_0
    iget-object v1, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;->email:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->access$302(Lcom/zendesk/sdk/model/access/AnonymousIdentity;Ljava/lang/String;)Ljava/lang/String;

    .line 212
    iget-object v1, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->access$402(Lcom/zendesk/sdk/model/access/AnonymousIdentity;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method

.method public withEmailIdentifier(Ljava/lang/String;)Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;
    .locals 0
    .param p1, "email"    # Ljava/lang/String;

    .prologue
    .line 176
    iput-object p1, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;->email:Ljava/lang/String;

    .line 177
    return-object p0
.end method

.method public withExternalIdentifier(Ljava/lang/String;)Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;
    .locals 0
    .param p1, "externalId"    # Ljava/lang/String;

    .prologue
    .line 191
    iput-object p1, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;->externalId:Ljava/lang/String;

    .line 192
    return-object p0
.end method

.method public withNameIdentifier(Ljava/lang/String;)Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;->name:Ljava/lang/String;

    .line 163
    return-object p0
.end method
