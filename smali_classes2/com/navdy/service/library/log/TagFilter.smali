.class public Lcom/navdy/service/library/log/TagFilter;
.super Ljava/lang/Object;
.source "TagFilter.java"

# interfaces
.implements Lcom/navdy/service/library/log/Filter;


# instance fields
.field private invert:Z

.field private pattern:Ljava/util/regex/Pattern;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "regex"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 47
    invoke-direct {p0, p1, v0, v0}, Lcom/navdy/service/library/log/TagFilter;-><init>(Ljava/lang/String;IZ)V

    .line 48
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IZ)V
    .locals 1
    .param p1, "regex"    # Ljava/lang/String;
    .param p2, "flags"    # I
    .param p3, "invert"    # Z

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/service/library/log/TagFilter;->invert:Z

    .line 42
    invoke-static {p1, p2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/log/TagFilter;->pattern:Ljava/util/regex/Pattern;

    .line 43
    iput-boolean p3, p0, Lcom/navdy/service/library/log/TagFilter;->invert:Z

    .line 44
    return-void
.end method

.method public constructor <init>(Ljava/util/regex/Pattern;Z)V
    .locals 1
    .param p1, "pattern"    # Ljava/util/regex/Pattern;
    .param p2, "invert"    # Z

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/service/library/log/TagFilter;->invert:Z

    .line 51
    iput-object p1, p0, Lcom/navdy/service/library/log/TagFilter;->pattern:Ljava/util/regex/Pattern;

    .line 52
    iput-boolean p2, p0, Lcom/navdy/service/library/log/TagFilter;->invert:Z

    .line 53
    return-void
.end method

.method public static varargs block([Ljava/lang/String;)Lcom/navdy/service/library/log/TagFilter;
    .locals 4
    .param p0, "tags"    # [Ljava/lang/String;

    .prologue
    .line 23
    new-instance v0, Lcom/navdy/service/library/log/TagFilter;

    invoke-static {p0}, Lcom/navdy/service/library/log/TagFilter;->startsWithAny([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/log/TagFilter;-><init>(Ljava/lang/String;IZ)V

    return-object v0
.end method

.method public static varargs pass([Ljava/lang/String;)Lcom/navdy/service/library/log/TagFilter;
    .locals 2
    .param p0, "tags"    # [Ljava/lang/String;

    .prologue
    .line 17
    new-instance v0, Lcom/navdy/service/library/log/TagFilter;

    invoke-static {p0}, Lcom/navdy/service/library/log/TagFilter;->startsWithAny([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/TagFilter;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static varargs startsWithAny([Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "tags"    # [Ljava/lang/String;

    .prologue
    .line 29
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "^(?:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 30
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "|"

    invoke-static {v1, p0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public matches(ILjava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "logLevel"    # I
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    .line 57
    iget-object v1, p0, Lcom/navdy/service/library/log/TagFilter;->pattern:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    .line 58
    .local v0, "matches":Z
    iget-boolean v1, p0, Lcom/navdy/service/library/log/TagFilter;->invert:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .end local v0    # "matches":Z
    :cond_0
    :goto_0
    return v0

    .restart local v0    # "matches":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
