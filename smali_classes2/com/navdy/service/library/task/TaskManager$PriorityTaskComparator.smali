.class Lcom/navdy/service/library/task/TaskManager$PriorityTaskComparator;
.super Ljava/lang/Object;
.source "TaskManager.java"

# interfaces
.implements Ljava/util/Comparator;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/task/TaskManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PriorityTaskComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Ljava/lang/Runnable;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/task/TaskManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/task/TaskManager$1;

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/navdy/service/library/task/TaskManager$PriorityTaskComparator;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 87
    check-cast p1, Ljava/lang/Runnable;

    check-cast p2, Ljava/lang/Runnable;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/service/library/task/TaskManager$PriorityTaskComparator;->compare(Ljava/lang/Runnable;Ljava/lang/Runnable;)I

    move-result v0

    return v0
.end method

.method public compare(Ljava/lang/Runnable;Ljava/lang/Runnable;)I
    .locals 1
    .param p1, "left"    # Ljava/lang/Runnable;
    .param p2, "right"    # Ljava/lang/Runnable;

    .prologue
    .line 90
    check-cast p1, Lcom/navdy/service/library/task/TaskManager$PriorityTask;

    .end local p1    # "left":Ljava/lang/Runnable;
    check-cast p2, Lcom/navdy/service/library/task/TaskManager$PriorityTask;

    .end local p2    # "right":Ljava/lang/Runnable;
    invoke-virtual {p1, p2}, Lcom/navdy/service/library/task/TaskManager$PriorityTask;->compareTo(Lcom/navdy/service/library/task/TaskManager$PriorityTask;)I

    move-result v0

    return v0
.end method
