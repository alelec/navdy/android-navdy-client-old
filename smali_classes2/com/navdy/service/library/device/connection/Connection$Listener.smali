.class public interface abstract Lcom/navdy/service/library/device/connection/Connection$Listener;
.super Ljava/lang/Object;
.source "Connection.java"

# interfaces
.implements Lcom/navdy/service/library/util/Listenable$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/device/connection/Connection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onConnected(Lcom/navdy/service/library/device/connection/Connection;)V
.end method

.method public abstract onConnectionFailed(Lcom/navdy/service/library/device/connection/Connection;Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;)V
.end method

.method public abstract onDisconnected(Lcom/navdy/service/library/device/connection/Connection;Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V
.end method
