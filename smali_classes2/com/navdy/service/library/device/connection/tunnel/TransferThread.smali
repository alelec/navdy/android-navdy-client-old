.class Lcom/navdy/service/library/device/connection/tunnel/TransferThread;
.super Ljava/lang/Thread;
.source "TransferThread.java"


# static fields
.field static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field final activityTracker:Lcom/navdy/service/library/util/NetworkActivityTracker;

.field private volatile canceled:Z

.field final inStream:Ljava/io/InputStream;

.field final outStream:Ljava/io/OutputStream;

.field final parentThread:Lcom/navdy/service/library/device/connection/tunnel/Tunnel;

.field final sending:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/device/connection/tunnel/Tunnel;Ljava/io/InputStream;Ljava/io/OutputStream;Z)V
    .locals 3
    .param p1, "parentThread"    # Lcom/navdy/service/library/device/connection/tunnel/Tunnel;
    .param p2, "inStream"    # Ljava/io/InputStream;
    .param p3, "outStream"    # Ljava/io/OutputStream;
    .param p4, "sending"    # Z

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 27
    sget-object v0, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "new transfer thread ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " -> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 28
    const-class v0, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->setName(Ljava/lang/String;)V

    .line 29
    iput-object p1, p0, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->parentThread:Lcom/navdy/service/library/device/connection/tunnel/Tunnel;

    .line 30
    iput-object p2, p0, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->inStream:Ljava/io/InputStream;

    .line 31
    iput-object p3, p0, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->outStream:Ljava/io/OutputStream;

    .line 32
    iput-boolean p4, p0, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->sending:Z

    .line 33
    iget-object v0, p1, Lcom/navdy/service/library/device/connection/tunnel/Tunnel;->transferThreads:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    invoke-static {}, Lcom/navdy/service/library/util/NetworkActivityTracker;->getInstance()Lcom/navdy/service/library/util/NetworkActivityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->activityTracker:Lcom/navdy/service/library/util/NetworkActivityTracker;

    .line 35
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->canceled:Z

    .line 73
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->inStream:Ljava/io/InputStream;

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 74
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->outStream:Ljava/io/OutputStream;

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 75
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->parentThread:Lcom/navdy/service/library/device/connection/tunnel/Tunnel;

    iget-object v0, v0, Lcom/navdy/service/library/device/connection/tunnel/Tunnel;->transferThreads:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 76
    return-void
.end method

.method public run()V
    .locals 9

    .prologue
    .line 38
    const/16 v4, 0x4000

    new-array v0, v4, [B

    .line 41
    .local v0, "buffer":[B
    :goto_0
    :try_start_0
    iget-object v4, p0, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->inStream:Ljava/io/InputStream;

    invoke-virtual {v4, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .line 42
    .local v1, "bytes":I
    if-gez v1, :cond_1

    .line 43
    sget-object v4, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "socket was closed"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    .end local v1    # "bytes":I
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->cancel()V

    .line 69
    return-void

    .line 46
    .restart local v1    # "bytes":I
    :cond_1
    :try_start_1
    sget-object v4, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 47
    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v2

    .line 48
    .local v2, "d":[B
    sget-object v4, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "transfer (%s -> %s): %d bytes%s"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->inStream:Ljava/io/InputStream;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->outStream:Ljava/io/OutputStream;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    .line 49
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const-string v8, ""

    aput-object v8, v6, v7

    .line 48
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 54
    .end local v2    # "d":[B
    :cond_2
    iget-object v4, p0, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->outStream:Ljava/io/OutputStream;

    const/4 v5, 0x0

    invoke-virtual {v4, v0, v5, v1}, Ljava/io/OutputStream;->write([BII)V

    .line 55
    iget-object v4, p0, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->outStream:Ljava/io/OutputStream;

    invoke-virtual {v4}, Ljava/io/OutputStream;->flush()V

    .line 56
    iget-boolean v4, p0, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->sending:Z

    if-eqz v4, :cond_3

    .line 57
    iget-object v4, p0, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->activityTracker:Lcom/navdy/service/library/util/NetworkActivityTracker;

    invoke-virtual {v4, v1}, Lcom/navdy/service/library/util/NetworkActivityTracker;->addBytesSent(I)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 61
    .end local v1    # "bytes":I
    :catch_0
    move-exception v3

    .line 62
    .local v3, "e":Ljava/lang/Throwable;
    iget-boolean v4, p0, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->canceled:Z

    if-nez v4, :cond_0

    .line 63
    sget-object v4, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Exception"

    invoke-virtual {v4, v5, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 59
    .end local v3    # "e":Ljava/lang/Throwable;
    .restart local v1    # "bytes":I
    :cond_3
    :try_start_2
    iget-object v4, p0, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->activityTracker:Lcom/navdy/service/library/util/NetworkActivityTracker;

    invoke-virtual {v4, v1}, Lcom/navdy/service/library/util/NetworkActivityTracker;->addBytesReceived(I)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0
.end method
