.class Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster$2;
.super Ljava/util/TimerTask;
.source "MDNSDeviceBroadcaster.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->registerRecord(Landroid/net/nsd/NsdServiceInfo;Landroid/net/nsd/NsdManager$RegistrationListener;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;

.field final synthetic val$registrationListener:Landroid/net/nsd/NsdManager$RegistrationListener;

.field final synthetic val$serviceInfo:Landroid/net/nsd/NsdServiceInfo;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;Landroid/net/nsd/NsdServiceInfo;Landroid/net/nsd/NsdManager$RegistrationListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster$2;->this$0:Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;

    iput-object p2, p0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster$2;->val$serviceInfo:Landroid/net/nsd/NsdServiceInfo;

    iput-object p3, p0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster$2;->val$registrationListener:Landroid/net/nsd/NsdManager$RegistrationListener;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 90
    sget-object v0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Register timer fired: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster$2;->val$serviceInfo:Landroid/net/nsd/NsdServiceInfo;

    invoke-virtual {v2}, Landroid/net/nsd/NsdServiceInfo;->getServiceType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster$2;->this$0:Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;

    iget-object v0, v0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->mNsdManager:Landroid/net/nsd/NsdManager;

    iget-object v1, p0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster$2;->val$serviceInfo:Landroid/net/nsd/NsdServiceInfo;

    iget-object v2, p0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster$2;->val$registrationListener:Landroid/net/nsd/NsdManager$RegistrationListener;

    invoke-virtual {v0, v1, v3, v2}, Landroid/net/nsd/NsdManager;->registerService(Landroid/net/nsd/NsdServiceInfo;ILandroid/net/nsd/NsdManager$RegistrationListener;)V

    .line 93
    iget-object v0, p0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster$2;->this$0:Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;

    iput-boolean v3, v0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->registered:Z

    .line 94
    sget-object v0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Registered: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster$2;->val$serviceInfo:Landroid/net/nsd/NsdServiceInfo;

    invoke-virtual {v2}, Landroid/net/nsd/NsdServiceInfo;->getServiceType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 95
    return-void
.end method
