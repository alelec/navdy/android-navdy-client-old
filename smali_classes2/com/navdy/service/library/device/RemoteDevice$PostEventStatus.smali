.class public final enum Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;
.super Ljava/lang/Enum;
.source "RemoteDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/device/RemoteDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PostEventStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;

.field public static final enum DISCONNECTED:Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;

.field public static final enum SEND_FAILED:Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;

.field public static final enum SUCCESS:Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;

.field public static final enum UNKNOWN_FAILURE:Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 67
    new-instance v0, Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;->SUCCESS:Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;

    .line 68
    new-instance v0, Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;

    const-string v1, "DISCONNECTED"

    invoke-direct {v0, v1, v3}, Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;->DISCONNECTED:Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;

    .line 69
    new-instance v0, Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;

    const-string v1, "SEND_FAILED"

    invoke-direct {v0, v1, v4}, Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;->SEND_FAILED:Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;

    .line 70
    new-instance v0, Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;

    const-string v1, "UNKNOWN_FAILURE"

    invoke-direct {v0, v1, v5}, Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;->UNKNOWN_FAILURE:Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;

    .line 66
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;

    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;->SUCCESS:Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;->DISCONNECTED:Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;->SEND_FAILED:Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;->UNKNOWN_FAILURE:Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;->$VALUES:[Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 66
    const-class v0, Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;->$VALUES:[Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;

    invoke-virtual {v0}, [Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;

    return-object v0
.end method
