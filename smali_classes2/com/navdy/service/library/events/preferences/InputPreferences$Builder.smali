.class public final Lcom/navdy/service/library/events/preferences/InputPreferences$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InputPreferences.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/preferences/InputPreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/preferences/InputPreferences;",
        ">;"
    }
.end annotation


# instance fields
.field public dial_sensitivity:Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

.field public serial_number:Ljava/lang/Long;

.field public use_gestures:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 93
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/preferences/InputPreferences;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/preferences/InputPreferences;

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 97
    if-nez p1, :cond_0

    .line 101
    :goto_0
    return-void

    .line 98
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/InputPreferences;->serial_number:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/InputPreferences$Builder;->serial_number:Ljava/lang/Long;

    .line 99
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/InputPreferences;->use_gestures:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/InputPreferences$Builder;->use_gestures:Ljava/lang/Boolean;

    .line 100
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/InputPreferences;->dial_sensitivity:Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/InputPreferences$Builder;->dial_sensitivity:Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/preferences/InputPreferences;
    .locals 2

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/navdy/service/library/events/preferences/InputPreferences$Builder;->checkRequiredFields()V

    .line 131
    new-instance v0, Lcom/navdy/service/library/events/preferences/InputPreferences;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/preferences/InputPreferences;-><init>(Lcom/navdy/service/library/events/preferences/InputPreferences$Builder;Lcom/navdy/service/library/events/preferences/InputPreferences$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/navdy/service/library/events/preferences/InputPreferences$Builder;->build()Lcom/navdy/service/library/events/preferences/InputPreferences;

    move-result-object v0

    return-object v0
.end method

.method public dial_sensitivity(Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;)Lcom/navdy/service/library/events/preferences/InputPreferences$Builder;
    .locals 0
    .param p1, "dial_sensitivity"    # Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

    .prologue
    .line 124
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/InputPreferences$Builder;->dial_sensitivity:Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

    .line 125
    return-object p0
.end method

.method public serial_number(Ljava/lang/Long;)Lcom/navdy/service/library/events/preferences/InputPreferences$Builder;
    .locals 0
    .param p1, "serial_number"    # Ljava/lang/Long;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/InputPreferences$Builder;->serial_number:Ljava/lang/Long;

    .line 109
    return-object p0
.end method

.method public use_gestures(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/InputPreferences$Builder;
    .locals 0
    .param p1, "use_gestures"    # Ljava/lang/Boolean;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/InputPreferences$Builder;->use_gestures:Ljava/lang/Boolean;

    .line 117
    return-object p0
.end method
