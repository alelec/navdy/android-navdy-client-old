.class public final Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CallEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/callcontrol/CallEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/callcontrol/CallEvent;",
        ">;"
    }
.end annotation


# instance fields
.field public answered:Ljava/lang/Boolean;

.field public contact_name:Ljava/lang/String;

.field public duration:Ljava/lang/Long;

.field public incoming:Ljava/lang/Boolean;

.field public number:Ljava/lang/String;

.field public start_time:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 133
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/callcontrol/CallEvent;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/callcontrol/CallEvent;

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 137
    if-nez p1, :cond_0

    .line 144
    :goto_0
    return-void

    .line 138
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/callcontrol/CallEvent;->incoming:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;->incoming:Ljava/lang/Boolean;

    .line 139
    iget-object v0, p1, Lcom/navdy/service/library/events/callcontrol/CallEvent;->answered:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;->answered:Ljava/lang/Boolean;

    .line 140
    iget-object v0, p1, Lcom/navdy/service/library/events/callcontrol/CallEvent;->number:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;->number:Ljava/lang/String;

    .line 141
    iget-object v0, p1, Lcom/navdy/service/library/events/callcontrol/CallEvent;->contact_name:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;->contact_name:Ljava/lang/String;

    .line 142
    iget-object v0, p1, Lcom/navdy/service/library/events/callcontrol/CallEvent;->start_time:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;->start_time:Ljava/lang/Long;

    .line 143
    iget-object v0, p1, Lcom/navdy/service/library/events/callcontrol/CallEvent;->duration:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;->duration:Ljava/lang/Long;

    goto :goto_0
.end method


# virtual methods
.method public answered(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;
    .locals 0
    .param p1, "answered"    # Ljava/lang/Boolean;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;->answered:Ljava/lang/Boolean;

    .line 163
    return-object p0
.end method

.method public build()Lcom/navdy/service/library/events/callcontrol/CallEvent;
    .locals 2

    .prologue
    .line 207
    invoke-virtual {p0}, Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;->checkRequiredFields()V

    .line 208
    new-instance v0, Lcom/navdy/service/library/events/callcontrol/CallEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/callcontrol/CallEvent;-><init>(Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;Lcom/navdy/service/library/events/callcontrol/CallEvent$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;->build()Lcom/navdy/service/library/events/callcontrol/CallEvent;

    move-result-object v0

    return-object v0
.end method

.method public contact_name(Ljava/lang/String;)Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;
    .locals 0
    .param p1, "contact_name"    # Ljava/lang/String;

    .prologue
    .line 178
    iput-object p1, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;->contact_name:Ljava/lang/String;

    .line 179
    return-object p0
.end method

.method public duration(Ljava/lang/Long;)Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;
    .locals 0
    .param p1, "duration"    # Ljava/lang/Long;

    .prologue
    .line 201
    iput-object p1, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;->duration:Ljava/lang/Long;

    .line 202
    return-object p0
.end method

.method public incoming(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;
    .locals 0
    .param p1, "incoming"    # Ljava/lang/Boolean;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;->incoming:Ljava/lang/Boolean;

    .line 153
    return-object p0
.end method

.method public number(Ljava/lang/String;)Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;
    .locals 0
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    .line 170
    iput-object p1, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;->number:Ljava/lang/String;

    .line 171
    return-object p0
.end method

.method public start_time(Ljava/lang/Long;)Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;
    .locals 0
    .param p1, "start_time"    # Ljava/lang/Long;

    .prologue
    .line 190
    iput-object p1, p0, Lcom/navdy/service/library/events/callcontrol/CallEvent$Builder;->start_time:Ljava/lang/Long;

    .line 191
    return-object p0
.end method
