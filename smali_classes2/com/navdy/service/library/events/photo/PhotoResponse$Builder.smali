.class public final Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PhotoResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/photo/PhotoResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/photo/PhotoResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public identifier:Ljava/lang/String;

.field public photo:Lokio/ByteString;

.field public photoChecksum:Ljava/lang/String;

.field public photoType:Lcom/navdy/service/library/events/photo/PhotoType;

.field public status:Lcom/navdy/service/library/events/RequestStatus;

.field public statusDetail:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 110
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/photo/PhotoResponse;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/photo/PhotoResponse;

    .prologue
    .line 113
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 114
    if-nez p1, :cond_0

    .line 121
    :goto_0
    return-void

    .line 115
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoResponse;->photo:Lokio/ByteString;

    iput-object v0, p0, Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;->photo:Lokio/ByteString;

    .line 116
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    iput-object v0, p0, Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 117
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoResponse;->statusDetail:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;->statusDetail:Ljava/lang/String;

    .line 118
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoResponse;->identifier:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;->identifier:Ljava/lang/String;

    .line 119
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoResponse;->photoChecksum:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;->photoChecksum:Ljava/lang/String;

    .line 120
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoResponse;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    iput-object v0, p0, Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/photo/PhotoResponse;
    .locals 2

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;->checkRequiredFields()V

    .line 171
    new-instance v0, Lcom/navdy/service/library/events/photo/PhotoResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/photo/PhotoResponse;-><init>(Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;Lcom/navdy/service/library/events/photo/PhotoResponse$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;->build()Lcom/navdy/service/library/events/photo/PhotoResponse;

    move-result-object v0

    return-object v0
.end method

.method public identifier(Ljava/lang/String;)Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;
    .locals 0
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    .line 151
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;->identifier:Ljava/lang/String;

    .line 152
    return-object p0
.end method

.method public photo(Lokio/ByteString;)Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;
    .locals 0
    .param p1, "photo"    # Lokio/ByteString;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;->photo:Lokio/ByteString;

    .line 128
    return-object p0
.end method

.method public photoChecksum(Ljava/lang/String;)Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;
    .locals 0
    .param p1, "photoChecksum"    # Ljava/lang/String;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;->photoChecksum:Ljava/lang/String;

    .line 157
    return-object p0
.end method

.method public photoType(Lcom/navdy/service/library/events/photo/PhotoType;)Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;
    .locals 0
    .param p1, "photoType"    # Lcom/navdy/service/library/events/photo/PhotoType;

    .prologue
    .line 164
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    .line 165
    return-object p0
.end method

.method public status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 136
    return-object p0
.end method

.method public statusDetail(Ljava/lang/String;)Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;
    .locals 0
    .param p1, "statusDetail"    # Ljava/lang/String;

    .prologue
    .line 143
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;->statusDetail:Ljava/lang/String;

    .line 144
    return-object p0
.end method
