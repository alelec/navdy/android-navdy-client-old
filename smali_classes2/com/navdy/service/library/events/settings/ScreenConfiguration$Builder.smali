.class public final Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ScreenConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/settings/ScreenConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/settings/ScreenConfiguration;",
        ">;"
    }
.end annotation


# instance fields
.field public marginBottom:Ljava/lang/Integer;

.field public marginLeft:Ljava/lang/Integer;

.field public marginRight:Ljava/lang/Integer;

.field public marginTop:Ljava/lang/Integer;

.field public scaleX:Ljava/lang/Float;

.field public scaleY:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 92
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/settings/ScreenConfiguration;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/settings/ScreenConfiguration;

    .prologue
    .line 95
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 96
    if-nez p1, :cond_0

    .line 103
    :goto_0
    return-void

    .line 97
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginLeft:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;->marginLeft:Ljava/lang/Integer;

    .line 98
    iget-object v0, p1, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginTop:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;->marginTop:Ljava/lang/Integer;

    .line 99
    iget-object v0, p1, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginRight:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;->marginRight:Ljava/lang/Integer;

    .line 100
    iget-object v0, p1, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginBottom:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;->marginBottom:Ljava/lang/Integer;

    .line 101
    iget-object v0, p1, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->scaleX:Ljava/lang/Float;

    iput-object v0, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;->scaleX:Ljava/lang/Float;

    .line 102
    iget-object v0, p1, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->scaleY:Ljava/lang/Float;

    iput-object v0, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;->scaleY:Ljava/lang/Float;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/settings/ScreenConfiguration;
    .locals 2

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;->checkRequiredFields()V

    .line 138
    new-instance v0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/settings/ScreenConfiguration;-><init>(Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;Lcom/navdy/service/library/events/settings/ScreenConfiguration$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;->build()Lcom/navdy/service/library/events/settings/ScreenConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public marginBottom(Ljava/lang/Integer;)Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;
    .locals 0
    .param p1, "marginBottom"    # Ljava/lang/Integer;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;->marginBottom:Ljava/lang/Integer;

    .line 122
    return-object p0
.end method

.method public marginLeft(Ljava/lang/Integer;)Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;
    .locals 0
    .param p1, "marginLeft"    # Ljava/lang/Integer;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;->marginLeft:Ljava/lang/Integer;

    .line 107
    return-object p0
.end method

.method public marginRight(Ljava/lang/Integer;)Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;
    .locals 0
    .param p1, "marginRight"    # Ljava/lang/Integer;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;->marginRight:Ljava/lang/Integer;

    .line 117
    return-object p0
.end method

.method public marginTop(Ljava/lang/Integer;)Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;
    .locals 0
    .param p1, "marginTop"    # Ljava/lang/Integer;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;->marginTop:Ljava/lang/Integer;

    .line 112
    return-object p0
.end method

.method public scaleX(Ljava/lang/Float;)Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;
    .locals 0
    .param p1, "scaleX"    # Ljava/lang/Float;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;->scaleX:Ljava/lang/Float;

    .line 127
    return-object p0
.end method

.method public scaleY(Ljava/lang/Float;)Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;
    .locals 0
    .param p1, "scaleY"    # Ljava/lang/Float;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;->scaleY:Ljava/lang/Float;

    .line 132
    return-object p0
.end method
