.class public Lcom/navdy/service/library/Version;
.super Ljava/lang/Object;
.source "Version.java"


# static fields
.field public static final PROTOCOL_VERSION:Lcom/navdy/service/library/Version;


# instance fields
.field public final majorVersion:I

.field public final minorVersion:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 12
    new-instance v0, Lcom/navdy/service/library/Version;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/Version;-><init>(II)V

    sput-object v0, Lcom/navdy/service/library/Version;->PROTOCOL_VERSION:Lcom/navdy/service/library/Version;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1, "majorVersion"    # I
    .param p2, "minorVersion"    # I

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput p1, p0, Lcom/navdy/service/library/Version;->majorVersion:I

    .line 19
    iput p2, p0, Lcom/navdy/service/library/Version;->minorVersion:I

    .line 20
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 24
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/navdy/service/library/Version;->majorVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/navdy/service/library/Version;->minorVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
