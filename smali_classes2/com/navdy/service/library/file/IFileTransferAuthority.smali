.class public interface abstract Lcom/navdy/service/library/file/IFileTransferAuthority;
.super Ljava/lang/Object;
.source "IFileTransferAuthority.java"


# virtual methods
.method public abstract getDirectoryForFileType(Lcom/navdy/service/library/events/file/FileType;)Ljava/lang/String;
.end method

.method public abstract getFileToSend(Lcom/navdy/service/library/events/file/FileType;)Ljava/lang/String;
.end method

.method public abstract isFileTypeAllowed(Lcom/navdy/service/library/events/file/FileType;)Z
.end method

.method public abstract onFileSent(Lcom/navdy/service/library/events/file/FileType;)V
.end method
