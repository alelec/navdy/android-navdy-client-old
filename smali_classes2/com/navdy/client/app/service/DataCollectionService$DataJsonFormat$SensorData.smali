.class Lcom/navdy/client/app/service/DataCollectionService$DataJsonFormat$SensorData;
.super Ljava/lang/Object;
.source "DataCollectionService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/service/DataCollectionService$DataJsonFormat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SensorData"
.end annotation


# static fields
.field static final ACCURACY:Ljava/lang/String; = "accuracy"

.field static final AZIMUTH:Ljava/lang/String; = "azimuth"

.field static final DT:Ljava/lang/String; = "dt"

.field static final PITCH:Ljava/lang/String; = "pitch"

.field static final ROLL:Ljava/lang/String; = "roll"

.field static final TIMESTAMP:Ljava/lang/String; = "timestamp"

.field static final W:Ljava/lang/String; = "w"

.field static final X:Ljava/lang/String; = "x"

.field static final Y:Ljava/lang/String; = "y"

.field static final Z:Ljava/lang/String; = "z"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
