.class Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$13;
.super Ljava/lang/Object;
.source "NavdyRouteHandler.java"

# interfaces
.implements Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->calculatePendingRoute(Lcom/navdy/client/app/framework/models/Destination;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

.field final synthetic val$destination:Lcom/navdy/client/app/framework/models/Destination;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    .prologue
    .line 736
    iput-object p1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$13;->this$0:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    iput-object p2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$13;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreCalculation(Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;)V
    .locals 1
    .param p1, "routeHandle"    # Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 739
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$13;->this$0:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-static {v0, p1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$1602(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;)Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;

    .line 740
    return-void
.end method

.method public onRouteCalculated(Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;Lcom/here/android/mpa/routing/Route;)V
    .locals 10
    .param p1, "error"    # Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "route"    # Lcom/here/android/mpa/routing/Route;

    .prologue
    const v5, 0xfffffff

    const/4 v2, 0x0

    .line 744
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$13;->this$0:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-static {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$000(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    move-result-object v0

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->CALCULATING_PENDING_ROUTE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    if-eq v0, v1, :cond_1

    .line 787
    :cond_0
    :goto_0
    return-void

    .line 747
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$13;->this$0:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->PENDING_ROUTE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$002(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;)Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    .line 748
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$13;->this$0:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-static {v0, v2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$1602(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;)Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;

    .line 750
    sget-object v0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;->NONE:Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;

    if-eq p1, v0, :cond_2

    .line 751
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$900()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "failed to calculate pending trip"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 754
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$900()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestNewRoute pending route has error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", setting state to CALCULATION_FAILED"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 758
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$13;->this$0:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;->NO_ROUTES:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$502(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;)Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    .line 779
    :goto_1
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$13;->this$0:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-static {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$600(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->isHudMapEngineReady()Z

    move-result v0

    if-nez v0, :cond_0

    .line 780
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$13;->this$0:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    new-instance v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$13$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$13$1;-><init>(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$13;)V

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$1100(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$CallListener;)V

    goto :goto_0

    .line 761
    :cond_2
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$900()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "requestNewRoute pending route success, setting state to PENDING_TRIP"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 764
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$13;->this$0:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;->NONE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$502(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;)Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    .line 765
    iget-object v9, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$13;->this$0:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    new-instance v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$13;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    new-instance v3, Lcom/here/android/mpa/common/GeoPolyline;

    .line 768
    invoke-virtual {p2}, Lcom/here/android/mpa/routing/Route;->getRouteGeometry()Ljava/util/List;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/here/android/mpa/common/GeoPolyline;-><init>(Ljava/util/List;)V

    sget-object v4, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->DISABLED:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    .line 771
    invoke-virtual {p2, v4, v5}, Lcom/here/android/mpa/routing/Route;->getTta(Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;I)Lcom/here/android/mpa/routing/RouteTta;

    move-result-object v4

    invoke-virtual {v4}, Lcom/here/android/mpa/routing/RouteTta;->getDuration()I

    move-result v6

    sget-object v4, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->OPTIMAL:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    .line 772
    invoke-virtual {p2, v4, v5}, Lcom/here/android/mpa/routing/Route;->getTta(Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;I)Lcom/here/android/mpa/routing/RouteTta;

    move-result-object v4

    invoke-virtual {v4}, Lcom/here/android/mpa/routing/RouteTta;->getDuration()I

    move-result v7

    .line 773
    invoke-virtual {p2}, Lcom/here/android/mpa/routing/Route;->getLength()I

    move-result v8

    move-object v4, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v8}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;-><init>(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/String;Lcom/here/android/mpa/common/GeoPolyline;Ljava/lang/String;Ljava/lang/String;III)V

    .line 765
    invoke-static {v9, v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$102(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    .line 776
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$13;->this$0:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-static {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$1800(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$13;->this$0:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-static {v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$1700(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/32 v2, 0x895440

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method
