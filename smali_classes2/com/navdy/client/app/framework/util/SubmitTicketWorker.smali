.class public Lcom/navdy/client/app/framework/util/SubmitTicketWorker;
.super Ljava/lang/Object;
.source "SubmitTicketWorker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/util/SubmitTicketWorker$OnFinishSubmittingTickets;
    }
.end annotation


# static fields
.field private static final ANDROID_BACKGROUND_SUBMISSION:Ljava/lang/String; = "android_background_submission"

.field private static final INTERNET_ERROR_CODE:I = -0x1

.field private static final MALFORMED_TICKET:I = 0x1a6

.field private static final TEN_DAYS_IN_MILLIS:J

.field private static final VERBOSE:Z = true

.field private static final ZIP_MIME_TYPE:Ljava/lang/String; = "application/zip"

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private callback:Lcom/navdy/client/app/framework/util/SubmitTicketWorker$OnFinishSubmittingTickets;

.field private displayLog:Ljava/io/File;

.field private failCount:I

.field private isRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final providerStore:Lcom/zendesk/sdk/network/impl/ProviderStore;

.field private showSuccessNotification:Z

.field private successCount:I

.field private ticketCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 48
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->logger:Lcom/navdy/service/library/log/Logger;

    .line 52
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->TEN_DAYS_IN_MILLIS:J

    return-void
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    sget-object v0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->provider()Lcom/zendesk/sdk/network/impl/ProviderStore;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->providerStore:Lcom/zendesk/sdk/network/impl/ProviderStore;

    .line 59
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->isRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 61
    iput v1, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->successCount:I

    .line 62
    iput v1, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->failCount:I

    .line 63
    iput v1, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->ticketCount:I

    .line 65
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->incrementFailureCount()V

    return-void
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200()J
    .locals 2

    .prologue
    .line 45
    sget-wide v0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->TEN_DAYS_IN_MILLIS:J

    return-wide v0
.end method

.method static synthetic access$300(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->incrementSuccessCount()V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->displayLog:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$402(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;Ljava/io/File;)Ljava/io/File;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/util/SubmitTicketWorker;
    .param p1, "x1"    # Ljava/io/File;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->displayLog:Ljava/io/File;

    return-object p1
.end method

.method static synthetic access$500(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;Lorg/json/JSONObject;[Ljava/io/File;Ljava/io/File;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/util/SubmitTicketWorker;
    .param p1, "x1"    # Lorg/json/JSONObject;
    .param p2, "x2"    # [Ljava/io/File;
    .param p3, "x3"    # Ljava/io/File;

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->attachLogsAndUpdateTicketAction(Lorg/json/JSONObject;[Ljava/io/File;Ljava/io/File;)V

    return-void
.end method

.method static synthetic access$600(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;Lorg/json/JSONObject;Ljava/io/File;Ljava/io/File;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/util/SubmitTicketWorker;
    .param p1, "x1"    # Lorg/json/JSONObject;
    .param p2, "x2"    # Ljava/io/File;
    .param p3, "x3"    # Ljava/io/File;

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->submitTicket(Lorg/json/JSONObject;Ljava/io/File;Ljava/io/File;)V

    return-void
.end method

.method static synthetic access$700(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;Ljava/io/File;Lorg/json/JSONObject;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/util/SubmitTicketWorker;
    .param p1, "x1"    # Ljava/io/File;
    .param p2, "x2"    # Lorg/json/JSONObject;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->sendToZenddesk(Ljava/io/File;Lorg/json/JSONObject;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$802(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/util/SubmitTicketWorker;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->showSuccessNotification:Z

    return p1
.end method

.method static synthetic access$900(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;[Ljava/io/File;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/util/SubmitTicketWorker;
    .param p1, "x1"    # [Ljava/io/File;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->processAllTickets([Ljava/io/File;)V

    return-void
.end method

.method private addCustomField(Ljava/util/ArrayList;JLjava/lang/String;)V
    .locals 2
    .param p2, "field"    # J
    .param p4, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/zendesk/sdk/model/request/CustomField;",
            ">;J",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 372
    .local p1, "customFields":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/zendesk/sdk/model/request/CustomField;>;"
    invoke-static {p4}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 373
    new-instance v0, Lcom/zendesk/sdk/model/request/CustomField;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {v0, v1, p4}, Lcom/zendesk/sdk/model/request/CustomField;-><init>(Ljava/lang/Long;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 375
    :cond_0
    return-void
.end method

.method private addCustomFieldFromSharedPref(Ljava/util/ArrayList;JLjava/lang/String;)V
    .locals 4
    .param p2, "field"    # J
    .param p4, "sharedPrefKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/zendesk/sdk/model/request/CustomField;",
            ">;J",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 378
    .local p1, "customFields":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/zendesk/sdk/model/request/CustomField;>;"
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 379
    .local v0, "customerPrefs":Landroid/content/SharedPreferences;
    const/4 v2, 0x0

    invoke-interface {v0, p4, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 380
    .local v1, "value":Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 381
    new-instance v2, Lcom/zendesk/sdk/model/request/CustomField;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/zendesk/sdk/model/request/CustomField;-><init>(Ljava/lang/Long;Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 383
    :cond_0
    return-void
.end method

.method private attachLogsAndUpdateTicketAction(Lorg/json/JSONObject;[Ljava/io/File;Ljava/io/File;)V
    .locals 4
    .param p1, "ticketContents"    # Lorg/json/JSONObject;
    .param p2, "logs"    # [Ljava/io/File;
    .param p3, "attachmentsZipFile"    # Ljava/io/File;

    .prologue
    .line 424
    sget-object v1, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "attachLogsAndUpdateTicketAction"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 425
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, p2, v2}, Lcom/navdy/service/library/util/IOUtils;->compressFilesToZip(Landroid/content/Context;[Ljava/io/File;Ljava/lang/String;)V

    .line 427
    :try_start_0
    const-string v1, "ticket_has_display_log"

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 428
    const-string v1, "ticket_action"

    const-string v2, ""

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "upload_await_hud_log"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 429
    const-string v1, "ticket_action"

    const-string v2, "upload_delete"

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 434
    :cond_0
    :goto_0
    return-void

    .line 431
    :catch_0
    move-exception v0

    .line 432
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "JSON Exception found: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static convertStreamToString(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 8
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 185
    :try_start_0
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/InputStreamReader;

    invoke-direct {v5, p0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 186
    .local v0, "bufferedReader":Ljava/io/BufferedReader;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 189
    .local v3, "stringBuilder":Ljava/lang/StringBuilder;
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    .local v2, "line":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 190
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 201
    .end local v0    # "bufferedReader":Ljava/io/BufferedReader;
    .end local v2    # "line":Ljava/lang/String;
    .end local v3    # "stringBuilder":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v1

    .line 202
    .local v1, "e":Ljava/lang/Exception;
    sget-object v5, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception found: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 203
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-object v4

    .line 193
    .restart local v0    # "bufferedReader":Ljava/io/BufferedReader;
    .restart local v2    # "line":Ljava/lang/String;
    .restart local v3    # "stringBuilder":Ljava/lang/StringBuilder;
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 195
    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 196
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 198
    :cond_1
    sget-object v5, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "convertStreamToString failed"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method private declared-synchronized fetchHudLogAndProcessTickets([Ljava/io/File;)V
    .locals 3
    .param p1, "folders"    # [Ljava/io/File;

    .prologue
    .line 386
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "fetchHudLogAndProcessTickets"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 387
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->displayLog:Ljava/io/File;

    if-nez v0, :cond_0

    .line 389
    new-instance v0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$4;

    invoke-direct {v0, p0, p1}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$4;-><init>(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;[Ljava/io/File;)V

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/SupportTicketService;->collectHudLog(Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 414
    :goto_0
    monitor-exit p0

    return-void

    .line 411
    :cond_0
    :try_start_1
    sget-object v0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "hud log exists: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->displayLog:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 412
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->processAllTickets([Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 386
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized finishIfAllTicketsAttemptedSubmission()V
    .locals 3

    .prologue
    .line 453
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->successCount:I

    iget v1, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->failCount:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->ticketCount:I

    if-ne v0, v1, :cond_0

    .line 454
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->isRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 455
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->callback:Lcom/navdy/client/app/framework/util/SubmitTicketWorker$OnFinishSubmittingTickets;

    iget v1, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->failCount:I

    iget-boolean v2, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->showSuccessNotification:Z

    invoke-interface {v0, v1, v2}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$OnFinishSubmittingTickets;->onFinish(IZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 457
    :cond_0
    monitor-exit p0

    return-void

    .line 453
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static getJSONObjectFromFile(Ljava/io/File;)Lorg/json/JSONObject;
    .locals 6
    .param p0, "file"    # Ljava/io/File;

    .prologue
    .line 153
    :try_start_0
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->getStringFromFile(Ljava/io/File;)Ljava/lang/String;

    move-result-object v1

    .line 154
    .local v1, "fileString":Ljava/lang/String;
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 156
    .local v2, "jsonObject":Lorg/json/JSONObject;
    sget-object v3, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "jsonObject status: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    .end local v1    # "fileString":Ljava/lang/String;
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    :goto_0
    return-object v2

    .line 159
    :catch_0
    move-exception v0

    .line 160
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getting the JSON Object from file failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 161
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static getStringFromFile(Ljava/io/File;)Ljava/lang/String;
    .locals 7
    .param p0, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 167
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 168
    .local v1, "fileInputStream":Ljava/io/FileInputStream;
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->convertStreamToString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    .line 169
    .local v2, "fileString":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 171
    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 179
    .end local v1    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v2    # "fileString":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 174
    .restart local v1    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v2    # "fileString":Ljava/lang/String;
    :cond_0
    sget-object v4, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "getStringFromFile failed"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v3

    .line 175
    goto :goto_0

    .line 177
    .end local v1    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v2    # "fileString":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 178
    .local v0, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception found: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    move-object v2, v3

    .line 179
    goto :goto_0
.end method

.method private declared-synchronized incrementFailureCount()V
    .locals 3

    .prologue
    .line 447
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->failCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->failCount:I

    .line 448
    sget-object v0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "incrementFailureCount: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->failCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 449
    invoke-direct {p0}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->finishIfAllTicketsAttemptedSubmission()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 450
    monitor-exit p0

    return-void

    .line 447
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized incrementSuccessCount()V
    .locals 3

    .prologue
    .line 441
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->successCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->successCount:I

    .line 442
    sget-object v0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "incrementSuccessCount: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->successCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 443
    invoke-direct {p0}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->finishIfAllTicketsAttemptedSubmission()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 444
    monitor-exit p0

    return-void

    .line 441
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private processAllTickets([Ljava/io/File;)V
    .locals 3
    .param p1, "folders"    # [Ljava/io/File;

    .prologue
    .line 417
    sget-object v1, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "processAllTickets"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 418
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v0, p1, v1

    .line 419
    .local v0, "folder":Ljava/io/File;
    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->processNextTicketFolder(Ljava/io/File;)V

    .line 418
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 421
    .end local v0    # "folder":Ljava/io/File;
    :cond_0
    return-void
.end method

.method private declared-synchronized processNextTicketFolder(Ljava/io/File;)V
    .locals 3
    .param p1, "ticketFolder"    # Ljava/io/File;

    .prologue
    .line 96
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$1;-><init>(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;Ljava/io/File;)V

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    monitor-exit p0

    return-void

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized sendToZenddesk(Ljava/io/File;Lorg/json/JSONObject;Ljava/lang/String;)V
    .locals 18
    .param p1, "folder"    # Ljava/io/File;
    .param p2, "ticketContents"    # Lorg/json/JSONObject;
    .param p3, "attachmentToken"    # Ljava/lang/String;

    .prologue
    .line 246
    monitor-enter p0

    :try_start_0
    sget-object v15, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v16, "sendToZendesk"

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 248
    const-string v15, "ticket_type"

    const-string v16, ""

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 249
    .local v13, "ticketType":Ljava/lang/String;
    const-string v15, "ticket_description"

    const-string v16, ""

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 250
    .local v12, "ticketDescription":Ljava/lang/String;
    const-string v15, "ticket_vin"

    const-string v16, ""

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 251
    .local v14, "vin":Ljava/lang/String;
    const-string v15, "ticket_email"

    const-string v16, ""

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 254
    .local v8, "email":Ljava/lang/String;
    sget-object v15, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "ticketDescription: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", ticketType: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 257
    invoke-static {v13}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_0

    invoke-static {v12}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 258
    :cond_0
    sget-object v15, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v16, "ticket contents were null."

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 259
    invoke-direct/range {p0 .. p0}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->incrementFailureCount()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 369
    :goto_0
    monitor-exit p0

    return-void

    .line 264
    :cond_1
    :try_start_1
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 266
    .local v6, "customFields":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/zendesk/sdk/model/request/CustomField;>;"
    new-instance v11, Lcom/zendesk/sdk/model/request/CreateRequest;

    invoke-direct {v11}, Lcom/zendesk/sdk/model/request/CreateRequest;-><init>()V

    .line 267
    .local v11, "request":Lcom/zendesk/sdk/model/request/CreateRequest;
    invoke-virtual {v11, v12}, Lcom/zendesk/sdk/model/request/CreateRequest;->setDescription(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 270
    :try_start_2
    const-string v15, "ticket_action"

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    const-string v16, "fetch_hud_log_upload_delete"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 271
    sget-object v15, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v16, "adding tag for android background submission"

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 272
    const-string v15, "android_background_submission"

    invoke-static {v15}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v15

    invoke-virtual {v11, v15}, Lcom/zendesk/sdk/model/request/CreateRequest;->setTags(Ljava/util/List;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 278
    :cond_2
    :goto_1
    :try_start_3
    invoke-static/range {p3 .. p3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_3

    .line 279
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 280
    .local v5, "attachmentTokenList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 281
    invoke-virtual {v11, v5}, Lcom/zendesk/sdk/model/request/CreateRequest;->setAttachments(Ljava/util/List;)V

    .line 284
    .end local v5    # "attachmentTokenList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_3
    sget-object v15, Lcom/navdy/client/app/ui/settings/ZendeskConstants;->PROBLEM_TYPES:Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v6, v1, v2, v13}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->addCustomField(Ljava/util/ArrayList;JLjava/lang/String;)V

    .line 286
    invoke-static {v8}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_4

    .line 287
    invoke-virtual {v11, v8}, Lcom/zendesk/sdk/model/request/CreateRequest;->setEmail(Ljava/lang/String;)V

    .line 290
    :cond_4
    invoke-static {v14}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_5

    .line 291
    sget-object v15, Lcom/navdy/client/app/ui/settings/ZendeskConstants;->VIN:Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v6, v1, v2, v14}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->addCustomField(Ljava/util/ArrayList;JLjava/lang/String;)V

    .line 294
    :cond_5
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    .line 296
    .local v4, "appContext":Landroid/content/Context;
    sget-object v15, Lcom/navdy/client/app/ui/settings/ZendeskConstants;->VEHICLE_MAKE:Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    const-string v15, "vehicle-make"

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v6, v1, v2, v15}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->addCustomFieldFromSharedPref(Ljava/util/ArrayList;JLjava/lang/String;)V

    .line 297
    sget-object v15, Lcom/navdy/client/app/ui/settings/ZendeskConstants;->VEHICLE_MODEL:Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    const-string v15, "vehicle-model"

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v6, v1, v2, v15}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->addCustomFieldFromSharedPref(Ljava/util/ArrayList;JLjava/lang/String;)V

    .line 298
    sget-object v15, Lcom/navdy/client/app/ui/settings/ZendeskConstants;->VEHICLE_YEAR:Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    const-string v15, "vehicle-year"

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v6, v1, v2, v15}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->addCustomFieldFromSharedPref(Ljava/util/ArrayList;JLjava/lang/String;)V

    .line 300
    sget-object v15, Lcom/navdy/client/app/ui/settings/ZendeskConstants;->MOBILE_APP_VERSION:Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    const-string v15, "1.3.1718-e615013"

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v6, v1, v2, v15}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->addCustomField(Ljava/util/ArrayList;JLjava/lang/String;)V

    .line 301
    sget-object v15, Lcom/navdy/client/app/ui/settings/ZendeskConstants;->MOBILE_APP_NAME:Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    const v15, 0x7f080529

    invoke-virtual {v4, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v6, v1, v2, v15}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->addCustomField(Ljava/util/ArrayList;JLjava/lang/String;)V

    .line 302
    sget-object v15, Lcom/navdy/client/app/ui/settings/ZendeskConstants;->MOBILE_DEVICE_MODEL:Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    sget-object v15, Landroid/os/Build;->MODEL:Ljava/lang/String;

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v6, v1, v2, v15}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->addCustomField(Ljava/util/ArrayList;JLjava/lang/String;)V

    .line 303
    sget-object v15, Lcom/navdy/client/app/ui/settings/ZendeskConstants;->MOBILE_DEVICE_MAKE:Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    sget-object v15, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v6, v1, v2, v15}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->addCustomField(Ljava/util/ArrayList;JLjava/lang/String;)V

    .line 304
    sget-object v15, Lcom/navdy/client/app/ui/settings/ZendeskConstants;->MOBILE_OS_VERSION:Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    sget-object v15, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-static {v15}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v6, v1, v2, v15}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->addCustomField(Ljava/util/ArrayList;JLjava/lang/String;)V

    .line 306
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v15

    invoke-virtual {v15}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v10

    .line 308
    .local v10, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v10, :cond_6

    .line 309
    invoke-virtual {v10}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v9

    .line 311
    .local v9, "info":Lcom/navdy/service/library/events/DeviceInfo;
    if-eqz v9, :cond_6

    .line 312
    sget-object v15, Lcom/navdy/client/app/ui/settings/ZendeskConstants;->DISPLAY_SOFTWARE_VERSION:Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    iget-object v15, v9, Lcom/navdy/service/library/events/DeviceInfo;->systemVersion:Ljava/lang/String;

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v6, v1, v2, v15}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->addCustomField(Ljava/util/ArrayList;JLjava/lang/String;)V

    .line 313
    sget-object v15, Lcom/navdy/client/app/ui/settings/ZendeskConstants;->DISPLAY_SERIAL_NUMBER:Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    iget-object v15, v9, Lcom/navdy/service/library/events/DeviceInfo;->deviceUuid:Ljava/lang/String;

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v6, v1, v2, v15}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->addCustomField(Ljava/util/ArrayList;JLjava/lang/String;)V

    .line 319
    .end local v9    # "info":Lcom/navdy/service/library/events/DeviceInfo;
    :cond_6
    sget-object v15, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "setting custom fields: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 320
    invoke-virtual {v11, v6}, Lcom/zendesk/sdk/model/request/CreateRequest;->setCustomFields(Ljava/util/List;)V

    .line 321
    sget-object v15, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v15, v6}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->setCustomFields(Ljava/util/List;)V

    .line 324
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->providerStore:Lcom/zendesk/sdk/network/impl/ProviderStore;

    invoke-interface {v15}, Lcom/zendesk/sdk/network/impl/ProviderStore;->requestProvider()Lcom/zendesk/sdk/network/RequestProvider;

    move-result-object v15

    new-instance v16, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$3;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move-object/from16 v3, p1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$3;-><init>(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;Lorg/json/JSONObject;Ljava/io/File;Landroid/content/Context;)V

    move-object/from16 v0, v16

    invoke-interface {v15, v11, v0}, Lcom/zendesk/sdk/network/RequestProvider;->createRequest(Lcom/zendesk/sdk/model/request/CreateRequest;Lcom/zendesk/service/ZendeskCallback;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 246
    .end local v4    # "appContext":Landroid/content/Context;
    .end local v6    # "customFields":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/zendesk/sdk/model/request/CustomField;>;"
    .end local v8    # "email":Ljava/lang/String;
    .end local v10    # "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    .end local v11    # "request":Lcom/zendesk/sdk/model/request/CreateRequest;
    .end local v12    # "ticketDescription":Ljava/lang/String;
    .end local v13    # "ticketType":Ljava/lang/String;
    .end local v14    # "vin":Ljava/lang/String;
    :catchall_0
    move-exception v15

    monitor-exit p0

    throw v15

    .line 274
    .restart local v6    # "customFields":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/zendesk/sdk/model/request/CustomField;>;"
    .restart local v8    # "email":Ljava/lang/String;
    .restart local v11    # "request":Lcom/zendesk/sdk/model/request/CreateRequest;
    .restart local v12    # "ticketDescription":Ljava/lang/String;
    .restart local v13    # "ticketType":Ljava/lang/String;
    .restart local v14    # "vin":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 275
    .local v7, "e":Ljava/lang/Exception;
    :try_start_4
    sget-object v15, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "JSON Exception: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1
.end method

.method private declared-synchronized submitTicket(Lorg/json/JSONObject;Ljava/io/File;Ljava/io/File;)V
    .locals 4
    .param p1, "ticketContents"    # Lorg/json/JSONObject;
    .param p2, "ticketAttachment"    # Ljava/io/File;
    .param p3, "folder"    # Ljava/io/File;

    .prologue
    .line 209
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "submitTicket content status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", attachment status : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 211
    if-nez p1, :cond_0

    .line 212
    sget-object v0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Ticket contents are null. Deleting folder for invalid support request form:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 213
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/navdy/service/library/util/IOUtils;->deleteDirectory(Landroid/content/Context;Ljava/io/File;)V

    .line 214
    invoke-direct {p0}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->incrementFailureCount()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 242
    :goto_0
    monitor-exit p0

    return-void

    .line 218
    :cond_0
    if-eqz p2, :cond_1

    :try_start_1
    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 220
    sget-object v0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ticket attachment size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 223
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->providerStore:Lcom/zendesk/sdk/network/impl/ProviderStore;

    invoke-interface {v0}, Lcom/zendesk/sdk/network/impl/ProviderStore;->uploadProvider()Lcom/zendesk/sdk/network/UploadProvider;

    move-result-object v0

    const-string v1, "attachments.zip"

    const-string v2, "application/zip"

    new-instance v3, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$2;

    invoke-direct {v3, p0, p3, p1}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$2;-><init>(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;Ljava/io/File;Lorg/json/JSONObject;)V

    invoke-interface {v0, v1, p2, v2, v3}, Lcom/zendesk/sdk/network/UploadProvider;->uploadAttachment(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 209
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 240
    :cond_1
    const/4 v0, 0x0

    :try_start_2
    invoke-direct {p0, p3, p1, v0}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->sendToZenddesk(Ljava/io/File;Lorg/json/JSONObject;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized isRunning()Z
    .locals 1

    .prologue
    .line 437
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->isRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized start(Lcom/navdy/client/app/framework/util/SubmitTicketWorker$OnFinishSubmittingTickets;[Ljava/io/File;)V
    .locals 4
    .param p1, "onFinishSubmittingTickets"    # Lcom/navdy/client/app/framework/util/SubmitTicketWorker$OnFinishSubmittingTickets;
    .param p2, "folders"    # [Ljava/io/File;

    .prologue
    .line 75
    monitor-enter p0

    :try_start_0
    array-length v1, p2

    iput v1, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->ticketCount:I

    .line 77
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->callback:Lcom/navdy/client/app/framework/util/SubmitTicketWorker$OnFinishSubmittingTickets;

    .line 79
    array-length v1, p2

    if-gtz v1, :cond_1

    .line 80
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->callback:Lcom/navdy/client/app/framework/util/SubmitTicketWorker$OnFinishSubmittingTickets;

    if-eqz v1, :cond_0

    .line 81
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->callback:Lcom/navdy/client/app/framework/util/SubmitTicketWorker$OnFinishSubmittingTickets;

    iget v2, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->failCount:I

    iget-boolean v3, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->showSuccessNotification:Z

    invoke-interface {v1, v2, v3}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$OnFinishSubmittingTickets;->onFinish(IZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 85
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->isRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 87
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "tickets_awaiting_hud_log_exist"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 88
    .local v0, "ticketsAwaitingHudLog":Z
    if-eqz v0, :cond_2

    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/AppInstance;->isDeviceConnected()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 89
    invoke-direct {p0, p2}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->fetchHudLogAndProcessTickets([Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 75
    .end local v0    # "ticketsAwaitingHudLog":Z
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 91
    .restart local v0    # "ticketsAwaitingHudLog":Z
    :cond_2
    :try_start_2
    invoke-direct {p0, p2}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->processAllTickets([Ljava/io/File;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
