.class public Lcom/navdy/client/app/framework/glances/GlanceConstants;
.super Ljava/lang/Object;
.source "GlanceConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;
    }
.end annotation


# static fields
.field public static final ACTION_EMAIL_GUESTS:Ljava/lang/String; = "Email guests"

.field public static final ACTION_MAP:Ljava/lang/String; = "Map"

.field public static final ANDROID_BIG_TEXT:Ljava/lang/String; = "android.bigText"

.field public static final ANDROID_CALENDAR:Ljava/lang/String; = "com.android.calendar"

.field public static final ANDROID_PEOPLE:Ljava/lang/String; = "android.people"

.field public static final ANDROID_SUB_TEXT:Ljava/lang/String; = "android.subText"

.field public static final ANDROID_SUMMARY_TEXT:Ljava/lang/String; = "android.summaryText"

.field public static final ANDROID_TEMPLATE:Ljava/lang/String; = "android.template"

.field public static final ANDROID_TEXT:Ljava/lang/String; = "android.text"

.field public static final ANDROID_TEXT_LINES:Ljava/lang/String; = "android.textLines"

.field public static final ANDROID_TITLE:Ljava/lang/String; = "android.title"

.field public static final APPLE_MUSIC:Ljava/lang/String; = "com.apple.android.music"

.field public static final CAR_EXT:Ljava/lang/String; = "android.car.EXTENSIONS"

.field public static final CAR_EXT_CONVERSATION:Ljava/lang/String; = "car_conversation"

.field public static final CAR_EXT_CONVERSATION_MESSAGES:Ljava/lang/String; = "messages"

.field public static final CAR_EXT_CONVERSATION_MESSAGES_AUTHOR:Ljava/lang/String; = "author"

.field public static final CAR_EXT_CONVERSATION_MESSAGES_TEXT:Ljava/lang/String; = "text"

.field public static final CAR_EXT_CONVERSATION_PARTICIPANTS:Ljava/lang/String; = "participants"

.field public static final COMMA:Ljava/lang/String; = ","

.field public static final EMAIL_AT:Ljava/lang/String; = "@"

.field public static final EMPTY:Ljava/lang/String; = ""

.field public static final FACEBOOK:Ljava/lang/String; = "com.facebook.katana"

.field public static final FACEBOOK_MESSENGER:Ljava/lang/String; = "com.facebook.orca"

.field public static final FUEL_PACKAGE:Ljava/lang/String; = "com.navdy.fuel"

.field public static final GLANCES_APP_GLANCES_ENABLED_DEFAULT:Z = false

.field public static final GLANCES_DRIVING_GLANCES_ENABLED_DEFAULT:Z = true

.field public static final GLANCES_OTHER_APPS_ENABLED_DEFAULT:Z = false

.field public static final GOOGLE_CALENDAR:Ljava/lang/String; = "com.google.android.calendar"

.field public static final GOOGLE_HANGOUTS:Ljava/lang/String; = "com.google.android.talk"

.field public static final GOOGLE_INBOX:Ljava/lang/String; = "com.google.android.apps.inbox"

.field public static final GOOGLE_MAIL:Ljava/lang/String; = "com.google.android.gm"

.field public static final GOOGLE_MUSIC:Ljava/lang/String; = "com.google.android.music"

.field public static final MAIL_TO:Ljava/lang/String; = "mailto:"

.field public static final MICROSOFT_OUTLOOK:Ljava/lang/String; = "com.microsoft.office.outlook"

.field public static final MUSIC_PACKAGE:Ljava/lang/String; = "com.navdy.music"

.field public static final NAPSTER:Ljava/lang/String; = "com.rhapsody"

.field public static final NEW_LINE:Ljava/lang/String; = "\n"

.field public static final NOTIFICATION_INBOX_STYLE:Ljava/lang/String; = "android.app.Notification$InboxStyle"

.field public static final PANDORA:Ljava/lang/String; = "com.pandora.android"

.field public static final PHONE_PACKAGE:Ljava/lang/String; = "com.navdy.phone"

.field public static final SAMSUNG_MUSIC:Ljava/lang/String; = "com.sec.android.app.music"

.field public static final SLACK:Ljava/lang/String; = "com.Slack"

.field public static final SMS_PACKAGE:Ljava/lang/String; = "com.navdy.sms"

.field public static final SOUNDCLOUD:Ljava/lang/String; = "com.soundcloud.android"

.field public static final SPOTIFY:Ljava/lang/String; = "com.spotify.music"

.field public static final TRAFFIC_PACKAGE:Ljava/lang/String; = "com.navdy.traffic"

.field public static final TWITTER:Ljava/lang/String; = "com.twitter.android"

.field public static final WHATS_APP:Ljava/lang/String; = "com.whatsapp"

.field public static final YOUTUBE_MUSIC:Ljava/lang/String; = "com.google.android.apps.youtube.music"

.field private static final groups:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    .line 15
    new-instance v8, Lcom/navdy/service/library/log/Logger;

    const-class v9, Lcom/navdy/client/app/framework/glances/GlanceConstants;

    invoke-direct {v8, v9}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v8, Lcom/navdy/client/app/framework/glances/GlanceConstants;->logger:Lcom/navdy/service/library/log/Logger;

    .line 99
    new-instance v8, Lcom/navdy/client/app/framework/glances/GlanceConstants$1;

    invoke-direct {v8}, Lcom/navdy/client/app/framework/glances/GlanceConstants$1;-><init>()V

    sput-object v8, Lcom/navdy/client/app/framework/glances/GlanceConstants;->groups:Ljava/util/Map;

    .line 112
    sget-object v8, Lcom/navdy/client/app/framework/glances/GlanceConstants;->groups:Ljava/util/Map;

    sget-object v9, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->DRIVING_GLANCES:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    .line 113
    .local v1, "drivingGlances":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    sget-object v8, Lcom/navdy/client/app/framework/glances/GlanceConstants;->groups:Ljava/util/Map;

    sget-object v9, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->WHITE_LIST:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Set;

    .line 114
    .local v7, "whiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    sget-object v8, Lcom/navdy/client/app/framework/glances/GlanceConstants;->groups:Ljava/util/Map;

    sget-object v9, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->EMAIL_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    .line 115
    .local v2, "emailGroup":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    sget-object v8, Lcom/navdy/client/app/framework/glances/GlanceConstants;->groups:Ljava/util/Map;

    sget-object v9, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->MESSAGING_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Set;

    .line 116
    .local v4, "messagingGroup":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    sget-object v8, Lcom/navdy/client/app/framework/glances/GlanceConstants;->groups:Ljava/util/Map;

    sget-object v9, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->CALENDAR_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 117
    .local v0, "calendarGroup":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    sget-object v8, Lcom/navdy/client/app/framework/glances/GlanceConstants;->groups:Ljava/util/Map;

    sget-object v9, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->SOCIAL_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Set;

    .line 118
    .local v6, "socialGroup":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    sget-object v8, Lcom/navdy/client/app/framework/glances/GlanceConstants;->groups:Ljava/util/Map;

    sget-object v9, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->MUSIC_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Set;

    .line 119
    .local v5, "musicGroup":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    sget-object v8, Lcom/navdy/client/app/framework/glances/GlanceConstants;->groups:Ljava/util/Map;

    sget-object v9, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->IGNORE_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Set;

    .line 122
    .local v3, "ignoreGroup":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v8, "com.google.android.gm"

    invoke-interface {v2, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 123
    const-string v8, "com.google.android.apps.inbox"

    invoke-interface {v2, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 124
    const-string v8, "com.microsoft.office.outlook"

    invoke-interface {v2, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 127
    const-string v8, "com.Slack"

    invoke-interface {v4, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 128
    const-string v8, "com.google.android.talk"

    invoke-interface {v4, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 129
    const-string v8, "com.whatsapp"

    invoke-interface {v4, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 130
    const-string v8, "com.facebook.orca"

    invoke-interface {v4, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 133
    const-string v8, "com.google.android.calendar"

    invoke-interface {v0, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 137
    const-string v8, "com.facebook.katana"

    invoke-interface {v6, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 138
    const-string v8, "com.twitter.android"

    invoke-interface {v6, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 141
    const-string v8, "com.google.android.music"

    invoke-interface {v5, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 142
    const-string v8, "com.pandora.android"

    invoke-interface {v5, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 143
    const-string v8, "com.spotify.music"

    invoke-interface {v5, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 144
    const-string v8, "com.apple.android.music"

    invoke-interface {v5, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 145
    const-string v8, "com.google.android.apps.youtube.music"

    invoke-interface {v5, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 146
    const-string v8, "com.sec.android.app.music"

    invoke-interface {v5, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 147
    const-string v8, "com.rhapsody"

    invoke-interface {v5, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 148
    const-string v8, "com.soundcloud.android"

    invoke-interface {v5, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 151
    const-string v8, "com.navdy.phone"

    invoke-interface {v1, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 152
    const-string v8, "com.navdy.sms"

    invoke-interface {v1, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 153
    const-string v8, "com.navdy.fuel"

    invoke-interface {v1, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 154
    const-string v8, "com.navdy.music"

    invoke-interface {v1, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 155
    const-string v8, "com.navdy.traffic"

    invoke-interface {v1, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 158
    const-string v8, "com.google.android.gm"

    invoke-interface {v7, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 159
    invoke-interface {v7, v4}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 160
    invoke-interface {v7, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 161
    invoke-interface {v7, v6}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 164
    const-string v8, "com.google.android.gms"

    invoke-interface {v3, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 165
    const-string v8, "com.android.mms"

    invoke-interface {v3, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 166
    const-string v8, "com.android.systemui"

    invoke-interface {v3, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 167
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addPackageToGroup(Ljava/lang/String;Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;)V
    .locals 4
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "group"    # Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    .prologue
    .line 180
    sget-object v1, Lcom/navdy/client/app/framework/glances/GlanceConstants;->groups:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 181
    .local v0, "packages":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    .line 182
    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 186
    :goto_0
    return-void

    .line 184
    :cond_0
    sget-object v1, Lcom/navdy/client/app/framework/glances/GlanceConstants;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot get package for group "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static isPackageInGroup(Ljava/lang/String;Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;)Z
    .locals 4
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "group"    # Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    .prologue
    .line 170
    sget-object v1, Lcom/navdy/client/app/framework/glances/GlanceConstants;->groups:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 171
    .local v0, "packages":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    .line 172
    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    .line 175
    :goto_0
    return v1

    .line 174
    :cond_0
    sget-object v1, Lcom/navdy/client/app/framework/glances/GlanceConstants;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot get package for group "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 175
    const/4 v1, 0x0

    goto :goto_0
.end method
