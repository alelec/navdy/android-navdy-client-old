.class public final Lcom/navdy/client/app/framework/map/HereGeocoder;
.super Ljava/lang/Object;
.source "HereGeocoder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;,
        Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;
    }
.end annotation


# static fields
.field private static final MAX_COLLECTION_SIZE:I = 0x1

.field private static final ROAD_ACCESS_TYPE:Ljava/lang/String; = "road"

.field private static final handler:Landroid/os/Handler;

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 40
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/map/HereGeocoder;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/map/HereGeocoder;->logger:Lcom/navdy/service/library/log/Logger;

    .line 45
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/navdy/client/app/framework/map/HereGeocoder;->handler:Landroid/os/Handler;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/navdy/client/app/framework/map/HereGeocoder;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    .prologue
    .line 38
    invoke-static {p0, p1, p2}, Lcom/navdy/client/app/framework/map/HereGeocoder;->makeRequestInternal(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)Lcom/here/android/mpa/search/ResultListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    .prologue
    .line 38
    invoke-static {p0}, Lcom/navdy/client/app/framework/map/HereGeocoder;->getPlaceResultListener(Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)Lcom/here/android/mpa/search/ResultListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/navdy/client/app/framework/map/HereGeocoder;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method private static getPlaceResultListener(Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)Lcom/here/android/mpa/search/ResultListener;
    .locals 2
    .param p0, "requestCallback"    # Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;",
            ")",
            "Lcom/here/android/mpa/search/ResultListener",
            "<",
            "Lcom/here/android/mpa/search/Place;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    new-instance v0, Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;

    new-instance v1, Lcom/navdy/client/app/framework/map/HereGeocoder$3;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/map/HereGeocoder$3;-><init>(Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V

    invoke-direct {v0, v1}, Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;-><init>(Lcom/here/android/mpa/search/ResultListener;)V

    return-object v0
.end method

.method private static getSearchResultsListener(Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)Lcom/here/android/mpa/search/ResultListener;
    .locals 2
    .param p0, "requestCallback"    # Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;",
            ")",
            "Lcom/here/android/mpa/search/ResultListener",
            "<",
            "Lcom/here/android/mpa/search/DiscoveryResultPage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 111
    new-instance v0, Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;

    new-instance v1, Lcom/navdy/client/app/framework/map/HereGeocoder$2;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/map/HereGeocoder$2;-><init>(Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V

    invoke-direct {v0, v1}, Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;-><init>(Lcom/here/android/mpa/search/ResultListener;)V

    return-object v0
.end method

.method public static makeRequest(Ljava/lang/String;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V
    .locals 2
    .param p0, "searchQuery"    # Ljava/lang/String;
    .param p1, "requestCallback"    # Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    .prologue
    .line 51
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->canReachInternet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 52
    sget-object v0, Lcom/here/android/mpa/search/ErrorCode;->NETWORK_COMMUNICATION:Lcom/here/android/mpa/search/ErrorCode;

    invoke-interface {p1, v0}, Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;->onError(Lcom/here/android/mpa/search/ErrorCode;)V

    .line 55
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/map/HereMapsManager;->getInstance()Lcom/navdy/client/app/framework/map/HereMapsManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/map/HereGeocoder$1;

    invoke-direct {v1, p1, p0}, Lcom/navdy/client/app/framework/map/HereGeocoder$1;-><init>(Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/map/HereMapsManager;->addOnInitializedListener(Lcom/here/android/mpa/common/OnEngineInitListener;)V

    .line 73
    return-void
.end method

.method private static makeRequestInternal(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V
    .locals 10
    .param p0, "coordinate"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p1, "searchQuery"    # Ljava/lang/String;
    .param p2, "requestCallback"    # Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    .prologue
    .line 76
    if-nez p2, :cond_1

    .line 77
    sget-object v3, Lcom/navdy/client/app/framework/map/HereGeocoder;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "makeHereRequestForDestination, no callback. Exiting"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    if-nez p0, :cond_2

    .line 82
    sget-object v3, Lcom/navdy/client/app/framework/map/HereGeocoder;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "makeHereRequestForDestination, no coordinates. Calling onError"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 83
    sget-object v3, Lcom/here/android/mpa/search/ErrorCode;->BAD_LOCATION:Lcom/here/android/mpa/search/ErrorCode;

    invoke-interface {p2, v3}, Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;->onError(Lcom/here/android/mpa/search/ErrorCode;)V

    goto :goto_0

    .line 87
    :cond_2
    sget-object v3, Lcom/navdy/client/app/framework/map/HereGeocoder;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "makeHereRequestForDestination, searchQuery["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " coordinate["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 90
    new-instance v3, Lcom/here/android/mpa/search/SearchRequest;

    invoke-direct {v3, p1}, Lcom/here/android/mpa/search/SearchRequest;-><init>(Ljava/lang/String;)V

    new-instance v4, Lcom/here/android/mpa/common/GeoCoordinate;

    iget-object v5, p0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    .line 91
    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    iget-object v5, p0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    invoke-direct {v4, v6, v7, v8, v9}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    invoke-virtual {v3, v4}, Lcom/here/android/mpa/search/SearchRequest;->setSearchCenter(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/search/SearchRequest;

    move-result-object v3

    const/4 v4, 0x1

    .line 92
    invoke-virtual {v3, v4}, Lcom/here/android/mpa/search/SearchRequest;->setCollectionSize(I)Lcom/here/android/mpa/search/DiscoveryRequest;

    move-result-object v0

    .line 94
    .local v0, "request":Lcom/here/android/mpa/search/DiscoveryRequest;
    invoke-static {p2}, Lcom/navdy/client/app/framework/map/HereGeocoder;->getSearchResultsListener(Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)Lcom/here/android/mpa/search/ResultListener;

    move-result-object v2

    .line 95
    .local v2, "searchResultsListener":Lcom/here/android/mpa/search/ResultListener;, "Lcom/here/android/mpa/search/ResultListener<Lcom/here/android/mpa/search/DiscoveryResultPage;>;"
    invoke-virtual {v0, v2}, Lcom/here/android/mpa/search/DiscoveryRequest;->execute(Lcom/here/android/mpa/search/ResultListener;)Lcom/here/android/mpa/search/ErrorCode;

    move-result-object v1

    .line 97
    .local v1, "searchRequestError":Lcom/here/android/mpa/search/ErrorCode;
    sget-object v3, Lcom/here/android/mpa/search/ErrorCode;->NONE:Lcom/here/android/mpa/search/ErrorCode;

    if-eq v1, v3, :cond_0

    .line 98
    invoke-interface {p2, v1}, Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;->onError(Lcom/here/android/mpa/search/ErrorCode;)V

    goto :goto_0
.end method
