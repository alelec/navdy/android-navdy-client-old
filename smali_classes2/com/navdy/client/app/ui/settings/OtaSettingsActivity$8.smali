.class Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8;
.super Ljava/lang/Object;
.source "OtaSettingsActivity.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->setBuildSources()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    .prologue
    .line 712
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 8
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 715
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->access$1002(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;Z)Z

    .line 716
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->access$000(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;)Lcom/navdy/client/ota/OTAUpdateService$State;

    move-result-object v7

    .line 717
    .local v7, "state":Lcom/navdy/client/ota/OTAUpdateService$State;
    invoke-static {}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->access$1100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "State of the update process :"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Lcom/navdy/client/ota/OTAUpdateService$State;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 719
    if-nez p2, :cond_0

    .line 720
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->access$1200(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;Z)V

    .line 721
    sget-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->FEATURE_MODE_RELEASE:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->setFeatureMode(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;)V

    .line 722
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    invoke-static {v0, v7}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->access$1300(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;Lcom/navdy/client/ota/OTAUpdateService$State;)V

    .line 747
    :goto_0
    return-void

    .line 724
    :cond_0
    const v2, 0x7f0800d2

    .line 725
    .local v2, "confirmationMessage":I
    sget-object v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$debug$util$S3Constants$BuildType:[I

    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->access$1400(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;)Lcom/navdy/client/debug/util/S3Constants$BuildType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/debug/util/S3Constants$BuildType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 733
    :goto_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    const v1, 0x7f0804f2

    const v3, 0x7f08031e

    const v4, 0x7f0800e5

    new-instance v5, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8$1;

    invoke-direct {v5, p0, v7}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8$1;-><init>(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8;Lcom/navdy/client/ota/OTAUpdateService$State;)V

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->showQuestionDialog(IIIILandroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_0

    .line 727
    :pswitch_0
    const v2, 0x7f0800d2

    .line 728
    goto :goto_1

    .line 730
    :pswitch_1
    const v2, 0x7f0804dd

    goto :goto_1

    .line 725
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
