.class Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1$1;
.super Ljava/lang/Object;
.source "MessagingSettingsActivity.java"

# interfaces
.implements Landroid/support/v7/widget/PopupMenu$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1;->onReplyClick(Ljava/lang/String;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static final DELETE_MENU_ITEM:I = 0x7f100421

.field private static final EDIT_MENU_ITEM:I = 0x7f100420


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1;

.field final synthetic val$popup:Landroid/support/v7/widget/PopupMenu;

.field final synthetic val$reply:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1;Landroid/support/v7/widget/PopupMenu;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1$1;->this$1:Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1;

    iput-object p2, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1$1;->val$popup:Landroid/support/v7/widget/PopupMenu;

    iput-object p3, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1$1;->val$reply:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v3, 0x1

    .line 72
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1$1;->val$popup:Landroid/support/v7/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/support/v7/widget/PopupMenu;->dismiss()V

    .line 73
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 76
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1$1;->this$1:Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1;

    iget-object v1, v1, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 77
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "extra_old_message"

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1$1;->val$reply:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 78
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1$1;->this$1:Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1;

    iget-object v1, v1, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;

    const/16 v2, 0x1092

    invoke-virtual {v1, v0, v2}, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 85
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return v3

    .line 81
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1$1;->this$1:Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1;

    iget-object v1, v1, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->access$000(Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;)Lcom/navdy/client/app/ui/settings/RepliesAdapter;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1$1;->val$reply:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->delete(Ljava/lang/String;)V

    .line 82
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1$1;->this$1:Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1;

    iget-object v1, v1, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;

    invoke-static {v1, v3}, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->access$102(Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;Z)Z

    goto :goto_0

    .line 73
    :pswitch_data_0
    .packed-switch 0x7f100421
        :pswitch_0
    .end packed-switch
.end method
