.class Lcom/navdy/client/app/ui/settings/RepliesAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "RepliesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/navdy/client/app/ui/settings/ReplyViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final TYPE_FOOTER:I = 0x2

.field private static final TYPE_HEADER:I = 0x0

.field private static final TYPE_REPLY:I = 0x1


# instance fields
.field private imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

.field private itemMovedCallback:Ljava/lang/Runnable;

.field private itemTouchHelperCallback:Landroid/support/v7/widget/helper/ItemTouchHelper$Callback;

.field private final replies:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private replyClickListener:Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$ReplyClickListener;


# direct methods
.method constructor <init>(Ljava/util/ArrayList;Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$ReplyClickListener;Ljava/lang/Runnable;Lcom/navdy/client/app/framework/util/ImageCache;)V
    .locals 1
    .param p2, "replyClickListener"    # Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$ReplyClickListener;
    .param p3, "itemMovedCallback"    # Ljava/lang/Runnable;
    .param p4, "imageCache"    # Lcom/navdy/client/app/framework/util/ImageCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$ReplyClickListener;",
            "Ljava/lang/Runnable;",
            "Lcom/navdy/client/app/framework/util/ImageCache;",
            ")V"
        }
    .end annotation

    .prologue
    .line 38
    .local p1, "replies":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 36
    new-instance v0, Lcom/navdy/client/app/framework/util/ImageCache;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/util/ImageCache;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    .line 146
    new-instance v0, Lcom/navdy/client/app/ui/settings/RepliesAdapter$2;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/settings/RepliesAdapter$2;-><init>(Lcom/navdy/client/app/ui/settings/RepliesAdapter;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->itemTouchHelperCallback:Landroid/support/v7/widget/helper/ItemTouchHelper$Callback;

    .line 39
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->replies:Ljava/util/ArrayList;

    .line 40
    iput-object p2, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->replyClickListener:Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$ReplyClickListener;

    .line 41
    iput-object p3, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->itemMovedCallback:Ljava/lang/Runnable;

    .line 42
    iput-object p4, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    .line 43
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/settings/RepliesAdapter;)Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$ReplyClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/RepliesAdapter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->replyClickListener:Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$ReplyClickListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/settings/RepliesAdapter;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/RepliesAdapter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->replies:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/settings/RepliesAdapter;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/RepliesAdapter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->itemMovedCallback:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method public add(Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->replies:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    .line 122
    const v0, 0x7f0802c4

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->showShortToast(I[Ljava/lang/Object;)V

    .line 128
    :goto_0
    return-void

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->replies:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public delete(Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 131
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->replies:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 132
    .local v0, "i":I
    if-gez v0, :cond_0

    .line 138
    :goto_0
    return-void

    .line 136
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->replies:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 137
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->notifyItemRemoved(I)V

    goto :goto_0
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->replies:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->replies:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    add-int/lit8 v0, v0, 0x2

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 102
    int-to-long v0, p1

    return-wide v0
.end method

.method getItemTouchHelperCallback()Landroid/support/v7/widget/helper/ItemTouchHelper$Callback;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->itemTouchHelperCallback:Landroid/support/v7/widget/helper/ItemTouchHelper$Callback;

    return-object v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 47
    if-nez p1, :cond_0

    .line 48
    const/4 v0, 0x0

    .line 52
    :goto_0
    return v0

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->replies:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->replies:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le p1, v0, :cond_1

    .line 50
    const/4 v0, 0x2

    goto :goto_0

    .line 52
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->replies:Ljava/util/ArrayList;

    return-object v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 25
    check-cast p1, Lcom/navdy/client/app/ui/settings/ReplyViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->onBindViewHolder(Lcom/navdy/client/app/ui/settings/ReplyViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/navdy/client/app/ui/settings/ReplyViewHolder;I)V
    .locals 4
    .param p1, "holder"    # Lcom/navdy/client/app/ui/settings/ReplyViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 74
    invoke-virtual {p1}, Lcom/navdy/client/app/ui/settings/ReplyViewHolder;->getItemViewType()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    add-int/lit8 p2, p2, -0x1

    .line 80
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->replies:Ljava/util/ArrayList;

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 81
    .local v1, "reply":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 82
    new-instance v0, Lcom/navdy/client/app/ui/settings/RepliesAdapter$1;

    invoke-direct {v0, p0, v1, p1}, Lcom/navdy/client/app/ui/settings/RepliesAdapter$1;-><init>(Lcom/navdy/client/app/ui/settings/RepliesAdapter;Ljava/lang/String;Lcom/navdy/client/app/ui/settings/ReplyViewHolder;)V

    .line 89
    .local v0, "clickListener":Landroid/view/View$OnClickListener;
    iget-object v2, p1, Lcom/navdy/client/app/ui/settings/ReplyViewHolder;->text:Landroid/widget/TextView;

    if-eqz v2, :cond_2

    .line 90
    iget-object v2, p1, Lcom/navdy/client/app/ui/settings/ReplyViewHolder;->text:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v2, p1, Lcom/navdy/client/app/ui/settings/ReplyViewHolder;->text:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    :cond_2
    iget-object v2, p1, Lcom/navdy/client/app/ui/settings/ReplyViewHolder;->more:Landroid/widget/ImageView;

    if-eqz v2, :cond_0

    .line 95
    iget-object v2, p1, Lcom/navdy/client/app/ui/settings/ReplyViewHolder;->more:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0, p1, p2}, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/navdy/client/app/ui/settings/ReplyViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/navdy/client/app/ui/settings/ReplyViewHolder;
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    const/4 v4, 0x0

    .line 58
    if-nez p2, :cond_1

    .line 59
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0300f6

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 60
    .local v1, "v":Landroid/view/View;
    const v2, 0x7f1000b1

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 61
    .local v0, "img":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 62
    const v2, 0x7f020213

    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    invoke-static {v0, v2, v3}, Lcom/navdy/client/app/framework/util/ImageUtils;->loadImage(Landroid/widget/ImageView;ILcom/navdy/client/app/framework/util/ImageCache;)V

    .line 69
    .end local v0    # "img":Landroid/widget/ImageView;
    :cond_0
    :goto_0
    new-instance v2, Lcom/navdy/client/app/ui/settings/ReplyViewHolder;

    invoke-direct {v2, v1}, Lcom/navdy/client/app/ui/settings/ReplyViewHolder;-><init>(Landroid/view/View;)V

    return-object v2

    .line 64
    .end local v1    # "v":Landroid/view/View;
    :cond_1
    const/4 v2, 0x2

    if-ne p2, v2, :cond_2

    .line 65
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0300cc

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .restart local v1    # "v":Landroid/view/View;
    goto :goto_0

    .line 67
    .end local v1    # "v":Landroid/view/View;
    :cond_2
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0300f7

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .restart local v1    # "v":Landroid/view/View;
    goto :goto_0
.end method

.method public replace(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "oldMessage"    # Ljava/lang/String;
    .param p2, "newMessage"    # Ljava/lang/String;

    .prologue
    .line 115
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->replies:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 116
    .local v0, "i":I
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->replies:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 117
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->notifyItemChanged(I)V

    .line 118
    return-void
.end method
