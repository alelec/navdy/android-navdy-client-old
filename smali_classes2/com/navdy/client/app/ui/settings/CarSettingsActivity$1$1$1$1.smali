.class Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1$1$1;
.super Ljava/lang/Object;
.source "CarSettingsActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$3:Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1$1;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1$1;)V
    .locals 0
    .param p1, "this$3"    # Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1$1;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1$1$1;->this$3:Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialogInterface"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 66
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    .line 67
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 68
    .local v0, "sharedPref":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "user_overrided_blacklist"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 69
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1$1$1;->this$3:Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1$1;

    iget-object v1, v1, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1$1;->this$2:Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1;

    iget-object v1, v1, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1;->this$1:Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;

    iget-object v1, v1, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/CarSettingsActivity;

    sget-object v2, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->SCAN_ON:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->access$202(Lcom/navdy/client/app/ui/settings/CarSettingsActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 73
    .end local v0    # "sharedPref":Landroid/content/SharedPreferences;
    :goto_0
    return-void

    .line 71
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1$1$1;->this$3:Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1$1;

    iget-object v1, v1, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1$1;->this$2:Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1;

    iget-object v1, v1, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1;->this$1:Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;

    iget-object v1, v1, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/CarSettingsActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->access$000(Lcom/navdy/client/app/ui/settings/CarSettingsActivity;)Landroid/widget/Switch;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setChecked(Z)V

    goto :goto_0
.end method
