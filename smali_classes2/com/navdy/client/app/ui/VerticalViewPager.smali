.class public Lcom/navdy/client/app/ui/VerticalViewPager;
.super Landroid/support/v4/view/ViewPager;
.source "VerticalViewPager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/ui/VerticalViewPager$VerticalPageTransformer;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    .line 21
    invoke-direct {p0}, Lcom/navdy/client/app/ui/VerticalViewPager;->init()V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    invoke-direct {p0}, Lcom/navdy/client/app/ui/VerticalViewPager;->init()V

    .line 27
    return-void
.end method

.method private init()V
    .locals 3

    .prologue
    .line 31
    const/4 v0, 0x1

    new-instance v1, Lcom/navdy/client/app/ui/VerticalViewPager$VerticalPageTransformer;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/navdy/client/app/ui/VerticalViewPager$VerticalPageTransformer;-><init>(Lcom/navdy/client/app/ui/VerticalViewPager$1;)V

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/ui/VerticalViewPager;->setPageTransformer(ZLandroid/support/v4/view/ViewPager$PageTransformer;)V

    .line 33
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/VerticalViewPager;->setOverScrollMode(I)V

    .line 34
    return-void
.end method

.method private swapXY(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 66
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 67
    .local v0, "ev":Landroid/view/MotionEvent;
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/VerticalViewPager;->getWidth()I

    move-result v5

    int-to-float v4, v5

    .line 68
    .local v4, "width":F
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/VerticalViewPager;->getHeight()I

    move-result v5

    int-to-float v1, v5

    .line 70
    .local v1, "height":F
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    div-float/2addr v5, v1

    mul-float v2, v5, v4

    .line 71
    .local v2, "newX":F
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    div-float/2addr v5, v4

    mul-float v3, v5, v1

    .line 73
    .local v3, "newY":F
    invoke-virtual {v0, v2, v3}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 75
    return-object v0
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/VerticalViewPager;->swapXY(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/support/v4/view/ViewPager;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/VerticalViewPager;->swapXY(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/support/v4/view/ViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
