.class Lcom/navdy/client/app/ui/trips/TripsFragment$1;
.super Landroid/widget/CursorAdapter;
.source "TripsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/trips/TripsFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/trips/TripsFragment;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/trips/TripsFragment;Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/trips/TripsFragment;
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # Landroid/database/Cursor;
    .param p4, "x2"    # Z

    .prologue
    .line 37
    iput-object p1, p0, Lcom/navdy/client/app/ui/trips/TripsFragment$1;->this$0:Lcom/navdy/client/app/ui/trips/TripsFragment;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 16
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 46
    const v10, 0x1020014

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 47
    .local v3, "text1":Landroid/widget/TextView;
    const v10, 0x1020015

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 49
    .local v8, "text2":Landroid/widget/TextView;
    const/4 v10, 0x0

    const/4 v11, 0x1

    invoke-virtual {v3, v10, v11}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 51
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v10

    move-object/from16 v0, p3

    invoke-static {v0, v10}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getTripsItemAt(Landroid/database/Cursor;I)Lcom/navdy/client/app/framework/models/Trip;

    move-result-object v9

    .line 52
    .local v9, "trip":Lcom/navdy/client/app/framework/models/Trip;
    iget v10, v9, Lcom/navdy/client/app/framework/models/Trip;->destinationId:I

    .line 53
    invoke-static {v10}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getThisDestination(I)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v2

    .line 55
    .local v2, "destination":Lcom/navdy/client/app/framework/models/Destination;
    if-eqz v2, :cond_1

    .line 56
    const-string v11, "%s%n(%s)"

    const/4 v10, 0x2

    new-array v12, v10, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Destination;->isFavoriteDestination()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Destination;->getFavoriteLabel()Ljava/lang/String;

    move-result-object v10

    :goto_0
    aput-object v10, v12, v13

    const/4 v10, 0x1

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Destination;->getAddressForDisplay()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v12, v10

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 60
    :goto_1
    iget-wide v6, v9, Lcom/navdy/client/app/framework/models/Trip;->startTime:J

    .line 61
    .local v6, "startTime":J
    iget-wide v4, v9, Lcom/navdy/client/app/framework/models/Trip;->endTime:J

    .line 62
    .local v4, "endTime":J
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v10

    const-string v11, "Start: %s (odo: %d)%n         %d + %d%n          lat: %f long: %f%nend: %s (odo: %d)%n          lat: %f long: %f%narrived: %d | trip number: %d | id: %d"

    const/16 v12, 0xd

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/client/app/ui/trips/TripsFragment$1;->this$0:Lcom/navdy/client/app/ui/trips/TripsFragment;

    .line 64
    invoke-virtual {v14, v6, v7}, Lcom/navdy/client/app/ui/trips/TripsFragment;->getDate(J)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    iget v14, v9, Lcom/navdy/client/app/framework/models/Trip;->startOdometer:I

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x2

    .line 65
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x3

    iget v14, v9, Lcom/navdy/client/app/framework/models/Trip;->offset:I

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x4

    iget-wide v14, v9, Lcom/navdy/client/app/framework/models/Trip;->startLat:D

    .line 66
    invoke-static {v14, v15}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x5

    iget-wide v14, v9, Lcom/navdy/client/app/framework/models/Trip;->startLong:D

    invoke-static {v14, v15}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x6

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/client/app/ui/trips/TripsFragment$1;->this$0:Lcom/navdy/client/app/ui/trips/TripsFragment;

    .line 67
    invoke-virtual {v14, v4, v5}, Lcom/navdy/client/app/ui/trips/TripsFragment;->getDate(J)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x7

    iget v14, v9, Lcom/navdy/client/app/framework/models/Trip;->endOdometer:I

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/16 v13, 0x8

    iget-wide v14, v9, Lcom/navdy/client/app/framework/models/Trip;->endLat:D

    .line 68
    invoke-static {v14, v15}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v14

    aput-object v14, v12, v13

    const/16 v13, 0x9

    iget-wide v14, v9, Lcom/navdy/client/app/framework/models/Trip;->endLong:D

    invoke-static {v14, v15}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v14

    aput-object v14, v12, v13

    const/16 v13, 0xa

    iget-wide v14, v9, Lcom/navdy/client/app/framework/models/Trip;->arrivedAtDestination:J

    .line 69
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    aput-object v14, v12, v13

    const/16 v13, 0xb

    iget-wide v14, v9, Lcom/navdy/client/app/framework/models/Trip;->tripNumber:J

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    aput-object v14, v12, v13

    const/16 v13, 0xc

    iget v14, v9, Lcom/navdy/client/app/framework/models/Trip;->id:I

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    .line 62
    invoke-static {v10, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    return-void

    .line 56
    .end local v4    # "endTime":J
    .end local v6    # "startTime":J
    :cond_0
    iget-object v10, v2, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    goto/16 :goto_0

    .line 58
    :cond_1
    const-string v10, "Unable to find destination: %s"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget v13, v9, Lcom/navdy/client/app/framework/models/Trip;->destinationId:I

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 41
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x1090004

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
