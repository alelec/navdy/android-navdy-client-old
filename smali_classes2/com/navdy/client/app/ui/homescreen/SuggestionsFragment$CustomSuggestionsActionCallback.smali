.class Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;
.super Ljava/lang/Object;
.source "SuggestionsFragment.java"

# interfaces
.implements Landroid/support/v7/view/ActionMode$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomSuggestionsActionCallback"
.end annotation


# instance fields
.field private final actionModePosition:I

.field private final homescreenActivity:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

.field private final selectedCardRow:Landroid/view/View;

.field private final suggestion:Lcom/navdy/client/app/framework/models/Suggestion;

.field final synthetic this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Lcom/navdy/client/app/framework/models/Suggestion;Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;ILandroid/view/View;)V
    .locals 0
    .param p2, "suggestion"    # Lcom/navdy/client/app/framework/models/Suggestion;
    .param p3, "homescreenActivity"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;
    .param p4, "actionModePosition"    # I
    .param p5, "selectedCardRow"    # Landroid/view/View;

    .prologue
    .line 988
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 989
    iput-object p2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->suggestion:Lcom/navdy/client/app/framework/models/Suggestion;

    .line 990
    iput-object p3, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->homescreenActivity:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .line 991
    iput p4, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->actionModePosition:I

    .line 992
    iput-object p5, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->selectedCardRow:Landroid/view/View;

    .line 993
    return-void
.end method

.method static synthetic access$1200(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;

    .prologue
    .line 979
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->showFavoriteEditErrorToast()V

    return-void
.end method

.method static synthetic access$1300(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;

    .prologue
    .line 979
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->showFavoriteEditSuccessToast()V

    return-void
.end method

.method static synthetic access$1400(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;)Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;

    .prologue
    .line 979
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->homescreenActivity:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    return-object v0
.end method

.method private deleteSuggestionInMenu(Lcom/navdy/client/app/framework/models/Suggestion;)V
    .locals 4
    .param p1, "suggestion"    # Lcom/navdy/client/app/framework/models/Suggestion;

    .prologue
    .line 1127
    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "menu delete"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1128
    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    if-eqz v1, :cond_0

    .line 1129
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1130
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0800c2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f080134

    .line 1131
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0800a1

    new-instance v3, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback$2;

    invoke-direct {v3, p0, p1}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback$2;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;Lcom/navdy/client/app/framework/models/Suggestion;)V

    .line 1132
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f080307

    const/4 v3, 0x0

    .line 1143
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 1144
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 1146
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_0
    return-void
.end method

.method private hideTabs()V
    .locals 3

    .prologue
    .line 1075
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->homescreenActivity:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->homescreenActivity:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1076
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->homescreenActivity:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    const v2, 0x7f1000e1

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1077
    .local v0, "tabs":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 1078
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1081
    .end local v0    # "tabs":Landroid/view/View;
    :cond_0
    return-void
.end method

.method private saveSuggestionAsFavorite(Lcom/navdy/client/app/framework/models/Suggestion;)V
    .locals 4
    .param p1, "suggestion"    # Lcom/navdy/client/app/framework/models/Suggestion;

    .prologue
    .line 1093
    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "menu save"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1094
    iget-object v1, p1, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    if-eqz v1, :cond_1

    .line 1096
    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "suggestion type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Suggestion;->getType()Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1097
    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "suggestion name: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v3, v3, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1099
    iget-object v0, p1, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    .line 1101
    .local v0, "destination":Lcom/navdy/client/app/framework/models/Destination;
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->isFavoriteDestination()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1102
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->showFavoriteEditErrorToast()V

    .line 1105
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->showProgressDialog()V

    .line 1107
    const/4 v1, -0x1

    new-instance v2, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback$1;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback$1;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;)V

    invoke-virtual {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Destination;->saveDestinationAsFavoritesAsync(ILcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;)V

    .line 1124
    .end local v0    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_1
    return-void
.end method

.method private showFavoriteEditErrorToast()V
    .locals 2

    .prologue
    .line 1169
    const v0, 0x7f0804bc

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->showLongToast(I[Ljava/lang/Object;)V

    .line 1170
    return-void
.end method

.method private showFavoriteEditSuccessToast()V
    .locals 2

    .prologue
    .line 1165
    const v0, 0x7f0804bd

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->showLongToast(I[Ljava/lang/Object;)V

    .line 1166
    return-void
.end method

.method private showTabs()V
    .locals 3

    .prologue
    .line 1084
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->homescreenActivity:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->homescreenActivity:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1085
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->homescreenActivity:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    const v2, 0x7f1000e1

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1086
    .local v0, "tabs":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 1087
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1090
    .end local v0    # "tabs":Landroid/view/View;
    :cond_0
    return-void
.end method

.method private stopRoute(Lcom/navdy/client/app/framework/models/Suggestion;)V
    .locals 2
    .param p1, "suggestion"    # Lcom/navdy/client/app/framework/models/Suggestion;

    .prologue
    .line 1149
    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "menu stop"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1150
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$200(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->stopRouting()V

    .line 1153
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->suggestion:Lcom/navdy/client/app/framework/models/Suggestion;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Suggestion;->isPendingTrip()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    if-eqz v0, :cond_1

    .line 1157
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iget-object v0, v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    if-eqz v0, :cond_0

    .line 1158
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iget-object v0, v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->endSelectionMode()V

    .line 1160
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$1500(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)V

    .line 1162
    :cond_1
    return-void
.end method


# virtual methods
.method public onActionItemClicked(Landroid/support/v7/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "mode"    # Landroid/support/v7/view/ActionMode;
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x0

    .line 1040
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iget-object v2, v2, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    if-nez v2, :cond_0

    .line 1060
    :goto_0
    return v1

    .line 1044
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iget-object v2, v2, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    iget v3, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->actionModePosition:I

    invoke-virtual {v2, v3}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->getItem(I)Lcom/navdy/client/app/framework/models/Suggestion;

    move-result-object v0

    .line 1046
    .local v0, "suggestion":Lcom/navdy/client/app/framework/models/Suggestion;
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 1048
    :pswitch_1
    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->saveSuggestionAsFavorite(Lcom/navdy/client/app/framework/models/Suggestion;)V

    .line 1059
    :goto_1
    invoke-virtual {p1}, Landroid/support/v7/view/ActionMode;->finish()V

    .line 1060
    const/4 v1, 0x1

    goto :goto_0

    .line 1051
    :pswitch_2
    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->deleteSuggestionInMenu(Lcom/navdy/client/app/framework/models/Suggestion;)V

    goto :goto_1

    .line 1054
    :pswitch_3
    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->stopRoute(Lcom/navdy/client/app/framework/models/Suggestion;)V

    goto :goto_1

    .line 1046
    :pswitch_data_0
    .packed-switch 0x7f10041e
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onCreateActionMode(Landroid/support/v7/view/ActionMode;Landroid/view/Menu;)Z
    .locals 2
    .param p1, "mode"    # Landroid/support/v7/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 999
    invoke-virtual {p1}, Landroid/support/v7/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 1001
    .local v0, "inflater":Landroid/view/MenuInflater;
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->suggestion:Lcom/navdy/client/app/framework/models/Suggestion;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/Suggestion;->isActiveTrip()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1002
    const v1, 0x7f110007

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1003
    const v1, 0x7f0800ab

    invoke-virtual {p1, v1}, Landroid/support/v7/view/ActionMode;->setTitle(I)V

    .line 1011
    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 1004
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->suggestion:Lcom/navdy/client/app/framework/models/Suggestion;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/Suggestion;->isPendingTrip()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1005
    const v1, 0x7f11000b

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1006
    const v1, 0x7f0802fb

    invoke-virtual {p1, v1}, Landroid/support/v7/view/ActionMode;->setTitle(I)V

    goto :goto_0

    .line 1007
    :cond_2
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->suggestion:Lcom/navdy/client/app/framework/models/Suggestion;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/Suggestion;->canBeRoutedTo()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1008
    const v1, 0x7f11000c

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1009
    const v1, 0x7f08014e

    invoke-virtual {p1, v1}, Landroid/support/v7/view/ActionMode;->setTitle(I)V

    goto :goto_0
.end method

.method public onDestroyActionMode(Landroid/support/v7/view/ActionMode;)V
    .locals 3
    .param p1, "mode"    # Landroid/support/v7/view/ActionMode;

    .prologue
    .line 1067
    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDestroyActionMode from Suggestion: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1068
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iget-object v0, v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    if-eqz v0, :cond_0

    .line 1069
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iget-object v0, v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->endSelectionMode()V

    .line 1071
    :cond_0
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->showTabs()V

    .line 1072
    return-void
.end method

.method public onPrepareActionMode(Landroid/support/v7/view/ActionMode;Landroid/view/Menu;)Z
    .locals 5
    .param p1, "mode"    # Landroid/support/v7/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1018
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iget-object v1, v1, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    if-eqz v1, :cond_0

    .line 1020
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iget-object v1, v1, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->selectedCardRow:Landroid/view/View;

    iget-object v3, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->suggestion:Lcom/navdy/client/app/framework/models/Suggestion;

    .line 1021
    invoke-virtual {v3}, Lcom/navdy/client/app/framework/models/Suggestion;->getType()Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    move-result-object v3

    iget v4, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->actionModePosition:I

    .line 1020
    invoke-virtual {v1, v2, v3, v4}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->setSelectedSuggestion(Landroid/view/View;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;I)V

    .line 1025
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iget-object v1, v1, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    iget v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->actionModePosition:I

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->getItem(I)Lcom/navdy/client/app/framework/models/Suggestion;

    move-result-object v0

    .line 1026
    .local v0, "suggestion":Lcom/navdy/client/app/framework/models/Suggestion;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    .line 1027
    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/Destination;->isFavoriteDestination()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1029
    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "Suggestion is a Favorite. Save option is disabled"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1031
    const v1, 0x7f10041e

    invoke-interface {p2, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1034
    .end local v0    # "suggestion":Lcom/navdy/client/app/framework/models/Suggestion;
    :cond_0
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->hideTabs()V

    .line 1035
    const/4 v1, 0x1

    return v1
.end method
