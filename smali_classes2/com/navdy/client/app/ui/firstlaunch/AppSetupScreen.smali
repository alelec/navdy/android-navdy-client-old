.class public Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;
.super Ljava/lang/Object;
.source "AppSetupScreen.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public blueTextRes:I

.field public buttonFailRes:I

.field public buttonRes:I

.field public descriptionFailRes:I

.field public descriptionRes:I

.field public hudRes:I

.field public hudResFail:I

.field public isMandatory:Z

.field public screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

.field public titleFailRes:I

.field public titleRes:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$1;

    invoke-direct {v0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$1;-><init>()V

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->UNKNOWN:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    .line 24
    return-void
.end method

.method public constructor <init>(IIIIIIIIILcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;Z)V
    .locals 1
    .param p1, "hudRes"    # I
    .param p2, "hudResFail"    # I
    .param p3, "titleRes"    # I
    .param p4, "descriptionRes"    # I
    .param p5, "blueTextRes"    # I
    .param p6, "buttonRes"    # I
    .param p7, "titleFailRes"    # I
    .param p8, "descriptionFailRes"    # I
    .param p9, "buttonFailRes"    # I
    .param p10, "screenType"    # Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;
    .param p11, "isMandatory"    # Z

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->UNKNOWN:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    .line 27
    iput p1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->hudRes:I

    .line 28
    iput p2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->hudResFail:I

    .line 29
    iput p3, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->titleRes:I

    .line 30
    iput p4, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->descriptionRes:I

    .line 31
    iput p5, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->blueTextRes:I

    .line 32
    iput p6, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->buttonRes:I

    .line 33
    iput p7, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->titleFailRes:I

    .line 34
    iput p8, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->descriptionFailRes:I

    .line 35
    iput p9, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->buttonFailRes:I

    .line 36
    iput-object p10, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    .line 37
    iput-boolean p11, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->isMandatory:Z

    .line 38
    return-void
.end method

.method public constructor <init>(IIIIIIIILcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;Z)V
    .locals 1
    .param p1, "hudRes"    # I
    .param p2, "hudResFail"    # I
    .param p3, "titleRes"    # I
    .param p4, "descriptionRes"    # I
    .param p5, "buttonRes"    # I
    .param p6, "titleFailRes"    # I
    .param p7, "descriptionFailRes"    # I
    .param p8, "buttonFailRes"    # I
    .param p9, "screenType"    # Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;
    .param p10, "isMandatory"    # Z

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->UNKNOWN:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    .line 41
    iput p1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->hudRes:I

    .line 42
    iput p2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->hudResFail:I

    .line 43
    iput p3, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->titleRes:I

    .line 44
    iput p4, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->descriptionRes:I

    .line 45
    iput p5, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->buttonRes:I

    .line 46
    iput p6, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->titleFailRes:I

    .line 47
    iput p7, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->descriptionFailRes:I

    .line 48
    iput p8, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->buttonFailRes:I

    .line 49
    iput-object p9, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    .line 50
    iput-boolean p10, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->isMandatory:Z

    .line 51
    return-void
.end method

.method public constructor <init>(IIIILcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;Z)V
    .locals 1
    .param p1, "hudRes"    # I
    .param p2, "titleRes"    # I
    .param p3, "descriptionRes"    # I
    .param p4, "buttonRes"    # I
    .param p5, "screenType"    # Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;
    .param p6, "isMandatory"    # Z

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->UNKNOWN:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    .line 54
    iput p1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->hudRes:I

    .line 55
    iput p2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->titleRes:I

    .line 56
    iput p3, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->descriptionRes:I

    .line 57
    iput p4, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->buttonRes:I

    .line 58
    iput-object p5, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    .line 59
    iput-boolean p6, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->isMandatory:Z

    .line 60
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->UNKNOWN:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    .line 97
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->hudRes:I

    .line 98
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->hudResFail:I

    .line 99
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->titleRes:I

    .line 100
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->descriptionRes:I

    .line 101
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->blueTextRes:I

    .line 102
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->buttonRes:I

    .line 103
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->titleFailRes:I

    .line 104
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->descriptionFailRes:I

    .line 105
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->buttonFailRes:I

    .line 106
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->fromValue(I)Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    .line 107
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->isMandatory:Z

    .line 108
    return-void

    .line 107
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 83
    iget v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->hudRes:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 84
    iget v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->hudResFail:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 85
    iget v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->titleRes:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 86
    iget v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->descriptionRes:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 87
    iget v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->blueTextRes:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 88
    iget v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->buttonRes:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 89
    iget v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->titleFailRes:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 90
    iget v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->descriptionFailRes:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 91
    iget v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->buttonFailRes:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 92
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 93
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->isMandatory:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 94
    return-void

    .line 93
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
