.class Lcom/navdy/client/app/ui/search/DropPinActivity$1$1;
.super Landroid/os/AsyncTask;
.source "DropPinActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/search/DropPinActivity$1;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$1;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/search/DropPinActivity$1;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/ui/search/DropPinActivity$1;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$1$1;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$1;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 69
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/search/DropPinActivity$1$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 12
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v10, 0x0

    .line 72
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$1$1;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$1;

    iget-object v0, v0, Lcom/navdy/client/app/ui/search/DropPinActivity$1;->this$0:Lcom/navdy/client/app/ui/search/DropPinActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/search/DropPinActivity;->access$000(Lcom/navdy/client/app/ui/search/DropPinActivity;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v1

    iget-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$1$1;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$1;

    iget-object v0, v0, Lcom/navdy/client/app/ui/search/DropPinActivity$1;->this$0:Lcom/navdy/client/app/ui/search/DropPinActivity;

    .line 73
    invoke-static {v0}, Lcom/navdy/client/app/ui/search/DropPinActivity;->access$000(Lcom/navdy/client/app/ui/search/DropPinActivity;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    iget-wide v2, v0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    iget-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$1$1;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$1;

    iget-object v0, v0, Lcom/navdy/client/app/ui/search/DropPinActivity$1;->this$0:Lcom/navdy/client/app/ui/search/DropPinActivity;

    .line 74
    invoke-static {v0}, Lcom/navdy/client/app/ui/search/DropPinActivity;->access$000(Lcom/navdy/client/app/ui/search/DropPinActivity;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    iget-wide v4, v0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    iget-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$1$1;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$1;

    iget-object v0, v0, Lcom/navdy/client/app/ui/search/DropPinActivity$1;->this$0:Lcom/navdy/client/app/ui/search/DropPinActivity;

    .line 75
    invoke-static {v0}, Lcom/navdy/client/app/ui/search/DropPinActivity;->access$000(Lcom/navdy/client/app/ui/search/DropPinActivity;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    iget-wide v6, v0, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    iget-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$1$1;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$1;

    iget-object v0, v0, Lcom/navdy/client/app/ui/search/DropPinActivity$1;->this$0:Lcom/navdy/client/app/ui/search/DropPinActivity;

    .line 76
    invoke-static {v0}, Lcom/navdy/client/app/ui/search/DropPinActivity;->access$000(Lcom/navdy/client/app/ui/search/DropPinActivity;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    iget-wide v8, v0, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    sget-object v11, Lcom/navdy/client/app/framework/models/Destination$Precision;->PRECISE:Lcom/navdy/client/app/framework/models/Destination$Precision;

    .line 72
    invoke-virtual/range {v1 .. v11}, Lcom/navdy/client/app/framework/models/Destination;->handleNewCoordsAndAddress(DDDDLcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/models/Destination$Precision;)V

    .line 79
    return-object v10
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 69
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/search/DropPinActivity$1$1;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .param p1, "aVoid"    # Ljava/lang/Void;

    .prologue
    .line 84
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$1$1;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$1;

    iget-object v1, v1, Lcom/navdy/client/app/ui/search/DropPinActivity$1;->this$0:Lcom/navdy/client/app/ui/search/DropPinActivity;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/search/DropPinActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 85
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "search_result"

    iget-object v2, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$1$1;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$1;

    iget-object v2, v2, Lcom/navdy/client/app/ui/search/DropPinActivity$1;->this$0:Lcom/navdy/client/app/ui/search/DropPinActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/search/DropPinActivity;->access$000(Lcom/navdy/client/app/ui/search/DropPinActivity;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 86
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$1$1;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$1;

    iget-object v1, v1, Lcom/navdy/client/app/ui/search/DropPinActivity$1;->this$0:Lcom/navdy/client/app/ui/search/DropPinActivity;

    const/16 v2, 0x2a

    invoke-virtual {v1, v0, v2}, Lcom/navdy/client/app/ui/search/DropPinActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 87
    return-void
.end method
