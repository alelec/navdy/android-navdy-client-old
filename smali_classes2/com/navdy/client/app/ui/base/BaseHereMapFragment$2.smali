.class Lcom/navdy/client/app/ui/base/BaseHereMapFragment$2;
.super Ljava/lang/Object;
.source "BaseHereMapFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/base/BaseHereMapFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$2;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 113
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$2;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$200(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Lcom/here/android/mpa/mapping/Map;

    move-result-object v0

    if-nez v0, :cond_1

    .line 114
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "checkDimensions, hereMap is null!"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 128
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$2;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$200(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Lcom/here/android/mpa/mapping/Map;

    move-result-object v0

    invoke-virtual {v0}, Lcom/here/android/mpa/mapping/Map;->getWidth()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$2;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$200(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Lcom/here/android/mpa/mapping/Map;

    move-result-object v0

    invoke-virtual {v0}, Lcom/here/android/mpa/mapping/Map;->getHeight()I

    move-result v0

    if-eqz v0, :cond_3

    .line 119
    :cond_2
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$2;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    sget-object v1, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;->READY:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$302(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;)Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;

    .line 120
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "fragment ready, calling readyCompleteListeners"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 122
    :goto_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$2;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$400(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$2;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$400(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentReady;

    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$2;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$200(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Lcom/here/android/mpa/mapping/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentReady;->onReady(Lcom/here/android/mpa/mapping/Map;)V

    goto :goto_1

    .line 126
    :cond_3
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$2;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$500(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x64

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
