.class public Lcom/navdy/client/app/tracking/TrackerConstants$Attributes$DestinationAttributes;
.super Ljava/lang/Object;
.source "TrackerConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/tracking/TrackerConstants$Attributes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DestinationAttributes"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/tracking/TrackerConstants$Attributes$DestinationAttributes$FAVORITE_TYPE_VALUES;,
        Lcom/navdy/client/app/tracking/TrackerConstants$Attributes$DestinationAttributes$SOURCE_VALUES;,
        Lcom/navdy/client/app/tracking/TrackerConstants$Attributes$DestinationAttributes$TYPE_VALUES;
    }
.end annotation


# static fields
.field public static final FAVORITE_TYPE:Ljava/lang/String; = "Favorite_Type"

.field public static final NEXT_TRIP:Ljava/lang/String; = "Next_Trip"

.field public static final ONLINE:Ljava/lang/String; = "Online"

.field public static final SOURCE:Ljava/lang/String; = "Source"

.field public static final TYPE:Ljava/lang/String; = "Type"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
