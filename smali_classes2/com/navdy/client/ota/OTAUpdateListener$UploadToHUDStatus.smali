.class public final enum Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;
.super Ljava/lang/Enum;
.source "OTAUpdateListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/ota/OTAUpdateListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UploadToHUDStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

.field public static final enum COMPLETED:Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

.field public static final enum DEVICE_NOT_CONNECTED:Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

.field public static final enum UPLOADING:Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

.field public static final enum UPLOAD_FAILED:Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 68
    new-instance v0, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    const-string v1, "UPLOADING"

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;->UPLOADING:Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    .line 72
    new-instance v0, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    const-string v1, "DEVICE_NOT_CONNECTED"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;->DEVICE_NOT_CONNECTED:Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    .line 76
    new-instance v0, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    const-string v1, "UPLOAD_FAILED"

    invoke-direct {v0, v1, v4}, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;->UPLOAD_FAILED:Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    .line 80
    new-instance v0, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    const-string v1, "COMPLETED"

    invoke-direct {v0, v1, v5}, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;->COMPLETED:Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    .line 64
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;->UPLOADING:Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;->DEVICE_NOT_CONNECTED:Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;->UPLOAD_FAILED:Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;->COMPLETED:Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;->$VALUES:[Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 64
    const-class v0, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;->$VALUES:[Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    invoke-virtual {v0}, [Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    return-object v0
.end method
