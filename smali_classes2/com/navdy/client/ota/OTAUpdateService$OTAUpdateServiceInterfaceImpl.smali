.class Lcom/navdy/client/ota/OTAUpdateService$OTAUpdateServiceInterfaceImpl;
.super Landroid/os/Binder;
.source "OTAUpdateService.java"

# interfaces
.implements Lcom/navdy/client/ota/OTAUpdateServiceInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/ota/OTAUpdateService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "OTAUpdateServiceInterfaceImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/ota/OTAUpdateService;


# direct methods
.method constructor <init>(Lcom/navdy/client/ota/OTAUpdateService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/ota/OTAUpdateService;

    .prologue
    .line 636
    iput-object p1, p0, Lcom/navdy/client/ota/OTAUpdateService$OTAUpdateServiceInterfaceImpl;->this$0:Lcom/navdy/client/ota/OTAUpdateService;

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    return-void
.end method


# virtual methods
.method public cancelDownload()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 650
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Download canceled by user"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 652
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService$OTAUpdateServiceInterfaceImpl;->this$0:Lcom/navdy/client/ota/OTAUpdateService;

    invoke-static {v0}, Lcom/navdy/client/ota/OTAUpdateService;->access$800(Lcom/navdy/client/ota/OTAUpdateService;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 654
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->bResetDownloadId()V

    .line 655
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService$OTAUpdateServiceInterfaceImpl;->this$0:Lcom/navdy/client/ota/OTAUpdateService;

    invoke-static {v0}, Lcom/navdy/client/ota/OTAUpdateService;->access$600(Lcom/navdy/client/ota/OTAUpdateService;)Lcom/navdy/client/ota/OTAUpdateManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/navdy/client/ota/OTAUpdateManager;->abortDownload()V

    .line 656
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService$OTAUpdateServiceInterfaceImpl;->this$0:Lcom/navdy/client/ota/OTAUpdateService;

    invoke-static {v0, v2}, Lcom/navdy/client/ota/OTAUpdateService;->access$900(Lcom/navdy/client/ota/OTAUpdateService;Z)V

    .line 658
    :cond_0
    return-void
.end method

.method public cancelUpload()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 662
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Upload cancelled by the user"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 664
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService$OTAUpdateServiceInterfaceImpl;->this$0:Lcom/navdy/client/ota/OTAUpdateService;

    invoke-static {v0}, Lcom/navdy/client/ota/OTAUpdateService;->access$1000(Lcom/navdy/client/ota/OTAUpdateService;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 665
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService$OTAUpdateServiceInterfaceImpl;->this$0:Lcom/navdy/client/ota/OTAUpdateService;

    invoke-static {v0}, Lcom/navdy/client/ota/OTAUpdateService;->access$600(Lcom/navdy/client/ota/OTAUpdateService;)Lcom/navdy/client/ota/OTAUpdateManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/navdy/client/ota/OTAUpdateManager;->abortUpload()V

    .line 666
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService$OTAUpdateServiceInterfaceImpl;->this$0:Lcom/navdy/client/ota/OTAUpdateService;

    invoke-static {v0, v2}, Lcom/navdy/client/ota/OTAUpdateService;->access$1100(Lcom/navdy/client/ota/OTAUpdateService;Z)V

    .line 668
    :cond_0
    return-void
.end method

.method public checkForUpdate()Z
    .locals 1

    .prologue
    .line 645
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService$OTAUpdateServiceInterfaceImpl;->this$0:Lcom/navdy/client/ota/OTAUpdateService;

    invoke-static {v0}, Lcom/navdy/client/ota/OTAUpdateService;->access$700(Lcom/navdy/client/ota/OTAUpdateService;)Z

    move-result v0

    return v0
.end method

.method public downloadOTAUpdate()V
    .locals 2

    .prologue
    .line 694
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "User initiated the download"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 695
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService$OTAUpdateServiceInterfaceImpl;->this$0:Lcom/navdy/client/ota/OTAUpdateService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/navdy/client/ota/OTAUpdateService;->access$1202(Lcom/navdy/client/ota/OTAUpdateService;Z)Z

    .line 696
    invoke-virtual {p0}, Lcom/navdy/client/ota/OTAUpdateService$OTAUpdateServiceInterfaceImpl;->checkForUpdate()Z

    .line 697
    return-void
.end method

.method public getHUDBuildVersionText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 711
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->access$1700()Ljava/lang/String;

    move-result-object v0

    .line 712
    .local v0, "lastConnectedDeviceId":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 713
    const-string v1, ""

    .line 719
    :cond_0
    :goto_0
    return-object v1

    .line 715
    :cond_1
    invoke-static {v0}, Lcom/navdy/client/ota/OTAUpdateService;->access$1800(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 716
    .local v1, "version":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 717
    const-string v1, ""

    goto :goto_0
.end method

.method public getOTAUpdateState()Lcom/navdy/client/ota/OTAUpdateService$State;
    .locals 1

    .prologue
    .line 684
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService$OTAUpdateServiceInterfaceImpl;->this$0:Lcom/navdy/client/ota/OTAUpdateService;

    invoke-static {v0}, Lcom/navdy/client/ota/OTAUpdateService;->access$1400(Lcom/navdy/client/ota/OTAUpdateService;)Lcom/navdy/client/ota/OTAUpdateService$State;

    move-result-object v0

    return-object v0
.end method

.method public getUpdateInfo()Lcom/navdy/client/ota/model/UpdateInfo;
    .locals 1

    .prologue
    .line 689
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService$OTAUpdateServiceInterfaceImpl;->this$0:Lcom/navdy/client/ota/OTAUpdateService;

    invoke-static {v0}, Lcom/navdy/client/ota/OTAUpdateService;->access$1500(Lcom/navdy/client/ota/OTAUpdateService;)Lcom/navdy/client/ota/model/UpdateInfo;

    move-result-object v0

    return-object v0
.end method

.method public isCheckingForUpdate()Z
    .locals 1

    .prologue
    .line 639
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService$OTAUpdateServiceInterfaceImpl;->this$0:Lcom/navdy/client/ota/OTAUpdateService;

    invoke-static {v0}, Lcom/navdy/client/ota/OTAUpdateService;->access$000(Lcom/navdy/client/ota/OTAUpdateService;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public lastKnownUploadSize()J
    .locals 2

    .prologue
    .line 724
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService$OTAUpdateServiceInterfaceImpl;->this$0:Lcom/navdy/client/ota/OTAUpdateService;

    invoke-static {v0}, Lcom/navdy/client/ota/OTAUpdateService;->access$1900(Lcom/navdy/client/ota/OTAUpdateService;)J

    move-result-wide v0

    return-wide v0
.end method

.method public registerUIClient(Lcom/navdy/client/ota/OTAUpdateUIClient;)V
    .locals 2
    .param p1, "client"    # Lcom/navdy/client/ota/OTAUpdateUIClient;

    .prologue
    .line 701
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService$OTAUpdateServiceInterfaceImpl;->this$0:Lcom/navdy/client/ota/OTAUpdateService;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-static {v0, v1}, Lcom/navdy/client/ota/OTAUpdateService;->access$1602(Lcom/navdy/client/ota/OTAUpdateService;Ljava/lang/ref/WeakReference;)Ljava/lang/ref/WeakReference;

    .line 702
    return-void
.end method

.method public resetUpdate()V
    .locals 2

    .prologue
    .line 678
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService$OTAUpdateServiceInterfaceImpl;->this$0:Lcom/navdy/client/ota/OTAUpdateService;

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateService$State;->UP_TO_DATE:Lcom/navdy/client/ota/OTAUpdateService$State;

    invoke-static {v0, v1}, Lcom/navdy/client/ota/OTAUpdateService;->access$100(Lcom/navdy/client/ota/OTAUpdateService;Lcom/navdy/client/ota/OTAUpdateService$State;)V

    .line 679
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService$OTAUpdateServiceInterfaceImpl;->this$0:Lcom/navdy/client/ota/OTAUpdateService;

    invoke-static {v0}, Lcom/navdy/client/ota/OTAUpdateService;->access$1300(Lcom/navdy/client/ota/OTAUpdateService;)V

    .line 680
    return-void
.end method

.method public setNetworkDownloadApproval(Z)V
    .locals 1
    .param p1, "isApproved"    # Z

    .prologue
    .line 729
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService$OTAUpdateServiceInterfaceImpl;->this$0:Lcom/navdy/client/ota/OTAUpdateService;

    invoke-static {v0, p1}, Lcom/navdy/client/ota/OTAUpdateService;->access$2002(Lcom/navdy/client/ota/OTAUpdateService;Z)Z

    .line 730
    return-void
.end method

.method public toggleAutoDownload(Z)V
    .locals 2
    .param p1, "enableAutoDownload"    # Z

    .prologue
    .line 672
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "toggleAutoDownload"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 673
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService$OTAUpdateServiceInterfaceImpl;->this$0:Lcom/navdy/client/ota/OTAUpdateService;

    invoke-static {v0, p1}, Lcom/navdy/client/ota/OTAUpdateService;->access$1202(Lcom/navdy/client/ota/OTAUpdateService;Z)Z

    .line 674
    return-void
.end method

.method public unregisterUIClient()V
    .locals 3

    .prologue
    .line 706
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService$OTAUpdateServiceInterfaceImpl;->this$0:Lcom/navdy/client/ota/OTAUpdateService;

    new-instance v1, Ljava/lang/ref/WeakReference;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-static {v0, v1}, Lcom/navdy/client/ota/OTAUpdateService;->access$1602(Lcom/navdy/client/ota/OTAUpdateService;Ljava/lang/ref/WeakReference;)Ljava/lang/ref/WeakReference;

    .line 707
    return-void
.end method
