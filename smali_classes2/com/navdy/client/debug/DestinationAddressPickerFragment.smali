.class public Lcom/navdy/client/debug/DestinationAddressPickerFragment;
.super Lcom/navdy/client/debug/RemoteAddressPickerFragment;
.source "DestinationAddressPickerFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/debug/DestinationAddressPickerFragment$DestinationListener;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected processSelectedLocation(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "location"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p2, "locationLabel"    # Ljava/lang/String;
    .param p3, "streetAddress"    # Ljava/lang/String;

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/navdy/client/debug/DestinationAddressPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    instance-of v1, v1, Lcom/navdy/client/debug/DestinationAddressPickerFragment$DestinationListener;

    if-eqz v1, :cond_0

    .line 16
    invoke-virtual {p0}, Lcom/navdy/client/debug/DestinationAddressPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/DestinationAddressPickerFragment$DestinationListener;

    .line 17
    .local v0, "listener":Lcom/navdy/client/debug/DestinationAddressPickerFragment$DestinationListener;
    invoke-interface {v0, p2, p1}, Lcom/navdy/client/debug/DestinationAddressPickerFragment$DestinationListener;->setDestination(Ljava/lang/String;Lcom/navdy/service/library/events/location/Coordinate;)V

    .line 19
    .end local v0    # "listener":Lcom/navdy/client/debug/DestinationAddressPickerFragment$DestinationListener;
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/client/debug/DestinationAddressPickerFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->popBackStack()V

    .line 20
    return-void
.end method
