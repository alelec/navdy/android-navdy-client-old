.class final enum Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;
.super Ljava/lang/Enum;
.source "ScreenTestingFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/ScreenTestingFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "ListItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

.field public static final enum CONTEXT_MENU:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

.field public static final enum DASH:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

.field public static final enum DIAL_PAIRING:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

.field public static final enum FAV_CONTACT:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

.field public static final enum FAV_PLACES:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

.field public static final enum HYBRID_MAP:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

.field public static final enum MAIN_MENU:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

.field public static final enum RECENT_CONTACT:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

.field public static final enum RECOMMENDED_PLACES:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

.field public static final enum SCREEN_BACK:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

.field public static final enum SCREEN_NOTIFICATION:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

.field public static final enum SCREEN_OPTIONS:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

.field public static final enum SCREEN_OTA_CONFIRM:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

.field public static final enum SHUTDOWN:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

.field public static final enum WELCOME:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;


# instance fields
.field private final mText:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 57
    new-instance v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    const-string v1, "WELCOME"

    const-string v2, "Welcome"

    invoke-direct {v0, v1, v4, v2}, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->WELCOME:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    .line 58
    new-instance v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    const-string v1, "HYBRID_MAP"

    const-string v2, "Map"

    invoke-direct {v0, v1, v5, v2}, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->HYBRID_MAP:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    .line 59
    new-instance v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    const-string v1, "DASH"

    const-string v2, "SmartDash"

    invoke-direct {v0, v1, v6, v2}, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->DASH:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    .line 60
    new-instance v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    const-string v1, "CONTEXT_MENU"

    const-string v2, "Context menu"

    invoke-direct {v0, v1, v7, v2}, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->CONTEXT_MENU:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    .line 61
    new-instance v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    const-string v1, "MAIN_MENU"

    const-string v2, "Main menu"

    invoke-direct {v0, v1, v8, v2}, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->MAIN_MENU:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    .line 62
    new-instance v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    const-string v1, "FAV_PLACES"

    const/4 v2, 0x5

    const-string v3, "Favorite Places"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->FAV_PLACES:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    .line 63
    new-instance v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    const-string v1, "RECOMMENDED_PLACES"

    const/4 v2, 0x6

    const-string v3, "Recommended Places"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->RECOMMENDED_PLACES:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    .line 64
    new-instance v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    const-string v1, "FAV_CONTACT"

    const/4 v2, 0x7

    const-string v3, "Favorite ContactModel"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->FAV_CONTACT:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    .line 65
    new-instance v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    const-string v1, "RECENT_CONTACT"

    const/16 v2, 0x8

    const-string v3, "Recent ContactModel"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->RECENT_CONTACT:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    .line 66
    new-instance v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    const-string v1, "SCREEN_BACK"

    const/16 v2, 0x9

    const-string v3, "Back"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->SCREEN_BACK:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    .line 67
    new-instance v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    const-string v1, "SCREEN_OTA_CONFIRM"

    const/16 v2, 0xa

    const-string v3, "OTA Confirmation"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->SCREEN_OTA_CONFIRM:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    .line 68
    new-instance v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    const-string v1, "SCREEN_OPTIONS"

    const/16 v2, 0xb

    const-string v3, "Options"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->SCREEN_OPTIONS:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    .line 69
    new-instance v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    const-string v1, "SCREEN_NOTIFICATION"

    const/16 v2, 0xc

    const-string v3, "Show Brightness Notification"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->SCREEN_NOTIFICATION:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    .line 70
    new-instance v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    const-string v1, "SHUTDOWN"

    const/16 v2, 0xd

    const-string v3, "ShutDown"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->SHUTDOWN:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    .line 71
    new-instance v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    const-string v1, "DIAL_PAIRING"

    const/16 v2, 0xe

    const-string v3, "Dial Pairing"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->DIAL_PAIRING:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    .line 56
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    sget-object v1, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->WELCOME:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->HYBRID_MAP:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->DASH:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->CONTEXT_MENU:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->MAIN_MENU:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->FAV_PLACES:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->RECOMMENDED_PLACES:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->FAV_CONTACT:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->RECENT_CONTACT:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->SCREEN_BACK:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->SCREEN_OTA_CONFIRM:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->SCREEN_OPTIONS:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->SCREEN_NOTIFICATION:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->SHUTDOWN:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->DIAL_PAIRING:Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->$VALUES:[Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "item"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 76
    iput-object p3, p0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->mText:Ljava/lang/String;

    .line 77
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 56
    const-class v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->$VALUES:[Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    invoke-virtual {v0}, [Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/navdy/client/debug/ScreenTestingFragment$ListItem;->mText:Ljava/lang/String;

    return-object v0
.end method
