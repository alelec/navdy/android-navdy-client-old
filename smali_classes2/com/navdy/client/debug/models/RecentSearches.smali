.class public Lcom/navdy/client/debug/models/RecentSearches;
.super Ljava/lang/Object;
.source "RecentSearches.java"


# static fields
.field private static final MAX_SIZE:I = 0xa


# instance fields
.field private results:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/service/library/events/places/PlacesSearchResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/debug/models/RecentSearches;->results:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public add(Lcom/navdy/service/library/events/places/PlacesSearchResult;)V
    .locals 3
    .param p1, "result"    # Lcom/navdy/service/library/events/places/PlacesSearchResult;

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/navdy/client/debug/models/RecentSearches;->findMatch(Lcom/navdy/service/library/events/places/PlacesSearchResult;)I

    move-result v0

    .line 25
    .local v0, "position":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 26
    iget-object v1, p0, Lcom/navdy/client/debug/models/RecentSearches;->results:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 28
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/debug/models/RecentSearches;->results:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/16 v2, 0xa

    if-lt v1, v2, :cond_1

    .line 29
    iget-object v1, p0, Lcom/navdy/client/debug/models/RecentSearches;->results:Ljava/util/ArrayList;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 31
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/debug/models/RecentSearches;->results:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 32
    return-void
.end method

.method public findMatch(Lcom/navdy/service/library/events/places/PlacesSearchResult;)I
    .locals 4
    .param p1, "result"    # Lcom/navdy/service/library/events/places/PlacesSearchResult;

    .prologue
    .line 37
    iget-object v0, p1, Lcom/navdy/service/library/events/places/PlacesSearchResult;->label:Ljava/lang/String;

    .line 39
    .local v0, "label":Ljava/lang/String;
    const/4 v1, 0x0

    .line 40
    .local v1, "position":I
    :goto_0
    iget-object v3, p0, Lcom/navdy/client/debug/models/RecentSearches;->results:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 41
    iget-object v3, p0, Lcom/navdy/client/debug/models/RecentSearches;->results:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/service/library/events/places/PlacesSearchResult;

    .line 42
    .local v2, "recentResult":Lcom/navdy/service/library/events/places/PlacesSearchResult;
    iget-object v3, v2, Lcom/navdy/service/library/events/places/PlacesSearchResult;->label:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 48
    .end local v1    # "position":I
    .end local v2    # "recentResult":Lcom/navdy/service/library/events/places/PlacesSearchResult;
    :goto_1
    return v1

    .line 45
    .restart local v1    # "position":I
    .restart local v2    # "recentResult":Lcom/navdy/service/library/events/places/PlacesSearchResult;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 46
    goto :goto_0

    .line 48
    .end local v2    # "recentResult":Lcom/navdy/service/library/events/places/PlacesSearchResult;
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public getResults()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/places/PlacesSearchResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 20
    iget-object v0, p0, Lcom/navdy/client/debug/models/RecentSearches;->results:Ljava/util/ArrayList;

    return-object v0
.end method
