.class public Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;
.super Landroid/widget/LinearLayout;
.source "ManeuverDetailPanel.java"


# static fields
.field public static final NO_DATA_PLACEHOLDER:Ljava/lang/String; = "(none)"


# instance fields
.field mActionView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f1002ff
    .end annotation
.end field

.field mAngleView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100302
    .end annotation
.end field

.field mCurStreetNameView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10030a
    .end annotation
.end field

.field mCurStreetNumView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100309
    .end annotation
.end field

.field mDistFromPrevView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100306
    .end annotation
.end field

.field mDistFromStartView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100305
    .end annotation
.end field

.field mDistToNextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100307
    .end annotation
.end field

.field mIconView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100300
    .end annotation
.end field

.field mInstructionView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100308
    .end annotation
.end field

.field protected mManeuver:Lcom/here/android/mpa/routing/Maneuver;

.field protected mMergedChildren:Z

.field mNextStreetImageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10030d
    .end annotation
.end field

.field mNextStreetNameView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10030c
    .end annotation
.end field

.field mNextStreetNumView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10030b
    .end annotation
.end field

.field mRoadElementsView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100312
    .end annotation
.end field

.field mSignpostIconView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100310
    .end annotation
.end field

.field mSignpostLabelDirectionView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100311
    .end annotation
.end field

.field mSignpostNumberView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10030e
    .end annotation
.end field

.field mSignpostTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10030f
    .end annotation
.end field

.field mStartTimeView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100313
    .end annotation
.end field

.field mTrafficDirectionView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100303
    .end annotation
.end field

.field mTransportModeView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100304
    .end annotation
.end field

.field mTurnView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100301
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 75
    invoke-virtual {p0, p1}, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mergeChildren(Landroid/content/Context;)V

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 79
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 80
    invoke-virtual {p0, p1}, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mergeChildren(Landroid/content/Context;)V

    .line 81
    return-void
.end method

.method public static inflate(Landroid/view/ViewGroup;)Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;
    .locals 4
    .param p0, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 68
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0300b9

    const/4 v3, 0x0

    .line 69
    invoke-virtual {v1, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;

    .line 70
    .local v0, "panel":Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;
    return-object v0
.end method


# virtual methods
.method protected defaultIfEmpty(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 245
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    const-string p1, "(none)"

    .line 248
    .end local p1    # "s":Ljava/lang/String;
    :cond_0
    return-object p1
.end method

.method protected defaultIfNull(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 237
    if-nez p1, :cond_0

    .line 238
    const-string v0, "(none)"

    .line 240
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getManeuver()Lcom/here/android/mpa/routing/Maneuver;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    return-object v0
.end method

.method protected mergeChildren(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 88
    iget-boolean v0, p0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mMergedChildren:Z

    if-nez v0, :cond_0

    .line 89
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300b8

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 90
    iput-boolean v2, p0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mMergedChildren:Z

    .line 91
    invoke-static {p0, p0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 92
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->updateViews()V

    .line 94
    :cond_0
    return-void
.end method

.method public setManeuver(Lcom/here/android/mpa/routing/Maneuver;)V
    .locals 2
    .param p1, "maneuver"    # Lcom/here/android/mpa/routing/Maneuver;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    .line 98
    iget-boolean v0, p0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mMergedChildren:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 99
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->updateViews()V

    .line 101
    :cond_0
    return-void
.end method

.method protected updateViews()V
    .locals 35

    .prologue
    .line 104
    const-string v2, "(none)"

    .line 105
    .local v2, "action":Ljava/lang/String;
    const-string v10, "(none)"

    .line 106
    .local v10, "icon":Ljava/lang/String;
    const-string v28, "(none)"

    .line 107
    .local v28, "turn":Ljava/lang/String;
    const-string v3, "(none)"

    .line 108
    .local v3, "angle":Ljava/lang/String;
    const-string v26, "(none)"

    .line 109
    .local v26, "trafficDirection":Ljava/lang/String;
    const-string v27, "(none)"

    .line 110
    .local v27, "transportMode":Ljava/lang/String;
    const-string v8, "(none)"

    .line 111
    .local v8, "distFromStart":Ljava/lang/String;
    const-string v7, "(none)"

    .line 112
    .local v7, "distFromPrev":Ljava/lang/String;
    const-string v9, "(none)"

    .line 113
    .local v9, "distToNext":Ljava/lang/String;
    const-string v11, "(none)"

    .line 114
    .local v11, "instruction":Ljava/lang/String;
    const-string v5, "(none)"

    .line 115
    .local v5, "curStreetNum":Ljava/lang/String;
    const-string v4, "(none)"

    .line 116
    .local v4, "curStreetName":Ljava/lang/String;
    const-string v15, "(none)"

    .line 117
    .local v15, "nextStreetNum":Ljava/lang/String;
    const-string v14, "(none)"

    .line 118
    .local v14, "nextStreetName":Ljava/lang/String;
    const/4 v13, 0x0

    .line 119
    .local v13, "nextStreetBitmap":Landroid/graphics/Bitmap;
    const-string v23, "(none)"

    .line 120
    .local v23, "signpostNumber":Ljava/lang/String;
    const-string v24, "(none)"

    .line 121
    .local v24, "signpostText":Ljava/lang/String;
    const/high16 v20, -0x1000000

    .line 122
    .local v20, "signpostFg":I
    const/16 v19, -0x1

    .line 123
    .local v19, "signpostBg":I
    const/16 v21, 0x0

    .line 124
    .local v21, "signpostIconBitmap":Landroid/graphics/Bitmap;
    const-string v22, "(none)"

    .line 125
    .local v22, "signpostLabelDirection":Ljava/lang/String;
    const-string v18, "(none)"

    .line 126
    .local v18, "roadElementsText":Ljava/lang/String;
    const-string v25, "(none)"

    .line 129
    .local v25, "startTime":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v29, v0

    if-eqz v29, :cond_6

    .line 130
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Maneuver;->getAction()Lcom/here/android/mpa/routing/Maneuver$Action;

    move-result-object v29

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->defaultIfNull(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 131
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Maneuver;->getIcon()Lcom/here/android/mpa/routing/Maneuver$Icon;

    move-result-object v29

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->defaultIfNull(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 132
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Maneuver;->getTurn()Lcom/here/android/mpa/routing/Maneuver$Turn;

    move-result-object v29

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->defaultIfNull(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    .line 133
    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, ""

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/here/android/mpa/routing/Maneuver;->getAngle()I

    move-result v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 134
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Maneuver;->getTrafficDirection()Lcom/here/android/mpa/routing/Maneuver$TrafficDirection;

    move-result-object v29

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->defaultIfNull(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    .line 135
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Maneuver;->getTransportMode()Lcom/here/android/mpa/routing/RouteOptions$TransportMode;

    move-result-object v29

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->defaultIfNull(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v27

    .line 136
    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, ""

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/here/android/mpa/routing/Maneuver;->getDistanceFromStart()I

    move-result v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 137
    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, ""

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/here/android/mpa/routing/Maneuver;->getDistanceFromPreviousManeuver()I

    move-result v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 138
    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, ""

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/here/android/mpa/routing/Maneuver;->getDistanceToNextManeuver()I

    move-result v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 139
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Maneuver;->getRoadNumber()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->defaultIfEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 140
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Maneuver;->getRoadName()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->defaultIfEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 141
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadNumber()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->defaultIfEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 142
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadName()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->defaultIfEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 144
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadImage()Lcom/here/android/mpa/common/Image;

    move-result-object v29

    if-eqz v29, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadImage()Lcom/here/android/mpa/common/Image;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/common/Image;->isValid()Z

    move-result v29

    if-eqz v29, :cond_0

    .line 145
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadImage()Lcom/here/android/mpa/common/Image;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/common/Image;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v13

    .line 148
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Maneuver;->getSignpost()Lcom/here/android/mpa/routing/Signpost;

    move-result-object v29

    if-eqz v29, :cond_3

    .line 149
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Maneuver;->getSignpost()Lcom/here/android/mpa/routing/Signpost;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Signpost;->getExitNumber()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->defaultIfEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 150
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Maneuver;->getSignpost()Lcom/here/android/mpa/routing/Signpost;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Signpost;->getExitText()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->defaultIfEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 151
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Maneuver;->getSignpost()Lcom/here/android/mpa/routing/Signpost;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Signpost;->getForegroundColor()I

    move-result v20

    .line 152
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Maneuver;->getSignpost()Lcom/here/android/mpa/routing/Signpost;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Signpost;->getBackgroundColor()I

    move-result v19

    .line 154
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Maneuver;->getSignpost()Lcom/here/android/mpa/routing/Signpost;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Signpost;->getExitIcon()Lcom/here/android/mpa/common/Image;

    move-result-object v29

    if-eqz v29, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Maneuver;->getSignpost()Lcom/here/android/mpa/routing/Signpost;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Signpost;->getExitIcon()Lcom/here/android/mpa/common/Image;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/common/Image;->isValid()Z

    move-result v29

    if-eqz v29, :cond_1

    .line 155
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadImage()Lcom/here/android/mpa/common/Image;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/common/Image;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v21

    .line 158
    :cond_1
    const-string v22, "(none)"

    .line 159
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 160
    .local v6, "directionLabelList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Maneuver;->getSignpost()Lcom/here/android/mpa/routing/Signpost;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Signpost;->getExitDirections()Ljava/util/List;

    move-result-object v29

    invoke-interface/range {v29 .. v29}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v29

    :goto_0
    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->hasNext()Z

    move-result v30

    if-eqz v30, :cond_2

    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;

    .line 161
    .local v12, "label":Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;
    const-string v30, "dir: %s name: %s text: %s"

    const/16 v31, 0x3

    move/from16 v0, v31

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    .line 162
    invoke-virtual {v12}, Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;->getRouteDirection()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->defaultIfEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    aput-object v33, v31, v32

    const/16 v32, 0x1

    invoke-virtual {v12}, Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;->getRouteName()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->defaultIfEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    aput-object v33, v31, v32

    const/16 v32, 0x2

    invoke-virtual {v12}, Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;->getText()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->defaultIfEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    aput-object v33, v31, v32

    .line 161
    invoke-static/range {v30 .. v31}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 165
    .end local v12    # "label":Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;
    :cond_2
    const-string v29, "\n"

    move-object/from16 v0, v29

    invoke-static {v0, v6}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v22

    .line 169
    .end local v6    # "directionLabelList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_3
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 170
    .local v17, "roadElements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Maneuver;->getRoadElements()Ljava/util/List;

    move-result-object v29

    invoke-interface/range {v29 .. v29}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v30

    :goto_1
    invoke-interface/range {v30 .. v30}, Ljava/util/Iterator;->hasNext()Z

    move-result v29

    if-eqz v29, :cond_5

    invoke-interface/range {v30 .. v30}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/here/android/mpa/common/RoadElement;

    .line 171
    .local v16, "roadElement":Lcom/here/android/mpa/common/RoadElement;
    const-string v31, "roadName: %s routeName: %s%nattr: %s%navgSpeed: %f formOfWay: %s lanes: %s%nspeedLmt: %f.1 plural: %s ped: %s%nstartTime: %s"

    const/16 v29, 0xa

    move/from16 v0, v29

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v32, v0

    const/16 v29, 0x0

    .line 176
    invoke-virtual/range {v16 .. v16}, Lcom/here/android/mpa/common/RoadElement;->getRoadName()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->defaultIfEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    aput-object v33, v32, v29

    const/16 v29, 0x1

    invoke-virtual/range {v16 .. v16}, Lcom/here/android/mpa/common/RoadElement;->getRouteName()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->defaultIfEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    aput-object v33, v32, v29

    const/16 v29, 0x2

    const-string v33, " "

    invoke-virtual/range {v16 .. v16}, Lcom/here/android/mpa/common/RoadElement;->getAttributes()Ljava/util/EnumSet;

    move-result-object v34

    invoke-static/range {v33 .. v34}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v33

    aput-object v33, v32, v29

    const/16 v29, 0x3

    .line 177
    invoke-virtual/range {v16 .. v16}, Lcom/here/android/mpa/common/RoadElement;->getDefaultSpeed()F

    move-result v33

    invoke-static/range {v33 .. v33}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v33

    aput-object v33, v32, v29

    const/16 v29, 0x4

    invoke-virtual/range {v16 .. v16}, Lcom/here/android/mpa/common/RoadElement;->getFormOfWay()Lcom/here/android/mpa/common/RoadElement$FormOfWay;

    move-result-object v33

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->defaultIfNull(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v33

    aput-object v33, v32, v29

    const/16 v29, 0x5

    invoke-virtual/range {v16 .. v16}, Lcom/here/android/mpa/common/RoadElement;->getNumberOfLanes()I

    move-result v33

    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v33

    aput-object v33, v32, v29

    const/16 v29, 0x6

    .line 178
    invoke-virtual/range {v16 .. v16}, Lcom/here/android/mpa/common/RoadElement;->getSpeedLimit()F

    move-result v33

    invoke-static/range {v33 .. v33}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v33

    aput-object v33, v32, v29

    const/16 v33, 0x7

    invoke-virtual/range {v16 .. v16}, Lcom/here/android/mpa/common/RoadElement;->isPlural()Z

    move-result v29

    if-eqz v29, :cond_4

    invoke-virtual/range {v16 .. v16}, Lcom/here/android/mpa/common/RoadElement;->getPluralType()Lcom/here/android/mpa/common/RoadElement$PluralType;

    move-result-object v29

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->defaultIfNull(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v29

    :goto_2
    aput-object v29, v32, v33

    const/16 v29, 0x8

    invoke-virtual/range {v16 .. v16}, Lcom/here/android/mpa/common/RoadElement;->isPedestrian()Z

    move-result v33

    invoke-static/range {v33 .. v33}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v33

    aput-object v33, v32, v29

    const/16 v29, 0x9

    .line 179
    invoke-virtual/range {v16 .. v16}, Lcom/here/android/mpa/common/RoadElement;->getStartTime()Ljava/util/Date;

    move-result-object v33

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->defaultIfNull(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v33

    aput-object v33, v32, v29

    .line 171
    invoke-static/range {v31 .. v32}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v17

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 178
    :cond_4
    const-string v29, "no"

    goto :goto_2

    .line 182
    .end local v16    # "roadElement":Lcom/here/android/mpa/common/RoadElement;
    :cond_5
    const-string v29, "\n\n"

    move-object/from16 v0, v29

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v18

    .line 183
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Maneuver;->getStartTime()Ljava/util/Date;

    move-result-object v29

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->defaultIfNull(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v25

    .line 186
    .end local v17    # "roadElements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mActionView:Landroid/widget/TextView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 187
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mIconView:Landroid/widget/TextView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mTurnView:Landroid/widget/TextView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 190
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mAngleView:Landroid/widget/TextView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mTrafficDirectionView:Landroid/widget/TextView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 192
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mTransportModeView:Landroid/widget/TextView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 194
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mDistFromStartView:Landroid/widget/TextView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mDistFromPrevView:Landroid/widget/TextView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mDistToNextView:Landroid/widget/TextView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 198
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mInstructionView:Landroid/widget/TextView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mCurStreetNumView:Landroid/widget/TextView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 201
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mCurStreetNameView:Landroid/widget/TextView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 203
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mNextStreetNumView:Landroid/widget/TextView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 204
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mNextStreetNameView:Landroid/widget/TextView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-virtual {v0, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 206
    if-eqz v13, :cond_7

    .line 207
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mNextStreetImageView:Landroid/widget/ImageView;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    invoke-virtual/range {v29 .. v30}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 208
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mNextStreetImageView:Landroid/widget/ImageView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-virtual {v0, v13}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 213
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mSignpostNumberView:Landroid/widget/TextView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mSignpostTextView:Landroid/widget/TextView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mSignpostNumberView:Landroid/widget/TextView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mSignpostTextView:Landroid/widget/TextView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 219
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mSignpostNumberView:Landroid/widget/TextView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mSignpostTextView:Landroid/widget/TextView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 223
    if-eqz v21, :cond_8

    .line 224
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mSignpostIconView:Landroid/widget/ImageView;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    invoke-virtual/range {v29 .. v30}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 225
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mSignpostIconView:Landroid/widget/ImageView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 230
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mSignpostLabelDirectionView:Landroid/widget/TextView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mRoadElementsView:Landroid/widget/TextView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 233
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mStartTimeView:Landroid/widget/TextView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 234
    return-void

    .line 210
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mNextStreetImageView:Landroid/widget/ImageView;

    move-object/from16 v29, v0

    const/16 v30, 0x8

    invoke-virtual/range {v29 .. v30}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 227
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mSignpostIconView:Landroid/widget/ImageView;

    move-object/from16 v29, v0

    const/16 v30, 0x4

    invoke-virtual/range {v29 .. v30}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4
.end method
