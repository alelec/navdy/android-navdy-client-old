.class Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment$1;
.super Ljava/lang/Object;
.source "NavRouteBrowserFragment.java"

# interfaces
.implements Lcom/here/android/mpa/routing/RouteManager$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->buildRoute()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;

    .prologue
    .line 192
    iput-object p1, p0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment$1;->this$0:Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCalculateRouteFinished(Lcom/here/android/mpa/routing/RouteManager$Error;Ljava/util/List;)V
    .locals 3
    .param p1, "error"    # Lcom/here/android/mpa/routing/RouteManager$Error;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/here/android/mpa/routing/RouteManager$Error;",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/routing/RouteResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "results":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/RouteResult;>;"
    const/4 v2, 0x0

    .line 195
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment$1;->this$0:Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;

    iput-boolean v2, v0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->mCalculatingRoute:Z

    .line 196
    sget-object v0, Lcom/here/android/mpa/routing/RouteManager$Error;->NONE:Lcom/here/android/mpa/routing/RouteManager$Error;

    if-ne p1, v0, :cond_1

    .line 197
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 198
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment$1;->this$0:Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/routing/RouteResult;

    invoke-virtual {v0}, Lcom/here/android/mpa/routing/RouteResult;->getRoute()Lcom/here/android/mpa/routing/Route;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->setRoute(Lcom/here/android/mpa/routing/Route;)V

    .line 206
    :goto_0
    return-void

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment$1;->this$0:Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;

    const-string v1, "No results."

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->showError(Ljava/lang/String;)V

    goto :goto_0

    .line 204
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment$1;->this$0:Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/here/android/mpa/routing/RouteManager$Error;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->showError(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onProgress(I)V
    .locals 3
    .param p1, "percentage"    # I

    .prologue
    .line 210
    sget-object v0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "... "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " percent done ..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 211
    return-void
.end method
