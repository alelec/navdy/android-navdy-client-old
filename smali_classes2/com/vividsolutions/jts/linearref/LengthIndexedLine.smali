.class public Lcom/vividsolutions/jts/linearref/LengthIndexedLine;
.super Ljava/lang/Object;
.source "LengthIndexedLine.java"


# instance fields
.field private linearGeom:Lcom/vividsolutions/jts/geom/Geometry;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 0
    .param p1, "linearGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/vividsolutions/jts/linearref/LengthIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    .line 60
    return-void
.end method

.method private locationOf(D)Lcom/vividsolutions/jts/linearref/LinearLocation;
    .locals 1
    .param p1, "index"    # D

    .prologue
    .line 127
    iget-object v0, p0, Lcom/vividsolutions/jts/linearref/LengthIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-static {v0, p1, p2}, Lcom/vividsolutions/jts/linearref/LengthLocationMap;->getLocation(Lcom/vividsolutions/jts/geom/Geometry;D)Lcom/vividsolutions/jts/linearref/LinearLocation;

    move-result-object v0

    return-object v0
.end method

.method private locationOf(DZ)Lcom/vividsolutions/jts/linearref/LinearLocation;
    .locals 1
    .param p1, "index"    # D
    .param p3, "resolveLower"    # Z

    .prologue
    .line 132
    iget-object v0, p0, Lcom/vividsolutions/jts/linearref/LengthIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-static {v0, p1, p2, p3}, Lcom/vividsolutions/jts/linearref/LengthLocationMap;->getLocation(Lcom/vividsolutions/jts/geom/Geometry;DZ)Lcom/vividsolutions/jts/linearref/LinearLocation;

    move-result-object v0

    return-object v0
.end method

.method private positiveIndex(D)D
    .locals 3
    .param p1, "index"    # D

    .prologue
    .line 268
    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-ltz v0, :cond_0

    .line 269
    .end local p1    # "index":D
    :goto_0
    return-wide p1

    .restart local p1    # "index":D
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/linearref/LengthIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->getLength()D

    move-result-wide v0

    add-double/2addr p1, v0

    goto :goto_0
.end method


# virtual methods
.method public clampIndex(D)D
    .locals 7
    .param p1, "index"    # D

    .prologue
    .line 256
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/linearref/LengthIndexedLine;->positiveIndex(D)D

    move-result-wide v2

    .line 257
    .local v2, "posIndex":D
    invoke-virtual {p0}, Lcom/vividsolutions/jts/linearref/LengthIndexedLine;->getStartIndex()D

    move-result-wide v4

    .line 258
    .local v4, "startIndex":D
    cmpg-double v6, v2, v4

    if-gez v6, :cond_0

    .line 263
    .end local v4    # "startIndex":D
    :goto_0
    return-wide v4

    .line 260
    .restart local v4    # "startIndex":D
    :cond_0
    invoke-virtual {p0}, Lcom/vividsolutions/jts/linearref/LengthIndexedLine;->getEndIndex()D

    move-result-wide v0

    .line 261
    .local v0, "endIndex":D
    cmpl-double v6, v2, v0

    if-lez v6, :cond_1

    move-wide v4, v0

    goto :goto_0

    :cond_1
    move-wide v4, v2

    .line 263
    goto :goto_0
.end method

.method public extractLine(DD)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 9
    .param p1, "startIndex"    # D
    .param p3, "endIndex"    # D

    .prologue
    .line 113
    new-instance v3, Lcom/vividsolutions/jts/linearref/LocationIndexedLine;

    iget-object v8, p0, Lcom/vividsolutions/jts/linearref/LengthIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-direct {v3, v8}, Lcom/vividsolutions/jts/linearref/LocationIndexedLine;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 114
    .local v3, "lil":Lcom/vividsolutions/jts/linearref/LocationIndexedLine;
    invoke-virtual {p0, p1, p2}, Lcom/vividsolutions/jts/linearref/LengthIndexedLine;->clampIndex(D)D

    move-result-wide v6

    .line 115
    .local v6, "startIndex2":D
    invoke-virtual {p0, p3, p4}, Lcom/vividsolutions/jts/linearref/LengthIndexedLine;->clampIndex(D)D

    move-result-wide v0

    .line 117
    .local v0, "endIndex2":D
    cmpl-double v8, v6, v0

    if-nez v8, :cond_0

    const/4 v4, 0x1

    .line 118
    .local v4, "resolveStartLower":Z
    :goto_0
    invoke-direct {p0, v6, v7, v4}, Lcom/vividsolutions/jts/linearref/LengthIndexedLine;->locationOf(DZ)Lcom/vividsolutions/jts/linearref/LinearLocation;

    move-result-object v5

    .line 121
    .local v5, "startLoc":Lcom/vividsolutions/jts/linearref/LinearLocation;
    invoke-direct {p0, v0, v1}, Lcom/vividsolutions/jts/linearref/LengthIndexedLine;->locationOf(D)Lcom/vividsolutions/jts/linearref/LinearLocation;

    move-result-object v2

    .line 122
    .local v2, "endLoc":Lcom/vividsolutions/jts/linearref/LinearLocation;
    iget-object v8, p0, Lcom/vividsolutions/jts/linearref/LengthIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-static {v8, v5, v2}, Lcom/vividsolutions/jts/linearref/ExtractLineByLocation;->extract(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/linearref/LinearLocation;Lcom/vividsolutions/jts/linearref/LinearLocation;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v8

    return-object v8

    .line 117
    .end local v2    # "endLoc":Lcom/vividsolutions/jts/linearref/LinearLocation;
    .end local v4    # "resolveStartLower":Z
    .end local v5    # "startLoc":Lcom/vividsolutions/jts/linearref/LinearLocation;
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public extractPoint(D)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 3
    .param p1, "index"    # D

    .prologue
    .line 75
    iget-object v1, p0, Lcom/vividsolutions/jts/linearref/LengthIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-static {v1, p1, p2}, Lcom/vividsolutions/jts/linearref/LengthLocationMap;->getLocation(Lcom/vividsolutions/jts/geom/Geometry;D)Lcom/vividsolutions/jts/linearref/LinearLocation;

    move-result-object v0

    .line 76
    .local v0, "loc":Lcom/vividsolutions/jts/linearref/LinearLocation;
    iget-object v1, p0, Lcom/vividsolutions/jts/linearref/LengthIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/linearref/LinearLocation;->getCoordinate(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    return-object v1
.end method

.method public extractPoint(DD)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 5
    .param p1, "index"    # D
    .param p3, "offsetDistance"    # D

    .prologue
    .line 97
    iget-object v1, p0, Lcom/vividsolutions/jts/linearref/LengthIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-static {v1, p1, p2}, Lcom/vividsolutions/jts/linearref/LengthLocationMap;->getLocation(Lcom/vividsolutions/jts/geom/Geometry;D)Lcom/vividsolutions/jts/linearref/LinearLocation;

    move-result-object v0

    .line 98
    .local v0, "loc":Lcom/vividsolutions/jts/linearref/LinearLocation;
    iget-object v1, p0, Lcom/vividsolutions/jts/linearref/LengthIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/linearref/LinearLocation;->getSegment(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/LineSegment;

    move-result-object v1

    invoke-virtual {v0}, Lcom/vividsolutions/jts/linearref/LinearLocation;->getSegmentFraction()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3, p3, p4}, Lcom/vividsolutions/jts/geom/LineSegment;->pointAlongOffset(DD)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    return-object v1
.end method

.method public getEndIndex()D
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, Lcom/vividsolutions/jts/linearref/LengthIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->getLength()D

    move-result-wide v0

    return-wide v0
.end method

.method public getStartIndex()D
    .locals 2

    .prologue
    .line 224
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public indexOf(Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 2
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 154
    iget-object v0, p0, Lcom/vividsolutions/jts/linearref/LengthIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-static {v0, p1}, Lcom/vividsolutions/jts/linearref/LengthIndexOfPoint;->indexOf(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    return-wide v0
.end method

.method public indexOfAfter(Lcom/vividsolutions/jts/geom/Coordinate;D)D
    .locals 2
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "minIndex"    # D

    .prologue
    .line 181
    iget-object v0, p0, Lcom/vividsolutions/jts/linearref/LengthIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-static {v0, p1, p2, p3}, Lcom/vividsolutions/jts/linearref/LengthIndexOfPoint;->indexOfAfter(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Coordinate;D)D

    move-result-wide v0

    return-wide v0
.end method

.method public indicesOf(Lcom/vividsolutions/jts/geom/Geometry;)[D
    .locals 6
    .param p1, "subLine"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 195
    iget-object v2, p0, Lcom/vividsolutions/jts/linearref/LengthIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-static {v2, p1}, Lcom/vividsolutions/jts/linearref/LocationIndexOfLine;->indicesOf(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/linearref/LinearLocation;

    move-result-object v1

    .line 196
    .local v1, "locIndex":[Lcom/vividsolutions/jts/linearref/LinearLocation;
    const/4 v2, 0x2

    new-array v0, v2, [D

    iget-object v2, p0, Lcom/vividsolutions/jts/linearref/LengthIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v3, v1, v4

    invoke-static {v2, v3}, Lcom/vividsolutions/jts/linearref/LengthLocationMap;->getLength(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/linearref/LinearLocation;)D

    move-result-wide v2

    aput-wide v2, v0, v4

    iget-object v2, p0, Lcom/vividsolutions/jts/linearref/LengthIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v3, v1, v5

    invoke-static {v2, v3}, Lcom/vividsolutions/jts/linearref/LengthLocationMap;->getLength(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/linearref/LinearLocation;)D

    move-result-wide v2

    aput-wide v2, v0, v5

    .line 200
    .local v0, "index":[D
    return-object v0
.end method

.method public isValidIndex(D)Z
    .locals 3
    .param p1, "index"    # D

    .prologue
    .line 244
    invoke-virtual {p0}, Lcom/vividsolutions/jts/linearref/LengthIndexedLine;->getStartIndex()D

    move-result-wide v0

    cmpl-double v0, p1, v0

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lcom/vividsolutions/jts/linearref/LengthIndexedLine;->getEndIndex()D

    move-result-wide v0

    cmpg-double v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public project(Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 2
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 215
    iget-object v0, p0, Lcom/vividsolutions/jts/linearref/LengthIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-static {v0, p1}, Lcom/vividsolutions/jts/linearref/LengthIndexOfPoint;->indexOf(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    return-wide v0
.end method
