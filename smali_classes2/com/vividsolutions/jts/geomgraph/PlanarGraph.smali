.class public Lcom/vividsolutions/jts/geomgraph/PlanarGraph;
.super Ljava/lang/Object;
.source "PlanarGraph.java"


# instance fields
.field protected edgeEndList:Ljava/util/List;

.field protected edges:Ljava/util/List;

.field protected nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->edges:Ljava/util/List;

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->edgeEndList:Ljava/util/List;

    .line 87
    new-instance v0, Lcom/vividsolutions/jts/geomgraph/NodeMap;

    new-instance v1, Lcom/vividsolutions/jts/geomgraph/NodeFactory;

    invoke-direct {v1}, Lcom/vividsolutions/jts/geomgraph/NodeFactory;-><init>()V

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/geomgraph/NodeMap;-><init>(Lcom/vividsolutions/jts/geomgraph/NodeFactory;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;

    .line 88
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geomgraph/NodeFactory;)V
    .locals 1
    .param p1, "nodeFact"    # Lcom/vividsolutions/jts/geomgraph/NodeFactory;

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->edges:Ljava/util/List;

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->edgeEndList:Ljava/util/List;

    .line 83
    new-instance v0, Lcom/vividsolutions/jts/geomgraph/NodeMap;

    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/geomgraph/NodeMap;-><init>(Lcom/vividsolutions/jts/geomgraph/NodeFactory;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;

    .line 84
    return-void
.end method

.method public static linkResultDirectedEdges(Ljava/util/Collection;)V
    .locals 3
    .param p0, "nodes"    # Ljava/util/Collection;

    .prologue
    .line 72
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "nodeit":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 73
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Node;

    .line 74
    .local v0, "node":Lcom/vividsolutions/jts/geomgraph/Node;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Node;->getEdges()Lcom/vividsolutions/jts/geomgraph/EdgeEndStar;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geomgraph/DirectedEdgeStar;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geomgraph/DirectedEdgeStar;->linkResultDirectedEdges()V

    goto :goto_0

    .line 76
    .end local v0    # "node":Lcom/vividsolutions/jts/geomgraph/Node;
    :cond_0
    return-void
.end method

.method private matchInSameDirection(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 3
    .param p1, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "ep0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p4, "ep1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const/4 v0, 0x0

    .line 227
    invoke-virtual {p1, p3}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 233
    :cond_0
    :goto_0
    return v0

    .line 230
    :cond_1
    invoke-static {p1, p2, p4}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->computeOrientation(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1, p2}, Lcom/vividsolutions/jts/geomgraph/Quadrant;->quadrant(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v1

    invoke-static {p3, p4}, Lcom/vividsolutions/jts/geomgraph/Quadrant;->quadrant(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 232
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public add(Lcom/vividsolutions/jts/geomgraph/EdgeEnd;)V
    .locals 1
    .param p1, "e"    # Lcom/vividsolutions/jts/geomgraph/EdgeEnd;

    .prologue
    .line 107
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geomgraph/NodeMap;->add(Lcom/vividsolutions/jts/geomgraph/EdgeEnd;)V

    .line 108
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->edgeEndList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    return-void
.end method

.method public addEdges(Ljava/util/List;)V
    .locals 5
    .param p1, "edgesToAdd"    # Ljava/util/List;

    .prologue
    .line 127
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 128
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 129
    .local v2, "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    iget-object v4, p0, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->edges:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    new-instance v0, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;

    const/4 v4, 0x1

    invoke-direct {v0, v2, v4}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;-><init>(Lcom/vividsolutions/jts/geomgraph/Edge;Z)V

    .line 132
    .local v0, "de1":Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    new-instance v1, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v4}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;-><init>(Lcom/vividsolutions/jts/geomgraph/Edge;Z)V

    .line 133
    .local v1, "de2":Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->setSym(Lcom/vividsolutions/jts/geomgraph/DirectedEdge;)V

    .line 134
    invoke-virtual {v1, v0}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->setSym(Lcom/vividsolutions/jts/geomgraph/DirectedEdge;)V

    .line 136
    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->add(Lcom/vividsolutions/jts/geomgraph/EdgeEnd;)V

    .line 137
    invoke-virtual {p0, v1}, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->add(Lcom/vividsolutions/jts/geomgraph/EdgeEnd;)V

    goto :goto_0

    .line 139
    .end local v0    # "de1":Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    .end local v1    # "de2":Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    .end local v2    # "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    :cond_0
    return-void
.end method

.method public addNode(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geomgraph/Node;
    .locals 1
    .param p1, "coord"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geomgraph/NodeMap;->addNode(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geomgraph/Node;

    move-result-object v0

    return-object v0
.end method

.method public addNode(Lcom/vividsolutions/jts/geomgraph/Node;)Lcom/vividsolutions/jts/geomgraph/Node;
    .locals 1
    .param p1, "node"    # Lcom/vividsolutions/jts/geomgraph/Node;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geomgraph/NodeMap;->addNode(Lcom/vividsolutions/jts/geomgraph/Node;)Lcom/vividsolutions/jts/geomgraph/Node;

    move-result-object v0

    return-object v0
.end method

.method debugPrint(Ljava/lang/Object;)V
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 248
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->print(Ljava/lang/Object;)V

    .line 249
    return-void
.end method

.method debugPrintln(Ljava/lang/Object;)V
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 252
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 253
    return-void
.end method

.method public find(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geomgraph/Node;
    .locals 1
    .param p1, "coord"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geomgraph/NodeMap;->find(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geomgraph/Node;

    move-result-object v0

    return-object v0
.end method

.method public findEdge(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geomgraph/Edge;
    .locals 4
    .param p1, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 190
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->edges:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 191
    iget-object v3, p0, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->edges:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 192
    .local v0, "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Edge;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 193
    .local v1, "eCoord":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v3, 0x0

    aget-object v3, v1, v3

    invoke-virtual {p1, v3}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    aget-object v3, v1, v3

    invoke-virtual {p2, v3}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 196
    .end local v0    # "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    .end local v1    # "eCoord":[Lcom/vividsolutions/jts/geom/Coordinate;
    :goto_1
    return-object v0

    .line 190
    .restart local v0    # "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    .restart local v1    # "eCoord":[Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 196
    .end local v0    # "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    .end local v1    # "eCoord":[Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public findEdgeEnd(Lcom/vividsolutions/jts/geomgraph/Edge;)Lcom/vividsolutions/jts/geomgraph/EdgeEnd;
    .locals 3
    .param p1, "e"    # Lcom/vividsolutions/jts/geomgraph/Edge;

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->getEdgeEnds()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 175
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/EdgeEnd;

    .line 176
    .local v0, "ee":Lcom/vividsolutions/jts/geomgraph/EdgeEnd;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/EdgeEnd;->getEdge()Lcom/vividsolutions/jts/geomgraph/Edge;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 179
    .end local v0    # "ee":Lcom/vividsolutions/jts/geomgraph/EdgeEnd;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public findEdgeInSameDirection(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geomgraph/Edge;
    .locals 5
    .param p1, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 207
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->edges:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 208
    iget-object v3, p0, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->edges:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 210
    .local v0, "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Edge;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 211
    .local v1, "eCoord":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v3, 0x0

    aget-object v3, v1, v3

    const/4 v4, 0x1

    aget-object v4, v1, v4

    invoke-direct {p0, p1, p2, v3, v4}, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->matchInSameDirection(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 217
    .end local v0    # "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    .end local v1    # "eCoord":[Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    :goto_1
    return-object v0

    .line 214
    .restart local v0    # "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    .restart local v1    # "eCoord":[Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_1
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    aget-object v3, v1, v3

    array-length v4, v1

    add-int/lit8 v4, v4, -0x2

    aget-object v4, v1, v4

    invoke-direct {p0, p1, p2, v3, v4}, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->matchInSameDirection(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 207
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 217
    .end local v0    # "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    .end local v1    # "eCoord":[Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getEdgeEnds()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->edgeEndList:Ljava/util/List;

    return-object v0
.end method

.method public getEdgeIterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->edges:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public getNodeIterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/NodeMap;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public getNodes()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/NodeMap;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method protected insertEdge(Lcom/vividsolutions/jts/geomgraph/Edge;)V
    .locals 1
    .param p1, "e"    # Lcom/vividsolutions/jts/geomgraph/Edge;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->edges:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    return-void
.end method

.method public isBoundaryNode(ILcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 5
    .param p1, "geomIndex"    # I
    .param p2, "coord"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 95
    iget-object v4, p0, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;

    invoke-virtual {v4, p2}, Lcom/vividsolutions/jts/geomgraph/NodeMap;->find(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geomgraph/Node;

    move-result-object v1

    .line 96
    .local v1, "node":Lcom/vividsolutions/jts/geomgraph/Node;
    if-nez v1, :cond_1

    .line 99
    :cond_0
    :goto_0
    return v2

    .line 97
    :cond_1
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/Node;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v0

    .line 98
    .local v0, "label":Lcom/vividsolutions/jts/geomgraph/Label;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geomgraph/Label;->getLocation(I)I

    move-result v4

    if-ne v4, v3, :cond_0

    move v2, v3

    goto :goto_0
.end method

.method public linkAllDirectedEdges()V
    .locals 3

    .prologue
    .line 160
    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geomgraph/NodeMap;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "nodeit":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 161
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Node;

    .line 162
    .local v0, "node":Lcom/vividsolutions/jts/geomgraph/Node;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Node;->getEdges()Lcom/vividsolutions/jts/geomgraph/EdgeEndStar;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geomgraph/DirectedEdgeStar;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geomgraph/DirectedEdgeStar;->linkAllDirectedEdges()V

    goto :goto_0

    .line 164
    .end local v0    # "node":Lcom/vividsolutions/jts/geomgraph/Node;
    :cond_0
    return-void
.end method

.method public linkResultDirectedEdges()V
    .locals 3

    .prologue
    .line 148
    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geomgraph/NodeMap;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "nodeit":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 149
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Node;

    .line 150
    .local v0, "node":Lcom/vividsolutions/jts/geomgraph/Node;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Node;->getEdges()Lcom/vividsolutions/jts/geomgraph/EdgeEndStar;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geomgraph/DirectedEdgeStar;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geomgraph/DirectedEdgeStar;->linkResultDirectedEdges()V

    goto :goto_0

    .line 152
    .end local v0    # "node":Lcom/vividsolutions/jts/geomgraph/Node;
    :cond_0
    return-void
.end method

.method public printEdges(Ljava/io/PrintStream;)V
    .locals 4
    .param p1, "out"    # Ljava/io/PrintStream;

    .prologue
    .line 238
    const-string v2, "Edges:"

    invoke-virtual {p1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 239
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->edges:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 240
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "edge "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 241
    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->edges:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 242
    .local v0, "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geomgraph/Edge;->print(Ljava/io/PrintStream;)V

    .line 243
    iget-object v2, v0, Lcom/vividsolutions/jts/geomgraph/Edge;->eiList:Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;

    invoke-virtual {v2, p1}, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->print(Ljava/io/PrintStream;)V

    .line 239
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 245
    .end local v0    # "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    :cond_0
    return-void
.end method
