.class public abstract Lcom/vividsolutions/jts/geomgraph/GraphComponent;
.super Ljava/lang/Object;
.source "GraphComponent.java"


# instance fields
.field private isCovered:Z

.field private isCoveredSet:Z

.field private isInResult:Z

.field private isVisited:Z

.field protected label:Lcom/vividsolutions/jts/geomgraph/Label;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-boolean v0, p0, Lcom/vividsolutions/jts/geomgraph/GraphComponent;->isInResult:Z

    .line 57
    iput-boolean v0, p0, Lcom/vividsolutions/jts/geomgraph/GraphComponent;->isCovered:Z

    .line 58
    iput-boolean v0, p0, Lcom/vividsolutions/jts/geomgraph/GraphComponent;->isCoveredSet:Z

    .line 59
    iput-boolean v0, p0, Lcom/vividsolutions/jts/geomgraph/GraphComponent;->isVisited:Z

    .line 62
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geomgraph/Label;)V
    .locals 1
    .param p1, "label"    # Lcom/vividsolutions/jts/geomgraph/Label;

    .prologue
    const/4 v0, 0x0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-boolean v0, p0, Lcom/vividsolutions/jts/geomgraph/GraphComponent;->isInResult:Z

    .line 57
    iput-boolean v0, p0, Lcom/vividsolutions/jts/geomgraph/GraphComponent;->isCovered:Z

    .line 58
    iput-boolean v0, p0, Lcom/vividsolutions/jts/geomgraph/GraphComponent;->isCoveredSet:Z

    .line 59
    iput-boolean v0, p0, Lcom/vividsolutions/jts/geomgraph/GraphComponent;->isVisited:Z

    .line 65
    iput-object p1, p0, Lcom/vividsolutions/jts/geomgraph/GraphComponent;->label:Lcom/vividsolutions/jts/geomgraph/Label;

    .line 66
    return-void
.end method


# virtual methods
.method protected abstract computeIM(Lcom/vividsolutions/jts/geom/IntersectionMatrix;)V
.end method

.method public abstract getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;
.end method

.method public getLabel()Lcom/vividsolutions/jts/geomgraph/Label;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/GraphComponent;->label:Lcom/vividsolutions/jts/geomgraph/Label;

    return-object v0
.end method

.method public isCovered()Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/vividsolutions/jts/geomgraph/GraphComponent;->isCovered:Z

    return v0
.end method

.method public isCoveredSet()Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/vividsolutions/jts/geomgraph/GraphComponent;->isCoveredSet:Z

    return v0
.end method

.method public isInResult()Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/vividsolutions/jts/geomgraph/GraphComponent;->isInResult:Z

    return v0
.end method

.method public abstract isIsolated()Z
.end method

.method public isVisited()Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/vividsolutions/jts/geomgraph/GraphComponent;->isVisited:Z

    return v0
.end method

.method public setCovered(Z)V
    .locals 1
    .param p1, "isCovered"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/vividsolutions/jts/geomgraph/GraphComponent;->isCovered:Z

    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vividsolutions/jts/geomgraph/GraphComponent;->isCoveredSet:Z

    .line 76
    return-void
.end method

.method public setInResult(Z)V
    .locals 0
    .param p1, "isInResult"    # Z

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/vividsolutions/jts/geomgraph/GraphComponent;->isInResult:Z

    return-void
.end method

.method public setLabel(Lcom/vividsolutions/jts/geomgraph/Label;)V
    .locals 0
    .param p1, "label"    # Lcom/vividsolutions/jts/geomgraph/Label;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/vividsolutions/jts/geomgraph/GraphComponent;->label:Lcom/vividsolutions/jts/geomgraph/Label;

    return-void
.end method

.method public setVisited(Z)V
    .locals 0
    .param p1, "isVisited"    # Z

    .prologue
    .line 80
    iput-boolean p1, p0, Lcom/vividsolutions/jts/geomgraph/GraphComponent;->isVisited:Z

    return-void
.end method

.method public updateIM(Lcom/vividsolutions/jts/geom/IntersectionMatrix;)V
    .locals 2
    .param p1, "im"    # Lcom/vividsolutions/jts/geom/IntersectionMatrix;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/GraphComponent;->label:Lcom/vividsolutions/jts/geomgraph/Label;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Label;->getGeometryCount()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "found partial label"

    invoke-static {v0, v1}, Lcom/vividsolutions/jts/util/Assert;->isTrue(ZLjava/lang/String;)V

    .line 104
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geomgraph/GraphComponent;->computeIM(Lcom/vividsolutions/jts/geom/IntersectionMatrix;)V

    .line 105
    return-void

    .line 103
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
