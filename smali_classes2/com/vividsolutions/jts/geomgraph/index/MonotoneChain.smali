.class public Lcom/vividsolutions/jts/geomgraph/index/MonotoneChain;
.super Ljava/lang/Object;
.source "MonotoneChain.java"


# instance fields
.field chainIndex:I

.field mce:Lcom/vividsolutions/jts/geomgraph/index/MonotoneChainEdge;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geomgraph/index/MonotoneChainEdge;I)V
    .locals 0
    .param p1, "mce"    # Lcom/vividsolutions/jts/geomgraph/index/MonotoneChainEdge;
    .param p2, "chainIndex"    # I

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/vividsolutions/jts/geomgraph/index/MonotoneChain;->mce:Lcom/vividsolutions/jts/geomgraph/index/MonotoneChainEdge;

    .line 48
    iput p2, p0, Lcom/vividsolutions/jts/geomgraph/index/MonotoneChain;->chainIndex:I

    .line 49
    return-void
.end method


# virtual methods
.method public computeIntersections(Lcom/vividsolutions/jts/geomgraph/index/MonotoneChain;Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;)V
    .locals 4
    .param p1, "mc"    # Lcom/vividsolutions/jts/geomgraph/index/MonotoneChain;
    .param p2, "si"    # Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/index/MonotoneChain;->mce:Lcom/vividsolutions/jts/geomgraph/index/MonotoneChainEdge;

    iget v1, p0, Lcom/vividsolutions/jts/geomgraph/index/MonotoneChain;->chainIndex:I

    iget-object v2, p1, Lcom/vividsolutions/jts/geomgraph/index/MonotoneChain;->mce:Lcom/vividsolutions/jts/geomgraph/index/MonotoneChainEdge;

    iget v3, p1, Lcom/vividsolutions/jts/geomgraph/index/MonotoneChain;->chainIndex:I

    invoke-virtual {v0, v1, v2, v3, p2}, Lcom/vividsolutions/jts/geomgraph/index/MonotoneChainEdge;->computeIntersectsForChain(ILcom/vividsolutions/jts/geomgraph/index/MonotoneChainEdge;ILcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;)V

    .line 54
    return-void
.end method
