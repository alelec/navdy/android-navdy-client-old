.class public Lcom/vividsolutions/jts/triangulate/SplitSegment;
.super Ljava/lang/Object;
.source "SplitSegment.java"


# instance fields
.field private minimumLen:D

.field private seg:Lcom/vividsolutions/jts/geom/LineSegment;

.field private segLen:D

.field private splitPt:Lcom/vividsolutions/jts/geom/Coordinate;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/LineSegment;)V
    .locals 2
    .param p1, "seg"    # Lcom/vividsolutions/jts/geom/LineSegment;

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vividsolutions/jts/triangulate/SplitSegment;->minimumLen:D

    .line 68
    iput-object p1, p0, Lcom/vividsolutions/jts/triangulate/SplitSegment;->seg:Lcom/vividsolutions/jts/geom/LineSegment;

    .line 69
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineSegment;->getLength()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/vividsolutions/jts/triangulate/SplitSegment;->segLen:D

    .line 70
    return-void
.end method

.method private getConstrainedLength(D)D
    .locals 3
    .param p1, "len"    # D

    .prologue
    .line 105
    iget-wide v0, p0, Lcom/vividsolutions/jts/triangulate/SplitSegment;->minimumLen:D

    cmpg-double v0, p1, v0

    if-gez v0, :cond_0

    .line 106
    iget-wide p1, p0, Lcom/vividsolutions/jts/triangulate/SplitSegment;->minimumLen:D

    .line 107
    .end local p1    # "len":D
    :cond_0
    return-wide p1
.end method

.method private static pointAlongReverse(Lcom/vividsolutions/jts/geom/LineSegment;D)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 9
    .param p0, "seg"    # Lcom/vividsolutions/jts/geom/LineSegment;
    .param p1, "segmentLengthFraction"    # D

    .prologue
    .line 56
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    .line 57
    .local v0, "coord":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v4, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v6, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v4, v6

    mul-double/2addr v4, p1

    sub-double/2addr v2, v4

    iput-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 58
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v4, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v6, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v4, v6

    mul-double/2addr v4, p1

    sub-double/2addr v2, v4

    iput-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    .line 59
    return-object v0
.end method


# virtual methods
.method public getSplitPoint()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/SplitSegment;->splitPt:Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

.method public setMinimumLength(D)V
    .locals 1
    .param p1, "minLen"    # D

    .prologue
    .line 73
    iput-wide p1, p0, Lcom/vividsolutions/jts/triangulate/SplitSegment;->minimumLen:D

    .line 74
    return-void
.end method

.method public splitAt(DLcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 7
    .param p1, "length"    # D
    .param p3, "endPt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/triangulate/SplitSegment;->getConstrainedLength(D)D

    move-result-wide v0

    .line 82
    .local v0, "actualLen":D
    iget-wide v4, p0, Lcom/vividsolutions/jts/triangulate/SplitSegment;->segLen:D

    div-double v2, v0, v4

    .line 83
    .local v2, "frac":D
    iget-object v4, p0, Lcom/vividsolutions/jts/triangulate/SplitSegment;->seg:Lcom/vividsolutions/jts/geom/LineSegment;

    iget-object v4, v4, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {p3, v4}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 84
    iget-object v4, p0, Lcom/vividsolutions/jts/triangulate/SplitSegment;->seg:Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-virtual {v4, v2, v3}, Lcom/vividsolutions/jts/geom/LineSegment;->pointAlong(D)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    iput-object v4, p0, Lcom/vividsolutions/jts/triangulate/SplitSegment;->splitPt:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 87
    :goto_0
    return-void

    .line 86
    :cond_0
    iget-object v4, p0, Lcom/vividsolutions/jts/triangulate/SplitSegment;->seg:Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-static {v4, v2, v3}, Lcom/vividsolutions/jts/triangulate/SplitSegment;->pointAlongReverse(Lcom/vividsolutions/jts/geom/LineSegment;D)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    iput-object v4, p0, Lcom/vividsolutions/jts/triangulate/SplitSegment;->splitPt:Lcom/vividsolutions/jts/geom/Coordinate;

    goto :goto_0
.end method

.method public splitAt(Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 6
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 91
    iget-wide v2, p0, Lcom/vividsolutions/jts/triangulate/SplitSegment;->minimumLen:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/triangulate/SplitSegment;->segLen:D

    div-double v0, v2, v4

    .line 92
    .local v0, "minFrac":D
    iget-object v2, p0, Lcom/vividsolutions/jts/triangulate/SplitSegment;->seg:Lcom/vividsolutions/jts/geom/LineSegment;

    iget-object v2, v2, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {p1, v2}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v2

    iget-wide v4, p0, Lcom/vividsolutions/jts/triangulate/SplitSegment;->minimumLen:D

    cmpg-double v2, v2, v4

    if-gez v2, :cond_0

    .line 93
    iget-object v2, p0, Lcom/vividsolutions/jts/triangulate/SplitSegment;->seg:Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-virtual {v2, v0, v1}, Lcom/vividsolutions/jts/geom/LineSegment;->pointAlong(D)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    iput-object v2, p0, Lcom/vividsolutions/jts/triangulate/SplitSegment;->splitPt:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 102
    :goto_0
    return-void

    .line 96
    :cond_0
    iget-object v2, p0, Lcom/vividsolutions/jts/triangulate/SplitSegment;->seg:Lcom/vividsolutions/jts/geom/LineSegment;

    iget-object v2, v2, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {p1, v2}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v2

    iget-wide v4, p0, Lcom/vividsolutions/jts/triangulate/SplitSegment;->minimumLen:D

    cmpg-double v2, v2, v4

    if-gez v2, :cond_1

    .line 97
    iget-object v2, p0, Lcom/vividsolutions/jts/triangulate/SplitSegment;->seg:Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-static {v2, v0, v1}, Lcom/vividsolutions/jts/triangulate/SplitSegment;->pointAlongReverse(Lcom/vividsolutions/jts/geom/LineSegment;D)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    iput-object v2, p0, Lcom/vividsolutions/jts/triangulate/SplitSegment;->splitPt:Lcom/vividsolutions/jts/geom/Coordinate;

    goto :goto_0

    .line 101
    :cond_1
    iput-object p1, p0, Lcom/vividsolutions/jts/triangulate/SplitSegment;->splitPt:Lcom/vividsolutions/jts/geom/Coordinate;

    goto :goto_0
.end method
