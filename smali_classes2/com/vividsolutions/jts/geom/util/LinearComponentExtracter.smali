.class public Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;
.super Ljava/lang/Object;
.source "LinearComponentExtracter.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/GeometryComponentFilter;


# instance fields
.field private isForcedToLineString:Z

.field private lines:Ljava/util/Collection;


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .param p1, "lines"    # Ljava/util/Collection;

    .prologue
    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;->isForcedToLineString:Z

    .line 158
    iput-object p1, p0, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;->lines:Ljava/util/Collection;

    .line 159
    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;Z)V
    .locals 1
    .param p1, "lines"    # Ljava/util/Collection;
    .param p2, "isForcedToLineString"    # Z

    .prologue
    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;->isForcedToLineString:Z

    .line 166
    iput-object p1, p0, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;->lines:Ljava/util/Collection;

    .line 167
    iput-boolean p2, p0, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;->isForcedToLineString:Z

    .line 168
    return-void
.end method

.method public static getLines(Lcom/vividsolutions/jts/geom/Geometry;Ljava/util/Collection;)Ljava/util/Collection;
    .locals 1
    .param p0, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "lines"    # Ljava/util/Collection;

    .prologue
    .line 95
    instance-of v0, p0, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v0, :cond_0

    .line 96
    invoke-interface {p1, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 101
    :goto_0
    return-object p1

    .line 99
    :cond_0
    new-instance v0, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;

    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/GeometryComponentFilter;)V

    goto :goto_0
.end method

.method public static getLines(Lcom/vividsolutions/jts/geom/Geometry;Ljava/util/Collection;Z)Ljava/util/Collection;
    .locals 1
    .param p0, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "lines"    # Ljava/util/Collection;
    .param p2, "forceToLineString"    # Z

    .prologue
    .line 115
    new-instance v0, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;

    invoke-direct {v0, p1, p2}, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;-><init>(Ljava/util/Collection;Z)V

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/GeometryComponentFilter;)V

    .line 116
    return-object p1
.end method

.method public static getLines(Ljava/util/Collection;Ljava/util/Collection;)Ljava/util/Collection;
    .locals 3
    .param p0, "geoms"    # Ljava/util/Collection;
    .param p1, "lines"    # Ljava/util/Collection;

    .prologue
    .line 60
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 61
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    .line 62
    .local v0, "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-static {v0, p1}, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;->getLines(Lcom/vividsolutions/jts/geom/Geometry;Ljava/util/Collection;)Ljava/util/Collection;

    goto :goto_0

    .line 64
    .end local v0    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    return-object p1
.end method

.method public static getLines(Ljava/util/Collection;Ljava/util/Collection;Z)Ljava/util/Collection;
    .locals 3
    .param p0, "geoms"    # Ljava/util/Collection;
    .param p1, "lines"    # Ljava/util/Collection;
    .param p2, "forceToLineString"    # Z

    .prologue
    .line 78
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 79
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    .line 80
    .local v0, "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-static {v0, p1, p2}, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;->getLines(Lcom/vividsolutions/jts/geom/Geometry;Ljava/util/Collection;Z)Ljava/util/Collection;

    goto :goto_0

    .line 82
    .end local v0    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    return-object p1
.end method

.method public static getLines(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;
    .locals 1
    .param p0, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 130
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;->getLines(Lcom/vividsolutions/jts/geom/Geometry;Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static getLines(Lcom/vividsolutions/jts/geom/Geometry;Z)Ljava/util/List;
    .locals 2
    .param p0, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "forceToLineString"    # Z

    .prologue
    .line 145
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 146
    .local v0, "lines":Ljava/util/List;
    new-instance v1, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;

    invoke-direct {v1, v0, p1}, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;-><init>(Ljava/util/Collection;Z)V

    invoke-virtual {p0, v1}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/GeometryComponentFilter;)V

    .line 147
    return-object v0
.end method


# virtual methods
.method public filter(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 3
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 183
    iget-boolean v1, p0, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;->isForcedToLineString:Z

    if-eqz v1, :cond_1

    instance-of v1, p1, Lcom/vividsolutions/jts/geom/LinearRing;

    if-eqz v1, :cond_1

    .line 184
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v1

    check-cast p1, Lcom/vividsolutions/jts/geom/LinearRing;

    .end local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinateSequence()Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v0

    .line 185
    .local v0, "line":Lcom/vividsolutions/jts/geom/LineString;
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;->lines:Ljava/util/Collection;

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 193
    .end local v0    # "line":Lcom/vividsolutions/jts/geom/LineString;
    :cond_0
    :goto_0
    return-void

    .line 189
    .restart local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    instance-of v1, p1, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v1, :cond_0

    .line 190
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;->lines:Ljava/util/Collection;

    invoke-interface {v1, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public setForceToLineString(Z)V
    .locals 0
    .param p1, "isForcedToLineString"    # Z

    .prologue
    .line 178
    iput-boolean p1, p0, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;->isForcedToLineString:Z

    .line 179
    return-void
.end method
