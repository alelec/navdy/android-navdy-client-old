.class Lcom/vividsolutions/jts/geom/prep/BasicPreparedGeometry;
.super Ljava/lang/Object;
.source "BasicPreparedGeometry.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/prep/PreparedGeometry;


# instance fields
.field private baseGeom:Lcom/vividsolutions/jts/geom/Geometry;

.field private representativePts:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/vividsolutions/jts/geom/prep/BasicPreparedGeometry;->baseGeom:Lcom/vividsolutions/jts/geom/Geometry;

    .line 62
    invoke-static {p1}, Lcom/vividsolutions/jts/geom/util/ComponentCoordinateExtracter;->getCoordinates(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/geom/prep/BasicPreparedGeometry;->representativePts:Ljava/util/List;

    .line 63
    return-void
.end method


# virtual methods
.method public contains(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 1
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/prep/BasicPreparedGeometry;->baseGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geom/Geometry;->contains(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    return v0
.end method

.method public containsProperly(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 2
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/prep/BasicPreparedGeometry;->baseGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Envelope;->contains(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 145
    const/4 v0, 0x0

    .line 148
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/prep/BasicPreparedGeometry;->baseGeom:Lcom/vividsolutions/jts/geom/Geometry;

    const-string v1, "T**FF*FF*"

    invoke-virtual {v0, p1, v1}, Lcom/vividsolutions/jts/geom/Geometry;->relate(Lcom/vividsolutions/jts/geom/Geometry;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public coveredBy(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 1
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 156
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/prep/BasicPreparedGeometry;->baseGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geom/Geometry;->coveredBy(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    return v0
.end method

.method public covers(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 1
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 164
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/prep/BasicPreparedGeometry;->baseGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geom/Geometry;->covers(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    return v0
.end method

.method public crosses(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 1
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 172
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/prep/BasicPreparedGeometry;->baseGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geom/Geometry;->crosses(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    return v0
.end method

.method public disjoint(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 1
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 181
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/prep/BasicPreparedGeometry;->intersects(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected envelopeCovers(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 2
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/prep/BasicPreparedGeometry;->baseGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Envelope;->covers(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    const/4 v0, 0x0

    .line 125
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected envelopesIntersect(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 2
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/prep/BasicPreparedGeometry;->baseGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Envelope;->intersects(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 109
    const/4 v0, 0x0

    .line 110
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getGeometry()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/prep/BasicPreparedGeometry;->baseGeom:Lcom/vividsolutions/jts/geom/Geometry;

    return-object v0
.end method

.method public getRepresentativePoints()Ljava/util/List;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/prep/BasicPreparedGeometry;->representativePts:Ljava/util/List;

    return-object v0
.end method

.method public intersects(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 1
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 189
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/prep/BasicPreparedGeometry;->baseGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geom/Geometry;->intersects(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    return v0
.end method

.method public isAnyTargetComponentInTest(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 4
    .param p1, "testGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 90
    new-instance v1, Lcom/vividsolutions/jts/algorithm/PointLocator;

    invoke-direct {v1}, Lcom/vividsolutions/jts/algorithm/PointLocator;-><init>()V

    .line 91
    .local v1, "locator":Lcom/vividsolutions/jts/algorithm/PointLocator;
    iget-object v3, p0, Lcom/vividsolutions/jts/geom/prep/BasicPreparedGeometry;->representativePts:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 92
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geom/Coordinate;

    .line 93
    .local v2, "p":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {v1, v2, p1}, Lcom/vividsolutions/jts/algorithm/PointLocator;->intersects(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 94
    const/4 v3, 0x1

    .line 96
    .end local v2    # "p":Lcom/vividsolutions/jts/geom/Coordinate;
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public overlaps(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 1
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 197
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/prep/BasicPreparedGeometry;->baseGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geom/Geometry;->overlaps(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/prep/BasicPreparedGeometry;->baseGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public touches(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 1
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 205
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/prep/BasicPreparedGeometry;->baseGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geom/Geometry;->touches(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    return v0
.end method

.method public within(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 1
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 213
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/prep/BasicPreparedGeometry;->baseGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geom/Geometry;->within(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    return v0
.end method
