.class public Lcom/vividsolutions/jts/index/quadtree/Root;
.super Lcom/vividsolutions/jts/index/quadtree/NodeBase;
.source "Root.java"


# static fields
.field private static final origin:Lcom/vividsolutions/jts/geom/Coordinate;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 51
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0, v2, v3, v2, v3}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    sput-object v0, Lcom/vividsolutions/jts/index/quadtree/Root;->origin:Lcom/vividsolutions/jts/geom/Coordinate;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/vividsolutions/jts/index/quadtree/NodeBase;-><init>()V

    .line 55
    return-void
.end method

.method private insertContained(Lcom/vividsolutions/jts/index/quadtree/Node;Lcom/vividsolutions/jts/geom/Envelope;Ljava/lang/Object;)V
    .locals 8
    .param p1, "tree"    # Lcom/vividsolutions/jts/index/quadtree/Node;
    .param p2, "itemEnv"    # Lcom/vividsolutions/jts/geom/Envelope;
    .param p3, "item"    # Ljava/lang/Object;

    .prologue
    .line 98
    invoke-virtual {p1}, Lcom/vividsolutions/jts/index/quadtree/Node;->getEnvelope()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v3

    invoke-virtual {v3, p2}, Lcom/vividsolutions/jts/geom/Envelope;->contains(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v3

    invoke-static {v3}, Lcom/vividsolutions/jts/util/Assert;->isTrue(Z)V

    .line 104
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Envelope;->getMinX()D

    move-result-wide v4

    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxX()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lcom/vividsolutions/jts/index/quadtree/IntervalSize;->isZeroWidth(DD)Z

    move-result v0

    .line 105
    .local v0, "isZeroX":Z
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Envelope;->getMinY()D

    move-result-wide v4

    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxY()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lcom/vividsolutions/jts/index/quadtree/IntervalSize;->isZeroWidth(DD)Z

    move-result v1

    .line 107
    .local v1, "isZeroY":Z
    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    .line 108
    :cond_0
    invoke-virtual {p1, p2}, Lcom/vividsolutions/jts/index/quadtree/Node;->find(Lcom/vividsolutions/jts/geom/Envelope;)Lcom/vividsolutions/jts/index/quadtree/NodeBase;

    move-result-object v2

    .line 111
    .local v2, "node":Lcom/vividsolutions/jts/index/quadtree/NodeBase;
    :goto_0
    invoke-virtual {v2, p3}, Lcom/vividsolutions/jts/index/quadtree/NodeBase;->add(Ljava/lang/Object;)V

    .line 112
    return-void

    .line 110
    .end local v2    # "node":Lcom/vividsolutions/jts/index/quadtree/NodeBase;
    :cond_1
    invoke-virtual {p1, p2}, Lcom/vividsolutions/jts/index/quadtree/Node;->getNode(Lcom/vividsolutions/jts/geom/Envelope;)Lcom/vividsolutions/jts/index/quadtree/Node;

    move-result-object v2

    .restart local v2    # "node":Lcom/vividsolutions/jts/index/quadtree/NodeBase;
    goto :goto_0
.end method


# virtual methods
.method public insert(Lcom/vividsolutions/jts/geom/Envelope;Ljava/lang/Object;)V
    .locals 8
    .param p1, "itemEnv"    # Lcom/vividsolutions/jts/geom/Envelope;
    .param p2, "item"    # Ljava/lang/Object;

    .prologue
    .line 62
    sget-object v3, Lcom/vividsolutions/jts/index/quadtree/Root;->origin:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v4, v3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sget-object v3, Lcom/vividsolutions/jts/index/quadtree/Root;->origin:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v6, v3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-static {p1, v4, v5, v6, v7}, Lcom/vividsolutions/jts/index/quadtree/Root;->getSubnodeIndex(Lcom/vividsolutions/jts/geom/Envelope;DD)I

    move-result v0

    .line 64
    .local v0, "index":I
    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    .line 65
    invoke-virtual {p0, p2}, Lcom/vividsolutions/jts/index/quadtree/Root;->add(Ljava/lang/Object;)V

    .line 89
    :goto_0
    return-void

    .line 72
    :cond_0
    iget-object v3, p0, Lcom/vividsolutions/jts/index/quadtree/Root;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    aget-object v2, v3, v0

    .line 78
    .local v2, "node":Lcom/vividsolutions/jts/index/quadtree/Node;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/vividsolutions/jts/index/quadtree/Node;->getEnvelope()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/vividsolutions/jts/geom/Envelope;->contains(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 79
    :cond_1
    invoke-static {v2, p1}, Lcom/vividsolutions/jts/index/quadtree/Node;->createExpanded(Lcom/vividsolutions/jts/index/quadtree/Node;Lcom/vividsolutions/jts/geom/Envelope;)Lcom/vividsolutions/jts/index/quadtree/Node;

    move-result-object v1

    .line 80
    .local v1, "largerNode":Lcom/vividsolutions/jts/index/quadtree/Node;
    iget-object v3, p0, Lcom/vividsolutions/jts/index/quadtree/Root;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    aput-object v1, v3, v0

    .line 86
    .end local v1    # "largerNode":Lcom/vividsolutions/jts/index/quadtree/Node;
    :cond_2
    iget-object v3, p0, Lcom/vividsolutions/jts/index/quadtree/Root;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    aget-object v3, v3, v0

    invoke-direct {p0, v3, p1, p2}, Lcom/vividsolutions/jts/index/quadtree/Root;->insertContained(Lcom/vividsolutions/jts/index/quadtree/Node;Lcom/vividsolutions/jts/geom/Envelope;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected isSearchMatch(Lcom/vividsolutions/jts/geom/Envelope;)Z
    .locals 1
    .param p1, "searchEnv"    # Lcom/vividsolutions/jts/geom/Envelope;

    .prologue
    .line 116
    const/4 v0, 0x1

    return v0
.end method
