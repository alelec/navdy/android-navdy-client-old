.class public Lcom/vividsolutions/jts/precision/PrecisionReducerCoordinateOperation;
.super Lcom/vividsolutions/jts/geom/util/GeometryEditor$CoordinateOperation;
.source "PrecisionReducerCoordinateOperation.java"


# instance fields
.field private removeCollapsed:Z

.field private targetPM:Lcom/vividsolutions/jts/geom/PrecisionModel;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/PrecisionModel;Z)V
    .locals 1
    .param p1, "targetPM"    # Lcom/vividsolutions/jts/geom/PrecisionModel;
    .param p2, "removeCollapsed"    # Z

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/vividsolutions/jts/geom/util/GeometryEditor$CoordinateOperation;-><init>()V

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vividsolutions/jts/precision/PrecisionReducerCoordinateOperation;->removeCollapsed:Z

    .line 51
    iput-object p1, p0, Lcom/vividsolutions/jts/precision/PrecisionReducerCoordinateOperation;->targetPM:Lcom/vividsolutions/jts/geom/PrecisionModel;

    .line 52
    iput-boolean p2, p0, Lcom/vividsolutions/jts/precision/PrecisionReducerCoordinateOperation;->removeCollapsed:Z

    .line 53
    return-void
.end method


# virtual methods
.method public edit([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 8
    .param p1, "coordinates"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 56
    array-length v7, p1

    if-nez v7, :cond_1

    .line 57
    const/4 v0, 0x0

    .line 96
    :cond_0
    :goto_0
    return-object v0

    .line 59
    :cond_1
    array-length v7, p1

    new-array v6, v7, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 61
    .local v6, "reducedCoords":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v7, p1

    if-ge v2, v7, :cond_2

    .line 62
    new-instance v1, Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v7, p1, v2

    invoke-direct {v1, v7}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 63
    .local v1, "coord":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v7, p0, Lcom/vividsolutions/jts/precision/PrecisionReducerCoordinateOperation;->targetPM:Lcom/vividsolutions/jts/geom/PrecisionModel;

    invoke-virtual {v7, v1}, Lcom/vividsolutions/jts/geom/PrecisionModel;->makePrecise(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 64
    aput-object v1, v6, v2

    .line 61
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 67
    .end local v1    # "coord":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_2
    new-instance v4, Lcom/vividsolutions/jts/geom/CoordinateList;

    const/4 v7, 0x0

    invoke-direct {v4, v6, v7}, Lcom/vividsolutions/jts/geom/CoordinateList;-><init>([Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 69
    .local v4, "noRepeatedCoordList":Lcom/vividsolutions/jts/geom/CoordinateList;
    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/CoordinateList;->toCoordinateArray()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v5

    .line 80
    .local v5, "noRepeatedCoords":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v3, 0x0

    .line 81
    .local v3, "minLength":I
    instance-of v7, p2, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v7, :cond_3

    .line 82
    const/4 v3, 0x2

    .line 83
    :cond_3
    instance-of v7, p2, Lcom/vividsolutions/jts/geom/LinearRing;

    if-eqz v7, :cond_4

    .line 84
    const/4 v3, 0x4

    .line 86
    :cond_4
    move-object v0, v6

    .line 87
    .local v0, "collapsedCoords":[Lcom/vividsolutions/jts/geom/Coordinate;
    iget-boolean v7, p0, Lcom/vividsolutions/jts/precision/PrecisionReducerCoordinateOperation;->removeCollapsed:Z

    if-eqz v7, :cond_5

    .line 88
    const/4 v0, 0x0

    .line 91
    :cond_5
    array-length v7, v5

    if-lt v7, v3, :cond_0

    move-object v0, v5

    .line 96
    goto :goto_0
.end method
