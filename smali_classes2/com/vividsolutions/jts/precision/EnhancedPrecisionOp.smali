.class public Lcom/vividsolutions/jts/precision/EnhancedPrecisionOp;
.super Ljava/lang/Object;
.source "EnhancedPrecisionOp.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static buffer(Lcom/vividsolutions/jts/geom/Geometry;D)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 7
    .param p0, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "distance"    # D

    .prologue
    .line 200
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/vividsolutions/jts/geom/Geometry;->buffer(D)Lcom/vividsolutions/jts/geom/Geometry;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 218
    :goto_0
    return-object v4

    .line 203
    :catch_0
    move-exception v1

    .line 205
    .local v1, "ex":Ljava/lang/RuntimeException;
    move-object v3, v1

    .line 213
    .local v3, "originalEx":Ljava/lang/RuntimeException;
    :try_start_1
    new-instance v0, Lcom/vividsolutions/jts/precision/CommonBitsOp;

    const/4 v6, 0x1

    invoke-direct {v0, v6}, Lcom/vividsolutions/jts/precision/CommonBitsOp;-><init>(Z)V

    .line 214
    .local v0, "cbo":Lcom/vividsolutions/jts/precision/CommonBitsOp;
    invoke-virtual {v0, p0, p1, p2}, Lcom/vividsolutions/jts/precision/CommonBitsOp;->buffer(Lcom/vividsolutions/jts/geom/Geometry;D)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v5

    .line 216
    .local v5, "resultEP":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {v5}, Lcom/vividsolutions/jts/geom/Geometry;->isValid()Z

    move-result v6

    if-nez v6, :cond_0

    .line 217
    throw v3
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 220
    .end local v0    # "cbo":Lcom/vividsolutions/jts/precision/CommonBitsOp;
    .end local v5    # "resultEP":Lcom/vividsolutions/jts/geom/Geometry;
    :catch_1
    move-exception v2

    .line 222
    .local v2, "ex2":Ljava/lang/RuntimeException;
    throw v3

    .end local v2    # "ex2":Ljava/lang/RuntimeException;
    .restart local v0    # "cbo":Lcom/vividsolutions/jts/precision/CommonBitsOp;
    .restart local v5    # "resultEP":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    move-object v4, v5

    .line 218
    goto :goto_0
.end method

.method public static difference(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 7
    .param p0, "geom0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "geom1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 127
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/Geometry;->difference(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 145
    :goto_0
    return-object v4

    .line 130
    :catch_0
    move-exception v1

    .line 132
    .local v1, "ex":Ljava/lang/RuntimeException;
    move-object v3, v1

    .line 140
    .local v3, "originalEx":Ljava/lang/RuntimeException;
    :try_start_1
    new-instance v0, Lcom/vividsolutions/jts/precision/CommonBitsOp;

    const/4 v6, 0x1

    invoke-direct {v0, v6}, Lcom/vividsolutions/jts/precision/CommonBitsOp;-><init>(Z)V

    .line 141
    .local v0, "cbo":Lcom/vividsolutions/jts/precision/CommonBitsOp;
    invoke-virtual {v0, p0, p1}, Lcom/vividsolutions/jts/precision/CommonBitsOp;->difference(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v5

    .line 143
    .local v5, "resultEP":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {v5}, Lcom/vividsolutions/jts/geom/Geometry;->isValid()Z

    move-result v6

    if-nez v6, :cond_0

    .line 144
    throw v3
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 147
    .end local v0    # "cbo":Lcom/vividsolutions/jts/precision/CommonBitsOp;
    .end local v5    # "resultEP":Lcom/vividsolutions/jts/geom/Geometry;
    :catch_1
    move-exception v2

    .line 149
    .local v2, "ex2":Ljava/lang/RuntimeException;
    throw v3

    .end local v2    # "ex2":Ljava/lang/RuntimeException;
    .restart local v0    # "cbo":Lcom/vividsolutions/jts/precision/CommonBitsOp;
    .restart local v5    # "resultEP":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    move-object v4, v5

    .line 145
    goto :goto_0
.end method

.method public static intersection(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 7
    .param p0, "geom0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "geom1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 57
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/Geometry;->intersection(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 75
    :goto_0
    return-object v4

    .line 60
    :catch_0
    move-exception v1

    .line 62
    .local v1, "ex":Ljava/lang/RuntimeException;
    move-object v3, v1

    .line 70
    .local v3, "originalEx":Ljava/lang/RuntimeException;
    :try_start_1
    new-instance v0, Lcom/vividsolutions/jts/precision/CommonBitsOp;

    const/4 v6, 0x1

    invoke-direct {v0, v6}, Lcom/vividsolutions/jts/precision/CommonBitsOp;-><init>(Z)V

    .line 71
    .local v0, "cbo":Lcom/vividsolutions/jts/precision/CommonBitsOp;
    invoke-virtual {v0, p0, p1}, Lcom/vividsolutions/jts/precision/CommonBitsOp;->intersection(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v5

    .line 73
    .local v5, "resultEP":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {v5}, Lcom/vividsolutions/jts/geom/Geometry;->isValid()Z

    move-result v6

    if-nez v6, :cond_0

    .line 74
    throw v3
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 77
    .end local v0    # "cbo":Lcom/vividsolutions/jts/precision/CommonBitsOp;
    .end local v5    # "resultEP":Lcom/vividsolutions/jts/geom/Geometry;
    :catch_1
    move-exception v2

    .line 79
    .local v2, "ex2":Ljava/lang/RuntimeException;
    throw v3

    .end local v2    # "ex2":Ljava/lang/RuntimeException;
    .restart local v0    # "cbo":Lcom/vividsolutions/jts/precision/CommonBitsOp;
    .restart local v5    # "resultEP":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    move-object v4, v5

    .line 75
    goto :goto_0
.end method

.method public static symDifference(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 7
    .param p0, "geom0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "geom1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 162
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/Geometry;->symDifference(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 180
    :goto_0
    return-object v4

    .line 165
    :catch_0
    move-exception v1

    .line 167
    .local v1, "ex":Ljava/lang/RuntimeException;
    move-object v3, v1

    .line 175
    .local v3, "originalEx":Ljava/lang/RuntimeException;
    :try_start_1
    new-instance v0, Lcom/vividsolutions/jts/precision/CommonBitsOp;

    const/4 v6, 0x1

    invoke-direct {v0, v6}, Lcom/vividsolutions/jts/precision/CommonBitsOp;-><init>(Z)V

    .line 176
    .local v0, "cbo":Lcom/vividsolutions/jts/precision/CommonBitsOp;
    invoke-virtual {v0, p0, p1}, Lcom/vividsolutions/jts/precision/CommonBitsOp;->symDifference(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v5

    .line 178
    .local v5, "resultEP":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {v5}, Lcom/vividsolutions/jts/geom/Geometry;->isValid()Z

    move-result v6

    if-nez v6, :cond_0

    .line 179
    throw v3
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 182
    .end local v0    # "cbo":Lcom/vividsolutions/jts/precision/CommonBitsOp;
    .end local v5    # "resultEP":Lcom/vividsolutions/jts/geom/Geometry;
    :catch_1
    move-exception v2

    .line 184
    .local v2, "ex2":Ljava/lang/RuntimeException;
    throw v3

    .end local v2    # "ex2":Ljava/lang/RuntimeException;
    .restart local v0    # "cbo":Lcom/vividsolutions/jts/precision/CommonBitsOp;
    .restart local v5    # "resultEP":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    move-object v4, v5

    .line 180
    goto :goto_0
.end method

.method public static union(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 7
    .param p0, "geom0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "geom1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 92
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/Geometry;->union(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 110
    :goto_0
    return-object v4

    .line 95
    :catch_0
    move-exception v1

    .line 97
    .local v1, "ex":Ljava/lang/RuntimeException;
    move-object v3, v1

    .line 105
    .local v3, "originalEx":Ljava/lang/RuntimeException;
    :try_start_1
    new-instance v0, Lcom/vividsolutions/jts/precision/CommonBitsOp;

    const/4 v6, 0x1

    invoke-direct {v0, v6}, Lcom/vividsolutions/jts/precision/CommonBitsOp;-><init>(Z)V

    .line 106
    .local v0, "cbo":Lcom/vividsolutions/jts/precision/CommonBitsOp;
    invoke-virtual {v0, p0, p1}, Lcom/vividsolutions/jts/precision/CommonBitsOp;->union(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v5

    .line 108
    .local v5, "resultEP":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {v5}, Lcom/vividsolutions/jts/geom/Geometry;->isValid()Z

    move-result v6

    if-nez v6, :cond_0

    .line 109
    throw v3
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 112
    .end local v0    # "cbo":Lcom/vividsolutions/jts/precision/CommonBitsOp;
    .end local v5    # "resultEP":Lcom/vividsolutions/jts/geom/Geometry;
    :catch_1
    move-exception v2

    .line 114
    .local v2, "ex2":Ljava/lang/RuntimeException;
    throw v3

    .end local v2    # "ex2":Ljava/lang/RuntimeException;
    .restart local v0    # "cbo":Lcom/vividsolutions/jts/precision/CommonBitsOp;
    .restart local v5    # "resultEP":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    move-object v4, v5

    .line 110
    goto :goto_0
.end method
