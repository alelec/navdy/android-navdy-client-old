.class Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer$PrecisionReducerCoordinateOperation;
.super Lcom/vividsolutions/jts/geom/util/GeometryEditor$CoordinateOperation;
.source "SimpleGeometryPrecisionReducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PrecisionReducerCoordinateOperation"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;


# direct methods
.method private constructor <init>(Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer$PrecisionReducerCoordinateOperation;->this$0:Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;

    invoke-direct {p0}, Lcom/vividsolutions/jts/geom/util/GeometryEditor$CoordinateOperation;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;
    .param p2, "x1"    # Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer$1;

    .prologue
    .line 123
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer$PrecisionReducerCoordinateOperation;-><init>(Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;)V

    return-void
.end method


# virtual methods
.method public edit([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 8
    .param p1, "coordinates"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 128
    array-length v7, p1

    if-nez v7, :cond_1

    const/4 v0, 0x0

    .line 164
    :cond_0
    :goto_0
    return-object v0

    .line 130
    :cond_1
    array-length v7, p1

    new-array v6, v7, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 132
    .local v6, "reducedCoords":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v7, p1

    if-ge v2, v7, :cond_2

    .line 133
    new-instance v1, Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v7, p1, v2

    invoke-direct {v1, v7}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 134
    .local v1, "coord":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v7, p0, Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer$PrecisionReducerCoordinateOperation;->this$0:Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;

    invoke-static {v7}, Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;->access$100(Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;)Lcom/vividsolutions/jts/geom/PrecisionModel;

    move-result-object v7

    invoke-virtual {v7, v1}, Lcom/vividsolutions/jts/geom/PrecisionModel;->makePrecise(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 135
    aput-object v1, v6, v2

    .line 132
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 138
    .end local v1    # "coord":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_2
    new-instance v4, Lcom/vividsolutions/jts/geom/CoordinateList;

    const/4 v7, 0x0

    invoke-direct {v4, v6, v7}, Lcom/vividsolutions/jts/geom/CoordinateList;-><init>([Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 139
    .local v4, "noRepeatedCoordList":Lcom/vividsolutions/jts/geom/CoordinateList;
    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/CoordinateList;->toCoordinateArray()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v5

    .line 151
    .local v5, "noRepeatedCoords":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v3, 0x0

    .line 152
    .local v3, "minLength":I
    instance-of v7, p2, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v7, :cond_3

    const/4 v3, 0x2

    .line 153
    :cond_3
    instance-of v7, p2, Lcom/vividsolutions/jts/geom/LinearRing;

    if-eqz v7, :cond_4

    const/4 v3, 0x4

    .line 155
    :cond_4
    move-object v0, v6

    .line 156
    .local v0, "collapsedCoords":[Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v7, p0, Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer$PrecisionReducerCoordinateOperation;->this$0:Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;

    invoke-static {v7}, Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;->access$200(Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;)Z

    move-result v7

    if-eqz v7, :cond_5

    const/4 v0, 0x0

    .line 159
    :cond_5
    array-length v7, v5

    if-lt v7, v3, :cond_0

    move-object v0, v5

    .line 164
    goto :goto_0
.end method
