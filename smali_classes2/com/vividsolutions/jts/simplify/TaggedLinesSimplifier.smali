.class Lcom/vividsolutions/jts/simplify/TaggedLinesSimplifier;
.super Ljava/lang/Object;
.source "TaggedLinesSimplifier.java"


# instance fields
.field private distanceTolerance:D

.field private inputIndex:Lcom/vividsolutions/jts/simplify/LineSegmentIndex;

.field private outputIndex:Lcom/vividsolutions/jts/simplify/LineSegmentIndex;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Lcom/vividsolutions/jts/simplify/LineSegmentIndex;

    invoke-direct {v0}, Lcom/vividsolutions/jts/simplify/LineSegmentIndex;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/simplify/TaggedLinesSimplifier;->inputIndex:Lcom/vividsolutions/jts/simplify/LineSegmentIndex;

    .line 48
    new-instance v0, Lcom/vividsolutions/jts/simplify/LineSegmentIndex;

    invoke-direct {v0}, Lcom/vividsolutions/jts/simplify/LineSegmentIndex;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/simplify/TaggedLinesSimplifier;->outputIndex:Lcom/vividsolutions/jts/simplify/LineSegmentIndex;

    .line 49
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vividsolutions/jts/simplify/TaggedLinesSimplifier;->distanceTolerance:D

    .line 54
    return-void
.end method


# virtual methods
.method public setDistanceTolerance(D)V
    .locals 1
    .param p1, "distanceTolerance"    # D

    .prologue
    .line 64
    iput-wide p1, p0, Lcom/vividsolutions/jts/simplify/TaggedLinesSimplifier;->distanceTolerance:D

    .line 65
    return-void
.end method

.method public simplify(Ljava/util/Collection;)V
    .locals 4
    .param p1, "taggedLines"    # Ljava/util/Collection;

    .prologue
    .line 73
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 74
    iget-object v3, p0, Lcom/vividsolutions/jts/simplify/TaggedLinesSimplifier;->inputIndex:Lcom/vividsolutions/jts/simplify/LineSegmentIndex;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/simplify/TaggedLineString;

    invoke-virtual {v3, v2}, Lcom/vividsolutions/jts/simplify/LineSegmentIndex;->add(Lcom/vividsolutions/jts/simplify/TaggedLineString;)V

    goto :goto_0

    .line 76
    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 77
    new-instance v1, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;

    iget-object v2, p0, Lcom/vividsolutions/jts/simplify/TaggedLinesSimplifier;->inputIndex:Lcom/vividsolutions/jts/simplify/LineSegmentIndex;

    iget-object v3, p0, Lcom/vividsolutions/jts/simplify/TaggedLinesSimplifier;->outputIndex:Lcom/vividsolutions/jts/simplify/LineSegmentIndex;

    invoke-direct {v1, v2, v3}, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;-><init>(Lcom/vividsolutions/jts/simplify/LineSegmentIndex;Lcom/vividsolutions/jts/simplify/LineSegmentIndex;)V

    .line 79
    .local v1, "tlss":Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;
    iget-wide v2, p0, Lcom/vividsolutions/jts/simplify/TaggedLinesSimplifier;->distanceTolerance:D

    invoke-virtual {v1, v2, v3}, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->setDistanceTolerance(D)V

    .line 80
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/simplify/TaggedLineString;

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->simplify(Lcom/vividsolutions/jts/simplify/TaggedLineString;)V

    goto :goto_1

    .line 82
    .end local v1    # "tlss":Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;
    :cond_1
    return-void
.end method
