.class public interface abstract Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;
.super Ljava/lang/Object;
.source "BoundaryNodeRule.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule$MonoValentEndPointBoundaryNodeRule;,
        Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule$MultiValentEndPointBoundaryNodeRule;,
        Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule$EndPointBoundaryNodeRule;,
        Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule$Mod2BoundaryNodeRule;
    }
.end annotation


# static fields
.field public static final ENDPOINT_BOUNDARY_RULE:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

.field public static final MOD2_BOUNDARY_RULE:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

.field public static final MONOVALENT_ENDPOINT_BOUNDARY_RULE:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

.field public static final MULTIVALENT_ENDPOINT_BOUNDARY_RULE:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

.field public static final OGC_SFS_BOUNDARY_RULE:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90
    new-instance v0, Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule$Mod2BoundaryNodeRule;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule$Mod2BoundaryNodeRule;-><init>()V

    sput-object v0, Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;->MOD2_BOUNDARY_RULE:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    .line 96
    new-instance v0, Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule$EndPointBoundaryNodeRule;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule$EndPointBoundaryNodeRule;-><init>()V

    sput-object v0, Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;->ENDPOINT_BOUNDARY_RULE:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    .line 102
    new-instance v0, Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule$MultiValentEndPointBoundaryNodeRule;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule$MultiValentEndPointBoundaryNodeRule;-><init>()V

    sput-object v0, Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;->MULTIVALENT_ENDPOINT_BOUNDARY_RULE:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    .line 108
    new-instance v0, Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule$MonoValentEndPointBoundaryNodeRule;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule$MonoValentEndPointBoundaryNodeRule;-><init>()V

    sput-object v0, Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;->MONOVALENT_ENDPOINT_BOUNDARY_RULE:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    .line 115
    sget-object v0, Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;->MOD2_BOUNDARY_RULE:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    sput-object v0, Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;->OGC_SFS_BOUNDARY_RULE:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    return-void
.end method


# virtual methods
.method public abstract isInBoundary(I)Z
.end method
