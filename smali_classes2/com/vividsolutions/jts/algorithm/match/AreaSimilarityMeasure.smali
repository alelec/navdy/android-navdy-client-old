.class public Lcom/vividsolutions/jts/algorithm/match/AreaSimilarityMeasure;
.super Ljava/lang/Object;
.source "AreaSimilarityMeasure.java"

# interfaces
.implements Lcom/vividsolutions/jts/algorithm/match/SimilarityMeasure;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    return-void
.end method


# virtual methods
.method public measure(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)D
    .locals 6
    .param p1, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "g2"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 67
    invoke-virtual {p1, p2}, Lcom/vividsolutions/jts/geom/Geometry;->intersection(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/Geometry;->getArea()D

    move-result-wide v0

    .line 68
    .local v0, "areaInt":D
    invoke-virtual {p1, p2}, Lcom/vividsolutions/jts/geom/Geometry;->union(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/Geometry;->getArea()D

    move-result-wide v2

    .line 69
    .local v2, "areaUnion":D
    div-double v4, v0, v2

    return-wide v4
.end method
