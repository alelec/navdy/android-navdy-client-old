.class public Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;
.super Ljava/lang/Object;
.source "PointPairDistance.java"


# instance fields
.field private distance:D

.field private isNull:Z

.field private pt:[Lcom/vividsolutions/jts/geom/Coordinate;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v1, 0x0

    new-instance v2, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v2}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    aput-object v2, v0, v1

    new-instance v1, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v1}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->pt:[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 45
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    iput-wide v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->distance:D

    .line 46
    iput-boolean v3, p0, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->isNull:Z

    .line 50
    return-void
.end method

.method private initialize(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;D)V
    .locals 3
    .param p1, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "distance"    # D

    .prologue
    const/4 v2, 0x0

    .line 70
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->pt:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v0, v0, v2

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geom/Coordinate;->setCoordinate(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 71
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->pt:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0, p2}, Lcom/vividsolutions/jts/geom/Coordinate;->setCoordinate(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 72
    iput-wide p3, p0, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->distance:D

    .line 73
    iput-boolean v2, p0, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->isNull:Z

    .line 74
    return-void
.end method


# virtual methods
.method public getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 80
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->pt:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->pt:[Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

.method public getDistance()D
    .locals 2

    .prologue
    .line 76
    iget-wide v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->distance:D

    return-wide v0
.end method

.method public initialize()V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->isNull:Z

    return-void
.end method

.method public initialize(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 3
    .param p1, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const/4 v2, 0x0

    .line 56
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->pt:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v0, v0, v2

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geom/Coordinate;->setCoordinate(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 57
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->pt:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0, p2}, Lcom/vividsolutions/jts/geom/Coordinate;->setCoordinate(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 58
    invoke-virtual {p1, p2}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->distance:D

    .line 59
    iput-boolean v2, p0, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->isNull:Z

    .line 60
    return-void
.end method

.method public setMaximum(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 4
    .param p1, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 89
    iget-boolean v2, p0, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->isNull:Z

    if-eqz v2, :cond_1

    .line 90
    invoke-virtual {p0, p1, p2}, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->initialize(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 93
    :cond_1
    invoke-virtual {p1, p2}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    .line 94
    .local v0, "dist":D
    iget-wide v2, p0, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->distance:D

    cmpl-double v2, v0, v2

    if-lez v2, :cond_0

    .line 95
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->initialize(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;D)V

    goto :goto_0
.end method

.method public setMaximum(Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;)V
    .locals 3
    .param p1, "ptDist"    # Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

    .prologue
    .line 84
    iget-object v0, p1, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->pt:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v1, p1, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->pt:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->setMaximum(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 85
    return-void
.end method

.method public setMinimum(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 4
    .param p1, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 105
    iget-boolean v2, p0, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->isNull:Z

    if-eqz v2, :cond_1

    .line 106
    invoke-virtual {p0, p1, p2}, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->initialize(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 112
    :cond_0
    :goto_0
    return-void

    .line 109
    :cond_1
    invoke-virtual {p1, p2}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    .line 110
    .local v0, "dist":D
    iget-wide v2, p0, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->distance:D

    cmpg-double v2, v0, v2

    if-gez v2, :cond_0

    .line 111
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->initialize(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;D)V

    goto :goto_0
.end method

.method public setMinimum(Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;)V
    .locals 3
    .param p1, "ptDist"    # Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

    .prologue
    .line 100
    iget-object v0, p1, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->pt:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v1, p1, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->pt:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->setMinimum(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 101
    return-void
.end method
