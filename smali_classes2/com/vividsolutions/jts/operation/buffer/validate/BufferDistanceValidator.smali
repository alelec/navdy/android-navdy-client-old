.class public Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;
.super Ljava/lang/Object;
.source "BufferDistanceValidator.java"


# static fields
.field private static final MAX_DISTANCE_DIFF_FRAC:D = 0.012

.field private static VERBOSE:Z


# instance fields
.field private bufDistance:D

.field private errMsg:Ljava/lang/String;

.field private errorIndicator:Lcom/vividsolutions/jts/geom/Geometry;

.field private errorLocation:Lcom/vividsolutions/jts/geom/Coordinate;

.field private input:Lcom/vividsolutions/jts/geom/Geometry;

.field private isValid:Z

.field private maxDistanceFound:D

.field private maxValidDistance:D

.field private minDistanceFound:D

.field private minValidDistance:D

.field private result:Lcom/vividsolutions/jts/geom/Geometry;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->VERBOSE:Z

    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;DLcom/vividsolutions/jts/geom/Geometry;)V
    .locals 2
    .param p1, "input"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "bufDistance"    # D
    .param p4, "result"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v1, 0x0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->isValid:Z

    .line 77
    iput-object v1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->errMsg:Ljava/lang/String;

    .line 78
    iput-object v1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->errorLocation:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 79
    iput-object v1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->errorIndicator:Lcom/vividsolutions/jts/geom/Geometry;

    .line 83
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->input:Lcom/vividsolutions/jts/geom/Geometry;

    .line 84
    iput-wide p2, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->bufDistance:D

    .line 85
    iput-object p4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->result:Lcom/vividsolutions/jts/geom/Geometry;

    .line 86
    return-void
.end method

.method private checkMaximumDistance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;D)V
    .locals 9
    .param p1, "input"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "bufCurve"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p3, "maxDist"    # D

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 218
    new-instance v0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;

    invoke-direct {v0, p2, p1}, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;-><init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 219
    .local v0, "haus":Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;
    const-wide/high16 v2, 0x3fd0000000000000L    # 0.25

    invoke-virtual {v0, v2, v3}, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;->setDensifyFraction(D)V

    .line 220
    invoke-virtual {v0}, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;->orientedDistance()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->maxDistanceFound:D

    .line 222
    iget-wide v2, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->maxDistanceFound:D

    cmpl-double v2, v2, p3

    if-lez v2, :cond_0

    .line 223
    iput-boolean v6, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->isValid:Z

    .line 224
    invoke-virtual {v0}, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 225
    .local v1, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    aget-object v2, v1, v7

    iput-object v2, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->errorLocation:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 226
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v2

    iput-object v2, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->errorIndicator:Lcom/vividsolutions/jts/geom/Geometry;

    .line 227
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Distance between buffer curve and input is too large ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->maxDistanceFound:D

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " at "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v1, v6

    aget-object v4, v1, v7

    invoke-static {v3, v4}, Lcom/vividsolutions/jts/io/WKTWriter;->toLineString(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->errMsg:Ljava/lang/String;

    .line 231
    .end local v1    # "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    return-void
.end method

.method private checkMinimumDistance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;D)V
    .locals 9
    .param p1, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "g2"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p3, "minDist"    # D

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 187
    new-instance v0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;-><init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;D)V

    .line 188
    .local v0, "distOp":Lcom/vividsolutions/jts/operation/distance/DistanceOp;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->distance()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->minDistanceFound:D

    .line 191
    iget-wide v2, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->minDistanceFound:D

    cmpg-double v2, v2, p3

    if-gez v2, :cond_0

    .line 192
    iput-boolean v6, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->isValid:Z

    .line 193
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->nearestPoints()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 194
    .local v1, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->nearestPoints()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    aget-object v2, v2, v7

    iput-object v2, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->errorLocation:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 195
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v2

    iput-object v2, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->errorIndicator:Lcom/vividsolutions/jts/geom/Geometry;

    .line 196
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Distance between buffer curve and input is too small ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->minDistanceFound:D

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " at "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v1, v6

    aget-object v4, v1, v7

    invoke-static {v3, v4}, Lcom/vividsolutions/jts/io/WKTWriter;->toLineString(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " )"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->errMsg:Ljava/lang/String;

    .line 200
    .end local v1    # "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    return-void
.end method

.method private checkNegativeValid()V
    .locals 4

    .prologue
    .line 153
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->input:Lcom/vividsolutions/jts/geom/Geometry;

    instance-of v1, v1, Lcom/vividsolutions/jts/geom/Polygon;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->input:Lcom/vividsolutions/jts/geom/Geometry;

    instance-of v1, v1, Lcom/vividsolutions/jts/geom/MultiPolygon;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->input:Lcom/vividsolutions/jts/geom/Geometry;

    instance-of v1, v1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    if-nez v1, :cond_1

    .line 164
    :cond_0
    :goto_0
    return-void

    .line 159
    :cond_1
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->input:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->getPolygonLines(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 160
    .local v0, "inputCurve":Lcom/vividsolutions/jts/geom/Geometry;
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->result:Lcom/vividsolutions/jts/geom/Geometry;

    iget-wide v2, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->minValidDistance:D

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->checkMinimumDistance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;D)V

    .line 161
    iget-boolean v1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->isValid:Z

    if-eqz v1, :cond_0

    .line 163
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->result:Lcom/vividsolutions/jts/geom/Geometry;

    iget-wide v2, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->maxValidDistance:D

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->checkMaximumDistance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;D)V

    goto :goto_0
.end method

.method private checkPositiveValid()V
    .locals 4

    .prologue
    .line 141
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->result:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Geometry;->getBoundary()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 142
    .local v0, "bufCurve":Lcom/vividsolutions/jts/geom/Geometry;
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->input:Lcom/vividsolutions/jts/geom/Geometry;

    iget-wide v2, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->minValidDistance:D

    invoke-direct {p0, v1, v0, v2, v3}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->checkMinimumDistance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;D)V

    .line 143
    iget-boolean v1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->isValid:Z

    if-nez v1, :cond_0

    .line 146
    :goto_0
    return-void

    .line 145
    :cond_0
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->input:Lcom/vividsolutions/jts/geom/Geometry;

    iget-wide v2, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->maxValidDistance:D

    invoke-direct {p0, v1, v0, v2, v3}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->checkMaximumDistance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;D)V

    goto :goto_0
.end method

.method private getPolygonLines(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 6
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 168
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 169
    .local v2, "lines":Ljava/util/List;
    new-instance v1, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;

    invoke-direct {v1, v2}, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;-><init>(Ljava/util/Collection;)V

    .line 170
    .local v1, "lineExtracter":Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;
    invoke-static {p1}, Lcom/vividsolutions/jts/geom/util/PolygonExtracter;->getPolygons(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;

    move-result-object v4

    .line 171
    .local v4, "polys":Ljava/util/List;
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 172
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/geom/Polygon;

    .line 173
    .local v3, "poly":Lcom/vividsolutions/jts/geom/Polygon;
    invoke-virtual {v3, v1}, Lcom/vividsolutions/jts/geom/Polygon;->apply(Lcom/vividsolutions/jts/geom/GeometryComponentFilter;)V

    goto :goto_0

    .line 175
    .end local v3    # "poly":Lcom/vividsolutions/jts/geom/Polygon;
    :cond_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->buildGeometry(Ljava/util/Collection;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v5

    return-object v5
.end method


# virtual methods
.method public getErrorIndicator()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->errorIndicator:Lcom/vividsolutions/jts/geom/Geometry;

    return-object v0
.end method

.method public getErrorLocation()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->errorLocation:Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

.method public getErrorMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->errMsg:Ljava/lang/String;

    return-object v0
.end method

.method public isValid()Z
    .locals 12

    .prologue
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    .line 90
    iget-wide v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->bufDistance:D

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    .line 91
    .local v2, "posDistance":D
    const-wide v4, 0x3f889374bc6a7efaL    # 0.012

    mul-double v0, v4, v2

    .line 92
    .local v0, "distDelta":D
    sub-double v4, v2, v0

    iput-wide v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->minValidDistance:D

    .line 93
    add-double v4, v2, v0

    iput-wide v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->maxValidDistance:D

    .line 96
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->input:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->result:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 97
    :cond_0
    const/4 v4, 0x1

    .line 112
    :goto_0
    return v4

    .line 99
    :cond_1
    iget-wide v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->bufDistance:D

    const-wide/16 v6, 0x0

    cmpl-double v4, v4, v6

    if-lez v4, :cond_3

    .line 100
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->checkPositiveValid()V

    .line 105
    :goto_1
    sget-boolean v4, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->VERBOSE:Z

    if-eqz v4, :cond_2

    .line 106
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Min Dist= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->minDistanceFound:D

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  err= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->minDistanceFound:D

    iget-wide v8, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->bufDistance:D

    div-double/2addr v6, v8

    sub-double v6, v10, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  Max Dist= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->maxDistanceFound:D

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  err= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->maxDistanceFound:D

    iget-wide v8, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->bufDistance:D

    div-double/2addr v6, v8

    sub-double/2addr v6, v10

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 112
    :cond_2
    iget-boolean v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->isValid:Z

    goto :goto_0

    .line 103
    :cond_3
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->checkNegativeValid()V

    goto :goto_1
.end method
