.class Lcom/vividsolutions/jts/operation/predicate/RectangleIntersectsSegmentVisitor;
.super Lcom/vividsolutions/jts/geom/util/ShortCircuitedGeometryVisitor;
.source "RectangleIntersects.java"


# instance fields
.field private hasIntersection:Z

.field private p0:Lcom/vividsolutions/jts/geom/Coordinate;

.field private p1:Lcom/vividsolutions/jts/geom/Coordinate;

.field private rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

.field private rectIntersector:Lcom/vividsolutions/jts/algorithm/RectangleLineIntersector;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Polygon;)V
    .locals 2
    .param p1, "rectangle"    # Lcom/vividsolutions/jts/geom/Polygon;

    .prologue
    .line 291
    invoke-direct {p0}, Lcom/vividsolutions/jts/geom/util/ShortCircuitedGeometryVisitor;-><init>()V

    .line 280
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersectsSegmentVisitor;->hasIntersection:Z

    .line 281
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersectsSegmentVisitor;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 282
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersectsSegmentVisitor;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 292
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Polygon;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersectsSegmentVisitor;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    .line 293
    new-instance v0, Lcom/vividsolutions/jts/algorithm/RectangleLineIntersector;

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersectsSegmentVisitor;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/algorithm/RectangleLineIntersector;-><init>(Lcom/vividsolutions/jts/geom/Envelope;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersectsSegmentVisitor;->rectIntersector:Lcom/vividsolutions/jts/algorithm/RectangleLineIntersector;

    .line 294
    return-void
.end method

.method private checkIntersectionWithLineStrings(Ljava/util/List;)V
    .locals 3
    .param p1, "lines"    # Ljava/util/List;

    .prologue
    .line 327
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 328
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/LineString;

    .line 329
    .local v1, "testLine":Lcom/vividsolutions/jts/geom/LineString;
    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersectsSegmentVisitor;->checkIntersectionWithSegments(Lcom/vividsolutions/jts/geom/LineString;)V

    .line 330
    iget-boolean v2, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersectsSegmentVisitor;->hasIntersection:Z

    if-eqz v2, :cond_0

    .line 333
    .end local v1    # "testLine":Lcom/vividsolutions/jts/geom/LineString;
    :cond_1
    return-void
.end method

.method private checkIntersectionWithSegments(Lcom/vividsolutions/jts/geom/LineString;)V
    .locals 5
    .param p1, "testLine"    # Lcom/vividsolutions/jts/geom/LineString;

    .prologue
    .line 337
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateSequence()Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v1

    .line 338
    .local v1, "seq1":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    const/4 v0, 0x1

    .local v0, "j":I
    :goto_0
    invoke-interface {v1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 339
    add-int/lit8 v2, v0, -0x1

    iget-object v3, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersectsSegmentVisitor;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-interface {v1, v2, v3}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 340
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersectsSegmentVisitor;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-interface {v1, v0, v2}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 342
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersectsSegmentVisitor;->rectIntersector:Lcom/vividsolutions/jts/algorithm/RectangleLineIntersector;

    iget-object v3, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersectsSegmentVisitor;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v4, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersectsSegmentVisitor;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v2, v3, v4}, Lcom/vividsolutions/jts/algorithm/RectangleLineIntersector;->intersects(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 343
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersectsSegmentVisitor;->hasIntersection:Z

    .line 347
    :cond_0
    return-void

    .line 338
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public intersects()Z
    .locals 1

    .prologue
    .line 304
    iget-boolean v0, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersectsSegmentVisitor;->hasIntersection:Z

    return v0
.end method

.method protected isDone()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 351
    iget-boolean v1, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersectsSegmentVisitor;->hasIntersection:Z

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected visit(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 3
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 314
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    .line 315
    .local v0, "elementEnv":Lcom/vividsolutions/jts/geom/Envelope;
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersectsSegmentVisitor;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v2, v0}, Lcom/vividsolutions/jts/geom/Envelope;->intersects(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 323
    :goto_0
    return-void

    .line 321
    :cond_0
    invoke-static {p1}, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;->getLines(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;

    move-result-object v1

    .line 322
    .local v1, "lines":Ljava/util/List;
    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersectsSegmentVisitor;->checkIntersectionWithLineStrings(Ljava/util/List;)V

    goto :goto_0
.end method
