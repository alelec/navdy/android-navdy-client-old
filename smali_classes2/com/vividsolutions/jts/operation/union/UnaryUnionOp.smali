.class public Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;
.super Ljava/lang/Object;
.source "UnaryUnionOp.java"


# instance fields
.field private geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

.field private lines:Ljava/util/List;

.field private points:Ljava/util/List;

.field private polygons:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->polygons:Ljava/util/List;

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->lines:Ljava/util/List;

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->points:Ljava/util/List;

    .line 102
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 117
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->extract(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 118
    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .param p1, "geoms"    # Ljava/util/Collection;

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->polygons:Ljava/util/List;

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->lines:Ljava/util/List;

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->points:Ljava/util/List;

    .line 102
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 112
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->extract(Ljava/util/Collection;)V

    .line 113
    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;Lcom/vividsolutions/jts/geom/GeometryFactory;)V
    .locals 1
    .param p1, "geoms"    # Ljava/util/Collection;
    .param p2, "geomFact"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->polygons:Ljava/util/List;

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->lines:Ljava/util/List;

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->points:Ljava/util/List;

    .line 102
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 106
    iput-object p2, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 107
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->extract(Ljava/util/Collection;)V

    .line 108
    return-void
.end method

.method private extract(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 2
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 130
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    if-nez v0, :cond_0

    .line 131
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 138
    :cond_0
    const-class v0, Lcom/vividsolutions/jts/geom/Polygon;

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->polygons:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lcom/vividsolutions/jts/geom/util/GeometryExtracter;->extract(Lcom/vividsolutions/jts/geom/Geometry;Ljava/lang/Class;Ljava/util/List;)Ljava/util/List;

    .line 139
    const-class v0, Lcom/vividsolutions/jts/geom/LineString;

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->lines:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lcom/vividsolutions/jts/geom/util/GeometryExtracter;->extract(Lcom/vividsolutions/jts/geom/Geometry;Ljava/lang/Class;Ljava/util/List;)Ljava/util/List;

    .line 140
    const-class v0, Lcom/vividsolutions/jts/geom/Point;

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->points:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lcom/vividsolutions/jts/geom/util/GeometryExtracter;->extract(Lcom/vividsolutions/jts/geom/Geometry;Ljava/lang/Class;Ljava/util/List;)Ljava/util/List;

    .line 141
    return-void
.end method

.method private extract(Ljava/util/Collection;)V
    .locals 3
    .param p1, "geoms"    # Ljava/util/Collection;

    .prologue
    .line 122
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 123
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    .line 124
    .local v0, "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->extract(Lcom/vividsolutions/jts/geom/Geometry;)V

    goto :goto_0

    .line 126
    .end local v0    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    return-void
.end method

.method public static union(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p0, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 94
    new-instance v0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 95
    .local v0, "op":Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->union()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method

.method public static union(Ljava/util/Collection;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p0, "geoms"    # Ljava/util/Collection;

    .prologue
    .line 82
    new-instance v0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;-><init>(Ljava/util/Collection;)V

    .line 83
    .local v0, "op":Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->union()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method

.method public static union(Ljava/util/Collection;Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p0, "geoms"    # Ljava/util/Collection;
    .param p1, "geomFact"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 88
    new-instance v0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;

    invoke-direct {v0, p0, p1}, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;-><init>(Ljava/util/Collection;Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 89
    .local v0, "op":Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->union()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method

.method private unionNoOpt(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 3
    .param p1, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 234
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    const/4 v1, 0x0

    check-cast v1, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v2, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Point;

    move-result-object v0

    .line 235
    .local v0, "empty":Lcom/vividsolutions/jts/geom/Geometry;
    const/4 v1, 0x2

    invoke-static {p1, v0, v1}, Lcom/vividsolutions/jts/operation/overlay/snap/SnapIfNeededOverlayOp;->overlayOp(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method

.method private unionWithNull(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 0
    .param p1, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 209
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    .line 210
    const/4 p1, 0x0

    .line 217
    .end local p1    # "g0":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    :goto_0
    return-object p1

    .line 212
    .restart local p1    # "g0":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    if-eqz p2, :cond_0

    .line 214
    if-nez p1, :cond_2

    move-object p1, p2

    .line 215
    goto :goto_0

    .line 217
    :cond_2
    invoke-virtual {p1, p2}, Lcom/vividsolutions/jts/geom/Geometry;->union(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object p1

    goto :goto_0
.end method


# virtual methods
.method public union()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 152
    iget-object v8, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    if-nez v8, :cond_1

    move-object v2, v7

    .line 195
    :cond_0
    :goto_0
    return-object v2

    .line 162
    :cond_1
    const/4 v5, 0x0

    .line 163
    .local v5, "unionPoints":Lcom/vividsolutions/jts/geom/Geometry;
    iget-object v8, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->points:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_2

    .line 164
    iget-object v8, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    iget-object v9, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->points:Ljava/util/List;

    invoke-virtual {v8, v9}, Lcom/vividsolutions/jts/geom/GeometryFactory;->buildGeometry(Ljava/util/Collection;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    .line 165
    .local v1, "ptGeom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->unionNoOpt(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v5

    .line 168
    .end local v1    # "ptGeom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_2
    const/4 v4, 0x0

    .line 169
    .local v4, "unionLines":Lcom/vividsolutions/jts/geom/Geometry;
    iget-object v8, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->lines:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_3

    .line 170
    iget-object v8, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    iget-object v9, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->lines:Ljava/util/List;

    invoke-virtual {v8, v9}, Lcom/vividsolutions/jts/geom/GeometryFactory;->buildGeometry(Ljava/util/Collection;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 171
    .local v0, "lineGeom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->unionNoOpt(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v4

    .line 174
    .end local v0    # "lineGeom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_3
    const/4 v6, 0x0

    .line 175
    .local v6, "unionPolygons":Lcom/vividsolutions/jts/geom/Geometry;
    iget-object v8, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->polygons:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_4

    .line 176
    iget-object v8, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->polygons:Ljava/util/List;

    invoke-static {v8}, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->union(Ljava/util/Collection;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v6

    .line 183
    :cond_4
    invoke-direct {p0, v4, v6}, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->unionWithNull(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    .line 184
    .local v3, "unionLA":Lcom/vividsolutions/jts/geom/Geometry;
    const/4 v2, 0x0

    .line 185
    .local v2, "union":Lcom/vividsolutions/jts/geom/Geometry;
    if-nez v5, :cond_5

    .line 186
    move-object v2, v3

    .line 192
    .end local v5    # "unionPoints":Lcom/vividsolutions/jts/geom/Geometry;
    :goto_1
    if-nez v2, :cond_0

    .line 193
    iget-object v8, p0, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v8, v7}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createGeometryCollection([Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/GeometryCollection;

    move-result-object v2

    goto :goto_0

    .line 187
    .restart local v5    # "unionPoints":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_5
    if-nez v3, :cond_6

    .line 188
    move-object v2, v5

    goto :goto_1

    .line 190
    :cond_6
    check-cast v5, Lcom/vividsolutions/jts/geom/Puntal;

    .end local v5    # "unionPoints":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-static {v5, v3}, Lcom/vividsolutions/jts/operation/union/PointGeometryUnion;->union(Lcom/vividsolutions/jts/geom/Puntal;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    goto :goto_1
.end method
