.class public Lorg/droidparts/inner/converter/ArrayCollectionConverter;
.super Lorg/droidparts/inner/converter/Converter;
.source "ArrayCollectionConverter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/droidparts/inner/converter/Converter",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field private static final SEP:Ljava/lang/String; = "|\u001e"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lorg/droidparts/inner/converter/Converter;-><init>()V

    .line 165
    return-void
.end method

.method private arrOrCollToList(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Object;)Ljava/util/ArrayList;
    .locals 2
    .param p3, "val"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 248
    .local p1, "valType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .local p2, "componentType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 249
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    invoke-static {p1}, Lorg/droidparts/inner/TypeHelper;->isArray(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 250
    invoke-static {p3}, Lorg/droidparts/util/Arrays2;->toObjectArray(Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 254
    .end local p3    # "val":Ljava/lang/Object;
    :goto_0
    return-object v0

    .line 252
    .restart local p3    # "val":Ljava/lang/Object;
    :cond_0
    check-cast p3, Ljava/util/Collection;

    .end local p3    # "val":Ljava/lang/Object;
    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method private final parseTypeArr(Lorg/droidparts/inner/converter/Converter;Ljava/lang/Class;[Ljava/lang/String;)Ljava/lang/Object;
    .locals 5
    .param p3, "arr"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/droidparts/inner/converter/Converter",
            "<TT;>;",
            "Ljava/lang/Class",
            "<TT;>;[",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Object;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 260
    .local p1, "converter":Lorg/droidparts/inner/converter/Converter;, "Lorg/droidparts/inner/converter/Converter<TT;>;"
    .local p2, "valType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    array-length v3, p3

    invoke-static {p2, v3}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v2

    .line 261
    .local v2, "objArr":Ljava/lang/Object;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p3

    if-ge v0, v3, :cond_0

    .line 262
    const/4 v3, 0x0

    aget-object v4, p3, v0

    invoke-virtual {p1, p2, v3, v4}, Lorg/droidparts/inner/converter/Converter;->parseFromString(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 263
    .local v1, "item":Ljava/lang/Object;, "TT;"
    invoke-static {v2, v0, v1}, Ljava/lang/reflect/Array;->set(Ljava/lang/Object;ILjava/lang/Object;)V

    .line 261
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 265
    .end local v1    # "item":Ljava/lang/Object;, "TT;"
    :cond_0
    return-object v2
.end method

.method private final parseTypeColl(Lorg/droidparts/inner/converter/Converter;Ljava/lang/Class;Ljava/lang/Class;[Ljava/lang/String;)Ljava/util/Collection;
    .locals 5
    .param p4, "arr"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/droidparts/inner/converter/Converter",
            "<TT;>;",
            "Ljava/lang/Class",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Class",
            "<TT;>;[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Collection",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 272
    .local p1, "converter":Lorg/droidparts/inner/converter/Converter;, "Lorg/droidparts/inner/converter/Converter<TT;>;"
    .local p2, "collType":Ljava/lang/Class;, "Ljava/lang/Class<Ljava/lang/Object;>;"
    .local p3, "componentType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-static {p2}, Lorg/droidparts/inner/ReflectionUtils;->newInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 273
    .local v0, "coll":Ljava/util/Collection;, "Ljava/util/Collection<TT;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, p4

    if-ge v1, v3, :cond_0

    .line 274
    const/4 v3, 0x0

    aget-object v4, p4, v1

    invoke-virtual {p1, p3, v3, v4}, Lorg/droidparts/inner/converter/Converter;->parseFromString(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 275
    .local v2, "item":Ljava/lang/Object;, "TT;"
    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 273
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 277
    .end local v2    # "item":Ljava/lang/Object;, "TT;"
    :cond_0
    return-object v0
.end method


# virtual methods
.method public canHandle(Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 53
    .local p1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {p1}, Lorg/droidparts/inner/TypeHelper;->isArray(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lorg/droidparts/inner/TypeHelper;->isCollection(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDBColumnType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    const-string v0, " TEXT"

    return-object v0
.end method

.method protected parseFromString(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p3, "str"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Class",
            "<TV;>;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 106
    .local p1, "valType":Ljava/lang/Class;, "Ljava/lang/Class<Ljava/lang/Object;>;"
    .local p2, "componentType":Ljava/lang/Class;, "Ljava/lang/Class<TV;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public putToContentValues(Ljava/lang/Class;Ljava/lang/Class;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 10
    .param p3, "cv"    # Landroid/content/ContentValues;
    .param p4, "key"    # Ljava/lang/String;
    .param p5, "val"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Class",
            "<TV;>;",
            "Landroid/content/ContentValues;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 217
    .local p1, "valueType":Ljava/lang/Class;, "Ljava/lang/Class<Ljava/lang/Object;>;"
    .local p2, "componentType":Ljava/lang/Class;, "Ljava/lang/Class<TV;>;"
    invoke-static {p2}, Lorg/droidparts/inner/ConverterRegistry;->getConverter(Ljava/lang/Class;)Lorg/droidparts/inner/converter/Converter;

    move-result-object v0

    .line 218
    .local v0, "converter":Lorg/droidparts/inner/converter/Converter;, "Lorg/droidparts/inner/converter/Converter<TV;>;"
    invoke-direct {p0, p1, p2, p5}, Lorg/droidparts/inner/converter/ArrayCollectionConverter;->arrOrCollToList(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v7

    .line 219
    .local v7, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TV;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 220
    .local v9, "vals":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 221
    .local v3, "tmpCV":Landroid/content/ContentValues;
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 222
    .local v5, "obj":Ljava/lang/Object;, "TV;"
    const/4 v2, 0x0

    const-string v4, "key"

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lorg/droidparts/inner/converter/Converter;->putToContentValues(Ljava/lang/Class;Ljava/lang/Class;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Object;)V

    .line 224
    const-string v1, "key"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 226
    .end local v5    # "obj":Ljava/lang/Object;, "TV;"
    :cond_0
    const-string v1, "|\u001e"

    invoke-static {v9, v1}, Lorg/droidparts/util/Strings;->join(Ljava/util/Collection;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 227
    .local v8, "strVal":Ljava/lang/String;
    invoke-virtual {p3, p4, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    return-void
.end method

.method public putToJSON(Ljava/lang/Class;Ljava/lang/Class;Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 9
    .param p3, "obj"    # Lorg/json/JSONObject;
    .param p4, "key"    # Ljava/lang/String;
    .param p5, "val"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Class",
            "<TV;>;",
            "Lorg/json/JSONObject;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 92
    .local p1, "valType":Ljava/lang/Class;, "Ljava/lang/Class<Ljava/lang/Object;>;"
    .local p2, "componentType":Ljava/lang/Class;, "Ljava/lang/Class<TV;>;"
    invoke-static {p2}, Lorg/droidparts/inner/ConverterRegistry;->getConverter(Ljava/lang/Class;)Lorg/droidparts/inner/converter/Converter;

    move-result-object v0

    .line 93
    .local v0, "converter":Lorg/droidparts/inner/converter/Converter;, "Lorg/droidparts/inner/converter/Converter<TV;>;"
    invoke-direct {p0, p1, p2, p5}, Lorg/droidparts/inner/converter/ArrayCollectionConverter;->arrOrCollToList(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v7

    .line 94
    .local v7, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TV;>;"
    new-instance v8, Lorg/json/JSONArray;

    invoke-direct {v8}, Lorg/json/JSONArray;-><init>()V

    .line 95
    .local v8, "vals":Lorg/json/JSONArray;
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 96
    .local v3, "tmpObj":Lorg/json/JSONObject;
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 97
    .local v5, "value":Ljava/lang/Object;, "TV;"
    const/4 v2, 0x0

    const-string v4, "key"

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lorg/droidparts/inner/converter/Converter;->putToJSON(Ljava/lang/Class;Ljava/lang/Class;Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 98
    const-string v1, "key"

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v8, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    .line 100
    .end local v5    # "value":Ljava/lang/Object;, "TV;"
    :cond_0
    invoke-virtual {p3, p4, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 101
    return-void
.end method

.method public readFromCursor(Ljava/lang/Class;Ljava/lang/Class;Landroid/database/Cursor;I)Ljava/lang/Object;
    .locals 4
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "columnIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Class",
            "<TV;>;",
            "Landroid/database/Cursor;",
            "I)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 234
    .local p1, "valType":Ljava/lang/Class;, "Ljava/lang/Class<Ljava/lang/Object;>;"
    .local p2, "componentType":Ljava/lang/Class;, "Ljava/lang/Class<TV;>;"
    invoke-static {p2}, Lorg/droidparts/inner/ConverterRegistry;->getConverter(Ljava/lang/Class;)Lorg/droidparts/inner/converter/Converter;

    move-result-object v0

    .line 235
    .local v0, "converter":Lorg/droidparts/inner/converter/Converter;, "Lorg/droidparts/inner/converter/Converter<TV;>;"
    invoke-interface {p3, p4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 236
    .local v2, "str":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    const-string v3, "\\|\u001e"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 238
    .local v1, "parts":[Ljava/lang/String;
    :goto_0
    invoke-static {p1}, Lorg/droidparts/inner/TypeHelper;->isArray(Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 239
    invoke-direct {p0, v0, p2, v1}, Lorg/droidparts/inner/converter/ArrayCollectionConverter;->parseTypeArr(Lorg/droidparts/inner/converter/Converter;Ljava/lang/Class;[Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 241
    :goto_1
    return-object v3

    .line 236
    .end local v1    # "parts":[Ljava/lang/String;
    :cond_0
    const/4 v3, 0x0

    new-array v1, v3, [Ljava/lang/String;

    goto :goto_0

    .line 241
    .restart local v1    # "parts":[Ljava/lang/String;
    :cond_1
    invoke-direct {p0, v0, p1, p2, v1}, Lorg/droidparts/inner/converter/ArrayCollectionConverter;->parseTypeColl(Lorg/droidparts/inner/converter/Converter;Ljava/lang/Class;Ljava/lang/Class;[Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v3

    goto :goto_1
.end method

.method public readFromJSON(Ljava/lang/Class;Ljava/lang/Class;Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/Object;
    .locals 3
    .param p3, "obj"    # Lorg/json/JSONObject;
    .param p4, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Class",
            "<TV;>;",
            "Lorg/json/JSONObject;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Object;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 65
    .local p1, "valType":Ljava/lang/Class;, "Ljava/lang/Class<Ljava/lang/Object;>;"
    .local p2, "componentType":Ljava/lang/Class;, "Ljava/lang/Class<TV;>;"
    new-instance v0, Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;

    invoke-virtual {p3, p4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;-><init>(Lorg/json/JSONArray;Ljava/util/ArrayList;)V

    .line 66
    .local v0, "w":Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;
    invoke-virtual {p0, p1, p2, v0}, Lorg/droidparts/inner/converter/ArrayCollectionConverter;->readFromWrapper(Ljava/lang/Class;Ljava/lang/Class;Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method protected readFromWrapper(Ljava/lang/Class;Ljava/lang/Class;Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;)Ljava/lang/Object;
    .locals 12
    .param p3, "wrapper"    # Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Class",
            "<TV;>;",
            "Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;",
            ")",
            "Ljava/lang/Object;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 113
    .local p1, "valType":Ljava/lang/Class;, "Ljava/lang/Class<Ljava/lang/Object;>;"
    .local p2, "componentType":Ljava/lang/Class;, "Ljava/lang/Class<TV;>;"
    invoke-static {p1}, Lorg/droidparts/inner/TypeHelper;->isArray(Ljava/lang/Class;)Z

    move-result v5

    .line 114
    .local v5, "isArr":Z
    invoke-static {p2}, Lorg/droidparts/inner/TypeHelper;->isModel(Ljava/lang/Class;)Z

    move-result v6

    .line 116
    .local v6, "isModel":Z
    if-eqz v5, :cond_2

    .line 117
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 121
    .local v8, "items":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Object;>;"
    :goto_0
    const/4 v10, 0x0

    .line 122
    .local v10, "serializer":Lorg/droidparts/persist/serializer/AbstractSerializer;, "Lorg/droidparts/persist/serializer/AbstractSerializer<Lorg/droidparts/model/Model;Ljava/lang/Object;Ljava/lang/Object;>;"
    if-eqz v6, :cond_0

    .line 123
    invoke-virtual {p3, p2}, Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;->makeSerializer(Ljava/lang/Class;)Lorg/droidparts/persist/serializer/AbstractSerializer;

    move-result-object v10

    .line 126
    :cond_0
    invoke-static {p2}, Lorg/droidparts/inner/ConverterRegistry;->getConverter(Ljava/lang/Class;)Lorg/droidparts/inner/converter/Converter;

    move-result-object v2

    .line 127
    .local v2, "converter":Lorg/droidparts/inner/converter/Converter;, "Lorg/droidparts/inner/converter/Converter<TV;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    invoke-virtual {p3}, Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;->length()I

    move-result v11

    if-ge v4, v11, :cond_4

    .line 130
    if-eqz v6, :cond_3

    .line 131
    :try_start_0
    invoke-virtual {p3, v4}, Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;->get(I)Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v10, v11}, Lorg/droidparts/persist/serializer/AbstractSerializer;->deserialize(Ljava/lang/Object;)Lorg/droidparts/model/Model;

    move-result-object v7

    .line 136
    :goto_2
    invoke-interface {v8, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 119
    .end local v2    # "converter":Lorg/droidparts/inner/converter/Converter;, "Lorg/droidparts/inner/converter/Converter<TV;>;"
    .end local v4    # "i":I
    .end local v8    # "items":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Object;>;"
    .end local v10    # "serializer":Lorg/droidparts/persist/serializer/AbstractSerializer;, "Lorg/droidparts/persist/serializer/AbstractSerializer<Lorg/droidparts/model/Model;Ljava/lang/Object;Ljava/lang/Object;>;"
    :cond_2
    invoke-static {p1}, Lorg/droidparts/inner/ReflectionUtils;->newInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Collection;

    .restart local v8    # "items":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Object;>;"
    goto :goto_0

    .line 133
    .restart local v2    # "converter":Lorg/droidparts/inner/converter/Converter;, "Lorg/droidparts/inner/converter/Converter<TV;>;"
    .restart local v4    # "i":I
    .restart local v10    # "serializer":Lorg/droidparts/persist/serializer/AbstractSerializer;, "Lorg/droidparts/persist/serializer/AbstractSerializer<Lorg/droidparts/model/Model;Ljava/lang/Object;Ljava/lang/Object;>;"
    :cond_3
    :try_start_1
    invoke-virtual {p3, v4}, Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;->get(I)Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {p3, v11, v2, p2}, Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;->convert(Ljava/lang/Object;Lorg/droidparts/inner/converter/Converter;Ljava/lang/Class;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v7

    .local v7, "item":Ljava/lang/Object;
    goto :goto_2

    .line 137
    .end local v7    # "item":Ljava/lang/Object;
    :catch_0
    move-exception v3

    .line 138
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {p3}, Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;->isJSON()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 139
    throw v3

    .line 145
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_4
    if-eqz v5, :cond_8

    .line 146
    invoke-interface {v8}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v0

    .line 147
    .local v0, "arr":[Ljava/lang/Object;
    if-eqz v6, :cond_5

    .line 148
    array-length v11, v0

    invoke-static {p2, v11}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v9

    .line 149
    .local v9, "modelArr":Ljava/lang/Object;
    const/4 v4, 0x0

    :goto_3
    array-length v11, v0

    if-ge v4, v11, :cond_7

    .line 150
    aget-object v11, v0, v4

    invoke-static {v9, v4, v11}, Ljava/lang/reflect/Array;->set(Ljava/lang/Object;ILjava/lang/Object;)V

    .line 149
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 154
    .end local v9    # "modelArr":Ljava/lang/Object;
    :cond_5
    array-length v11, v0

    new-array v1, v11, [Ljava/lang/String;

    .line 155
    .local v1, "arr2":[Ljava/lang/String;
    const/4 v4, 0x0

    :goto_4
    array-length v11, v0

    if-ge v4, v11, :cond_6

    .line 156
    aget-object v11, v0, v4

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v1, v4

    .line 155
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 158
    :cond_6
    invoke-direct {p0, v2, p2, v1}, Lorg/droidparts/inner/converter/ArrayCollectionConverter;->parseTypeArr(Lorg/droidparts/inner/converter/Converter;Ljava/lang/Class;[Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    .line 161
    .end local v0    # "arr":[Ljava/lang/Object;
    .end local v1    # "arr2":[Ljava/lang/String;
    :cond_7
    :goto_5
    return-object v9

    :cond_8
    move-object v9, v8

    goto :goto_5
.end method

.method public readFromXML(Ljava/lang/Class;Ljava/lang/Class;Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/Object;
    .locals 6
    .param p3, "node"    # Lorg/w3c/dom/Node;
    .param p4, "nodeListItemTagHint"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Class",
            "<TV;>;",
            "Lorg/w3c/dom/Node;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Object;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 73
    .local p1, "valType":Ljava/lang/Class;, "Ljava/lang/Class<Ljava/lang/Object;>;"
    .local p2, "componentType":Ljava/lang/Class;, "Ljava/lang/Class<TV;>;"
    invoke-interface {p3}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v2

    .line 74
    .local v2, "nl":Lorg/w3c/dom/NodeList;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 75
    .local v3, "nodes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/w3c/dom/Node;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v5

    if-ge v0, v5, :cond_2

    .line 76
    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 77
    .local v1, "n":Lorg/w3c/dom/Node;
    invoke-static {p4}, Lorg/droidparts/util/Strings;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 78
    invoke-interface {v1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 79
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 75
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 82
    :cond_1
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 85
    .end local v1    # "n":Lorg/w3c/dom/Node;
    :cond_2
    new-instance v4, Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;

    const/4 v5, 0x0

    invoke-direct {v4, v5, v3}, Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;-><init>(Lorg/json/JSONArray;Ljava/util/ArrayList;)V

    .line 86
    .local v4, "w":Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;
    invoke-virtual {p0, p1, p2, v4}, Lorg/droidparts/inner/converter/ArrayCollectionConverter;->readFromWrapper(Ljava/lang/Class;Ljava/lang/Class;Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;)Ljava/lang/Object;

    move-result-object v5

    return-object v5
.end method
