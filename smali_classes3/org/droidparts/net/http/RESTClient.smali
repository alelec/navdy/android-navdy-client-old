.class public Lorg/droidparts/net/http/RESTClient;
.super Ljava/lang/Object;
.source "RESTClient.java"


# static fields
.field private static volatile cookieJar:Lorg/droidparts/net/http/CookieJar;


# instance fields
.field private final ctx:Landroid/content/Context;

.field private final httpClientWorker:Lorg/droidparts/net/http/worker/HttpClientWorker;

.field private final httpURLConnectionWorker:Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 46
    invoke-static {}, Lorg/droidparts/net/http/UserAgent;->getDefault()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/droidparts/net/http/RESTClient;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "userAgent"    # Ljava/lang/String;

    .prologue
    .line 50
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xa

    if-lt v0, v1, :cond_0

    new-instance v0, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;

    invoke-direct {v0, p1, p2}, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    :goto_0
    invoke-direct {p0, p1, v0}, Lorg/droidparts/net/http/RESTClient;-><init>(Landroid/content/Context;Lorg/droidparts/net/http/worker/HTTPWorker;)V

    .line 52
    return-void

    .line 50
    :cond_0
    new-instance v0, Lorg/droidparts/net/http/worker/HttpClientWorker;

    invoke-direct {v0, p2}, Lorg/droidparts/net/http/worker/HttpClientWorker;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lorg/droidparts/net/http/worker/HTTPWorker;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "worker"    # Lorg/droidparts/net/http/worker/HTTPWorker;

    .prologue
    const/4 v1, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lorg/droidparts/net/http/RESTClient;->ctx:Landroid/content/Context;

    .line 56
    instance-of v0, p2, Lorg/droidparts/net/http/worker/HttpClientWorker;

    if-eqz v0, :cond_1

    move-object v0, p2

    check-cast v0, Lorg/droidparts/net/http/worker/HttpClientWorker;

    :goto_0
    iput-object v0, p0, Lorg/droidparts/net/http/RESTClient;->httpClientWorker:Lorg/droidparts/net/http/worker/HttpClientWorker;

    .line 58
    instance-of v0, p2, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;

    if-eqz v0, :cond_2

    check-cast p2, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;

    .end local p2    # "worker":Lorg/droidparts/net/http/worker/HTTPWorker;
    :goto_1
    iput-object p2, p0, Lorg/droidparts/net/http/RESTClient;->httpURLConnectionWorker:Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;

    .line 60
    sget-object v0, Lorg/droidparts/net/http/RESTClient;->cookieJar:Lorg/droidparts/net/http/CookieJar;

    if-nez v0, :cond_0

    .line 61
    new-instance v0, Lorg/droidparts/net/http/CookieJar;

    invoke-direct {v0, p1}, Lorg/droidparts/net/http/CookieJar;-><init>(Landroid/content/Context;)V

    sput-object v0, Lorg/droidparts/net/http/RESTClient;->cookieJar:Lorg/droidparts/net/http/CookieJar;

    .line 63
    :cond_0
    return-void

    .restart local p2    # "worker":Lorg/droidparts/net/http/worker/HTTPWorker;
    :cond_1
    move-object v0, v1

    .line 56
    goto :goto_0

    :cond_2
    move-object p2, v1

    .line 58
    goto :goto_1
.end method


# virtual methods
.method public authenticateBasic(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "username"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;

    .prologue
    .line 83
    invoke-virtual {p0}, Lorg/droidparts/net/http/RESTClient;->getWorker()Lorg/droidparts/net/http/worker/HTTPWorker;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/droidparts/net/http/worker/HTTPWorker;->authenticateBasic(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    return-void
.end method

.method public delete(Ljava/lang/String;)Lorg/droidparts/net/http/HTTPResponse;
    .locals 7
    .param p1, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/droidparts/net/http/HTTPException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 187
    const-string v3, "DELETE on \'%s\'."

    new-array v4, v6, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v3, v4}, Lorg/droidparts/util/L;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 189
    iget-object v3, p0, Lorg/droidparts/net/http/RESTClient;->httpURLConnectionWorker:Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;

    if-eqz v3, :cond_0

    .line 190
    iget-object v3, p0, Lorg/droidparts/net/http/RESTClient;->httpURLConnectionWorker:Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;

    const-string v4, "DELETE"

    invoke-virtual {v3, p1, v4}, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->getConnection(Ljava/lang/String;Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object v0

    .line 192
    .local v0, "conn":Ljava/net/HttpURLConnection;
    invoke-static {v0, v6}, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->getResponse(Ljava/net/HttpURLConnection;Z)Lorg/droidparts/net/http/HTTPResponse;

    move-result-object v2

    .line 197
    .end local v0    # "conn":Ljava/net/HttpURLConnection;
    .local v2, "response":Lorg/droidparts/net/http/HTTPResponse;
    :goto_0
    invoke-static {v2}, Lorg/droidparts/util/L;->d(Ljava/lang/Object;)V

    .line 198
    return-object v2

    .line 194
    .end local v2    # "response":Lorg/droidparts/net/http/HTTPResponse;
    :cond_0
    new-instance v1, Lorg/apache/http/client/methods/HttpDelete;

    invoke-direct {v1, p1}, Lorg/apache/http/client/methods/HttpDelete;-><init>(Ljava/lang/String;)V

    .line 195
    .local v1, "req":Lorg/apache/http/client/methods/HttpDelete;
    iget-object v3, p0, Lorg/droidparts/net/http/RESTClient;->httpClientWorker:Lorg/droidparts/net/http/worker/HttpClientWorker;

    invoke-virtual {v3, v1, v6}, Lorg/droidparts/net/http/worker/HttpClientWorker;->getResponse(Lorg/apache/http/client/methods/HttpUriRequest;Z)Lorg/droidparts/net/http/HTTPResponse;

    move-result-object v2

    .restart local v2    # "response":Lorg/droidparts/net/http/HTTPResponse;
    goto :goto_0
.end method

.method public get(Ljava/lang/String;)Lorg/droidparts/net/http/HTTPResponse;
    .locals 6
    .param p1, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/droidparts/net/http/HTTPException;
        }
    .end annotation

    .prologue
    .line 89
    const-wide/16 v2, -0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lorg/droidparts/net/http/RESTClient;->get(Ljava/lang/String;JLjava/lang/String;Z)Lorg/droidparts/net/http/HTTPResponse;

    move-result-object v0

    return-object v0
.end method

.method public get(Ljava/lang/String;JLjava/lang/String;Z)Lorg/droidparts/net/http/HTTPResponse;
    .locals 10
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "ifModifiedSince"    # J
    .param p4, "etag"    # Ljava/lang/String;
    .param p5, "body"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/droidparts/net/http/HTTPException;
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0x0

    .line 98
    const-string v3, "GET on \'%s\', If-Modified-Since: \'%d\', ETag: \'%s\', body: \'%b\'."

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    aput-object p4, v4, v5

    const/4 v5, 0x3

    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lorg/droidparts/util/L;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 101
    iget-object v3, p0, Lorg/droidparts/net/http/RESTClient;->httpURLConnectionWorker:Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;

    if-eqz v3, :cond_2

    .line 102
    iget-object v3, p0, Lorg/droidparts/net/http/RESTClient;->httpURLConnectionWorker:Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;

    const-string v4, "GET"

    invoke-virtual {v3, p1, v4}, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->getConnection(Ljava/lang/String;Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object v0

    .line 104
    .local v0, "conn":Ljava/net/HttpURLConnection;
    cmp-long v3, p2, v8

    if-lez v3, :cond_0

    .line 105
    invoke-virtual {v0, p2, p3}, Ljava/net/HttpURLConnection;->setIfModifiedSince(J)V

    .line 107
    :cond_0
    if-eqz p4, :cond_1

    .line 108
    const-string v3, "If-None-Match"

    invoke-virtual {v0, v3, p4}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    :cond_1
    invoke-static {v0, p5}, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->getResponse(Ljava/net/HttpURLConnection;Z)Lorg/droidparts/net/http/HTTPResponse;

    move-result-object v2

    .line 122
    .end local v0    # "conn":Ljava/net/HttpURLConnection;
    .local v2, "response":Lorg/droidparts/net/http/HTTPResponse;
    :goto_0
    invoke-static {v2}, Lorg/droidparts/util/L;->d(Ljava/lang/Object;)V

    .line 123
    return-object v2

    .line 112
    .end local v2    # "response":Lorg/droidparts/net/http/HTTPResponse;
    :cond_2
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, p1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 113
    .local v1, "req":Lorg/apache/http/client/methods/HttpGet;
    cmp-long v3, p2, v8

    if-lez v3, :cond_3

    .line 114
    const-string v3, "If-Modified-Since"

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, p2, p3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4}, Ljava/util/Date;->toGMTString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_3
    if-eqz p4, :cond_4

    .line 118
    const-string v3, "If-None-Match"

    invoke-virtual {v1, v3, p4}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    :cond_4
    iget-object v3, p0, Lorg/droidparts/net/http/RESTClient;->httpClientWorker:Lorg/droidparts/net/http/worker/HttpClientWorker;

    invoke-virtual {v3, v1, p5}, Lorg/droidparts/net/http/worker/HttpClientWorker;->getResponse(Lorg/apache/http/client/methods/HttpUriRequest;Z)Lorg/droidparts/net/http/HTTPResponse;

    move-result-object v2

    .restart local v2    # "response":Lorg/droidparts/net/http/HTTPResponse;
    goto :goto_0
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lorg/droidparts/net/http/RESTClient;->ctx:Landroid/content/Context;

    return-object v0
.end method

.method public getInputStream(Ljava/lang/String;)Lorg/droidparts/net/http/HTTPResponse;
    .locals 6
    .param p1, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/droidparts/net/http/HTTPException;
        }
    .end annotation

    .prologue
    .line 93
    const-wide/16 v2, -0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lorg/droidparts/net/http/RESTClient;->get(Ljava/lang/String;JLjava/lang/String;Z)Lorg/droidparts/net/http/HTTPResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final getWorker()Lorg/droidparts/net/http/worker/HTTPWorker;
    .locals 2

    .prologue
    .line 202
    iget-object v1, p0, Lorg/droidparts/net/http/RESTClient;->httpClientWorker:Lorg/droidparts/net/http/worker/HttpClientWorker;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lorg/droidparts/net/http/RESTClient;->httpClientWorker:Lorg/droidparts/net/http/worker/HttpClientWorker;

    .line 204
    .local v0, "worker":Lorg/droidparts/net/http/worker/HTTPWorker;
    :goto_0
    return-object v0

    .line 202
    .end local v0    # "worker":Lorg/droidparts/net/http/worker/HTTPWorker;
    :cond_0
    iget-object v0, p0, Lorg/droidparts/net/http/RESTClient;->httpURLConnectionWorker:Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;

    goto :goto_0
.end method

.method public post(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/droidparts/net/http/HTTPResponse;
    .locals 7
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "contentType"    # Ljava/lang/String;
    .param p3, "data"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/droidparts/net/http/HTTPException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 128
    const-string v3, "POST on \'%s\', data: \'%s\'."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    aput-object p3, v4, v6

    invoke-static {v3, v4}, Lorg/droidparts/util/L;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 130
    iget-object v3, p0, Lorg/droidparts/net/http/RESTClient;->httpURLConnectionWorker:Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;

    if-eqz v3, :cond_0

    .line 131
    iget-object v3, p0, Lorg/droidparts/net/http/RESTClient;->httpURLConnectionWorker:Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;

    const-string v4, "POST"

    invoke-virtual {v3, p1, v4}, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->getConnection(Ljava/lang/String;Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object v0

    .line 133
    .local v0, "conn":Ljava/net/HttpURLConnection;
    invoke-static {v0, p2, p3}, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->postOrPut(Ljava/net/HttpURLConnection;Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    invoke-static {v0, v6}, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->getResponse(Ljava/net/HttpURLConnection;Z)Lorg/droidparts/net/http/HTTPResponse;

    move-result-object v2

    .line 140
    .end local v0    # "conn":Ljava/net/HttpURLConnection;
    .local v2, "response":Lorg/droidparts/net/http/HTTPResponse;
    :goto_0
    invoke-static {v2}, Lorg/droidparts/util/L;->d(Ljava/lang/Object;)V

    .line 141
    return-object v2

    .line 136
    .end local v2    # "response":Lorg/droidparts/net/http/HTTPResponse;
    :cond_0
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v1, p1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 137
    .local v1, "req":Lorg/apache/http/client/methods/HttpPost;
    invoke-static {p2, p3}, Lorg/droidparts/net/http/worker/HttpClientWorker;->buildStringEntity(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/entity/StringEntity;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 138
    iget-object v3, p0, Lorg/droidparts/net/http/RESTClient;->httpClientWorker:Lorg/droidparts/net/http/worker/HttpClientWorker;

    invoke-virtual {v3, v1, v6}, Lorg/droidparts/net/http/worker/HttpClientWorker;->getResponse(Lorg/apache/http/client/methods/HttpUriRequest;Z)Lorg/droidparts/net/http/HTTPResponse;

    move-result-object v2

    .restart local v2    # "response":Lorg/droidparts/net/http/HTTPResponse;
    goto :goto_0
.end method

.method public postMultipart(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Lorg/droidparts/net/http/HTTPResponse;
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/droidparts/net/http/HTTPException;
        }
    .end annotation

    .prologue
    .line 146
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lorg/droidparts/net/http/RESTClient;->postMultipart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Lorg/droidparts/net/http/HTTPResponse;

    move-result-object v0

    return-object v0
.end method

.method public postMultipart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Lorg/droidparts/net/http/HTTPResponse;
    .locals 7
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "contentType"    # Ljava/lang/String;
    .param p4, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/droidparts/net/http/HTTPException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 151
    const-string v3, "POST on \'%s\', file: \'%s\' ."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {p4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Lorg/droidparts/util/L;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 153
    iget-object v3, p0, Lorg/droidparts/net/http/RESTClient;->httpURLConnectionWorker:Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;

    if-eqz v3, :cond_0

    .line 154
    iget-object v3, p0, Lorg/droidparts/net/http/RESTClient;->httpURLConnectionWorker:Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;

    const-string v4, "POST"

    invoke-virtual {v3, p1, v4}, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->getConnection(Ljava/lang/String;Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object v0

    .line 156
    .local v0, "conn":Ljava/net/HttpURLConnection;
    invoke-static {v0, p2, p3, p4}, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->postMultipart(Ljava/net/HttpURLConnection;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    .line 158
    invoke-static {v0, v6}, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->getResponse(Ljava/net/HttpURLConnection;Z)Lorg/droidparts/net/http/HTTPResponse;

    move-result-object v2

    .line 165
    .end local v0    # "conn":Ljava/net/HttpURLConnection;
    .local v2, "response":Lorg/droidparts/net/http/HTTPResponse;
    :goto_0
    return-object v2

    .line 160
    .end local v2    # "response":Lorg/droidparts/net/http/HTTPResponse;
    :cond_0
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v1, p1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 161
    .local v1, "req":Lorg/apache/http/client/methods/HttpPost;
    invoke-static {p2, p3, p4}, Lorg/droidparts/net/http/worker/HttpClientWorker;->buildMultipartEntity(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Lorg/apache/http/HttpEntity;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 163
    iget-object v3, p0, Lorg/droidparts/net/http/RESTClient;->httpClientWorker:Lorg/droidparts/net/http/worker/HttpClientWorker;

    invoke-virtual {v3, v1, v6}, Lorg/droidparts/net/http/worker/HttpClientWorker;->getResponse(Lorg/apache/http/client/methods/HttpUriRequest;Z)Lorg/droidparts/net/http/HTTPResponse;

    move-result-object v2

    .restart local v2    # "response":Lorg/droidparts/net/http/HTTPResponse;
    goto :goto_0
.end method

.method public put(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/droidparts/net/http/HTTPResponse;
    .locals 7
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "contentType"    # Ljava/lang/String;
    .param p3, "data"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/droidparts/net/http/HTTPException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 170
    const-string v3, "PUT on \'%s\', data: \'%s\'."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    aput-object p3, v4, v6

    invoke-static {v3, v4}, Lorg/droidparts/util/L;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 172
    iget-object v3, p0, Lorg/droidparts/net/http/RESTClient;->httpURLConnectionWorker:Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;

    if-eqz v3, :cond_0

    .line 173
    iget-object v3, p0, Lorg/droidparts/net/http/RESTClient;->httpURLConnectionWorker:Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;

    const-string v4, "PUT"

    invoke-virtual {v3, p1, v4}, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->getConnection(Ljava/lang/String;Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object v0

    .line 175
    .local v0, "conn":Ljava/net/HttpURLConnection;
    invoke-static {v0, p2, p3}, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->postOrPut(Ljava/net/HttpURLConnection;Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    invoke-static {v0, v6}, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->getResponse(Ljava/net/HttpURLConnection;Z)Lorg/droidparts/net/http/HTTPResponse;

    move-result-object v2

    .line 182
    .end local v0    # "conn":Ljava/net/HttpURLConnection;
    .local v2, "response":Lorg/droidparts/net/http/HTTPResponse;
    :goto_0
    invoke-static {v2}, Lorg/droidparts/util/L;->d(Ljava/lang/Object;)V

    .line 183
    return-object v2

    .line 178
    .end local v2    # "response":Lorg/droidparts/net/http/HTTPResponse;
    :cond_0
    new-instance v1, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v1, p1}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    .line 179
    .local v1, "req":Lorg/apache/http/client/methods/HttpPut;
    invoke-static {p2, p3}, Lorg/droidparts/net/http/worker/HttpClientWorker;->buildStringEntity(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/entity/StringEntity;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 180
    iget-object v3, p0, Lorg/droidparts/net/http/RESTClient;->httpClientWorker:Lorg/droidparts/net/http/worker/HttpClientWorker;

    invoke-virtual {v3, v1, v6}, Lorg/droidparts/net/http/worker/HttpClientWorker;->getResponse(Lorg/apache/http/client/methods/HttpUriRequest;Z)Lorg/droidparts/net/http/HTTPResponse;

    move-result-object v2

    .restart local v2    # "response":Lorg/droidparts/net/http/HTTPResponse;
    goto :goto_0
.end method

.method public setCookieCacheEnabled(ZZ)V
    .locals 2
    .param p1, "enabled"    # Z
    .param p2, "persistent"    # Z

    .prologue
    .line 70
    sget-object v0, Lorg/droidparts/net/http/RESTClient;->cookieJar:Lorg/droidparts/net/http/CookieJar;

    invoke-virtual {v0, p2}, Lorg/droidparts/net/http/CookieJar;->setPersistent(Z)V

    .line 71
    invoke-virtual {p0}, Lorg/droidparts/net/http/RESTClient;->getWorker()Lorg/droidparts/net/http/worker/HTTPWorker;

    move-result-object v1

    if-eqz p1, :cond_0

    sget-object v0, Lorg/droidparts/net/http/RESTClient;->cookieJar:Lorg/droidparts/net/http/CookieJar;

    :goto_0
    invoke-virtual {v1, v0}, Lorg/droidparts/net/http/worker/HTTPWorker;->setCookieJar(Lorg/droidparts/net/http/CookieJar;)V

    .line 72
    return-void

    .line 71
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFollowRedirects(Z)V
    .locals 1
    .param p1, "follow"    # Z

    .prologue
    .line 75
    invoke-virtual {p0}, Lorg/droidparts/net/http/RESTClient;->getWorker()Lorg/droidparts/net/http/worker/HTTPWorker;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/droidparts/net/http/worker/HTTPWorker;->setFollowRedirects(Z)V

    .line 76
    return-void
.end method

.method public setHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 79
    invoke-virtual {p0}, Lorg/droidparts/net/http/RESTClient;->getWorker()Lorg/droidparts/net/http/worker/HTTPWorker;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/droidparts/net/http/worker/HTTPWorker;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    return-void
.end method
