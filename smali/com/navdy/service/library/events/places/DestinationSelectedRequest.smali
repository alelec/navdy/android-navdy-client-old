.class public final Lcom/navdy/service/library/events/places/DestinationSelectedRequest;
.super Lcom/squareup/wire/Message;
.source "DestinationSelectedRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/places/DestinationSelectedRequest$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_REQUEST_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final destination:Lcom/navdy/service/library/events/destination/Destination;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
    .end annotation
.end field

.field public final request_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/navdy/service/library/events/places/DestinationSelectedRequest$Builder;)V
    .locals 2
    .param p1, "builder"    # Lcom/navdy/service/library/events/places/DestinationSelectedRequest$Builder;

    .prologue
    .line 37
    iget-object v0, p1, Lcom/navdy/service/library/events/places/DestinationSelectedRequest$Builder;->request_id:Ljava/lang/String;

    iget-object v1, p1, Lcom/navdy/service/library/events/places/DestinationSelectedRequest$Builder;->destination:Lcom/navdy/service/library/events/destination/Destination;

    invoke-direct {p0, v0, v1}, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination;)V

    .line 38
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 39
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/places/DestinationSelectedRequest$Builder;Lcom/navdy/service/library/events/places/DestinationSelectedRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/places/DestinationSelectedRequest$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/places/DestinationSelectedRequest$1;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;-><init>(Lcom/navdy/service/library/events/places/DestinationSelectedRequest$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination;)V
    .locals 0
    .param p1, "request_id"    # Ljava/lang/String;
    .param p2, "destination"    # Lcom/navdy/service/library/events/destination/Destination;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;->request_id:Ljava/lang/String;

    .line 33
    iput-object p2, p0, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;->destination:Lcom/navdy/service/library/events/destination/Destination;

    .line 34
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 43
    if-ne p1, p0, :cond_1

    .line 47
    :cond_0
    :goto_0
    return v1

    .line 44
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 45
    check-cast v0, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;

    .line 46
    .local v0, "o":Lcom/navdy/service/library/events/places/DestinationSelectedRequest;
    iget-object v3, p0, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;->request_id:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;->request_id:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;->destination:Lcom/navdy/service/library/events/destination/Destination;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;->destination:Lcom/navdy/service/library/events/destination/Destination;

    .line 47
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 52
    iget v0, p0, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;->hashCode:I

    .line 53
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 54
    iget-object v2, p0, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;->request_id:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;->request_id:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 55
    :goto_0
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;->destination:Lcom/navdy/service/library/events/destination/Destination;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;->destination:Lcom/navdy/service/library/events/destination/Destination;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/destination/Destination;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 56
    iput v0, p0, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;->hashCode:I

    .line 58
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 54
    goto :goto_0
.end method
