.class public final Lcom/navdy/service/library/events/contacts/PhoneNumber;
.super Lcom/squareup/wire/Message;
.source "PhoneNumber.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_CUSTOMTYPE:Ljava/lang/String; = ""

.field public static final DEFAULT_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_NUMBERTYPE:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

.field private static final serialVersionUID:J


# instance fields
.field public final customType:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final numberType:Lcom/navdy/service/library/events/contacts/PhoneNumberType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/navdy/service/library/events/contacts/PhoneNumberType;->PHONE_NUMBER_HOME:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    sput-object v0, Lcom/navdy/service/library/events/contacts/PhoneNumber;->DEFAULT_NUMBERTYPE:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;)V
    .locals 3
    .param p1, "builder"    # Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;

    .prologue
    .line 46
    iget-object v0, p1, Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;->number:Ljava/lang/String;

    iget-object v1, p1, Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;->numberType:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    iget-object v2, p1, Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;->customType:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/navdy/service/library/events/contacts/PhoneNumber;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/contacts/PhoneNumberType;Ljava/lang/String;)V

    .line 47
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/contacts/PhoneNumber;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 48
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;Lcom/navdy/service/library/events/contacts/PhoneNumber$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/contacts/PhoneNumber$1;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/contacts/PhoneNumber;-><init>(Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/navdy/service/library/events/contacts/PhoneNumberType;Ljava/lang/String;)V
    .locals 0
    .param p1, "number"    # Ljava/lang/String;
    .param p2, "numberType"    # Lcom/navdy/service/library/events/contacts/PhoneNumberType;
    .param p3, "customType"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/navdy/service/library/events/contacts/PhoneNumber;->number:Ljava/lang/String;

    .line 41
    iput-object p2, p0, Lcom/navdy/service/library/events/contacts/PhoneNumber;->numberType:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    .line 42
    iput-object p3, p0, Lcom/navdy/service/library/events/contacts/PhoneNumber;->customType:Ljava/lang/String;

    .line 43
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 52
    if-ne p1, p0, :cond_1

    .line 57
    :cond_0
    :goto_0
    return v1

    .line 53
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/contacts/PhoneNumber;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 54
    check-cast v0, Lcom/navdy/service/library/events/contacts/PhoneNumber;

    .line 55
    .local v0, "o":Lcom/navdy/service/library/events/contacts/PhoneNumber;
    iget-object v3, p0, Lcom/navdy/service/library/events/contacts/PhoneNumber;->number:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/contacts/PhoneNumber;->number:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/contacts/PhoneNumber;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/contacts/PhoneNumber;->numberType:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    iget-object v4, v0, Lcom/navdy/service/library/events/contacts/PhoneNumber;->numberType:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    .line 56
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/contacts/PhoneNumber;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/contacts/PhoneNumber;->customType:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/contacts/PhoneNumber;->customType:Ljava/lang/String;

    .line 57
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/contacts/PhoneNumber;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 62
    iget v0, p0, Lcom/navdy/service/library/events/contacts/PhoneNumber;->hashCode:I

    .line 63
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 64
    iget-object v2, p0, Lcom/navdy/service/library/events/contacts/PhoneNumber;->number:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/contacts/PhoneNumber;->number:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 65
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/contacts/PhoneNumber;->numberType:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/contacts/PhoneNumber;->numberType:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/contacts/PhoneNumberType;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 66
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/contacts/PhoneNumber;->customType:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/contacts/PhoneNumber;->customType:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 67
    iput v0, p0, Lcom/navdy/service/library/events/contacts/PhoneNumber;->hashCode:I

    .line 69
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 64
    goto :goto_0

    :cond_3
    move v2, v1

    .line 65
    goto :goto_1
.end method
