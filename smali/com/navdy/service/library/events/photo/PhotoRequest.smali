.class public final Lcom/navdy/service/library/events/photo/PhotoRequest;
.super Lcom/squareup/wire/Message;
.source "PhotoRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/photo/PhotoRequest$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_IDENTIFIER:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PHOTOCHECKSUM:Ljava/lang/String; = ""

.field public static final DEFAULT_PHOTOTYPE:Lcom/navdy/service/library/events/photo/PhotoType;

.field private static final serialVersionUID:J


# instance fields
.field public final identifier:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final photoChecksum:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final photoType:Lcom/navdy/service/library/events/photo/PhotoType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_CONTACT:Lcom/navdy/service/library/events/photo/PhotoType;

    sput-object v0, Lcom/navdy/service/library/events/photo/PhotoRequest;->DEFAULT_PHOTOTYPE:Lcom/navdy/service/library/events/photo/PhotoType;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/photo/PhotoRequest$Builder;)V
    .locals 4
    .param p1, "builder"    # Lcom/navdy/service/library/events/photo/PhotoRequest$Builder;

    .prologue
    .line 53
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoRequest$Builder;->identifier:Ljava/lang/String;

    iget-object v1, p1, Lcom/navdy/service/library/events/photo/PhotoRequest$Builder;->photoChecksum:Ljava/lang/String;

    iget-object v2, p1, Lcom/navdy/service/library/events/photo/PhotoRequest$Builder;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    iget-object v3, p1, Lcom/navdy/service/library/events/photo/PhotoRequest$Builder;->name:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/navdy/service/library/events/photo/PhotoRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;Ljava/lang/String;)V

    .line 54
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/photo/PhotoRequest;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 55
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/photo/PhotoRequest$Builder;Lcom/navdy/service/library/events/photo/PhotoRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/photo/PhotoRequest$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/photo/PhotoRequest$1;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/photo/PhotoRequest;-><init>(Lcom/navdy/service/library/events/photo/PhotoRequest$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;Ljava/lang/String;)V
    .locals 0
    .param p1, "identifier"    # Ljava/lang/String;
    .param p2, "photoChecksum"    # Ljava/lang/String;
    .param p3, "photoType"    # Lcom/navdy/service/library/events/photo/PhotoType;
    .param p4, "name"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoRequest;->identifier:Ljava/lang/String;

    .line 47
    iput-object p2, p0, Lcom/navdy/service/library/events/photo/PhotoRequest;->photoChecksum:Ljava/lang/String;

    .line 48
    iput-object p3, p0, Lcom/navdy/service/library/events/photo/PhotoRequest;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    .line 49
    iput-object p4, p0, Lcom/navdy/service/library/events/photo/PhotoRequest;->name:Ljava/lang/String;

    .line 50
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 59
    if-ne p1, p0, :cond_1

    .line 65
    :cond_0
    :goto_0
    return v1

    .line 60
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/photo/PhotoRequest;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 61
    check-cast v0, Lcom/navdy/service/library/events/photo/PhotoRequest;

    .line 62
    .local v0, "o":Lcom/navdy/service/library/events/photo/PhotoRequest;
    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoRequest;->identifier:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/photo/PhotoRequest;->identifier:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/photo/PhotoRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoRequest;->photoChecksum:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/photo/PhotoRequest;->photoChecksum:Ljava/lang/String;

    .line 63
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/photo/PhotoRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoRequest;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    iget-object v4, v0, Lcom/navdy/service/library/events/photo/PhotoRequest;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    .line 64
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/photo/PhotoRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoRequest;->name:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/photo/PhotoRequest;->name:Ljava/lang/String;

    .line 65
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/photo/PhotoRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 70
    iget v0, p0, Lcom/navdy/service/library/events/photo/PhotoRequest;->hashCode:I

    .line 71
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 72
    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoRequest;->identifier:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoRequest;->identifier:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 73
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoRequest;->photoChecksum:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoRequest;->photoChecksum:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 74
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoRequest;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoRequest;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/photo/PhotoType;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 75
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoRequest;->name:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/photo/PhotoRequest;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 76
    iput v0, p0, Lcom/navdy/service/library/events/photo/PhotoRequest;->hashCode:I

    .line 78
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 72
    goto :goto_0

    :cond_3
    move v2, v1

    .line 73
    goto :goto_1

    :cond_4
    move v2, v1

    .line 74
    goto :goto_2
.end method
