.class public Lcom/navdy/client/debug/file/RemoteFileTransferManager;
.super Lcom/navdy/client/debug/file/FileTransferManager;
.source "RemoteFileTransferManager.java"


# static fields
.field private static final CHUNK_TIMEOUT:J = 0x7530L

.field private static final FLOW_CONTROL_ENABLED:Z = true

.field private static final INITIAL_REQUEST_TIMEOUT:J = 0x2710L

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private mBytesTransferredSoFar:J

.field private mChunkId:I

.field private mChunkSize:I

.field private volatile mFileUploadCancelled:Z

.field private mFileWriter:Ljava/io/FileOutputStream;

.field private mIsDone:Z

.field private final mRemoteDeviceLock:Ljava/lang/Object;

.field private mRequestTimeout:J

.field private mSerialExecutor:Ljava/util/concurrent/ExecutorService;

.field private mServerSupportsFlowControl:Z

.field private mStartOffset:J

.field private mTransferId:I

.field private timeoutTimer:Ljava/util/Timer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 37
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-string v1, "HUDFileTransferManager"

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/navdy/service/library/events/file/FileType;JLcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fileType"    # Lcom/navdy/service/library/events/file/FileType;
    .param p3, "dataLength"    # J
    .param p5, "fileTransferListener"    # Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;

    .prologue
    const-wide/16 v8, 0x0

    const/4 v7, -0x1

    const/4 v6, 0x0

    .line 82
    invoke-static {p3, p4}, Lcom/navdy/service/library/file/TransferDataSource;->testSource(J)Lcom/navdy/service/library/file/TransferDataSource;

    move-result-object v2

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object/from16 v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/debug/file/FileTransferManager;-><init>(Landroid/content/Context;Lcom/navdy/service/library/file/TransferDataSource;Lcom/navdy/service/library/events/file/FileType;Ljava/lang/String;Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;)V

    .line 45
    iput-boolean v6, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mIsDone:Z

    .line 46
    iput-wide v8, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mStartOffset:J

    .line 47
    iput-wide v8, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mBytesTransferredSoFar:J

    .line 48
    iput v7, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mTransferId:I

    .line 49
    iput v7, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mChunkId:I

    .line 50
    iput v6, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mChunkSize:I

    .line 54
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mRemoteDeviceLock:Ljava/lang/Object;

    .line 55
    iput-boolean v6, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mFileUploadCancelled:Z

    .line 56
    iput-boolean v6, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mServerSupportsFlowControl:Z

    .line 83
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/navdy/service/library/events/file/FileType;Ljava/lang/String;JLcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fileType"    # Lcom/navdy/service/library/events/file/FileType;
    .param p3, "destinationFolder"    # Ljava/lang/String;
    .param p4, "requestTimeOut"    # J
    .param p6, "fileTransferListener"    # Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 61
    invoke-direct {p0, p1, p2, p3, p6}, Lcom/navdy/client/debug/file/FileTransferManager;-><init>(Landroid/content/Context;Lcom/navdy/service/library/events/file/FileType;Ljava/lang/String;Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;)V

    .line 45
    iput-boolean v1, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mIsDone:Z

    .line 46
    iput-wide v2, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mStartOffset:J

    .line 47
    iput-wide v2, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mBytesTransferredSoFar:J

    .line 48
    iput v0, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mTransferId:I

    .line 49
    iput v0, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mChunkId:I

    .line 50
    iput v1, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mChunkSize:I

    .line 54
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mRemoteDeviceLock:Ljava/lang/Object;

    .line 55
    iput-boolean v1, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mFileUploadCancelled:Z

    .line 56
    iput-boolean v1, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mServerSupportsFlowControl:Z

    .line 64
    iput-wide p4, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mRequestTimeout:J

    .line 65
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mSerialExecutor:Ljava/util/concurrent/ExecutorService;

    .line 66
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/io/File;Lcom/navdy/service/library/events/file/FileType;Ljava/lang/String;Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fileToSend"    # Ljava/io/File;
    .param p3, "fileType"    # Lcom/navdy/service/library/events/file/FileType;
    .param p4, "destinationFileName"    # Ljava/lang/String;
    .param p5, "fileTransferListener"    # Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;

    .prologue
    const-wide/16 v8, 0x0

    const/4 v7, -0x1

    const/4 v6, 0x0

    .line 72
    .line 73
    invoke-static {p2}, Lcom/navdy/service/library/file/TransferDataSource;->fileSource(Ljava/io/File;)Lcom/navdy/service/library/file/TransferDataSource;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 72
    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/debug/file/FileTransferManager;-><init>(Landroid/content/Context;Lcom/navdy/service/library/file/TransferDataSource;Lcom/navdy/service/library/events/file/FileType;Ljava/lang/String;Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;)V

    .line 45
    iput-boolean v6, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mIsDone:Z

    .line 46
    iput-wide v8, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mStartOffset:J

    .line 47
    iput-wide v8, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mBytesTransferredSoFar:J

    .line 48
    iput v7, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mTransferId:I

    .line 49
    iput v7, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mChunkId:I

    .line 50
    iput v6, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mChunkSize:I

    .line 54
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mRemoteDeviceLock:Ljava/lang/Object;

    .line 55
    iput-boolean v6, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mFileUploadCancelled:Z

    .line 56
    iput-boolean v6, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mServerSupportsFlowControl:Z

    .line 77
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 78
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/debug/file/RemoteFileTransferManager;Lcom/navdy/service/library/events/file/FileTransferError;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/file/RemoteFileTransferManager;
    .param p1, "x1"    # Lcom/navdy/service/library/events/file/FileTransferError;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->doneWithError(Lcom/navdy/service/library/events/file/FileTransferError;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/client/debug/file/RemoteFileTransferManager;)Ljava/io/FileOutputStream;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/file/RemoteFileTransferManager;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mFileWriter:Ljava/io/FileOutputStream;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/debug/file/RemoteFileTransferManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/file/RemoteFileTransferManager;

    .prologue
    .line 35
    iget v0, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mTransferId:I

    return v0
.end method

.method static synthetic access$300(Lcom/navdy/client/debug/file/RemoteFileTransferManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/file/RemoteFileTransferManager;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->done()V

    return-void
.end method

.method static synthetic access$400()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method private declared-synchronized buildTimerInstance()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/ConcurrentModificationException;
        }
    .end annotation

    .prologue
    .line 195
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->timeoutTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 196
    new-instance v0, Ljava/util/ConcurrentModificationException;

    const-string v1, "Multiple instances of timeout timer are not allowed"

    invoke-direct {v0, v1}, Ljava/util/ConcurrentModificationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 195
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 199
    :cond_0
    :try_start_1
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->timeoutTimer:Ljava/util/Timer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 200
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized cancelTimeoutTimer()V
    .locals 2

    .prologue
    .line 203
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->timeoutTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->timeoutTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 205
    iget-object v0, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->timeoutTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 206
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->timeoutTimer:Ljava/util/Timer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    :goto_0
    monitor-exit p0

    return-void

    .line 208
    :cond_0
    :try_start_1
    sget-object v0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Timeout timer for file transfer was null while trying to cancel it"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 203
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private done()V
    .locals 1

    .prologue
    .line 168
    iget-boolean v0, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mIsDone:Z

    if-nez v0, :cond_0

    .line 169
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->unregister(Ljava/lang/Object;)V

    .line 171
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mIsDone:Z

    .line 172
    return-void
.end method

.method private doneWithError(Lcom/navdy/service/library/events/file/FileTransferError;Ljava/lang/String;)V
    .locals 0
    .param p1, "errorCode"    # Lcom/navdy/service/library/events/file/FileTransferError;
    .param p2, "error"    # Ljava/lang/String;

    .prologue
    .line 175
    invoke-direct {p0}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->done()V

    .line 176
    invoke-virtual {p0, p1, p2}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->postOnError(Lcom/navdy/service/library/events/file/FileTransferError;Ljava/lang/String;)V

    .line 177
    return-void
.end method

.method private postEvent(Lcom/squareup/wire/Message;)Z
    .locals 3
    .param p1, "message"    # Lcom/squareup/wire/Message;

    .prologue
    .line 157
    iget-object v1, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mRemoteDeviceLock:Ljava/lang/Object;

    monitor-enter v1

    .line 158
    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mFileUploadCancelled:Z

    if-nez v0, :cond_0

    .line 159
    invoke-static {p1}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    move-result v0

    monitor-exit v1

    .line 164
    :goto_0
    return v0

    .line 161
    :cond_0
    sget-object v0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Not sending the event to the HUD as the file upload is cancelled"

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 163
    monitor-exit v1

    .line 164
    const/4 v0, 0x0

    goto :goto_0

    .line 163
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private sendFileTransferRequest(JZ)Z
    .locals 7
    .param p1, "offset"    # J
    .param p3, "override"    # Z

    .prologue
    .line 144
    iget-object v2, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mDataSource:Lcom/navdy/service/library/file/TransferDataSource;

    invoke-virtual {v2, p1, p2}, Lcom/navdy/service/library/file/TransferDataSource;->checkSum(J)Ljava/lang/String;

    move-result-object v1

    .line 145
    .local v1, "hash":Ljava/lang/String;
    new-instance v2, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;-><init>()V

    iget-object v3, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mFileType:Lcom/navdy/service/library/events/file/FileType;

    .line 146
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->fileType(Lcom/navdy/service/library/events/file/FileType;)Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mDestinationFileName:Ljava/lang/String;

    .line 147
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->destinationFileName(Ljava/lang/String;)Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;

    move-result-object v2

    .line 148
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->offset(Ljava/lang/Long;)Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;

    move-result-object v2

    .line 149
    invoke-virtual {v2, v1}, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->fileDataChecksum(Ljava/lang/String;)Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mDataSource:Lcom/navdy/service/library/file/TransferDataSource;

    .line 150
    invoke-virtual {v3}, Lcom/navdy/service/library/file/TransferDataSource;->length()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->fileSize(Ljava/lang/Long;)Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;

    move-result-object v2

    .line 151
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->override(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;

    move-result-object v2

    .line 152
    invoke-virtual {v2}, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferRequest;

    move-result-object v0

    .line 153
    .local v0, "fileTransferRequest":Lcom/navdy/service/library/events/file/FileTransferRequest;
    invoke-direct {p0, v0}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->postEvent(Lcom/squareup/wire/Message;)Z

    move-result v2

    return v2
.end method

.method private sendNextChunk()V
    .locals 10

    .prologue
    .line 426
    :try_start_0
    iget v0, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mTransferId:I

    iget-object v1, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mDataSource:Lcom/navdy/service/library/file/TransferDataSource;

    iget v2, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mChunkId:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mChunkId:I

    iget v3, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mChunkSize:I

    iget-wide v4, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mStartOffset:J

    iget-wide v6, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mBytesTransferredSoFar:J

    invoke-static/range {v0 .. v7}, Lcom/navdy/service/library/file/FileTransferSessionManager;->prepareFileTransferData(ILcom/navdy/service/library/file/TransferDataSource;IIJJ)Lcom/navdy/service/library/events/file/FileTransferData;

    move-result-object v8

    .line 427
    .local v8, "data":Lcom/navdy/service/library/events/file/FileTransferData;
    invoke-direct {p0, v8}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->postEvent(Lcom/squareup/wire/Message;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 428
    const-wide/16 v0, 0x7530

    const-string v2, "Timeout while waiting for response on sent chunk"

    invoke-direct {p0, v0, v1, v2}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->setTimer(JLjava/lang/String;)V

    .line 436
    .end local v8    # "data":Lcom/navdy/service/library/events/file/FileTransferData;
    :goto_0
    return-void

    .line 430
    .restart local v8    # "data":Lcom/navdy/service/library/events/file/FileTransferData;
    :cond_0
    sget-object v0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "sendNextChunk: cannot send the chunk"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 432
    .end local v8    # "data":Lcom/navdy/service/library/events/file/FileTransferData;
    :catch_0
    move-exception v9

    .line 433
    .local v9, "throwable":Ljava/lang/Throwable;
    sget-object v0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Error sending the chunk"

    invoke-virtual {v0, v1, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 434
    const/4 v0, 0x0

    const-string v1, "Error sending chunk"

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->postOnError(Lcom/navdy/service/library/events/file/FileTransferError;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setTimer(JLjava/lang/String;)V
    .locals 3
    .param p1, "timeout"    # J
    .param p3, "timeoutMessage"    # Ljava/lang/String;

    .prologue
    .line 181
    :try_start_0
    invoke-direct {p0}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->buildTimerInstance()V
    :try_end_0
    .catch Ljava/util/ConcurrentModificationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 185
    :goto_0
    iget-object v1, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->timeoutTimer:Ljava/util/Timer;

    new-instance v2, Lcom/navdy/client/debug/file/RemoteFileTransferManager$1;

    invoke-direct {v2, p0, p3}, Lcom/navdy/client/debug/file/RemoteFileTransferManager$1;-><init>(Lcom/navdy/client/debug/file/RemoteFileTransferManager;Ljava/lang/String;)V

    invoke-virtual {v1, v2, p1, p2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 192
    return-void

    .line 182
    :catch_0
    move-exception v0

    .line 183
    .local v0, "e":Ljava/util/ConcurrentModificationException;
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/util/ConcurrentModificationException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->doneWithError(Lcom/navdy/service/library/events/file/FileTransferError;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public cancelFileUpload()V
    .locals 2

    .prologue
    .line 133
    sget-object v0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "File upload cancel called"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 134
    iget-object v1, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mRemoteDeviceLock:Ljava/lang/Object;

    monitor-enter v1

    .line 135
    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mFileUploadCancelled:Z

    if-nez v0, :cond_0

    .line 136
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mFileUploadCancelled:Z

    .line 137
    invoke-direct {p0}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->cancelTimeoutTimer()V

    .line 138
    invoke-direct {p0}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->done()V

    .line 140
    :cond_0
    monitor-exit v1

    .line 141
    return-void

    .line 140
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onDeviceDisconnectedEvent(Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 440
    const/4 v0, 0x0

    const-string v1, "Disconnected from the file receiver during the transfer process"

    invoke-direct {p0, v0, v1}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->doneWithError(Lcom/navdy/service/library/events/file/FileTransferError;Ljava/lang/String;)V

    .line 441
    return-void
.end method

.method public onFileTransferData(Lcom/navdy/service/library/events/file/FileTransferData;)V
    .locals 10
    .param p1, "data"    # Lcom/navdy/service/library/events/file/FileTransferData;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 348
    iget-boolean v4, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mFileUploadCancelled:Z

    if-eqz v4, :cond_1

    .line 418
    :cond_0
    :goto_0
    return-void

    .line 352
    :cond_1
    iget-object v4, p1, Lcom/navdy/service/library/events/file/FileTransferData;->transferId:Ljava/lang/Integer;

    if-eqz v4, :cond_0

    iget-object v4, p1, Lcom/navdy/service/library/events/file/FileTransferData;->transferId:Ljava/lang/Integer;

    iget v5, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mTransferId:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 353
    sget-object v4, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Transfer data received"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 359
    invoke-direct {p0}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->cancelTimeoutTimer()V

    .line 361
    iget-object v4, p1, Lcom/navdy/service/library/events/file/FileTransferData;->chunkIndex:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget v5, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mChunkId:I

    add-int/lit8 v5, v5, 0x1

    if-eq v4, v5, :cond_5

    move v1, v3

    .line 362
    .local v1, "illegalChunk":Z
    :goto_1
    if-eqz v1, :cond_2

    .line 363
    sget-object v4, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Illegal chuck received from HUD Expected :"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v7, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mChunkId:I

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", Received :"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, p1, Lcom/navdy/service/library/events/file/FileTransferData;->chunkIndex:Ljava/lang/Integer;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 366
    :cond_2
    iget-wide v8, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mBytesTransferredSoFar:J

    if-nez v1, :cond_6

    iget-object v4, p1, Lcom/navdy/service/library/events/file/FileTransferData;->dataBytes:Lokio/ByteString;

    invoke-virtual {v4}, Lokio/ByteString;->size()I

    move-result v4

    int-to-long v4, v4

    :goto_2
    add-long/2addr v4, v8

    iput-wide v4, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mBytesTransferredSoFar:J

    .line 367
    iget-object v4, p1, Lcom/navdy/service/library/events/file/FileTransferData;->chunkIndex:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mChunkId:I

    .line 368
    iget-boolean v4, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mServerSupportsFlowControl:Z

    if-eqz v4, :cond_3

    .line 369
    new-instance v0, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;-><init>()V

    .line 371
    .local v0, "builder":Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
    iget-object v4, p1, Lcom/navdy/service/library/events/file/FileTransferData;->transferId:Ljava/lang/Integer;

    invoke-virtual {v0, v4}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->transferId(Ljava/lang/Integer;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    .line 373
    if-nez v1, :cond_7

    .line 374
    iget-object v4, p1, Lcom/navdy/service/library/events/file/FileTransferData;->chunkIndex:Ljava/lang/Integer;

    invoke-virtual {v0, v4}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->chunkIndex(Ljava/lang/Integer;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    move-result-object v4

    .line 375
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->success(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    move-result-object v3

    iget-wide v4, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mBytesTransferredSoFar:J

    .line 376
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->totalBytesTransferred(Ljava/lang/Long;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    .line 377
    iget-object v3, p1, Lcom/navdy/service/library/events/file/FileTransferData;->lastChunk:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->transferComplete(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    .line 378
    invoke-virtual {v0}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferStatus;

    move-result-object v2

    .line 379
    .local v2, "status":Lcom/navdy/service/library/events/file/FileTransferStatus;
    invoke-direct {p0, v2}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 387
    .end local v0    # "builder":Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
    .end local v2    # "status":Lcom/navdy/service/library/events/file/FileTransferStatus;
    :cond_3
    if-nez v1, :cond_0

    .line 390
    iget-object v3, p1, Lcom/navdy/service/library/events/file/FileTransferData;->lastChunk:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_4

    .line 392
    const-wide/16 v4, 0x7530

    const-string v3, "Error receiving next chunk"

    invoke-direct {p0, v4, v5, v3}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->setTimer(JLjava/lang/String;)V

    .line 394
    :cond_4
    iget-object v3, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mSerialExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/navdy/client/debug/file/RemoteFileTransferManager$2;

    invoke-direct {v4, p0, p1}, Lcom/navdy/client/debug/file/RemoteFileTransferManager$2;-><init>(Lcom/navdy/client/debug/file/RemoteFileTransferManager;Lcom/navdy/service/library/events/file/FileTransferData;)V

    invoke-interface {v3, v4}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto/16 :goto_0

    .end local v1    # "illegalChunk":Z
    :cond_5
    move v1, v6

    .line 361
    goto/16 :goto_1

    .line 366
    .restart local v1    # "illegalChunk":Z
    :cond_6
    const-wide/16 v4, 0x0

    goto :goto_2

    .line 381
    .restart local v0    # "builder":Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
    :cond_7
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->success(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    move-result-object v3

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->transferComplete(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    .line 382
    invoke-virtual {v0}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferStatus;

    move-result-object v2

    .line 383
    .restart local v2    # "status":Lcom/navdy/service/library/events/file/FileTransferStatus;
    invoke-direct {p0, v2}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->postEvent(Lcom/squareup/wire/Message;)Z

    goto/16 :goto_0
.end method

.method public onFileTransferResponse(Lcom/navdy/service/library/events/file/FileTransferResponse;)V
    .locals 13
    .param p1, "response"    # Lcom/navdy/service/library/events/file/FileTransferResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const-wide/16 v10, 0x0

    .line 214
    iget-boolean v8, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mFileUploadCancelled:Z

    if-eqz v8, :cond_1

    .line 303
    :cond_0
    :goto_0
    return-void

    .line 218
    :cond_1
    iget-object v8, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->fileType:Lcom/navdy/service/library/events/file/FileType;

    iget-object v9, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mFileType:Lcom/navdy/service/library/events/file/FileType;

    if-ne v8, v9, :cond_0

    .line 219
    iget-object v8, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->supportsAcks:Ljava/lang/Boolean;

    sget-object v9, Lcom/navdy/service/library/events/file/FileTransferResponse;->DEFAULT_SUPPORTSACKS:Ljava/lang/Boolean;

    invoke-static {v8, v9}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    iput-boolean v8, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mServerSupportsFlowControl:Z

    .line 221
    iget-object v8, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mDestinationFileName:Ljava/lang/String;

    if-eqz v8, :cond_2

    .line 223
    iget-object v8, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->destinationFileName:Ljava/lang/String;

    if-eqz v8, :cond_0

    iget-object v8, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->destinationFileName:Ljava/lang/String;

    iget-object v9, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mDestinationFileName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 224
    sget-object v8, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Response received "

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v12, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->fileType:Lcom/navdy/service/library/events/file/FileType;

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, " "

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v12, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->destinationFileName:Ljava/lang/String;

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 235
    :cond_2
    invoke-direct {p0}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->cancelTimeoutTimer()V

    .line 236
    invoke-virtual {p0, p1}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->postOnFileTransferResponse(Lcom/navdy/service/library/events/file/FileTransferResponse;)V

    .line 238
    iget-object v8, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_c

    .line 239
    iget-object v8, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->fileType:Lcom/navdy/service/library/events/file/FileType;

    invoke-static {v8}, Lcom/navdy/service/library/file/FileTransferSessionManager;->isPullRequest(Lcom/navdy/service/library/events/file/FileType;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 240
    iget-object v8, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->transferId:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    iput v8, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mTransferId:I

    .line 241
    iget-object v1, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->destinationFileName:Ljava/lang/String;

    .line 242
    .local v1, "destinationFileName":Ljava/lang/String;
    iget-object v8, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->maxChunkSize:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    iput v8, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mChunkSize:I

    .line 244
    new-instance v0, Ljava/io/File;

    iget-object v8, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mDestinationFolder:Ljava/lang/String;

    invoke-direct {v0, v8, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    .local v0, "destinationFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 246
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v7

    .line 247
    .local v7, "worked":Z
    if-nez v7, :cond_3

    .line 248
    sget-object v8, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unable to delete destination file: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mDestinationFolder:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 253
    .end local v7    # "worked":Z
    :cond_3
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    move-result v8

    if-nez v8, :cond_4

    .line 254
    sget-object v8, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_PERMISSION_DENIED:Lcom/navdy/service/library/events/file/FileTransferError;

    const-string v9, "Cannot create the file"

    invoke-virtual {p0, v8, v9}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->postOnError(Lcom/navdy/service/library/events/file/FileTransferError;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 258
    :catch_0
    move-exception v2

    .line 259
    .local v2, "e":Ljava/lang/Exception;
    sget-object v8, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_IO_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

    const-string v9, "Exception while creating the file"

    invoke-virtual {p0, v8, v9}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->postOnError(Lcom/navdy/service/library/events/file/FileTransferError;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 257
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_4
    :try_start_1
    new-instance v8, Ljava/io/FileOutputStream;

    invoke-direct {v8, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    iput-object v8, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mFileWriter:Ljava/io/FileOutputStream;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 262
    const-wide/16 v8, 0x7530

    const-string v10, "Failed to receive chunks in time"

    invoke-direct {p0, v8, v9, v10}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->setTimer(JLjava/lang/String;)V

    goto/16 :goto_0

    .line 265
    .end local v0    # "destinationFile":Ljava/io/File;
    .end local v1    # "destinationFileName":Ljava/lang/String;
    :cond_5
    iget-object v8, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->transferId:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    iput v8, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mTransferId:I

    .line 266
    iget-object v8, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->offset:Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 267
    .local v4, "negotiatedOffset":J
    iget-object v8, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->maxChunkSize:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    iput v8, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mChunkSize:I

    .line 268
    iget-wide v8, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mStartOffset:J

    cmp-long v8, v4, v8

    if-eqz v8, :cond_b

    .line 270
    const/4 v6, 0x0

    .line 271
    .local v6, "override":Z
    cmp-long v8, v4, v10

    if-nez v8, :cond_6

    .line 273
    iput-wide v10, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mStartOffset:J

    .line 288
    .end local v4    # "negotiatedOffset":J
    :goto_1
    if-eqz v6, :cond_b

    .line 290
    sget-object v8, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "Overriding the negotiated offset as the file may be corrupted"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 291
    iget-wide v8, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mStartOffset:J

    const/4 v10, 0x1

    invoke-direct {p0, v8, v9, v10}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->sendFileTransferRequest(JZ)Z

    goto/16 :goto_0

    .line 275
    .restart local v4    # "negotiatedOffset":J
    :cond_6
    cmp-long v8, v4, v10

    if-ltz v8, :cond_7

    iget-object v8, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mDataSource:Lcom/navdy/service/library/file/TransferDataSource;

    invoke-virtual {v8}, Lcom/navdy/service/library/file/TransferDataSource;->length()J

    move-result-wide v8

    cmp-long v8, v4, v8

    if-lez v8, :cond_a

    .line 277
    :cond_7
    const/4 v6, 0x1

    .line 286
    :cond_8
    :goto_2
    if-eqz v6, :cond_9

    move-wide v4, v10

    .end local v4    # "negotiatedOffset":J
    :cond_9
    iput-wide v4, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mStartOffset:J

    goto :goto_1

    .line 280
    .restart local v4    # "negotiatedOffset":J
    :cond_a
    iget-object v8, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mDataSource:Lcom/navdy/service/library/file/TransferDataSource;

    invoke-virtual {v8, v4, v5}, Lcom/navdy/service/library/file/TransferDataSource;->checkSum(J)Ljava/lang/String;

    move-result-object v3

    .line 281
    .local v3, "localChecksum":Ljava/lang/String;
    iget-object v8, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->checksum:Ljava/lang/String;

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_8

    .line 283
    const/4 v6, 0x1

    goto :goto_2

    .line 296
    .end local v3    # "localChecksum":Ljava/lang/String;
    .end local v4    # "negotiatedOffset":J
    .end local v6    # "override":Z
    :cond_b
    iget-wide v8, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mStartOffset:J

    iput-wide v8, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mBytesTransferredSoFar:J

    .line 297
    iget-wide v8, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mStartOffset:J

    iget-object v10, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mDataSource:Lcom/navdy/service/library/file/TransferDataSource;

    invoke-virtual {v10}, Lcom/navdy/service/library/file/TransferDataSource;->length()J

    move-result-wide v10

    cmp-long v8, v8, v10

    if-gtz v8, :cond_0

    .line 298
    invoke-direct {p0}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->sendNextChunk()V

    goto/16 :goto_0

    .line 301
    :cond_c
    iget-object v8, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    const-string v9, "File transfer response error"

    invoke-virtual {p0, v8, v9}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->postOnError(Lcom/navdy/service/library/events/file/FileTransferError;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onFileTransferStatus(Lcom/navdy/service/library/events/file/FileTransferStatus;)V
    .locals 6
    .param p1, "fileTransferStatus"    # Lcom/navdy/service/library/events/file/FileTransferStatus;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 307
    iget-boolean v1, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mFileUploadCancelled:Z

    if-eqz v1, :cond_1

    .line 338
    :cond_0
    :goto_0
    return-void

    .line 311
    :cond_1
    iget-object v1, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferId:Ljava/lang/Integer;

    iget v2, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mTransferId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 312
    sget-object v1, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "File transfer status received "

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 318
    invoke-direct {p0}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->cancelTimeoutTimer()V

    .line 319
    invoke-virtual {p0, p1}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->postOnFileTransferStatus(Lcom/navdy/service/library/events/file/FileTransferStatus;)V

    .line 320
    iget-object v1, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->success:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->chunkIndex:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v2, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mChunkId:I

    if-ne v1, v2, :cond_5

    .line 322
    iget-object v1, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->totalBytesTransferred:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mBytesTransferredSoFar:J

    .line 323
    iget-object v1, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferComplete:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 324
    invoke-direct {p0}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->done()V

    goto :goto_0

    .line 326
    :cond_2
    iget-object v1, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mDataSource:Lcom/navdy/service/library/file/TransferDataSource;

    if-eqz v1, :cond_4

    .line 327
    iget-object v1, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mDataSource:Lcom/navdy/service/library/file/TransferDataSource;

    invoke-virtual {v1}, Lcom/navdy/service/library/file/TransferDataSource;->getFile()Ljava/io/File;

    move-result-object v0

    .line 328
    .local v0, "file":Ljava/io/File;
    const-string v1, "<TESTDATA>"

    iget-object v2, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mDataSource:Lcom/navdy/service/library/file/TransferDataSource;

    invoke-virtual {v2}, Lcom/navdy/service/library/file/TransferDataSource;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_4

    .line 329
    :cond_3
    invoke-direct {p0}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->sendNextChunk()V

    goto :goto_0

    .line 333
    .end local v0    # "file":Ljava/io/File;
    :cond_4
    iget-object v1, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    const-string v2, "File transfer status error: File empty or does not exist anymore."

    invoke-virtual {p0, v1, v2}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->postOnError(Lcom/navdy/service/library/events/file/FileTransferError;Ljava/lang/String;)V

    goto :goto_0

    .line 336
    :cond_5
    iget-object v1, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    const-string v2, "File transfer status error: Invalid chunk ID"

    invoke-virtual {p0, v1, v2}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->postOnError(Lcom/navdy/service/library/events/file/FileTransferError;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected postOnError(Lcom/navdy/service/library/events/file/FileTransferError;Ljava/lang/String;)V
    .locals 0
    .param p1, "errorCode"    # Lcom/navdy/service/library/events/file/FileTransferError;
    .param p2, "error"    # Ljava/lang/String;

    .prologue
    .line 342
    invoke-super {p0, p1, p2}, Lcom/navdy/client/debug/file/FileTransferManager;->postOnError(Lcom/navdy/service/library/events/file/FileTransferError;Ljava/lang/String;)V

    .line 343
    invoke-direct {p0}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->done()V

    .line 344
    return-void
.end method

.method public pullFile()Z
    .locals 4

    .prologue
    .line 117
    new-instance v1, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;-><init>()V

    iget-object v2, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mFileType:Lcom/navdy/service/library/events/file/FileType;

    .line 118
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->fileType(Lcom/navdy/service/library/events/file/FileType;)Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;

    move-result-object v1

    const/4 v2, 0x1

    .line 119
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->supportsAcks(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;

    move-result-object v1

    .line 120
    invoke-virtual {v1}, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferRequest;

    move-result-object v0

    .line 121
    .local v0, "fileTransferRequest":Lcom/navdy/service/library/events/file/FileTransferRequest;
    const/4 v1, -0x1

    iput v1, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mChunkId:I

    .line 122
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mBytesTransferredSoFar:J

    .line 123
    iget-wide v2, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mRequestTimeout:J

    const-string v1, "Display did not respond in time"

    invoke-direct {p0, v2, v3, v1}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->setTimer(JLjava/lang/String;)V

    .line 124
    invoke-static {}, Lcom/navdy/client/app/framework/DeviceConnection;->isConnected()Z

    move-result v1

    if-nez v1, :cond_0

    .line 125
    sget-object v1, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_IO_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

    const-string v2, "Phone is not connected to the Navdy display"

    invoke-virtual {p0, v1, v2}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->postOnError(Lcom/navdy/service/library/events/file/FileTransferError;Ljava/lang/String;)V

    .line 126
    const/4 v1, 0x0

    .line 128
    :goto_0
    return v1

    :cond_0
    invoke-static {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    move-result v1

    goto :goto_0
.end method

.method public sendFile()Z
    .locals 2

    .prologue
    .line 88
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->sendFile(J)Z

    move-result v0

    return v0
.end method

.method public sendFile(J)Z
    .locals 11
    .param p1, "offset"    # J

    .prologue
    const/4 v10, 0x0

    .line 100
    sget-object v3, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Sending file %s (%sB) to the HUD"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mDataSource:Lcom/navdy/service/library/file/TransferDataSource;

    .line 101
    invoke-virtual {v6}, Lcom/navdy/service/library/file/TransferDataSource;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v10

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mDataSource:Lcom/navdy/service/library/file/TransferDataSource;

    invoke-virtual {v7}, Lcom/navdy/service/library/file/TransferDataSource;->length()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    .line 100
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 102
    iget-object v3, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mDataSource:Lcom/navdy/service/library/file/TransferDataSource;

    invoke-virtual {v3}, Lcom/navdy/service/library/file/TransferDataSource;->length()J

    move-result-wide v0

    .line 103
    .local v0, "fileLength":J
    cmp-long v3, p1, v0

    if-ltz v3, :cond_0

    .line 105
    move-wide p1, v0

    .line 107
    :cond_0
    iput-wide p1, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->mStartOffset:J

    .line 108
    invoke-direct {p0, p1, p2, v10}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->sendFileTransferRequest(JZ)Z

    move-result v2

    .line 109
    .local v2, "result":Z
    const-wide/16 v4, 0x2710

    const-string v3, "Timeout while waiting for initial file transfer response"

    invoke-direct {p0, v4, v5, v3}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->setTimer(JLjava/lang/String;)V

    .line 111
    return v2
.end method
