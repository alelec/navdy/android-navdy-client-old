.class public Lcom/navdy/client/debug/ScreenConfigFragment;
.super Landroid/app/Fragment;
.source "ScreenConfigFragment.java"


# static fields
.field private static MAX_SCALE:I = 0x0

.field private static final REPEAT_INTERVAL_MS:I = 0xfa

.field private static final TAG:Ljava/lang/String; = "ScreenConfigFragment"


# instance fields
.field private margins:Landroid/graphics/Rect;

.field private pressedButton:Landroid/view/View;

.field private repeatAction:Ljava/lang/Runnable;

.field private repeatHandler:Landroid/os/Handler;

.field private scale:I

.field private scaleX:F

.field private scaleY:F

.field private screenConfiguration:Lcom/navdy/service/library/events/settings/ScreenConfiguration;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const/16 v0, 0xa

    sput v0, Lcom/navdy/client/debug/ScreenConfigFragment;->MAX_SCALE:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 37
    const/4 v0, 0x1

    iput v0, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->scale:I

    .line 128
    new-instance v0, Lcom/navdy/client/debug/ScreenConfigFragment$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/debug/ScreenConfigFragment$1;-><init>(Lcom/navdy/client/debug/ScreenConfigFragment;)V

    iput-object v0, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->repeatAction:Ljava/lang/Runnable;

    .line 50
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/debug/ScreenConfigFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/ScreenConfigFragment;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->pressedButton:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/debug/ScreenConfigFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/ScreenConfigFragment;

    .prologue
    .line 31
    iget v0, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->scale:I

    return v0
.end method

.method static synthetic access$102(Lcom/navdy/client/debug/ScreenConfigFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/ScreenConfigFragment;
    .param p1, "x1"    # I

    .prologue
    .line 31
    iput p1, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->scale:I

    return p1
.end method

.method static synthetic access$200(Lcom/navdy/client/debug/ScreenConfigFragment;Landroid/view/View;I)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/ScreenConfigFragment;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # I

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/debug/ScreenConfigFragment;->adjustMargins(Landroid/view/View;I)V

    return-void
.end method

.method static synthetic access$300()I
    .locals 1

    .prologue
    .line 31
    sget v0, Lcom/navdy/client/debug/ScreenConfigFragment;->MAX_SCALE:I

    return v0
.end method

.method static synthetic access$400(Lcom/navdy/client/debug/ScreenConfigFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/ScreenConfigFragment;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->repeatHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private adjustMargins(IIIII)V
    .locals 7
    .param p1, "leftOffset"    # I
    .param p2, "topOffset"    # I
    .param p3, "rightOffset"    # I
    .param p4, "bottomOffset"    # I
    .param p5, "scale"    # I

    .prologue
    const/4 v6, 0x0

    .line 86
    iget-object v0, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->margins:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->margins:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->margins:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    mul-int v2, p1, p5

    add-int/2addr v1, v2

    invoke-static {v1, v6}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget-object v2, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->margins:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    mul-int v3, p2, p5

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->margins:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    mul-int v4, p3, p5

    add-int/2addr v3, v4

    .line 89
    invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget-object v4, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->margins:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    mul-int v5, p4, p5

    add-int/2addr v4, v5

    .line 90
    invoke-static {v4, v6}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 87
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 91
    invoke-direct {p0}, Lcom/navdy/client/debug/ScreenConfigFragment;->updateSettings()V

    .line 93
    :cond_0
    return-void
.end method

.method private adjustMargins(Landroid/view/View;I)V
    .locals 11
    .param p1, "button"    # Landroid/view/View;
    .param p2, "scale"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v4, -0x1

    const/4 v1, 0x0

    .line 96
    if-nez p1, :cond_0

    .line 126
    :goto_0
    return-void

    .line 99
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    move-object v5, p0

    move v6, v1

    move v7, v4

    move v8, v1

    move v9, v2

    move v10, p2

    .line 123
    invoke-direct/range {v5 .. v10}, Lcom/navdy/client/debug/ScreenConfigFragment;->adjustMargins(IIIII)V

    goto :goto_0

    :pswitch_0
    move-object v0, p0

    move v3, v1

    move v5, p2

    .line 119
    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/debug/ScreenConfigFragment;->adjustMargins(IIIII)V

    goto :goto_0

    .line 99
    :pswitch_data_0
    .packed-switch 0x7f10027f
        :pswitch_0
    .end packed-switch
.end method

.method private sendEvent(Lcom/squareup/wire/Message;)V
    .locals 0
    .param p1, "event"    # Lcom/squareup/wire/Message;

    .prologue
    .line 166
    invoke-static {p1}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 167
    return-void
.end method

.method private updateSettings()V
    .locals 7

    .prologue
    .line 170
    iget-object v1, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->margins:Landroid/graphics/Rect;

    if-eqz v1, :cond_0

    .line 171
    new-instance v0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;

    iget-object v1, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->margins:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->margins:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->margins:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->margins:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget v5, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->scaleX:F

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    iget v6, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->scaleY:F

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/navdy/service/library/events/settings/ScreenConfiguration;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Float;Ljava/lang/Float;)V

    .line 173
    .local v0, "configuration":Lcom/navdy/service/library/events/settings/ScreenConfiguration;
    new-instance v1, Lcom/navdy/service/library/events/settings/UpdateSettings;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/navdy/service/library/events/settings/UpdateSettings;-><init>(Lcom/navdy/service/library/events/settings/ScreenConfiguration;Ljava/util/List;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/ScreenConfigFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    .line 175
    .end local v0    # "configuration":Lcom/navdy/service/library/events/settings/ScreenConfiguration;
    :cond_0
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 54
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 56
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->repeatHandler:Landroid/os/Handler;

    .line 57
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 68
    const v1, 0x7f030098

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 69
    .local v0, "rootView":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 71
    return-object v0
.end method

.method public onDeviceConnectedEvent(Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 179
    new-instance v0, Lcom/navdy/service/library/events/settings/ReadSettingsRequest;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/navdy/service/library/events/settings/ReadSettingsRequest;-><init>(Ljava/util/List;)V

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/ScreenConfigFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    .line 180
    return-void
.end method

.method public onDeviceDisconnectedEvent(Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 184
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->screenConfiguration:Lcom/navdy/service/library/events/settings/ScreenConfiguration;

    .line 185
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 61
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->unregister(Ljava/lang/Object;)V

    .line 62
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 63
    return-void
.end method

.method public onReadSettingsResponse(Lcom/navdy/service/library/events/settings/ReadSettingsResponse;)V
    .locals 5
    .param p1, "response"    # Lcom/navdy/service/library/events/settings/ReadSettingsResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 189
    iget-object v0, p1, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;->screenConfiguration:Lcom/navdy/service/library/events/settings/ScreenConfiguration;

    iput-object v0, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->screenConfiguration:Lcom/navdy/service/library/events/settings/ScreenConfiguration;

    .line 190
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->screenConfiguration:Lcom/navdy/service/library/events/settings/ScreenConfiguration;

    iget-object v1, v1, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginLeft:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->screenConfiguration:Lcom/navdy/service/library/events/settings/ScreenConfiguration;

    iget-object v2, v2, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginTop:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->screenConfiguration:Lcom/navdy/service/library/events/settings/ScreenConfiguration;

    iget-object v3, v3, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginRight:Ljava/lang/Integer;

    .line 191
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v4, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->screenConfiguration:Lcom/navdy/service/library/events/settings/ScreenConfiguration;

    iget-object v4, v4, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginBottom:Ljava/lang/Integer;

    .line 192
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->margins:Landroid/graphics/Rect;

    .line 193
    iget-object v0, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->screenConfiguration:Lcom/navdy/service/library/events/settings/ScreenConfiguration;

    iget-object v0, v0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->scaleX:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->scaleX:F

    .line 194
    iget-object v0, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->screenConfiguration:Lcom/navdy/service/library/events/settings/ScreenConfiguration;

    iget-object v0, v0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->scaleY:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->scaleY:F

    .line 195
    return-void
.end method

.method public onResetScale()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100281
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 160
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->margins:Landroid/graphics/Rect;

    .line 161
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->scaleY:F

    iput v0, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->scaleX:F

    .line 162
    invoke-direct {p0}, Lcom/navdy/client/debug/ScreenConfigFragment;->updateSettings()V

    .line 163
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 76
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 78
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 80
    iget-object v0, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->screenConfiguration:Lcom/navdy/service/library/events/settings/ScreenConfiguration;

    if-nez v0, :cond_0

    .line 81
    new-instance v0, Lcom/navdy/service/library/events/settings/ReadSettingsRequest;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/navdy/service/library/events/settings/ReadSettingsRequest;-><init>(Ljava/util/List;)V

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/ScreenConfigFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    .line 83
    :cond_0
    return-void
.end method

.method onTouchButton(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;
    .annotation build Lbutterknife/OnTouch;
        value = {
            0x7f10027f,
            0x7f100280
        }
    .end annotation

    .prologue
    .line 140
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 141
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 152
    :pswitch_0
    const-string v1, "ScreenConfigFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onTouchButton: unknown action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    :goto_0
    const/4 v1, 0x0

    return v1

    .line 143
    :pswitch_1
    iput-object p1, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->pressedButton:Landroid/view/View;

    .line 144
    const/4 v1, 0x1

    iput v1, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->scale:I

    .line 145
    iget-object v1, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->repeatHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->repeatAction:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 149
    :pswitch_2
    iget-object v1, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->repeatHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/client/debug/ScreenConfigFragment;->repeatAction:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 141
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
