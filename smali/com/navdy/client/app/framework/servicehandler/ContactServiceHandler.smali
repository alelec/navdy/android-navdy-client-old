.class public Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;
.super Ljava/lang/Object;
.source "ContactServiceHandler.java"


# static fields
.field public static final DEFAULT_MAX_CONTACT:I = 0x1e

.field private static singleton:Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 49
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;->sanitizeContacts(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;I)Lcom/navdy/service/library/events/contacts/PhoneNumberType;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;
    .param p1, "x1"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;->getNumberType(I)Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized getInstance()Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;
    .locals 2

    .prologue
    .line 40
    const-class v1, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;->singleton:Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;->singleton:Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;

    .line 43
    :cond_0
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;->singleton:Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getNumberType(I)Lcom/navdy/service/library/events/contacts/PhoneNumberType;
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 185
    packed-switch p1, :pswitch_data_0

    .line 196
    sget-object v0, Lcom/navdy/service/library/events/contacts/PhoneNumberType;->PHONE_NUMBER_OTHER:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    :goto_0
    return-object v0

    .line 187
    :pswitch_0
    sget-object v0, Lcom/navdy/service/library/events/contacts/PhoneNumberType;->PHONE_NUMBER_HOME:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    goto :goto_0

    .line 190
    :pswitch_1
    sget-object v0, Lcom/navdy/service/library/events/contacts/PhoneNumberType;->PHONE_NUMBER_WORK:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    goto :goto_0

    .line 193
    :pswitch_2
    sget-object v0, Lcom/navdy/service/library/events/contacts/PhoneNumberType;->PHONE_NUMBER_MOBILE:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    goto :goto_0

    .line 185
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private sanitizeContacts(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/ContactModel;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/ContactModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 116
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 118
    .local v1, "sanitizedList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/ContactModel;

    .line 119
    .local v0, "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/ContactModel;->deduplicatePhoneNumbers()V

    .line 120
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 122
    .end local v0    # "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    :cond_0
    return-object v1
.end method


# virtual methods
.method public forceSendFavoriteContactsToHud()V
    .locals 3

    .prologue
    .line 57
    new-instance v1, Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest$Builder;-><init>()V

    const/16 v2, 0x1e

    .line 58
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest$Builder;->maxContacts(Ljava/lang/Integer;)Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest$Builder;

    move-result-object v1

    .line 59
    invoke-virtual {v1}, Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest$Builder;->build()Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest;

    move-result-object v0

    .line 60
    .local v0, "favoriteContactsRequest":Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest;
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;->onFavoriteContactsRequest(Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest;)V

    .line 61
    return-void
.end method

.method public onContactRequest(Lcom/navdy/service/library/events/contacts/ContactRequest;)V
    .locals 4
    .param p1, "request"    # Lcom/navdy/service/library/events/contacts/ContactRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 127
    new-instance v0, Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;-><init>()V

    .line 129
    .local v0, "responseBuilder":Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;
    iget-object v1, p1, Lcom/navdy/service/library/events/contacts/ContactRequest;->identifier:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 130
    iget-object v1, p1, Lcom/navdy/service/library/events/contacts/ContactRequest;->identifier:Ljava/lang/String;

    .line 131
    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;->identifier(Ljava/lang/String;)Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;

    move-result-object v1

    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_INVALID_REQUEST:Lcom/navdy/service/library/events/RequestStatus;

    .line 132
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;->status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;

    .line 133
    invoke-virtual {v0}, Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;->build()Lcom/navdy/service/library/events/contacts/ContactResponse;

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 182
    :goto_0
    return-void

    .line 137
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->weHaveContactsPermission()Z

    move-result v1

    if-nez v1, :cond_1

    .line 138
    iget-object v1, p1, Lcom/navdy/service/library/events/contacts/ContactRequest;->identifier:Ljava/lang/String;

    .line 139
    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;->identifier(Ljava/lang/String;)Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;

    move-result-object v1

    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_NOT_AVAILABLE:Lcom/navdy/service/library/events/RequestStatus;

    .line 140
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;->status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;

    .line 142
    invoke-virtual {v0}, Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;->build()Lcom/navdy/service/library/events/contacts/ContactResponse;

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    goto :goto_0

    .line 147
    :cond_1
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    new-instance v2, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler$2;

    invoke-direct {v2, p0, p1, v0}, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler$2;-><init>(Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;Lcom/navdy/service/library/events/contacts/ContactRequest;Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;)V

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public onDeviceConnectedEvent(Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectedEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;->forceSendFavoriteContactsToHud()V

    .line 54
    return-void
.end method

.method public onFavoriteContactsRequest(Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest;)V
    .locals 4
    .param p1, "request"    # Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 65
    new-instance v0, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;-><init>()V

    .line 67
    .local v0, "responseBuilder":Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;
    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->weHaveContactsPermission()Z

    move-result v1

    if-nez v1, :cond_0

    .line 68
    sget-object v1, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_NOT_AVAILABLE:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;->status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;

    .line 69
    invoke-virtual {v0}, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;->build()Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 107
    :goto_0
    return-void

    .line 73
    :cond_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    new-instance v2, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler$1;

    invoke-direct {v2, p0, p1, v0}, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler$1;-><init>(Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest;Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;)V

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method
