.class public Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;
.super Ljava/lang/Object;
.source "RecordingServiceManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager$Listener;
    }
.end annotation


# static fields
.field private static final sInstance:Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;


# instance fields
.field private mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager$Listener;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;->sInstance:Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;->mListeners:Ljava/util/List;

    .line 42
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 43
    return-void
.end method

.method public static getInstance()Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;->sInstance:Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;

    return-object v0
.end method


# virtual methods
.method public addListener(Ljava/lang/ref/WeakReference;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager$Listener;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    .local p1, "listener":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager$Listener;>;"
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    return-void
.end method

.method public onDriveRecordingsResponse(Lcom/navdy/service/library/events/debug/DriveRecordingsResponse;)V
    .locals 4
    .param p1, "response"    # Lcom/navdy/service/library/events/debug/DriveRecordingsResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 93
    iget-object v2, p0, Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 94
    .local v1, "listenerRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager$Listener;>;"
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager$Listener;

    .line 96
    .local v0, "listener":Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager$Listener;
    if-eqz v0, :cond_0

    .line 97
    iget-object v3, p1, Lcom/navdy/service/library/events/debug/DriveRecordingsResponse;->recordings:Ljava/util/List;

    invoke-interface {v0, v3}, Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager$Listener;->onDriveRecordingsResponse(Ljava/util/List;)V

    goto :goto_0

    .line 100
    .end local v0    # "listener":Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager$Listener;
    .end local v1    # "listenerRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager$Listener;>;"
    :cond_1
    return-void
.end method

.method public onStartDrivePlaybackResponse(Lcom/navdy/service/library/events/debug/StartDrivePlaybackResponse;)V
    .locals 3
    .param p1, "response"    # Lcom/navdy/service/library/events/debug/StartDrivePlaybackResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 75
    iget-object v0, p1, Lcom/navdy/service/library/events/debug/StartDrivePlaybackResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v1, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    if-ne v0, v1, :cond_0

    .line 76
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "Drive playback started successfully"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 80
    :goto_0
    return-void

    .line 78
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "Drive playback ERROR"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onStartDriveRecordingResponse(Lcom/navdy/service/library/events/debug/StartDriveRecordingResponse;)V
    .locals 3
    .param p1, "response"    # Lcom/navdy/service/library/events/debug/StartDriveRecordingResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 84
    iget-object v0, p1, Lcom/navdy/service/library/events/debug/StartDriveRecordingResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v1, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    if-ne v0, v1, :cond_0

    .line 85
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "Drive recording started successfully"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 89
    :goto_0
    return-void

    .line 87
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "Drive recording ERROR"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onStopDriveRecordingResponse(Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;)V
    .locals 3
    .param p1, "response"    # Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 104
    iget-object v0, p1, Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v1, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    if-ne v0, v1, :cond_0

    .line 105
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "Drive recording was successfully saved!"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 109
    :goto_0
    return-void

    .line 107
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "Drive recording finished with ERROR"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public removeListener(Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager$Listener;)V
    .locals 2
    .param p1, "listener"    # Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager$Listener;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;->mListeners:Ljava/util/List;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 51
    return-void
.end method

.method public sendDriveRecordingsRequest()V
    .locals 1

    .prologue
    .line 70
    new-instance v0, Lcom/navdy/service/library/events/debug/DriveRecordingsRequest;

    invoke-direct {v0}, Lcom/navdy/service/library/events/debug/DriveRecordingsRequest;-><init>()V

    invoke-static {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 71
    return-void
.end method

.method public sendStartDrivePlaybackEvent(Ljava/lang/String;)V
    .locals 2
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 62
    new-instance v0, Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent;-><init>(Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-static {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 63
    return-void
.end method

.method public sendStartDriveRecordingEvent(Ljava/lang/String;)V
    .locals 1
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 54
    new-instance v0, Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;

    invoke-direct {v0, p1}, Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 55
    return-void
.end method

.method public sendStopDrivePlaybackEvent()V
    .locals 1

    .prologue
    .line 66
    new-instance v0, Lcom/navdy/service/library/events/debug/StopDrivePlaybackEvent;

    invoke-direct {v0}, Lcom/navdy/service/library/events/debug/StopDrivePlaybackEvent;-><init>()V

    invoke-static {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 67
    return-void
.end method

.method public sendStopDriveRecordingEvent()V
    .locals 1

    .prologue
    .line 58
    new-instance v0, Lcom/navdy/service/library/events/debug/StopDriveRecordingEvent;

    invoke-direct {v0}, Lcom/navdy/service/library/events/debug/StopDriveRecordingEvent;-><init>()V

    invoke-static {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 59
    return-void
.end method
