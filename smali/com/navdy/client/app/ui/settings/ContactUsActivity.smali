.class public Lcom/navdy/client/app/ui/settings/ContactUsActivity;
.super Lcom/navdy/client/app/ui/base/BaseToolbarActivity;
.source "ContactUsActivity.java"

# interfaces
.implements Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;


# static fields
.field private static final DELAY_BEFORE_FINISHING_ACTIVITY:I = 0xbb8

.field private static final DIALOG_PHOTO_LIMIT_EXCEEDED_ID:I = 0x1

.field private static final DIALOG_PICK_FROM_GALLERY_OPTION:I = 0x0

.field private static final DIALOG_TAKE_PHOTO_OPTION:I = 0x1

.field public static final DISPLAY_LOG_CONTAIN_FILTER:Ljava/lang/String; = "display_log"

.field public static final EXTRA_DEFAULT_PROBLEM_TYPE:Ljava/lang/String; = "default_problem_type"

.field private static final FILE_SIZE_LIMIT:I = 0x200000

.field private static final GALLERY_IMAGE_SIZE:I = 0x20

.field private static final MAX_PIXEL_DIMENSION:I = 0x500

.field private static final PHOTO_ATTACHMENT_LIMIT:I = 0x5

.field private static final STORAGE_PERMISSIONS_REQUEST_CODE:I = 0x3

.field public static final TEMP_PHOTO_FILENAME:Ljava/lang/String; = "temp_photo_filename"


# instance fields
.field private attachPhotoTextView:Landroid/widget/TextView;

.field private attachments:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private currentTicketId:Ljava/lang/String;

.field private dialogs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Dialog;",
            ">;>;"
        }
    .end annotation
.end field

.field private dismissDialogsAndFinishRunnable:Ljava/lang/Runnable;

.field private displayLogAttached:Z

.field private email:Ljava/lang/String;

.field private emailAddress:Landroid/widget/EditText;

.field private failedToGetStorageRunnable:Ljava/lang/Runnable;

.field private folderHasBeenCreatedForThisTicket:Z

.field private imageViewIterator:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private myToolbar:Landroid/support/v7/widget/Toolbar;

.field private photoAttachmentCount:I

.field private problemInput:Landroid/widget/EditText;

.field private problemTypes:Lcom/navdy/client/app/ui/customviews/CustomSpinner;

.field private tempPhotoFilename:Ljava/lang/String;

.field private userWantsLogs:Z

.field private vin:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;-><init>()V

    .line 92
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->currentTicketId:Ljava/lang/String;

    .line 93
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->photoAttachmentCount:I

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->attachments:Ljava/util/ArrayList;

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->dialogs:Ljava/util/ArrayList;

    .line 104
    new-instance v0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity$1;-><init>(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->failedToGetStorageRunnable:Ljava/lang/Runnable;

    .line 112
    new-instance v0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$2;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity$2;-><init>(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->dismissDialogsAndFinishRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->showGetStoragePermissionDialog()V

    return-void
.end method

.method static synthetic access$1000(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->tempPhotoFilename:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/navdy/client/app/ui/settings/ContactUsActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->tempPhotoFilename:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/navdy/client/app/ui/settings/ContactUsActivity;Landroid/graphics/Bitmap;Ljava/io/File;Landroid/graphics/Bitmap$CompressFormat;I)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;
    .param p1, "x1"    # Landroid/graphics/Bitmap;
    .param p2, "x2"    # Ljava/io/File;
    .param p3, "x3"    # Landroid/graphics/Bitmap$CompressFormat;
    .param p4, "x4"    # I

    .prologue
    .line 68
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->compressBitmapToFile(Landroid/graphics/Bitmap;Ljava/io/File;Landroid/graphics/Bitmap$CompressFormat;I)V

    return-void
.end method

.method static synthetic access$1400(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->showFailureSnackBar()V

    return-void
.end method

.method static synthetic access$1800(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->attachments:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->dismissAllDialogs()V

    return-void
.end method

.method static synthetic access$2000(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->showSuccessSnackBar()V

    return-void
.end method

.method static synthetic access$2100(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->attachPhotoTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Ljava/util/Iterator;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->imageViewIterator:Ljava/util/Iterator;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/navdy/client/app/ui/settings/ContactUsActivity;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->addPhotoToGallery(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic access$2400(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->showAttachPhotoDialog()V

    return-void
.end method

.method static synthetic access$2500(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->startSubmitTicketService()V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Lcom/navdy/client/app/ui/customviews/CustomSpinner;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->problemTypes:Lcom/navdy/client/app/ui/customviews/CustomSpinner;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->updateSubmitButton()V

    return-void
.end method

.method static synthetic access$600(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->problemInput:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$702(Lcom/navdy/client/app/ui/settings/ContactUsActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->email:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$800(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->emailAddress:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->showMoreDetailsRequiredDialog()V

    return-void
.end method

.method private addPhotoToGallery(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "attachmentPhoto"    # Landroid/graphics/Bitmap;

    .prologue
    const/16 v3, 0x20

    .line 821
    const/4 v2, 0x1

    invoke-static {p1, v3, v3, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 822
    .local v0, "icon":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 823
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->imageViewIterator:Ljava/util/Iterator;

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 824
    .local v1, "imageView":Landroid/widget/ImageView;
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 825
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 827
    .end local v1    # "imageView":Landroid/widget/ImageView;
    :cond_0
    return-void
.end method

.method private addSubmitButtonToMenu(Landroid/view/Menu;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v2, 0x0

    .line 274
    const v0, 0x7f1003ed

    const v1, 0x7f080473

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x2

    .line 278
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 283
    return-void
.end method

.method private compressBitmapToFile(Landroid/graphics/Bitmap;Ljava/io/File;Landroid/graphics/Bitmap$CompressFormat;I)V
    .locals 6
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "outputFile"    # Ljava/io/File;
    .param p3, "format"    # Landroid/graphics/Bitmap$CompressFormat;
    .param p4, "quality"    # I

    .prologue
    .line 705
    const/4 v1, 0x0

    .line 707
    .local v1, "outputStream":Ljava/io/BufferedOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/BufferedOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 708
    .end local v1    # "outputStream":Ljava/io/BufferedOutputStream;
    .local v2, "outputStream":Ljava/io/BufferedOutputStream;
    :try_start_1
    invoke-virtual {p1, p3, p4, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 712
    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    move-object v1, v2

    .line 714
    .end local v2    # "outputStream":Ljava/io/BufferedOutputStream;
    .restart local v1    # "outputStream":Ljava/io/BufferedOutputStream;
    :goto_0
    return-void

    .line 709
    :catch_0
    move-exception v0

    .line 710
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_2
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception compressing bitmap: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 712
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    :goto_2
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v3

    .end local v1    # "outputStream":Ljava/io/BufferedOutputStream;
    .restart local v2    # "outputStream":Ljava/io/BufferedOutputStream;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "outputStream":Ljava/io/BufferedOutputStream;
    .restart local v1    # "outputStream":Ljava/io/BufferedOutputStream;
    goto :goto_2

    .line 709
    .end local v1    # "outputStream":Ljava/io/BufferedOutputStream;
    .restart local v2    # "outputStream":Ljava/io/BufferedOutputStream;
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2    # "outputStream":Ljava/io/BufferedOutputStream;
    .restart local v1    # "outputStream":Ljava/io/BufferedOutputStream;
    goto :goto_1
.end method

.method private createJSONFromTicketContents(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 5
    .param p1, "ticketDescription"    # Ljava/lang/String;
    .param p2, "vin"    # Ljava/lang/String;

    .prologue
    .line 764
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 765
    .local v1, "jsonObject":Lorg/json/JSONObject;
    const-string v2, "ticket_type"

    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->getTagFromProblemType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 766
    const-string v2, "ticket_description"

    invoke-virtual {v1, v2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 767
    const-string v2, "ticket_vin"

    invoke-virtual {v1, v2, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 768
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->emailAddress:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 769
    const-string v2, "ticket_email"

    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->emailAddress:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 771
    :cond_0
    const-string v2, "ticket_has_display_log"

    iget-boolean v3, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->displayLogAttached:Z

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 772
    iget-boolean v2, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->userWantsLogs:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->displayLogAttached:Z

    if-nez v2, :cond_1

    .line 773
    const-string v2, "ticket_action"

    const-string v3, "upload_await_hud_log"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 780
    .end local v1    # "jsonObject":Lorg/json/JSONObject;
    :goto_0
    return-object v1

    .line 775
    .restart local v1    # "jsonObject":Lorg/json/JSONObject;
    :cond_1
    const-string v2, "ticket_action"

    const-string v3, "upload_delete"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 778
    .end local v1    # "jsonObject":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 779
    .local v0, "e":Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to create JSON from Ticket Contents: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 780
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private createReadMeIfDoesNotExist()V
    .locals 6

    .prologue
    .line 791
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/navdy/client/app/framework/PathManager;->getInstance()Lcom/navdy/client/app/framework/PathManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/client/app/framework/PathManager;->getTicketFolderPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "readme.txt"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 792
    .local v2, "readMe":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 794
    :try_start_0
    new-instance v0, Ljava/io/BufferedWriter;

    new-instance v3, Ljava/io/FileWriter;

    const/4 v4, 0x1

    invoke-direct {v3, v2, v4}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V

    invoke-direct {v0, v3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 795
    .local v0, "bufferedWriter":Ljava/io/BufferedWriter;
    const v3, 0x7f080510

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/io/BufferedWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 796
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->newLine()V

    .line 797
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 802
    .end local v0    # "bufferedWriter":Ljava/io/BufferedWriter;
    :cond_0
    :goto_0
    return-void

    .line 798
    :catch_0
    move-exception v1

    .line 799
    .local v1, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "There was an exception creating the ReadMe file: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private createTicket()V
    .locals 3

    .prologue
    .line 356
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "create a Request Ticket"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 358
    const v1, 0x7f100394

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 359
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_2

    .line 360
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->emailIsValid()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->problemTypeIsValid()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->messageIsValid()Z

    move-result v1

    if-nez v1, :cond_2

    .line 361
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->isDestroyed()Z

    move-result v1

    if-nez v1, :cond_1

    .line 362
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->showMoreDetailsRequiredDialog()V

    .line 386
    :cond_1
    :goto_0
    return-void

    .line 368
    :cond_2
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/SystemUtils;->dismissKeyboard(Landroid/app/Activity;)V

    .line 369
    iget-boolean v1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->userWantsLogs:Z

    if-eqz v1, :cond_3

    .line 370
    new-instance v1, Lcom/navdy/client/app/ui/settings/ContactUsActivity$7;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity$7;-><init>(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->failedToGetStorageRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v1, v2}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->requestStoragePermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 379
    :cond_3
    new-instance v1, Lcom/navdy/client/app/ui/settings/ContactUsActivity$8;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity$8;-><init>(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->failedToGetStorageRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v1, v2}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->requestStoragePermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private disableSubmitButton()V
    .locals 3

    .prologue
    .line 306
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->myToolbar:Landroid/support/v7/widget/Toolbar;

    if-eqz v2, :cond_0

    .line 307
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->myToolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v2}, Landroid/support/v7/widget/Toolbar;->getMenu()Landroid/view/Menu;

    move-result-object v0

    .line 308
    .local v0, "menu":Landroid/view/Menu;
    if-eqz v0, :cond_0

    .line 309
    const v2, 0x7f1003ed

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 310
    .local v1, "sendButton":Landroid/view/MenuItem;
    if-eqz v1, :cond_0

    .line 311
    new-instance v2, Lcom/navdy/client/app/ui/settings/ContactUsActivity$6;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity$6;-><init>(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 323
    .end local v0    # "menu":Landroid/view/Menu;
    .end local v1    # "sendButton":Landroid/view/MenuItem;
    :cond_0
    return-void
.end method

.method private dismissAllDialogs()V
    .locals 4

    .prologue
    .line 852
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->dialogs:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->listIterator()Ljava/util/ListIterator;

    move-result-object v1

    .line 853
    .local v1, "dialogIterator":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Ljava/lang/ref/WeakReference<Landroid/app/Dialog;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/ListIterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 854
    invoke-interface {v1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    .line 855
    .local v2, "weakReferenceToDialog":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Landroid/app/Dialog;>;"
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 856
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Dialog;

    .line 857
    .local v0, "dialog":Landroid/app/Dialog;
    if-eqz v0, :cond_0

    .line 858
    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    goto :goto_0

    .line 861
    .end local v0    # "dialog":Landroid/app/Dialog;
    :cond_1
    invoke-interface {v1}, Ljava/util/ListIterator;->remove()V

    goto :goto_0

    .line 864
    .end local v2    # "weakReferenceToDialog":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Landroid/app/Dialog;>;"
    :cond_2
    return-void
.end method

.method private emailIsValid()Z
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->emailAddress:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->emailAddress:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isValidEmail(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getTagFromProblemType()Ljava/lang/String;
    .locals 4

    .prologue
    .line 785
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->problemTypes:Lcom/navdy/client/app/ui/customviews/CustomSpinner;

    invoke-virtual {v2}, Lcom/navdy/client/app/ui/customviews/CustomSpinner;->getSelectedItemPosition()I

    move-result v1

    .line 786
    .local v1, "position":I
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090027

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 787
    .local v0, "options":[Ljava/lang/String;
    aget-object v2, v0, v1

    return-object v2
.end method

.method private hideSubmitButton()V
    .locals 2

    .prologue
    .line 286
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->myToolbar:Landroid/support/v7/widget/Toolbar;

    if-eqz v1, :cond_0

    .line 287
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->myToolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1}, Landroid/support/v7/widget/Toolbar;->getMenu()Landroid/view/Menu;

    move-result-object v0

    .line 288
    .local v0, "menu":Landroid/view/Menu;
    if-eqz v0, :cond_0

    .line 289
    const v1, 0x7f1003ed

    invoke-interface {v0, v1}, Landroid/view/Menu;->removeItem(I)V

    .line 292
    .end local v0    # "menu":Landroid/view/Menu;
    :cond_0
    return-void
.end method

.method private messageIsValid()Z
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->problemInput:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->problemInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private problemTypeIsValid()Z
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->problemTypes:Lcom/navdy/client/app/ui/customviews/CustomSpinner;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->problemTypes:Lcom/navdy/client/app/ui/customviews/CustomSpinner;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/customviews/CustomSpinner;->getSelectedItemPosition()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setupGallery()V
    .locals 2

    .prologue
    .line 808
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 809
    .local v0, "imageViewArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/widget/ImageView;>;"
    const v1, 0x7f10039b

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 810
    const v1, 0x7f10039c

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 811
    const v1, 0x7f10039d

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 812
    const v1, 0x7f10039e

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 813
    const v1, 0x7f10039f

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 814
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->imageViewIterator:Ljava/util/Iterator;

    .line 815
    return-void
.end method

.method private showAttachPhotoDialog()V
    .locals 6
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 487
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureOnMainThread()V

    .line 489
    invoke-static {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isEnding(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 523
    :goto_0
    return-void

    .line 493
    :cond_0
    iget v3, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->photoAttachmentCount:I

    const/4 v4, 0x5

    if-lt v3, v4, :cond_1

    .line 495
    const v3, 0x7f0802a9

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0802aa

    invoke-virtual {p0, v4}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v5, v3, v4}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 497
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090006

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 499
    .local v2, "options":[Ljava/lang/CharSequence;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 500
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 501
    const v3, 0x7f0800c8

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 502
    new-instance v3, Lcom/navdy/client/app/ui/settings/ContactUsActivity$12;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity$12;-><init>(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 518
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 519
    .local v1, "dialog":Landroid/app/AlertDialog;
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->dialogs:Ljava/util/ArrayList;

    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-direct {v4, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 520
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private showFailureSnackBar()V
    .locals 4

    .prologue
    .line 717
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 734
    :cond_0
    :goto_0
    return-void

    .line 720
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->hideProgressDialog()V

    .line 721
    const v1, 0x7f100394

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 722
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 723
    const v1, 0x7f08033a

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/support/design/widget/Snackbar;->make(Landroid/view/View;II)Landroid/support/design/widget/Snackbar;

    move-result-object v1

    const v2, 0x7f0803dc

    new-instance v3, Lcom/navdy/client/app/ui/settings/ContactUsActivity$16;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity$16;-><init>(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V

    .line 726
    invoke-virtual {v1, v2, v3}, Landroid/support/design/widget/Snackbar;->setAction(ILandroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;

    move-result-object v1

    .line 732
    invoke-virtual {v1}, Landroid/support/design/widget/Snackbar;->show()V

    goto :goto_0
.end method

.method private showGetStoragePermissionDialog()V
    .locals 4
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    .line 451
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureOnMainThread()V

    .line 453
    invoke-static {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isEnding(Landroid/app/Activity;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 483
    :goto_0
    return-void

    .line 457
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 458
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 459
    const v2, 0x7f0804fa

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 460
    const v2, 0x7f080246

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 461
    const v2, 0x7f0801e3

    new-instance v3, Lcom/navdy/client/app/ui/settings/ContactUsActivity$10;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity$10;-><init>(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 474
    const v2, 0x7f0800e8

    new-instance v3, Lcom/navdy/client/app/ui/settings/ContactUsActivity$11;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity$11;-><init>(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 480
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 481
    .local v1, "dialog":Landroid/app/AlertDialog;
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->dialogs:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 482
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private showMoreDetailsRequiredDialog()V
    .locals 5
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    .line 527
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureOnMainThread()V

    .line 529
    invoke-static {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isEnding(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 552
    :goto_0
    return-void

    .line 533
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 534
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v3, 0x7f0802cc

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 535
    new-instance v2, Ljava/lang/StringBuilder;

    const v3, 0x7f0804f9

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 537
    .local v2, "problemsString":Ljava/lang/StringBuilder;
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->emailIsValid()Z

    move-result v3

    if-nez v3, :cond_1

    .line 538
    const v3, 0x7f08015d

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 540
    :cond_1
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->problemTypeIsValid()Z

    move-result v3

    if-nez v3, :cond_2

    .line 541
    const v3, 0x7f0803f6

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 543
    :cond_2
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->messageIsValid()Z

    move-result v3

    if-nez v3, :cond_3

    .line 544
    const v3, 0x7f080125

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 547
    :cond_3
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 548
    const v3, 0x7f08031e

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 549
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 550
    .local v1, "dialog":Landroid/app/AlertDialog;
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->dialogs:Ljava/util/ArrayList;

    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-direct {v4, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 551
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private showSubmitButton()V
    .locals 2

    .prologue
    .line 295
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->myToolbar:Landroid/support/v7/widget/Toolbar;

    if-eqz v1, :cond_0

    .line 296
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->myToolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1}, Landroid/support/v7/widget/Toolbar;->getMenu()Landroid/view/Menu;

    move-result-object v0

    .line 297
    .local v0, "menu":Landroid/view/Menu;
    if-eqz v0, :cond_0

    .line 299
    const v1, 0x7f1003ed

    invoke-interface {v0, v1}, Landroid/view/Menu;->removeItem(I)V

    .line 300
    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->addSubmitButtonToMenu(Landroid/view/Menu;)V

    .line 303
    .end local v0    # "menu":Landroid/view/Menu;
    :cond_0
    return-void
.end method

.method private showSuccessSnackBar()V
    .locals 3

    .prologue
    .line 737
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 748
    :cond_0
    :goto_0
    return-void

    .line 740
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->hideProgressDialog()V

    .line 741
    const v1, 0x7f100394

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 742
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 743
    const v1, 0x7f08033b

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/support/design/widget/Snackbar;->make(Landroid/view/View;II)Landroid/support/design/widget/Snackbar;

    move-result-object v1

    .line 746
    invoke-virtual {v1}, Landroid/support/design/widget/Snackbar;->show()V

    goto :goto_0
.end method

.method private showSuccessfulTicketDialog()V
    .locals 4
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    .line 393
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureOnMainThread()V

    .line 395
    invoke-static {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isEnding(Landroid/app/Activity;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 412
    :goto_0
    return-void

    .line 399
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 400
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0803d9

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 401
    const v2, 0x7f0803da

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 402
    const v2, 0x7f0801ed

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 403
    new-instance v2, Lcom/navdy/client/app/ui/settings/ContactUsActivity$9;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity$9;-><init>(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 409
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 410
    .local v1, "dialog":Landroid/app/AlertDialog;
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->dialogs:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 411
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private startSubmitTicketService()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 903
    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->problemInput:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 905
    .local v3, "ticketDesc":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->hideProgressDialog()V

    .line 906
    iget-boolean v4, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->folderHasBeenCreatedForThisTicket:Z

    if-nez v4, :cond_1

    .line 907
    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->vin:Ljava/lang/String;

    invoke-direct {p0, v3, v4}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->createJSONFromTicketContents(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 908
    .local v2, "jsonObjectForTicketContents":Lorg/json/JSONObject;
    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->attachments:Ljava/util/ArrayList;

    invoke-static {v4, v2}, Lcom/navdy/client/app/framework/util/SupportTicketService;->createFolderForTicket(Ljava/util/ArrayList;Lorg/json/JSONObject;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 909
    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "creating folder for ticket failed"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 927
    .end local v2    # "jsonObjectForTicketContents":Lorg/json/JSONObject;
    :goto_0
    return-void

    .line 912
    .restart local v2    # "jsonObjectForTicketContents":Lorg/json/JSONObject;
    :cond_0
    iput-boolean v6, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->folderHasBeenCreatedForThisTicket:Z

    .line 914
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->createReadMeIfDoesNotExist()V

    .line 918
    .end local v2    # "jsonObjectForTicketContents":Lorg/json/JSONObject;
    :goto_1
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 919
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    const-class v4, Lcom/navdy/client/app/framework/util/SupportTicketService;

    invoke-direct {v1, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 920
    .local v1, "i":Landroid/content/Intent;
    const-string v4, "reset_retry_counter"

    invoke-virtual {v1, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 921
    const-string v4, "extra_email"

    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->email:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 922
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->currentTicketId:Ljava/lang/String;

    .line 923
    const-string v4, "random_id"

    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->currentTicketId:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 924
    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 925
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->hideSubmitButton()V

    .line 926
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->showSuccessfulTicketDialog()V

    goto :goto_0

    .line 916
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "i":Landroid/content/Intent;
    :cond_1
    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "folder for this ticket already exists"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private updateSubmitButton()V
    .locals 1

    .prologue
    .line 238
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->emailIsValid()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->messageIsValid()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->problemTypeIsValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 239
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->showSubmitButton()V

    .line 243
    :goto_0
    return-void

    .line 241
    :cond_0
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->disableSubmitButton()V

    goto :goto_0
.end method


# virtual methods
.method public attachPhotoToZendesk(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 559
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->showAttachPhotoDialog()V

    .line 560
    return-void
.end method

.method public incrementPhotoAttachments()V
    .locals 3

    .prologue
    .line 939
    iget v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->photoAttachmentCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->photoAttachmentCount:I

    .line 940
    iget v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->photoAttachmentCount:I

    const/4 v1, 0x5

    if-lt v0, v1, :cond_0

    .line 941
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->attachPhotoTextView:Landroid/widget/TextView;

    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0f0063

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 943
    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "responseIntent"    # Landroid/content/Intent;

    .prologue
    .line 564
    const/4 v5, -0x1

    if-ne p2, v5, :cond_0

    .line 566
    const/4 v5, 0x1

    if-ne p1, v5, :cond_1

    .line 567
    new-instance v5, Lcom/navdy/client/app/ui/settings/ContactUsActivity$13;

    invoke-direct {v5, p0, p3}, Lcom/navdy/client/app/ui/settings/ContactUsActivity$13;-><init>(Lcom/navdy/client/app/ui/settings/ContactUsActivity;Landroid/content/Intent;)V

    new-instance v6, Lcom/navdy/client/app/ui/settings/ContactUsActivity$14;

    invoke-direct {v6, p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity$14;-><init>(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V

    invoke-virtual {p0, v5, v6}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->requestStoragePermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    .line 697
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 698
    return-void

    .line 639
    :cond_1
    const/4 v5, 0x2

    if-ne p1, v5, :cond_4

    if-eqz p3, :cond_4

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 645
    :try_start_0
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    .line 647
    .local v4, "uri":Landroid/net/Uri;
    if-eqz v4, :cond_0

    .line 649
    new-instance v5, Lcom/navdy/client/app/ui/settings/ContactUsActivity$15;

    invoke-direct {v5, p0, v4}, Lcom/navdy/client/app/ui/settings/ContactUsActivity$15;-><init>(Lcom/navdy/client/app/ui/settings/ContactUsActivity;Landroid/net/Uri;)V

    const/16 v6, 0x500

    const/16 v7, 0x500

    invoke-static {v5, v6, v7}, Lcom/navdy/client/app/framework/util/ImageUtils;->getScaledBitmap(Lcom/navdy/client/app/framework/util/ImageUtils$StreamFactory;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 657
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 659
    invoke-static {}, Lcom/navdy/service/library/util/IOUtils;->getTempFilename()Ljava/lang/String;

    move-result-object v2

    .line 661
    .local v2, "fileName":Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/navdy/client/app/tracking/Tracker;->saveBitmapToInternalStorage(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 663
    new-instance v3, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 665
    .local v3, "galleryPhoto":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 671
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->attachments:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 672
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->showSuccessSnackBar()V

    .line 673
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->incrementPhotoAttachments()V

    .line 674
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->attachPhotoTextView:Landroid/widget/TextView;

    const v6, 0x7f0800c6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    .line 676
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->imageViewIterator:Ljava/util/Iterator;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->imageViewIterator:Ljava/util/Iterator;

    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 677
    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->addPhotoToGallery(Landroid/graphics/Bitmap;)V

    .line 684
    :cond_2
    :goto_1
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 688
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "fileName":Ljava/lang/String;
    .end local v3    # "galleryPhoto":Ljava/io/File;
    .end local v4    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v1

    .line 689
    .local v1, "e":Ljava/lang/Exception;
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get photo gallery image: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 690
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->showFailureSnackBar()V

    goto/16 :goto_0

    .line 680
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v2    # "fileName":Ljava/lang/String;
    .restart local v3    # "galleryPhoto":Ljava/io/File;
    .restart local v4    # "uri":Landroid/net/Uri;
    :cond_3
    :try_start_1
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "photo file is null"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 682
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->showFailureSnackBar()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 693
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "fileName":Ljava/lang/String;
    .end local v3    # "galleryPhoto":Ljava/io/File;
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_4
    const/4 v5, 0x3

    if-ne p1, v5, :cond_0

    .line 694
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->createTicket()V

    goto/16 :goto_0
.end method

.method public onAlwaysClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 444
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 445
    .local v0, "sharedPrefs":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "attach_logs"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 446
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->onOnceClick(Landroid/view/View;)V

    .line 447
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v10, 0x1090009

    const v9, 0x1090008

    const/4 v8, 0x0

    .line 123
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 124
    const v6, 0x7f0300f1

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->setContentView(I)V

    .line 127
    const v6, 0x7f1000a9

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/support/v7/widget/Toolbar;

    iput-object v6, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->myToolbar:Landroid/support/v7/widget/Toolbar;

    .line 128
    new-instance v6, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    invoke-direct {v6, p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    const v7, 0x7f08010e

    invoke-virtual {v6, v7}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    .line 131
    const v6, 0x7f100395

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    iput-object v6, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->emailAddress:Landroid/widget/EditText;

    .line 132
    const v6, 0x7f100397

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/navdy/client/app/ui/customviews/CustomSpinner;

    iput-object v6, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->problemTypes:Lcom/navdy/client/app/ui/customviews/CustomSpinner;

    .line 133
    const v6, 0x7f1003a1

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    iput-object v6, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->problemInput:Landroid/widget/EditText;

    .line 134
    const v6, 0x7f10039a

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->attachPhotoTextView:Landroid/widget/TextView;

    .line 136
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->setupGallery()V

    .line 139
    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->problemTypes:Lcom/navdy/client/app/ui/customviews/CustomSpinner;

    if-eqz v6, :cond_0

    .line 140
    const v6, 0x7f090009

    invoke-static {p0, v6, v9}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v3

    .line 143
    .local v3, "introAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    invoke-virtual {v3, v10}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 145
    const v6, 0x7f090008

    invoke-static {p0, v6, v9}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v4

    .line 148
    .local v4, "problemAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    invoke-virtual {v4, v10}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 150
    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->problemTypes:Lcom/navdy/client/app/ui/customviews/CustomSpinner;

    invoke-virtual {v6, v3}, Lcom/navdy/client/app/ui/customviews/CustomSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 152
    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->problemTypes:Lcom/navdy/client/app/ui/customviews/CustomSpinner;

    new-instance v7, Lcom/navdy/client/app/ui/settings/ContactUsActivity$3;

    invoke-direct {v7, p0, v4, v3}, Lcom/navdy/client/app/ui/settings/ContactUsActivity$3;-><init>(Lcom/navdy/client/app/ui/settings/ContactUsActivity;Landroid/widget/ArrayAdapter;Landroid/widget/ArrayAdapter;)V

    invoke-virtual {v6, v7}, Lcom/navdy/client/app/ui/customviews/CustomSpinner;->setListener(Lcom/navdy/client/app/ui/customviews/CustomSpinner$OnSpinnerStateChangedListener;)V

    .line 175
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 176
    .local v2, "intent":Landroid/content/Intent;
    if-eqz v2, :cond_0

    .line 177
    const-string v6, "default_problem_type"

    const/4 v7, -0x1

    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 178
    .local v1, "defaultProblemType":I
    if-lez v1, :cond_0

    .line 179
    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->problemTypes:Lcom/navdy/client/app/ui/customviews/CustomSpinner;

    invoke-virtual {v6, v4}, Lcom/navdy/client/app/ui/customviews/CustomSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 180
    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->problemTypes:Lcom/navdy/client/app/ui/customviews/CustomSpinner;

    invoke-virtual {v6, v1}, Lcom/navdy/client/app/ui/customviews/CustomSpinner;->setSelection(I)V

    .line 186
    .end local v1    # "defaultProblemType":I
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "introAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    .end local v4    # "problemAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 187
    .local v0, "customerPrefs":Landroid/content/SharedPreferences;
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v5

    .line 189
    .local v5, "sharedPrefs":Landroid/content/SharedPreferences;
    const-string v6, "attach_logs"

    invoke-interface {v5, v6, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    iput-boolean v6, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->userWantsLogs:Z

    .line 192
    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->emailAddress:Landroid/widget/EditText;

    if-eqz v6, :cond_2

    .line 193
    invoke-static {}, Lcom/navdy/client/app/framework/util/SupportTicketService;->getEmail()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->email:Ljava/lang/String;

    .line 194
    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->email:Ljava/lang/String;

    invoke-static {v6}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 195
    const-string v6, "email"

    const-string v7, ""

    invoke-interface {v0, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->email:Ljava/lang/String;

    .line 197
    :cond_1
    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->emailAddress:Landroid/widget/EditText;

    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->email:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 198
    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->emailAddress:Landroid/widget/EditText;

    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->email:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setSelection(I)V

    .line 199
    sget-object v6, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v6}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->isInitialized()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 200
    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->emailAddress:Landroid/widget/EditText;

    invoke-virtual {v6, v8}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 221
    :cond_2
    :goto_0
    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->problemInput:Landroid/widget/EditText;

    new-instance v7, Lcom/navdy/client/app/ui/settings/ContactUsActivity$5;

    invoke-direct {v7, p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity$5;-><init>(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 234
    new-instance v6, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;

    invoke-direct {v6}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;-><init>()V

    invoke-virtual {v6}, Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;->logConfiguration()V

    .line 235
    return-void

    .line 202
    :cond_3
    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->emailAddress:Landroid/widget/EditText;

    new-instance v7, Lcom/navdy/client/app/ui/settings/ContactUsActivity$4;

    invoke-direct {v7, p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity$4;-><init>(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    goto :goto_0
.end method

.method public onLogCollectionFailed()V
    .locals 2

    .prologue
    .line 893
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onLogCollectionFailed"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 894
    new-instance v0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$18;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity$18;-><init>(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 900
    return-void
.end method

.method public onLogCollectionSucceeded(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 870
    .local p1, "logAttachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onLogCollectionSucceeded: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 871
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->attachments:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    .line 872
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->attachments:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 874
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->attachments:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 875
    .local v0, "attachment":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "display_log"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 876
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->displayLogAttached:Z

    .line 880
    .end local v0    # "attachment":Ljava/io/File;
    :cond_1
    iget-boolean v1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->displayLogAttached:Z

    if-nez v1, :cond_2

    .line 881
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "display log was not found"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 884
    :cond_2
    new-instance v1, Lcom/navdy/client/app/ui/settings/ContactUsActivity$17;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity$17;-><init>(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 890
    return-void
.end method

.method public onNoLogsClick(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 439
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->dismissAllDialogs()V

    .line 440
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->startSubmitTicketService()V

    .line 441
    return-void
.end method

.method public onObdStatusResponse(Lcom/navdy/service/library/events/obd/ObdStatusResponse;)V
    .locals 2
    .param p1, "obdStatusResponse"    # Lcom/navdy/service/library/events/obd/ObdStatusResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 351
    iget-object v0, p1, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->vin:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->vin:Ljava/lang/String;

    :goto_0
    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->vin:Ljava/lang/String;

    .line 352
    return-void

    .line 351
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0804da

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public onOnceClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 432
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->dismissAllDialogs()V

    .line 433
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->userWantsLogs:Z

    .line 434
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->showProgressDialog()V

    .line 435
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/SupportTicketService;->collectLogs(Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;)V

    .line 436
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 266
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f1003ed

    if-ne v0, v1, :cond_0

    .line 268
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->createTicket()V

    .line 270
    :cond_0
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 846
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->dismissAllDialogs()V

    .line 848
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onPause()V

    .line 849
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 259
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->addSubmitButtonToMenu(Landroid/view/Menu;)V

    .line 260
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->disableSubmitButton()V

    .line 261
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onPrivacyPolicyClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 426
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/WebViewActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 427
    .local v0, "browserIntent":Landroid/content/Intent;
    const-string v1, "type"

    const-string v2, "Privacy"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 428
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->startActivity(Landroid/content/Intent;)V

    .line 429
    return-void
.end method

.method public onReceiveSubmitTicketFinishedEvent(Lcom/navdy/client/app/framework/util/SupportTicketService$SubmitTicketFinishedEvent;)V
    .locals 6
    .param p1, "submitTicketFinishedEvent"    # Lcom/navdy/client/app/framework/util/SupportTicketService$SubmitTicketFinishedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 831
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/util/SupportTicketService$SubmitTicketFinishedEvent;->getTicketId()Ljava/lang/String;

    move-result-object v0

    .line 832
    .local v0, "ticketId":Ljava/lang/String;
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->currentTicketId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 842
    :goto_0
    return-void

    .line 836
    :cond_0
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/util/SupportTicketService$SubmitTicketFinishedEvent;->getConditionsMet()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 837
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->dismissDialogsAndFinishRunnable:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 840
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->dismissDialogsAndFinishRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0xbb8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 342
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 343
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->tempPhotoFilename:Ljava/lang/String;

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 344
    const-string v0, "temp_photo_filename"

    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->tempPhotoFilename:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->tempPhotoFilename:Ljava/lang/String;

    .line 346
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 327
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onResume()V

    .line 329
    const-string v0, "Settings_Contact_Us"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 330
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 334
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 335
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->tempPhotoFilename:Ljava/lang/String;

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 336
    const-string v0, "temp_photo_filename"

    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->tempPhotoFilename:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    :cond_0
    return-void
.end method

.method public onSelectProblemInputClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 933
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->problemInput:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 934
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 935
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->toggleSoftInput(II)V

    .line 936
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1, "hasFocus"    # Z

    .prologue
    .line 755
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onWindowFocusChanged(Z)V

    .line 757
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->problemTypes:Lcom/navdy/client/app/ui/customviews/CustomSpinner;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->problemTypes:Lcom/navdy/client/app/ui/customviews/CustomSpinner;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/customviews/CustomSpinner;->hasBeenOpened()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 758
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->problemTypes:Lcom/navdy/client/app/ui/customviews/CustomSpinner;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/customviews/CustomSpinner;->spinnerClosed()V

    .line 760
    :cond_0
    return-void
.end method

.method public showSingleChoiceDialogWithMessage()V
    .locals 6

    .prologue
    .line 415
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f03004b

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 416
    .local v2, "dialogView":Landroid/view/View;
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 418
    .local v1, "dialog":Landroid/app/AlertDialog;
    const v3, 0x7f10013a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 419
    .local v0, "details":Landroid/widget/TextView;
    const v3, 0x7f0800ca

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->fromHtml(I)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 421
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->dialogs:Ljava/util/ArrayList;

    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-direct {v4, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 422
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 423
    return-void
.end method
