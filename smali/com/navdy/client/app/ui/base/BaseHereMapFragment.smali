.class public Lcom/navdy/client/app/ui/base/BaseHereMapFragment;
.super Lcom/here/android/mpa/mapping/MapFragment;
.source "BaseHereMapFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;,
        Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentAttached;,
        Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;,
        Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentReady;,
        Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;
    }
.end annotation


# static fields
.field private static final CHECK_DIMENSIONS_INTERVAL:I = 0x64

.field private static final DEFAULT_ORIENTATION:F = 0.0f

.field private static final DEFAULT_TILT:F = 0.0f

.field private static final DEFAULT_ZOOM:D = 14.0

.field private static final HERE_MAP_SCHEME:Ljava/lang/String; = "normal.day"

.field private static final HERE_MAP_THREAD_TAG:Ljava/lang/String; = "here-map-fragment-"

.field private static final POS_INDICATOR_Z_INDEX:I

.field private static final hereMapFragmentThreadCounter:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static final logger:Lcom/navdy/service/library/log/Logger;

.field private static markerImg:Lcom/here/android/mpa/common/Image;


# instance fields
.field private final attachCompleteListeners:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentAttached;",
            ">;"
        }
    .end annotation
.end field

.field private final backgroundThread:Landroid/os/Handler;

.field private final bus:Lcom/navdy/client/app/framework/util/BusProvider;

.field private final checkDimensions:Ljava/lang/Runnable;

.field private currentError:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;

.field private currentPositionMarker:Lcom/here/android/mpa/mapping/MapMarker;

.field private currentState:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;

.field private currentTransformCenter:Landroid/graphics/PointF;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private currentViewRect:Lcom/here/android/mpa/common/ViewRect;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private final fragmentInitListener:Lcom/here/android/mpa/common/OnEngineInitListener;

.field private hereMap:Lcom/here/android/mpa/mapping/Map;

.field private final initCompleteListeners:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;",
            ">;"
        }
    .end annotation
.end field

.field private isAttached:Z

.field private final navdyLocationListener:Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;

.field private final navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

.field private final readyCompleteListeners:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentReady;",
            ">;"
        }
    .end annotation
.end field

.field private final uiThread:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 51
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->logger:Lcom/navdy/service/library/log/Logger;

    .line 60
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->hereMapFragmentThreadCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 67
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0014

    .line 68
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->POS_INDICATOR_Z_INDEX:I

    .line 69
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->initMarkerImage()V

    .line 70
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 202
    invoke-direct {p0}, Lcom/here/android/mpa/mapping/MapFragment;-><init>()V

    .line 110
    new-instance v1, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$2;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$2;-><init>(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)V

    iput-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->checkDimensions:Ljava/lang/Runnable;

    .line 131
    new-instance v1, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;-><init>(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)V

    iput-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->fragmentInitListener:Lcom/here/android/mpa/common/OnEngineInitListener;

    .line 175
    new-instance v1, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4;-><init>(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)V

    iput-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->navdyLocationListener:Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;

    .line 203
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->uiThread:Landroid/os/Handler;

    .line 204
    new-instance v0, Landroid/os/HandlerThread;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "here-map-fragment-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->hereMapFragmentThreadCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 205
    .local v0, "background":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 206
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->backgroundThread:Landroid/os/Handler;

    .line 208
    invoke-static {}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getInstance()Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    .line 209
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->bus:Lcom/navdy/client/app/framework/util/BusProvider;

    .line 211
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->attachCompleteListeners:Ljava/util/Queue;

    .line 212
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->initCompleteListeners:Ljava/util/Queue;

    .line 213
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->readyCompleteListeners:Ljava/util/Queue;

    .line 215
    sget-object v1, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;->INITIALIZING:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;

    iput-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->currentState:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;

    .line 216
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->currentError:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;

    .line 217
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->isAttached:Z

    .line 218
    return-void
.end method

.method static synthetic access$000()Lcom/here/android/mpa/common/Image;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->markerImg:Lcom/here/android/mpa/common/Image;

    return-object v0
.end method

.method static synthetic access$002(Lcom/here/android/mpa/common/Image;)Lcom/here/android/mpa/common/Image;
    .locals 0
    .param p0, "x0"    # Lcom/here/android/mpa/common/Image;

    .prologue
    .line 49
    sput-object p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->markerImg:Lcom/here/android/mpa/common/Image;

    return-object p0
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->checkDimensions:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Lcom/here/android/mpa/mapping/MapMarker;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->currentPositionMarker:Lcom/here/android/mpa/mapping/MapMarker;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/here/android/mpa/mapping/MapMarker;)Lcom/here/android/mpa/mapping/MapMarker;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;
    .param p1, "x1"    # Lcom/here/android/mpa/mapping/MapMarker;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->currentPositionMarker:Lcom/here/android/mpa/mapping/MapMarker;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/navdy/service/library/events/location/Coordinate;)Lcom/here/android/mpa/mapping/MapMarker;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;
    .param p1, "x1"    # Lcom/navdy/service/library/events/location/Coordinate;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->initPositionMarker(Lcom/navdy/service/library/events/location/Coordinate;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->setupTransformCenter()V

    return-void
.end method

.method static synthetic access$1400(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Lcom/navdy/client/app/framework/location/NavdyLocationManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;
    .param p1, "x1"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p2, "x2"    # Lcom/here/android/mpa/mapping/Map$Animation;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->move(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;
    .param p1, "x1"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p2, "x2"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p3, "x3"    # Lcom/here/android/mpa/mapping/Map$Animation;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->centerOnTwoPoints(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Lcom/here/android/mpa/common/ViewRect;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->currentViewRect:Lcom/here/android/mpa/common/ViewRect;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/here/android/mpa/common/ViewRect;)Lcom/here/android/mpa/common/ViewRect;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;
    .param p1, "x1"    # Lcom/here/android/mpa/common/ViewRect;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->currentViewRect:Lcom/here/android/mpa/common/ViewRect;

    return-object p1
.end method

.method static synthetic access$1802(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Landroid/graphics/PointF;)Landroid/graphics/PointF;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;
    .param p1, "x1"    # Landroid/graphics/PointF;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->currentTransformCenter:Landroid/graphics/PointF;

    return-object p1
.end method

.method static synthetic access$1900(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Lcom/here/android/mpa/common/OnEngineInitListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->fragmentInitListener:Lcom/here/android/mpa/common/OnEngineInitListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Lcom/here/android/mpa/mapping/Map;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->hereMap:Lcom/here/android/mpa/mapping/Map;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Ljava/util/Queue;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->attachCompleteListeners:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic access$202(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/here/android/mpa/mapping/Map;)Lcom/here/android/mpa/mapping/Map;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;
    .param p1, "x1"    # Lcom/here/android/mpa/mapping/Map;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->hereMap:Lcom/here/android/mpa/mapping/Map;

    return-object p1
.end method

.method static synthetic access$300(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->currentState:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;

    return-object v0
.end method

.method static synthetic access$302(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;)Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;
    .param p1, "x1"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->currentState:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;

    return-object p1
.end method

.method static synthetic access$400(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Ljava/util/Queue;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->readyCompleteListeners:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->backgroundThread:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->currentError:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;

    return-object v0
.end method

.method static synthetic access$602(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;)Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;
    .param p1, "x1"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->currentError:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;

    return-object p1
.end method

.method static synthetic access$700(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;
    .param p1, "x1"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->callOnErrors(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;)V

    return-void
.end method

.method static synthetic access$800(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->initMap()V

    return-void
.end method

.method static synthetic access$900(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Ljava/util/Queue;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->initCompleteListeners:Ljava/util/Queue;

    return-object v0
.end method

.method private addPositionIndicator()V
    .locals 3

    .prologue
    .line 618
    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->currentPositionMarker:Lcom/here/android/mpa/mapping/MapMarker;

    if-eqz v1, :cond_1

    .line 627
    :cond_0
    :goto_0
    return-void

    .line 621
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getPhoneCoordinates()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v0

    .line 622
    .local v0, "phoneCoords":Lcom/navdy/service/library/events/location/Coordinate;
    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->initPositionMarker(Lcom/navdy/service/library/events/location/Coordinate;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->currentPositionMarker:Lcom/here/android/mpa/mapping/MapMarker;

    .line 624
    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->currentPositionMarker:Lcom/here/android/mpa/mapping/MapMarker;

    if-eqz v1, :cond_0

    .line 625
    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->hereMap:Lcom/here/android/mpa/mapping/Map;

    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->currentPositionMarker:Lcom/here/android/mpa/mapping/MapMarker;

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/mapping/Map;->addMapObject(Lcom/here/android/mpa/mapping/MapObject;)Z

    goto :goto_0
.end method

.method private callOnErrors(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;)V
    .locals 1
    .param p1, "error"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;

    .prologue
    .line 608
    :goto_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->initCompleteListeners:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 609
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->initCompleteListeners:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;

    invoke-interface {v0, p1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;->onError(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;)V

    goto :goto_0

    .line 611
    :cond_0
    return-void
.end method

.method private centerOnTwoPoints(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;)V
    .locals 28
    .param p1, "startGeoCoordinates"    # Lcom/here/android/mpa/common/GeoCoordinate;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "endGeoCoordinates"    # Lcom/here/android/mpa/common/GeoCoordinate;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "animation"    # Lcom/here/android/mpa/mapping/Map$Animation;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 553
    invoke-direct/range {p0 .. p1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->isValid(Lcom/here/android/mpa/common/GeoCoordinate;)Z

    move-result v24

    if-eqz v24, :cond_0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->isValid(Lcom/here/android/mpa/common/GeoCoordinate;)Z

    move-result v24

    if-nez v24, :cond_2

    .line 554
    :cond_0
    sget-object v24, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "called centerOnTwoPoints and coordinates are invalid, no-op. startGeoCoordinates="

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    .line 555
    invoke-virtual/range {p1 .. p1}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v26

    invoke-virtual/range {v25 .. v27}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ","

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {p1 .. p1}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v26

    invoke-virtual/range {v25 .. v27}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "; endGeoCoordinates="

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    .line 556
    invoke-virtual/range {p2 .. p2}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v26

    invoke-virtual/range {v25 .. v27}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ","

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    .line 557
    invoke-virtual/range {p2 .. p2}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v26

    invoke-virtual/range {v25 .. v27}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    .line 554
    invoke-virtual/range {v24 .. v25}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 590
    :cond_1
    :goto_0
    return-void

    .line 561
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v20

    .line 562
    .local v20, "startLat":D
    invoke-virtual/range {p1 .. p1}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v22

    .line 564
    .local v22, "startLng":D
    invoke-virtual/range {p2 .. p2}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v6

    .line 565
    .local v6, "endLat":D
    invoke-virtual/range {p2 .. p2}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v8

    .line 567
    .local v8, "endLng":D
    cmpl-double v24, v20, v6

    if-lez v24, :cond_3

    move-wide/from16 v12, v20

    .line 568
    .local v12, "maxLat":D
    :goto_1
    cmpl-double v24, v22, v8

    if-lez v24, :cond_4

    move-wide/from16 v14, v22

    .line 570
    .local v14, "maxLng":D
    :goto_2
    cmpg-double v24, v20, v6

    if-gez v24, :cond_5

    move-wide/from16 v16, v20

    .line 571
    .local v16, "minLat":D
    :goto_3
    cmpg-double v24, v22, v8

    if-gez v24, :cond_6

    move-wide/from16 v18, v22

    .line 574
    .local v18, "minLng":D
    :goto_4
    new-instance v11, Lcom/here/android/mpa/common/GeoCoordinate;

    move-wide/from16 v0, v18

    invoke-direct {v11, v12, v13, v0, v1}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    .line 575
    .local v11, "topLeft":Lcom/here/android/mpa/common/GeoCoordinate;
    new-instance v5, Lcom/here/android/mpa/common/GeoCoordinate;

    move-wide/from16 v0, v16

    invoke-direct {v5, v0, v1, v14, v15}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    .line 576
    .local v5, "bottomRight":Lcom/here/android/mpa/common/GeoCoordinate;
    new-instance v10, Lcom/here/android/mpa/common/GeoBoundingBox;

    invoke-direct {v10, v11, v5}, Lcom/here/android/mpa/common/GeoBoundingBox;-><init>(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;)V

    .line 578
    .local v10, "geoBoundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    if-eqz p3, :cond_7

    move-object/from16 v4, p3

    .line 583
    .local v4, "appliedAnimation":Lcom/here/android/mpa/mapping/Map$Animation;
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->hereMap:Lcom/here/android/mpa/mapping/Map;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/here/android/mpa/mapping/Map;->getHeight()I

    move-result v24

    if-lez v24, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->hereMap:Lcom/here/android/mpa/mapping/Map;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/here/android/mpa/mapping/Map;->getWidth()I

    move-result v24

    if-lez v24, :cond_1

    .line 584
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->currentViewRect:Lcom/here/android/mpa/common/ViewRect;

    move-object/from16 v24, v0

    if-eqz v24, :cond_8

    .line 585
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->hereMap:Lcom/here/android/mpa/mapping/Map;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->currentViewRect:Lcom/here/android/mpa/common/ViewRect;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, p3

    move/from16 v3, v26

    invoke-virtual {v0, v10, v1, v2, v3}, Lcom/here/android/mpa/mapping/Map;->zoomTo(Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/common/ViewRect;Lcom/here/android/mpa/mapping/Map$Animation;F)V

    goto :goto_0

    .end local v4    # "appliedAnimation":Lcom/here/android/mpa/mapping/Map$Animation;
    .end local v5    # "bottomRight":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v10    # "geoBoundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    .end local v11    # "topLeft":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v12    # "maxLat":D
    .end local v14    # "maxLng":D
    .end local v16    # "minLat":D
    .end local v18    # "minLng":D
    :cond_3
    move-wide v12, v6

    .line 567
    goto :goto_1

    .restart local v12    # "maxLat":D
    :cond_4
    move-wide v14, v8

    .line 568
    goto :goto_2

    .restart local v14    # "maxLng":D
    :cond_5
    move-wide/from16 v16, v6

    .line 570
    goto :goto_3

    .restart local v16    # "minLat":D
    :cond_6
    move-wide/from16 v18, v8

    .line 571
    goto :goto_4

    .line 578
    .restart local v5    # "bottomRight":Lcom/here/android/mpa/common/GeoCoordinate;
    .restart local v10    # "geoBoundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    .restart local v11    # "topLeft":Lcom/here/android/mpa/common/GeoCoordinate;
    .restart local v18    # "minLng":D
    :cond_7
    sget-object v4, Lcom/here/android/mpa/mapping/Map$Animation;->NONE:Lcom/here/android/mpa/mapping/Map$Animation;

    goto :goto_5

    .line 587
    .restart local v4    # "appliedAnimation":Lcom/here/android/mpa/mapping/Map$Animation;
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->hereMap:Lcom/here/android/mpa/mapping/Map;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v0, v10, v4, v1}, Lcom/here/android/mpa/mapping/Map;->zoomTo(Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/mapping/Map$Animation;F)V

    goto/16 :goto_0
.end method

.method private initMap()V
    .locals 3

    .prologue
    .line 598
    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->hereMap:Lcom/here/android/mpa/mapping/Map;

    const-string v2, "normal.day"

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/mapping/Map;->setMapScheme(Ljava/lang/String;)Lcom/here/android/mpa/mapping/Map;

    .line 600
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->centerOnUserLocation()V

    .line 601
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->addPositionIndicator()V

    .line 603
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->isLimitingCellularData()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 604
    .local v0, "shouldHaveTraffic":Z
    :goto_0
    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->toggleTraffic(Z)V

    .line 605
    return-void

    .line 603
    .end local v0    # "shouldHaveTraffic":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static initMarkerImage()V
    .locals 2

    .prologue
    .line 73
    invoke-static {}, Lcom/navdy/client/app/framework/map/HereMapsManager;->getInstance()Lcom/navdy/client/app/framework/map/HereMapsManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$1;

    invoke-direct {v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$1;-><init>()V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/map/HereMapsManager;->addOnInitializedListener(Lcom/here/android/mpa/common/OnEngineInitListener;)V

    .line 84
    return-void
.end method

.method private initPositionMarker(Lcom/navdy/service/library/events/location/Coordinate;)Lcom/here/android/mpa/mapping/MapMarker;
    .locals 6
    .param p1, "phoneLocation"    # Lcom/navdy/service/library/events/location/Coordinate;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 647
    if-nez p1, :cond_0

    .line 662
    :goto_0
    return-object v0

    .line 651
    :cond_0
    sget-object v1, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->markerImg:Lcom/here/android/mpa/common/Image;

    if-nez v1, :cond_1

    .line 652
    sget-object v1, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "markerImg is null and it shouldn\'t be!"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 656
    :cond_1
    new-instance v0, Lcom/here/android/mpa/mapping/MapMarker;

    new-instance v1, Lcom/here/android/mpa/common/GeoCoordinate;

    iget-object v2, p1, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    .line 657
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iget-object v4, p1, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    sget-object v2, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->markerImg:Lcom/here/android/mpa/common/Image;

    invoke-direct {v0, v1, v2}, Lcom/here/android/mpa/mapping/MapMarker;-><init>(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/Image;)V

    .line 660
    .local v0, "positionMarker":Lcom/here/android/mpa/mapping/MapMarker;
    sget v1, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->POS_INDICATOR_Z_INDEX:I

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/mapping/MapMarker;->setZIndex(I)Lcom/here/android/mpa/mapping/MapObject;

    goto :goto_0
.end method

.method private isValid(Lcom/here/android/mpa/common/GeoCoordinate;)Z
    .locals 4
    .param p1, "geoCoordinate"    # Lcom/here/android/mpa/common/GeoCoordinate;

    .prologue
    const-wide/16 v2, 0x0

    .line 630
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private move(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;)V
    .locals 8
    .param p1, "geo"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p2, "animation"    # Lcom/here/android/mpa/mapping/Map$Animation;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x0

    .line 593
    if-eqz p2, :cond_0

    .line 594
    :goto_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->hereMap:Lcom/here/android/mpa/mapping/Map;

    const-wide/high16 v4, 0x402c000000000000L    # 14.0

    move-object v2, p1

    move-object v3, p2

    move v7, v6

    invoke-virtual/range {v1 .. v7}, Lcom/here/android/mpa/mapping/Map;->setCenter(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V

    .line 595
    return-void

    .line 593
    :cond_0
    sget-object p2, Lcom/here/android/mpa/mapping/Map$Animation;->NONE:Lcom/here/android/mpa/mapping/Map$Animation;

    goto :goto_0
.end method

.method private setupTransformCenter()V
    .locals 2

    .prologue
    .line 666
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->hereMap:Lcom/here/android/mpa/mapping/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->hereMap:Lcom/here/android/mpa/mapping/Map;

    invoke-virtual {v0}, Lcom/here/android/mpa/mapping/Map;->getTransformCenter()Landroid/graphics/PointF;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->currentTransformCenter:Landroid/graphics/PointF;

    invoke-virtual {v0, v1}, Landroid/graphics/PointF;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 667
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->hereMap:Lcom/here/android/mpa/mapping/Map;

    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->currentTransformCenter:Landroid/graphics/PointF;

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/mapping/Map;->setTransformCenter(Landroid/graphics/PointF;)Lcom/here/android/mpa/mapping/Map;

    .line 669
    :cond_0
    return-void
.end method

.method private toggleTraffic(Z)V
    .locals 1
    .param p1, "active"    # Z

    .prologue
    .line 634
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$16;

    invoke-direct {v0, p0, p1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$16;-><init>(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Z)V

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->whenInitialized(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;)V

    .line 643
    return-void
.end method

.method private whenAttached(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentAttached;)V
    .locals 2
    .param p1, "attachListener"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentAttached;

    .prologue
    .line 530
    if-nez p1, :cond_0

    .line 531
    sget-object v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "tried to add a null attachListener"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 548
    :goto_0
    return-void

    .line 535
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->uiThread:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$15;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$15;-><init>(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentAttached;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method


# virtual methods
.method public centerOnRoute(Lcom/here/android/mpa/common/GeoPolyline;)V
    .locals 1
    .param p1, "route"    # Lcom/here/android/mpa/common/GeoPolyline;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 385
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->centerOnRoute(Lcom/here/android/mpa/common/GeoPolyline;Lcom/here/android/mpa/mapping/Map$Animation;)V

    .line 386
    return-void
.end method

.method public centerOnRoute(Lcom/here/android/mpa/common/GeoPolyline;Lcom/here/android/mpa/mapping/Map$Animation;)V
    .locals 1
    .param p1, "route"    # Lcom/here/android/mpa/common/GeoPolyline;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "animation"    # Lcom/here/android/mpa/mapping/Map$Animation;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 389
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$11;

    invoke-direct {v0, p0, p1, p2}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$11;-><init>(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/here/android/mpa/common/GeoPolyline;Lcom/here/android/mpa/mapping/Map$Animation;)V

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->whenReady(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentReady;)V

    .line 410
    return-void
.end method

.method public centerOnUserLocation()V
    .locals 1

    .prologue
    .line 325
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->centerOnUserLocation(Lcom/here/android/mpa/mapping/Map$Animation;)V

    .line 326
    return-void
.end method

.method public centerOnUserLocation(Lcom/here/android/mpa/mapping/Map$Animation;)V
    .locals 1
    .param p1, "animation"    # Lcom/here/android/mpa/mapping/Map$Animation;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 329
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$9;

    invoke-direct {v0, p0, p1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$9;-><init>(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/here/android/mpa/mapping/Map$Animation;)V

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->whenInitialized(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;)V

    .line 352
    return-void
.end method

.method public centerOnUserLocationAndDestination(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 1
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 355
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->centerOnUserLocationAndDestination(Lcom/navdy/client/app/framework/models/Destination;Lcom/here/android/mpa/mapping/Map$Animation;)V

    .line 356
    return-void
.end method

.method public centerOnUserLocationAndDestination(Lcom/navdy/client/app/framework/models/Destination;Lcom/here/android/mpa/mapping/Map$Animation;)V
    .locals 1
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "animation"    # Lcom/here/android/mpa/mapping/Map$Animation;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 359
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$10;

    invoke-direct {v0, p0, p1, p2}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$10;-><init>(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/navdy/client/app/framework/models/Destination;Lcom/here/android/mpa/mapping/Map$Animation;)V

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->whenReady(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentReady;)V

    .line 382
    return-void
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 311
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$8;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$8;-><init>(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)V

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->whenAttached(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentAttached;)V

    .line 322
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 482
    invoke-super {p0, p1}, Lcom/here/android/mpa/mapping/MapFragment;->onAttach(Landroid/content/Context;)V

    .line 483
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->isAttached:Z

    .line 484
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->bus:Lcom/navdy/client/app/framework/util/BusProvider;

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 485
    sget-object v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onAttach, attachCompleteListeners executing"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 487
    :goto_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->attachCompleteListeners:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 488
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->attachCompleteListeners:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentAttached;

    invoke-interface {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentAttached;->onAttached()V

    goto :goto_0

    .line 490
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "layoutInflater"    # Landroid/view/LayoutInflater;
    .param p2, "viewGroup"    # Landroid/view/ViewGroup;
    .param p3, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 494
    invoke-static {}, Lcom/navdy/client/app/framework/map/HereMapsManager;->getInstance()Lcom/navdy/client/app/framework/map/HereMapsManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$14;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$14;-><init>(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/map/HereMapsManager;->addOnInitializedListener(Lcom/here/android/mpa/common/OnEngineInitListener;)V

    .line 505
    invoke-super {p0, p1, p2, p3}, Lcom/here/android/mpa/mapping/MapFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 522
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->isAttached:Z

    if-eqz v0, :cond_0

    .line 523
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->bus:Lcom/navdy/client/app/framework/util/BusProvider;

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->unregister(Ljava/lang/Object;)V

    .line 525
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->isAttached:Z

    .line 526
    invoke-super {p0}, Lcom/here/android/mpa/mapping/MapFragment;->onDetach()V

    .line 527
    return-void
.end method

.method public onLimitCellDataEvent(Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity$LimitCellDataEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity$LimitCellDataEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 476
    iget-boolean v1, p1, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity$LimitCellDataEvent;->hasLimitedCellData:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 477
    .local v0, "shouldHaveTraffic":Z
    :goto_0
    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->toggleTraffic(Z)V

    .line 478
    return-void

    .line 476
    .end local v0    # "shouldHaveTraffic":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 516
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->navdyLocationListener:Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->removeListener(Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;)V

    .line 517
    invoke-super {p0}, Lcom/here/android/mpa/mapping/MapFragment;->onPause()V

    .line 518
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 510
    invoke-super {p0}, Lcom/here/android/mpa/mapping/MapFragment;->onResume()V

    .line 511
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->navdyLocationListener:Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->addListener(Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;)V

    .line 512
    return-void
.end method

.method public setMapGesture(Lcom/here/android/mpa/mapping/MapGesture$OnGestureListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/here/android/mpa/mapping/MapGesture$OnGestureListener;

    .prologue
    .line 455
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$13;

    invoke-direct {v0, p0, p1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$13;-><init>(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/here/android/mpa/mapping/MapGesture$OnGestureListener;)V

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->whenInitialized(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;)V

    .line 472
    return-void
.end method

.method public setUsableArea(IIII)V
    .locals 6
    .param p1, "paddingTop"    # I
    .param p2, "paddingRight"    # I
    .param p3, "paddingBottom"    # I
    .param p4, "paddingLeft"    # I

    .prologue
    .line 413
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$12;

    move-object v1, p0

    move v2, p1

    move v3, p3

    move v4, p4

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$12;-><init>(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;IIII)V

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->whenReady(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentReady;)V

    .line 452
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 294
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$7;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$7;-><init>(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)V

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->whenAttached(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentAttached;)V

    .line 308
    return-void
.end method

.method public whenInitialized(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;)V
    .locals 2
    .param p1, "initListener"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;

    .prologue
    .line 231
    if-nez p1, :cond_0

    .line 232
    sget-object v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "tried to add a null initListener"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 255
    :goto_0
    return-void

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->backgroundThread:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$5;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$5;-><init>(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public whenReady(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentReady;)V
    .locals 2
    .param p1, "readyListener"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentReady;

    .prologue
    .line 266
    if-nez p1, :cond_0

    .line 267
    sget-object v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "tried to add a null readyListener"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 291
    :goto_0
    return-void

    .line 272
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->backgroundThread:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$6;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$6;-><init>(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentReady;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
