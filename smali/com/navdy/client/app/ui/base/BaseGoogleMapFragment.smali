.class public Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;
.super Lcom/google/android/gms/maps/MapFragment;
.source "BaseGoogleMapFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnGoogleMapFragmentReady;,
        Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnGoogleMapFragmentAttached;,
        Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnShowMapListener;
    }
.end annotation


# static fields
.field private static final AUTO_CENTER_DEFAULT:Z = false

.field private static final CAR_LOCATION_ENABLED_DEFAULT:Z = true

.field private static final CENTER_MAP_ON_LAST_KNOWN_LOCATION_ENABLED_DEFAULT:Z = true

.field private static final CENTER_MAP_ZOOM:F = 16.0f

.field public static final KEEP_ZOOM:F = -1.0f

.field private static final MAP_TOOLBAR_ENABLED_DEFAULT:Z = false

.field private static final MIN_DISTANCE_CENTER_MAP_BOTH:D = 50.0

.field private static final MY_LOCATION_BUTTON_ENABLED_DEFAULT:Z = false

.field private static final MY_LOCATION_ENABLED_DEFAULT:Z = true

.field private static final SCROLL_GESTURES_ENABLED_DEFAULT:Z = true

.field private static final TRAFFIC_ENABLED_DEFAULT:Z = true

.field public static final ZOOM_DEFAULT:F = 14.0f

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final attachCompleteListeners:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnGoogleMapFragmentAttached;",
            ">;"
        }
    .end annotation
.end field

.field private autoCenter:Z

.field private final bus:Lcom/navdy/client/app/framework/util/BusProvider;

.field private carLocationEnabled:Z

.field private carMarker:Lcom/google/android/gms/maps/model/Marker;

.field private centerMapOnLastKnownLocation:Z

.field isAttached:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mapPaddingBottom:I

.field private mapPaddingLeft:I

.field private mapPaddingRight:I

.field private mapPaddingTop:I

.field private mapToolbarEnabled:Z

.field private myLocationButtonEnabled:Z

.field private myLocationEnabled:Z

.field private final navdyLocationListener:Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;

.field private final navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

.field private final readyCompleteListeners:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnGoogleMapFragmentReady;",
            ">;"
        }
    .end annotation
.end field

.field private scrollGesturesEnabled:Z

.field private trafficEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 54
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->logger:Lcom/navdy/service/library/log/Logger;

    .line 79
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/maps/MapsInitializer;->initialize(Landroid/content/Context;)I

    .line 80
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 130
    invoke-direct {p0}, Lcom/google/android/gms/maps/MapFragment;-><init>()V

    .line 104
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->isAttached:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 106
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$1;-><init>(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->navdyLocationListener:Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;

    .line 131
    invoke-static {}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getInstance()Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    .line 132
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->bus:Lcom/navdy/client/app/framework/util/BusProvider;

    .line 133
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->attachCompleteListeners:Ljava/util/Queue;

    .line 134
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->readyCompleteListeners:Ljava/util/Queue;

    .line 135
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->autoCenter:Z

    return v0
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->scrollGesturesEnabled:Z

    return v0
.end method

.method static synthetic access$1100(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->myLocationButtonEnabled:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->mapToolbarEnabled:Z

    return v0
.end method

.method static synthetic access$1300(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->trafficEnabled:Z

    return v0
.end method

.method static synthetic access$1400(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->myLocationEnabled:Z

    return v0
.end method

.method static synthetic access$1500(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)Lcom/google/android/gms/maps/model/Marker;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->carMarker:Lcom/google/android/gms/maps/model/Marker;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;Lcom/google/android/gms/maps/model/Marker;)Lcom/google/android/gms/maps/model/Marker;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;
    .param p1, "x1"    # Lcom/google/android/gms/maps/model/Marker;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->carMarker:Lcom/google/android/gms/maps/model/Marker;

    return-object p1
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;Lcom/navdy/service/library/events/location/Coordinate;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;
    .param p1, "x1"    # Lcom/navdy/service/library/events/location/Coordinate;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->updateCarMarker(Lcom/navdy/service/library/events/location/Coordinate;)V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->centerMapOnLastKnownLocation:Z

    return v0
.end method

.method static synthetic access$400(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->fadeIn()V

    return-void
.end method

.method static synthetic access$500(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .prologue
    .line 52
    iget v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->mapPaddingRight:I

    return v0
.end method

.method static synthetic access$502(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;
    .param p1, "x1"    # I

    .prologue
    .line 52
    iput p1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->mapPaddingRight:I

    return p1
.end method

.method static synthetic access$600(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .prologue
    .line 52
    iget v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->mapPaddingLeft:I

    return v0
.end method

.method static synthetic access$602(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;
    .param p1, "x1"    # I

    .prologue
    .line 52
    iput p1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->mapPaddingLeft:I

    return p1
.end method

.method static synthetic access$700(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .prologue
    .line 52
    iget v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->mapPaddingTop:I

    return v0
.end method

.method static synthetic access$702(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;
    .param p1, "x1"    # I

    .prologue
    .line 52
    iput p1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->mapPaddingTop:I

    return p1
.end method

.method static synthetic access$800(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .prologue
    .line 52
    iget v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->mapPaddingBottom:I

    return v0
.end method

.method static synthetic access$802(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;
    .param p1, "x1"    # I

    .prologue
    .line 52
    iput p1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->mapPaddingBottom:I

    return p1
.end method

.method static synthetic access$900(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;Lcom/google/android/gms/maps/CameraUpdate;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;
    .param p1, "x1"    # Lcom/google/android/gms/maps/CameraUpdate;
    .param p2, "x2"    # Z

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->moveMapInternal(Lcom/google/android/gms/maps/CameraUpdate;Z)V

    return-void
.end method

.method private buildCameraUpdate(Lcom/google/android/gms/maps/model/LatLng;F)Lcom/google/android/gms/maps/CameraUpdate;
    .locals 2
    .param p1, "latLng"    # Lcom/google/android/gms/maps/model/LatLng;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "zoom"    # F

    .prologue
    .line 594
    new-instance v1, Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    invoke-direct {v1}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;-><init>()V

    .line 595
    invoke-virtual {v1, p1}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->target(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    move-result-object v0

    .line 597
    .local v0, "builder":Lcom/google/android/gms/maps/model/CameraPosition$Builder;
    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v1, p2, v1

    if-eqz v1, :cond_0

    .line 599
    invoke-virtual {v0, p2}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->zoom(F)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    .line 602
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->build()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newCameraPosition(Lcom/google/android/gms/maps/model/CameraPosition;)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v1

    return-object v1
.end method

.method private centerMap(ZLcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;)V
    .locals 7
    .param p1, "animate"    # Z
    .param p2, "phoneLocation"    # Lcom/navdy/service/library/events/location/Coordinate;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "carLocation"    # Lcom/navdy/service/library/events/location/Coordinate;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/high16 v6, 0x41800000    # 16.0f

    .line 511
    invoke-static {p2}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(Lcom/navdy/service/library/events/location/Coordinate;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p3}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(Lcom/navdy/service/library/events/location/Coordinate;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 512
    invoke-static {p2, p3}, Lcom/navdy/client/app/framework/map/MapUtils;->distanceBetween(Lcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;)D

    move-result-wide v2

    const-wide/high16 v4, 0x4049000000000000L    # 50.0

    cmpl-double v1, v2, v4

    if-lez v1, :cond_1

    .line 513
    new-instance v0, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;-><init>()V

    .line 515
    .local v0, "builder":Lcom/google/android/gms/maps/model/LatLngBounds$Builder;
    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v2, p2, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iget-object v4, p2, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->include(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    .line 516
    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v2, p3, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iget-object v4, p3, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->include(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    .line 518
    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->build()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v1

    invoke-virtual {p0, v1, p1}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->zoomTo(Lcom/google/android/gms/maps/model/LatLngBounds;Z)V

    .line 527
    .end local v0    # "builder":Lcom/google/android/gms/maps/model/LatLngBounds$Builder;
    :cond_0
    :goto_0
    return-void

    .line 520
    :cond_1
    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v2, p2, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iget-object v4, p2, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {p0, v1, v6, p1}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->moveMap(Lcom/google/android/gms/maps/model/LatLng;FZ)V

    goto :goto_0

    .line 522
    :cond_2
    invoke-static {p2}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(Lcom/navdy/service/library/events/location/Coordinate;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 523
    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v2, p2, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iget-object v4, p2, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {p0, v1, v6, p1}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->moveMap(Lcom/google/android/gms/maps/model/LatLng;FZ)V

    goto :goto_0

    .line 524
    :cond_3
    invoke-static {p3}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(Lcom/navdy/service/library/events/location/Coordinate;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 525
    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v2, p3, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iget-object v4, p3, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {p0, v1, v6, p1}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->moveMap(Lcom/google/android/gms/maps/model/LatLng;FZ)V

    goto :goto_0
.end method

.method private fadeIn()V
    .locals 1

    .prologue
    .line 496
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$8;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$8;-><init>(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)V

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->whenAttached(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnGoogleMapFragmentAttached;)V

    .line 508
    return-void
.end method

.method private getLatLongBoundsSafely(Lcom/google/android/gms/maps/GoogleMap;)Lcom/google/android/gms/maps/model/LatLngBounds;
    .locals 4
    .param p1, "googleMap"    # Lcom/google/android/gms/maps/GoogleMap;

    .prologue
    .line 586
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/maps/GoogleMap;->getProjection()Lcom/google/android/gms/maps/Projection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/maps/Projection;->getVisibleRegion()Lcom/google/android/gms/maps/model/VisibleRegion;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/maps/model/VisibleRegion;->latLngBounds:Lcom/google/android/gms/maps/model/LatLngBounds;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 589
    :goto_0
    return-object v1

    .line 587
    :catch_0
    move-exception v0

    .line 588
    .local v0, "e":Ljava/lang/RuntimeException;
    sget-object v1, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to get lat long bounds because: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 589
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getTargetSafely(Lcom/google/android/gms/maps/GoogleMap;)Lcom/google/android/gms/maps/model/LatLng;
    .locals 4
    .param p1, "googleMap"    # Lcom/google/android/gms/maps/GoogleMap;

    .prologue
    .line 577
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/maps/GoogleMap;->getCameraPosition()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/maps/model/CameraPosition;->target:Lcom/google/android/gms/maps/model/LatLng;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 580
    :goto_0
    return-object v1

    .line 578
    :catch_0
    move-exception v0

    .line 579
    .local v0, "e":Ljava/lang/RuntimeException;
    sget-object v1, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to get camera target because: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 580
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private moveMapInternal(Lcom/google/android/gms/maps/CameraUpdate;Z)V
    .locals 1
    .param p1, "cameraUpdate"    # Lcom/google/android/gms/maps/CameraUpdate;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "animate"    # Z

    .prologue
    .line 530
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$9;

    invoke-direct {v0, p0, p2, p1}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$9;-><init>(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;ZLcom/google/android/gms/maps/CameraUpdate;)V

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->getMapAsync(Lcom/google/android/gms/maps/OnMapReadyCallback;)V

    .line 573
    return-void
.end method

.method private updateCarMarker(Lcom/navdy/service/library/events/location/Coordinate;)V
    .locals 1
    .param p1, "carLocation"    # Lcom/navdy/service/library/events/location/Coordinate;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 606
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->carLocationEnabled:Z

    if-eqz v0, :cond_0

    .line 607
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$10;

    invoke-direct {v0, p0, p1}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$10;-><init>(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;Lcom/navdy/service/library/events/location/Coordinate;)V

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->getMapAsync(Lcom/google/android/gms/maps/OnMapReadyCallback;)V

    .line 630
    :cond_0
    return-void
.end method

.method private whenAttached(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnGoogleMapFragmentAttached;)V
    .locals 2
    .param p1, "attachListener"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnGoogleMapFragmentAttached;
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    .line 461
    if-nez p1, :cond_0

    .line 462
    sget-object v0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "tried to add a null attachListener"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 475
    :goto_0
    return-void

    .line 466
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureOnMainThread()V

    .line 468
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 469
    invoke-interface {p1}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnGoogleMapFragmentAttached;->onAttached()V

    goto :goto_0

    .line 470
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->isRemoving()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 471
    sget-object v0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "whenAttached, isRemoving, no-op"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 473
    :cond_2
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->attachCompleteListeners:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private whenReady(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnGoogleMapFragmentReady;)V
    .locals 2
    .param p1, "readyListener"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnGoogleMapFragmentReady;
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    .line 479
    if-nez p1, :cond_0

    .line 480
    sget-object v0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "tried to add a null readyListener"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 493
    :goto_0
    return-void

    .line 484
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureOnMainThread()V

    .line 486
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 487
    invoke-interface {p1}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnGoogleMapFragmentReady;->onReady()V

    goto :goto_0

    .line 488
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->isRemoving()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 489
    sget-object v0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "whenAttached, isRemoving, no-op"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 491
    :cond_2
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->readyCompleteListeners:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public centerOnLastKnownLocation(Z)V
    .locals 2
    .param p1, "animate"    # Z
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    .line 169
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureOnMainThread()V

    .line 171
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    .line 173
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getPhoneCoordinates()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    .line 174
    invoke-virtual {v1}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getCarCoordinates()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v1

    .line 171
    invoke-direct {p0, p1, v0, v1}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->centerMap(ZLcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;)V

    .line 176
    return-void
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 456
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 251
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$4;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$4;-><init>(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)V

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->whenAttached(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnGoogleMapFragmentAttached;)V

    .line 265
    return-void
.end method

.method public moveMap(Landroid/location/Location;FZ)V
    .locals 6
    .param p1, "location"    # Landroid/location/Location;
    .param p2, "zoom"    # F
    .param p3, "animate"    # Z
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    .line 180
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureOnMainThread()V

    .line 183
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 184
    .local v0, "myLatLng":Lcom/google/android/gms/maps/model/LatLng;
    invoke-virtual {p0, v0, p2, p3}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->moveMap(Lcom/google/android/gms/maps/model/LatLng;FZ)V

    .line 185
    return-void
.end method

.method public moveMap(Lcom/google/android/gms/maps/model/LatLng;FZ)V
    .locals 1
    .param p1, "latLng"    # Lcom/google/android/gms/maps/model/LatLng;
    .param p2, "zoom"    # F
    .param p3, "animate"    # Z
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    .line 189
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureOnMainThread()V

    .line 190
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->buildCameraUpdate(Lcom/google/android/gms/maps/model/LatLng;F)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->moveMapInternal(Lcom/google/android/gms/maps/CameraUpdate;Z)V

    .line 191
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 280
    invoke-super {p0, p1}, Lcom/google/android/gms/maps/MapFragment;->onAttach(Landroid/content/Context;)V

    .line 281
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->bus:Lcom/navdy/client/app/framework/util/BusProvider;

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 282
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->isAttached:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 284
    :goto_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->attachCompleteListeners:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 285
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->attachCompleteListeners:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnGoogleMapFragmentAttached;

    invoke-interface {v0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnGoogleMapFragmentAttached;->onAttached()V

    goto :goto_0

    .line 287
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 325
    sget-object v3, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "onCreateView"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 327
    new-instance v3, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$6;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$6;-><init>(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)V

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->getMapAsync(Lcom/google/android/gms/maps/OnMapReadyCallback;)V

    .line 361
    :try_start_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/maps/MapFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    .line 363
    .local v2, "view":Landroid/view/View;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 364
    move-object v0, v2

    .line 366
    .local v0, "finalView":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    new-instance v4, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$7;

    invoke-direct {v4, p0, v0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$7;-><init>(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;Landroid/view/View;)V

    invoke-virtual {v3, v4}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 402
    .end local v0    # "finalView":Landroid/view/View;
    :goto_0
    return-object v2

    .line 394
    :cond_0
    sget-object v3, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "container view is null or its viewtreeobserver is not alive"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 396
    .end local v2    # "view":Landroid/view/View;
    :catch_0
    move-exception v1

    .line 398
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "could not inflate Google Map Fragment"

    invoke-virtual {v3, v4, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 399
    const v3, 0x7f030055

    invoke-virtual {p1, v3, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .restart local v2    # "view":Landroid/view/View;
    goto :goto_0
.end method

.method public onDetach()V
    .locals 2

    .prologue
    .line 440
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->isAttached:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 441
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->bus:Lcom/navdy/client/app/framework/util/BusProvider;

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->unregister(Ljava/lang/Object;)V

    .line 442
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->isAttached:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 444
    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/maps/MapFragment;->onDetach()V

    .line 445
    return-void
.end method

.method public onInflate(Landroid/app/Activity;Landroid/util/AttributeSet;Landroid/os/Bundle;)V
    .locals 5
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 295
    sget-object v1, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "onInflate"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 297
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/GmsUtils;->finishIfGmsIsNotUpToDate(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 299
    sget-object v1, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "GMS is out of date or missing, preventing inflation"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 320
    :goto_0
    return-void

    .line 303
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/maps/MapFragment;->onInflate(Landroid/app/Activity;Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    .line 305
    sget-object v1, Lcom/navdy/client/R$styleable;->GoogleMapFragment:[I

    invoke-virtual {p1, p2, v1}, Landroid/app/Activity;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 306
    .local v0, "viewStyles":Landroid/content/res/TypedArray;
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->myLocationEnabled:Z

    .line 307
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->myLocationButtonEnabled:Z

    .line 308
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->carLocationEnabled:Z

    .line 309
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->scrollGesturesEnabled:Z

    .line 310
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->centerMapOnLastKnownLocation:Z

    .line 311
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->trafficEnabled:Z

    .line 312
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->mapToolbarEnabled:Z

    .line 313
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->autoCenter:Z

    .line 315
    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->mapPaddingTop:I

    .line 316
    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->mapPaddingBottom:I

    .line 317
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->mapPaddingLeft:I

    .line 318
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->mapPaddingRight:I

    .line 319
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0
.end method

.method public onLimitCellDataEvent(Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity$LimitCellDataEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity$LimitCellDataEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 269
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$5;

    invoke-direct {v0, p0, p1}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$5;-><init>(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity$LimitCellDataEvent;)V

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->getMapAsync(Lcom/google/android/gms/maps/OnMapReadyCallback;)V

    .line 276
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 432
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->myLocationEnabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->carLocationEnabled:Z

    if-eqz v0, :cond_1

    .line 433
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->navdyLocationListener:Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->removeListener(Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;)V

    .line 435
    :cond_1
    invoke-super {p0}, Lcom/google/android/gms/maps/MapFragment;->onPause()V

    .line 436
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 407
    invoke-super {p0}, Lcom/google/android/gms/maps/MapFragment;->onResume()V

    .line 409
    :goto_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->readyCompleteListeners:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 410
    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->readyCompleteListeners:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnGoogleMapFragmentReady;

    invoke-interface {v1}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnGoogleMapFragmentReady;->onReady()V

    goto :goto_0

    .line 413
    :cond_0
    iget-boolean v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->centerMapOnLastKnownLocation:Z

    if-eqz v1, :cond_1

    .line 414
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->centerOnLastKnownLocation(Z)V

    .line 417
    :cond_1
    iget-boolean v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->carLocationEnabled:Z

    if-eqz v1, :cond_2

    .line 418
    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getCarCoordinates()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v0

    .line 420
    .local v0, "carCoords":Lcom/navdy/service/library/events/location/Coordinate;
    if-eqz v0, :cond_2

    .line 421
    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getCarCoordinates()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->updateCarMarker(Lcom/navdy/service/library/events/location/Coordinate;)V

    .line 425
    .end local v0    # "carCoords":Lcom/navdy/service/library/events/location/Coordinate;
    :cond_2
    iget-boolean v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->myLocationEnabled:Z

    if-nez v1, :cond_3

    iget-boolean v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->carLocationEnabled:Z

    if-eqz v1, :cond_4

    .line 426
    :cond_3
    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->navdyLocationListener:Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->addListener(Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;)V

    .line 428
    :cond_4
    return-void
.end method

.method public show(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnShowMapListener;)V
    .locals 2
    .param p1, "onShowMapListener"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnShowMapListener;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 138
    sget-object v0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "show"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 140
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2;

    invoke-direct {v0, p0, p1}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2;-><init>(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnShowMapListener;)V

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->whenReady(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnGoogleMapFragmentReady;)V

    .line 165
    return-void
.end method

.method public zoomTo(Lcom/google/android/gms/maps/model/LatLngBounds;Z)V
    .locals 1
    .param p1, "latLngBounds"    # Lcom/google/android/gms/maps/model/LatLngBounds;
    .param p2, "animate"    # Z
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    .line 195
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureOnMainThread()V

    .line 198
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;

    invoke-direct {v0, p0, p1, p2}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;-><init>(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;Lcom/google/android/gms/maps/model/LatLngBounds;Z)V

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->getMapAsync(Lcom/google/android/gms/maps/OnMapReadyCallback;)V

    .line 248
    return-void
.end method
