.class public Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;
.super Ljava/lang/Object;
.source "CategoryItem.java"

# interfaces
.implements Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;


# instance fields
.field private children:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;",
            ">;"
        }
    .end annotation
.end field

.field private expanded:Z

.field private id:Ljava/lang/Long;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id"
    .end annotation
.end field

.field private name:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "name"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;->expanded:Z

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 88
    if-ne p0, p1, :cond_1

    .line 93
    :cond_0
    :goto_0
    return v1

    .line 89
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 91
    check-cast v0, Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;

    .line 93
    .local v0, "that":Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;
    iget-object v3, p0, Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;->id:Ljava/lang/Long;

    if-eqz v3, :cond_4

    iget-object v1, p0, Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;->id:Ljava/lang/Long;

    iget-object v2, v0, Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;->id:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    :cond_4
    iget-object v3, v0, Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;->id:Ljava/lang/Long;

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getChildren()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;->children:Ljava/util/List;

    invoke-static {v0}, Lcom/zendesk/util/CollectionUtils;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getId()Ljava/lang/Long;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;->id:Ljava/lang/Long;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;->name:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method public getParentId()Ljava/lang/Long;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 43
    const/4 v0, 0x0

    return-object v0
.end method

.method public getViewType()I
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x1

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;->id:Ljava/lang/Long;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;->id:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isExpanded()Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;->expanded:Z

    return v0
.end method

.method public setExpanded(Z)Z
    .locals 1
    .param p1, "expanded"    # Z

    .prologue
    .line 77
    iput-boolean p1, p0, Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;->expanded:Z

    .line 78
    iget-boolean v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;->expanded:Z

    return v0
.end method

.method public setSections(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 58
    .local p1, "sections":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;>;"
    invoke-static {p1}, Lcom/zendesk/util/CollectionUtils;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;->children:Ljava/util/List;

    .line 59
    return-void
.end method
