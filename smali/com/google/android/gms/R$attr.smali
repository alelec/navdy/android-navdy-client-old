.class public final Lcom/google/android/gms/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final adSize:I = 0x7f01005a

.field public static final adSizes:I = 0x7f01005b

.field public static final adUnitId:I = 0x7f01005c

.field public static final allowShortcuts:I = 0x7f01014f

.field public static final ambientEnabled:I = 0x7f010188

.field public static final buttonSize:I = 0x7f0101d6

.field public static final cameraBearing:I = 0x7f010179

.field public static final cameraMaxZoomPreference:I = 0x7f01018a

.field public static final cameraMinZoomPreference:I = 0x7f010189

.field public static final cameraTargetLat:I = 0x7f01017a

.field public static final cameraTargetLng:I = 0x7f01017b

.field public static final cameraTilt:I = 0x7f01017c

.field public static final cameraZoom:I = 0x7f01017d

.field public static final circleCrop:I = 0x7f010177

.field public static final colorScheme:I = 0x7f0101d7

.field public static final contentProviderUri:I = 0x7f01010f

.field public static final corpusId:I = 0x7f01010d

.field public static final corpusVersion:I = 0x7f01010e

.field public static final defaultIntentAction:I = 0x7f01014c

.field public static final defaultIntentActivity:I = 0x7f01014e

.field public static final defaultIntentData:I = 0x7f01014d

.field public static final documentMaxAgeSecs:I = 0x7f010113

.field public static final featureType:I = 0x7f0101d5

.field public static final imageAspectRatio:I = 0x7f010176

.field public static final imageAspectRatioAdjust:I = 0x7f010175

.field public static final indexPrefixes:I = 0x7f0101d2

.field public static final inputEnabled:I = 0x7f01016c

.field public static final latLngBoundsNorthEastLatitude:I = 0x7f01018d

.field public static final latLngBoundsNorthEastLongitude:I = 0x7f01018e

.field public static final latLngBoundsSouthWestLatitude:I = 0x7f01018b

.field public static final latLngBoundsSouthWestLongitude:I = 0x7f01018c

.field public static final liteMode:I = 0x7f01017e

.field public static final mapType:I = 0x7f010178

.field public static final noIndex:I = 0x7f0101d0

.field public static final paramName:I = 0x7f010124

.field public static final paramValue:I = 0x7f010125

.field public static final perAccountTemplate:I = 0x7f010114

.field public static final schemaOrgProperty:I = 0x7f0101d4

.field public static final schemaOrgType:I = 0x7f010111

.field public static final scopeUris:I = 0x7f0101d8

.field public static final searchEnabled:I = 0x7f010149

.field public static final searchLabel:I = 0x7f01014a

.field public static final sectionContent:I = 0x7f010151

.field public static final sectionFormat:I = 0x7f0101cf

.field public static final sectionId:I = 0x7f0101ce

.field public static final sectionType:I = 0x7f010150

.field public static final sectionWeight:I = 0x7f0101d1

.field public static final semanticallySearchable:I = 0x7f010112

.field public static final settingsDescription:I = 0x7f01014b

.field public static final sourceClass:I = 0x7f01016d

.field public static final subsectionSeparator:I = 0x7f0101d3

.field public static final toAddressesSection:I = 0x7f010171

.field public static final trimmable:I = 0x7f010110

.field public static final uiCompass:I = 0x7f01017f

.field public static final uiMapToolbar:I = 0x7f010187

.field public static final uiRotateGestures:I = 0x7f010180

.field public static final uiScrollGestures:I = 0x7f010181

.field public static final uiTiltGestures:I = 0x7f010182

.field public static final uiZoomControls:I = 0x7f010183

.field public static final uiZoomGestures:I = 0x7f010184

.field public static final useViewLifecycle:I = 0x7f010185

.field public static final userInputSection:I = 0x7f01016f

.field public static final userInputTag:I = 0x7f01016e

.field public static final userInputValue:I = 0x7f010170

.field public static final zOrderOnTop:I = 0x7f010186


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
